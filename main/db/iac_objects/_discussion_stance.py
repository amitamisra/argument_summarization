from collections import namedtuple

import sqlalchemy
from ._sql_connect import SQL_Base, sql_metadata

Discussion_Stance_Full_ID = namedtuple("Discussion_Stance_Full_ID", \
        ["dataset_id", "discussion_id", "discussion_stance_id"])

class Discussion_Stance(SQL_Base):
    __table__ = sqlalchemy.Table("discussion_stances", sql_metadata, autoload=True)

    def __init__(self, dataset_id: int, discussion_id: int, \
            discussion_stance_id: int, discussion_stance: str, topic_id: int, \
            topic_stance_id: int):
        self.dataset_id = dataset_id
        self.discussion_id = discussion_id
        self.discussion_stance_id = discussion_stance_id
        self.discussion_stance = discussion_stance
        self.topic_id = topic_id
        self.topic_stance_id = topic_stance_id

