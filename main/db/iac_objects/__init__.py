#"from iac_objects import *"

#Note: Might want to use __all__ = ['...']

import sqlalchemy

from ._sql_connect import sql_engine, sql_metadata, SQL_Base, sql_session
from ._dataset import Dataset, create_dataset, get_dataset_id
from ._discussion import Discussion
from ._post import Post
from ._text import Text, load_text_obj
from ._iac_queries import *

# from ._extras import root_dir, data_root_dir, temp_root_dir, resources_root_dir, resources_index

def load_dataset(name: str, load_extras=True):
    if name == 'fourforums':
        return load_fourforums()
    else:
        dataset = sql_session.query(Dataset).filter_by(name=name).scalar()
    if dataset is None:
        raise ValueError('Dataset <%s> not found in database. Check spelling.'%str(name))
    if load_extras:
        dataset.load_extras(sql_session)
    return dataset

from .specific_dataset_extras._fourforums import FourforumsDataset
def load_fourforums(name='fourforums', load_extras=True) -> FourforumsDataset:
    dataset = sql_session.query(FourforumsDataset).filter_by(name=name).scalar()
    if load_extras:
        dataset.load_extras(sql_session)
    return dataset
