import operator
from collections import namedtuple

from ._post import Post

import sqlalchemy
from sqlalchemy.orm import relationship
from ._sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata

Discussion_Full_ID = namedtuple('Discussion_Full_ID', ['dataset_id', 'discussion_id'])
DiscussionStanceInfo = namedtuple('DiscussionStanceInfo', ['discussion_stance_id', 'discussion_stance', 'topic_id', 'topic_stance_id', 'stance'])

topics_table = sqlalchemy.Table('topics', sql_metadata, autoload=True)

class Discussion(SQL_Base):
    #TODO: join topics directly
    __table__ = sqlalchemy.Table('discussions', sql_metadata, autoload=True)
    posts = relationship("Post", backref="discussion", viewonly=True)
    initiating_author_obj = relationship('Author')

    def __init__(self, dataset_id: int, discussion_id: int, discussion_url: str, title: str, topic_id: int, initiating_author_id: int, native_discussion_id: int):
        self.dataset_id = dataset_id
        self.discussion_id = discussion_id
        self.discussion_url = discussion_url
        self.title = title
        self.topic_id = topic_id
        self.initiating_author_id = initiating_author_id
        self.native_discussion_id = native_discussion_id
        self.post_dict = None
        self._post_list = None

    @reconstructor
    def init(self):
        """For sqlalchemy ORM"""
        self.initiating_author = self.initiating_author_obj.username if self.initiating_author_obj else None
        # self.authors = set([post.author for post in self.posts])
        self.post_dict = None
        self._post_list = None
        self.topic = sqlalchemy.select([topics_table.c.topic], topics_table.c.topic_id==self.topic_id).execute().scalar()
        self.stances = self._load_stances()

    def __iter__(self):
        return iter(self.get_posts())

    def full_id(self):
        return Discussion_Full_ID(self.dataset_id, self.discussion_id)

    def get_posts(self) -> [Post]:
        # Other sorting methods might be desirable (e.g. traversal if connected)
        # note that the discussion's posts might form multiple trees, no trees, or contain loops
        if self._post_list is None:
            self._post_list = list(self.posts)

            post_stances = self._load_post_stances()

            #TODO: fix!
            self._post_list = [post for post in self._post_list if post.timestamp is not None and post.native_post_id is not None]
            self._post_list.sort(key=operator.attrgetter('timestamp', 'native_post_id'))
            self.post_dict = {post.full_id(): post for post in self._post_list}

            #quote sources
            for post in self._post_list:
                for quote in post.get_all_quotes():
                    source_id = quote.source_full_id()
                    if source_id in self.post_dict:
                        quote.source = self.post_dict[source_id]

            for post in self._post_list:
                if (post.dataset_id, post.discussion_id, post.parent_post_id) in self.post_dict:
                    post._set_parent(self.post_dict[(post.dataset_id, post.discussion_id, post.parent_post_id)])
                if len(self.stances) > 0 and post.post_id in post_stances:
                    post.stance = self.stances[post_stances[post.post_id]]

        return self._post_list

    def _load_stances(self):
        discussion_stances = sqlalchemy.Table('discussion_stances', sql_metadata, autoload=True)
        topic_stances = sqlalchemy.Table('topic_stances', sql_metadata, autoload=True)
        table = discussion_stances.outerjoin(topic_stances,
                                        sqlalchemy.and_(discussion_stances.c.topic_id == topic_stances.c.topic_id,
                                                        discussion_stances.c.topic_stance_id == topic_stances.c.topic_stance_id))
        s = sqlalchemy.select([discussion_stances.c.discussion_stance_id, discussion_stances.c.discussion_stance, discussion_stances.c.topic_id, discussion_stances.c.topic_stance_id, topic_stances.c.stance],
                              sqlalchemy.and_(discussion_stances.c.dataset_id == self.dataset_id, discussion_stances.c.discussion_id == self.discussion_id)).select_from(table)
        entries = list(map(dict, s.execute()))
        stances = {entry['discussion_stance_id']: DiscussionStanceInfo(**entry) for entry in entries}
        return stances

    def _load_post_stances(self):
        table = sqlalchemy.Table('post_stances', sql_metadata, autoload=True)
        s = sqlalchemy.select([table.c.post_id, table.c.discussion_stance_id], sqlalchemy.and_(table.c.dataset_id == self.dataset_id, table.c.discussion_id == self.discussion_id))
        entries = {post_id: discussion_stance_id for post_id, discussion_stance_id in s.execute()}
        return entries

