import operator
import re
import datetime
from collections import namedtuple

from ._sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata
import sqlalchemy
from sqlalchemy.orm import relationship
from ._text import Text, Markup
from ._author import Author

Post_Full_ID = namedtuple('Post_Full_ID', ['dataset_id', 'discussion_id', 'post_id'])
Quote_Full_ID = namedtuple('Quote_Full_ID', ['dataset_id', 'discussion_id', 'post_id', 'quote_id'])

# class Post():
class Post(SQL_Base):
    __table__ = sqlalchemy.Table('posts', sql_metadata, autoload=True)
    text_obj = relationship("Text", uselist=False)
    author_obj = relationship("Author", uselist=False, viewonly=True)
    _all_quotes = relationship("Quote", viewonly=True) # contains even quotes of quotes
    #TODO: parent_post = relationship("Posts")

    def __init__(self, dataset_id: int, discussion_id: int, post_id: int, author_id: int, timestamp: datetime.datetime, parent_post_id: int, parent_missing: bool, native_post_id: int, text_id: int):
        self.dataset_id = dataset_id
        self.discussion_id = discussion_id
        self.post_id = post_id
        self.author_id = author_id
        self.timestamp = timestamp
        self.parent_post_id = parent_post_id
        self.parent_missing = parent_missing
        self.native_post_id = native_post_id
        self.text_id = text_id
        self.parent_post = None
        self.quotes = list()

        #self.init()

    @reconstructor
    def init(self):
        """For sqlalchemy ORM"""
        self.annotations = dict()
        self.text = self.text_obj.text
        self.author = self.author_obj.username if self.author_obj else None
        self.parent_post = None
        self.quotes = sorted([quote for quote in self._all_quotes if quote.parent_quote_index is None], key=operator.attrgetter('text_index'))

        #populates quotes with their embedded quotes
        self.all_quotes_dict = {quote.full_id(): quote for quote in self._all_quotes}
        for quote in self._all_quotes:
            if quote.parent_quote_index is not None:
                parent_full_id = (quote.dataset_id, quote.discussion_id, quote.post_id, quote.parent_quote_index)
                self.all_quotes_dict[parent_full_id].quotes.append(quote)

        #TODO: temporary hack, do right
        try:
            if self.parent_relation_id == 0:
                self.parent_relation = 'Supported'
            elif self.parent_relation_id == 1:
                self.parent_relation = 'Disputed'
            elif self.parent_relation_id == 2:
                self.parent_relation = 'Clarified'
            else:
                self.parent_relation = None
        except:
            pass

    def full_id(self):
        return Post_Full_ID(self.dataset_id, self.discussion_id, self.post_id)

    def text_with_quotes(self, merge_newlines=True, quote_marker_start='[Quote]\n', quote_marker_end='\n[/Quote]'):
        """Adds the post's quotes to the text.
        Use for human consumable text.
        """
        return _unified_text_with_quotes(self.text, self.quotes, merge_newlines=merge_newlines, quote_marker_start=quote_marker_start, quote_marker_end=quote_marker_end)

    def text_partition_using_quotes(self) -> ['pre quote', 'post quote']:
        """Separates out segments between quotes"""
        splits = {quote.text_index for quote in self.quotes}
        splits.add(0)
        splits.add(len(self.text))
        splits = sorted(splits)
        partitions = [self.text[splits[i]:splits[i+1]] for i in range(len(splits)-1)]
        return partitions

    def get_all_quotes(self):
        """Returns all quotes within this post, even quotes within quotes (embedded quotes)
        Also populates all_quotes if it doesn't already exist
        Probably only useful in construction before all_quotes is populated
        """
        if not hasattr(self, '_all_quotes') or not self._all_quotes:
            self._all_quotes = list()
            for quote in self.quotes:
                self._all_quotes.append(quote)
                self._all_quotes.extend(quote.get_all_quotes())
        return self._all_quotes

    def _set_parent(self, parent_post):
        """Called by the Discussion container"""
        self.parent_post = parent_post

import json
class Quote(SQL_Base):
    __table__ = sqlalchemy.Table('quotes', sql_metadata, autoload=True)
    text_obj = relationship("Text", uselist=False)

    def __init__(self, dataset_id: int, text_index: int, quote_text: str, attributes: dict, markup: [Markup], quotes: list):
        self.dataset_id = dataset_id
        self.discussion_id = None  # TDB
        self.post_id = None  # TBD
        self.quote_index = None  # Which quote is it? Part of primary key
        self.parent_quote_index = None # If it is a quote within a quote, which is its parent
        self.text_index = text_index  # the quote directly _precedes_ this character (index) in the quoting post/quote, frequently 0, sometimes len(text)
        self.text_id = None  # TBD, might be shared with other quote/posts!
        self.source_discussion_id = None #Likely same as discussion_id, sometimes different, sometimes unknown (null)
        self.source_post_id = None
        self.source_start = None
        self.source_end = None
        self.source_truncated = False
        self.source_altered = False
        self.alternative_source_info = json.dumps(attributes)

        # Not stored
        self.text = quote_text
        self.attributes = attributes
        self.markup = markup
        self.quotes = quotes
        self.source = None

    @reconstructor
    def init(self):
        """For sqlalchemy ORM
            Data to be filled in by the post or discussion object
        """
        self.quotes = list()
        self.source = None
        self.text = self.text_obj.text

    def full_id(self):
        return Quote_Full_ID(self.dataset_id, self.discussion_id, self.post_id, self.quote_index)

    def source_full_id(self):
        if self.source is not None:
            return self.source.full_id()
        elif self.dataset_id is not None and self.source_discussion_id is not None and self.source_post_id is not None:
            return Post_Full_ID(self.dataset_id, self.source_discussion_id, self.source_post_id)
        else:
            return None

    def text_with_quotes(self, merge_newlines=True, quote_marker_start='[Quote]\n', quote_marker_end='\n[/Quote]'):
        """Adds the post's quotes to the text."""

        return _unified_text_with_quotes(self.text, self.quotes, merge_newlines=merge_newlines, quote_marker_start=quote_marker_start, quote_marker_end=quote_marker_end)

    def get_all_quotes(self):
        """Returns all quotes within this post, even quotes within quotes (embedded quotes)"""
        if not hasattr(self, '_all_quotes') or not self._all_quotes:
            self._all_quotes = list()
            for quote in self.quotes:
                self._all_quotes.append(quote)
                self._all_quotes.extend(quote.get_all_quotes())
        return self._all_quotes

    def text_partition_using_quotes(self) -> ['pre quote', 'post quote']:
        """Separates out segments between quotes"""
        splits = {quote.text_index for quote in self.quotes}
        splits.add(0)
        splits.add(len(self.text))
        splits = sorted(splits)
        partitions = [self.text[splits[i]:splits[i+1]] for i in range(len(splits)-1)]
        return partitions


def _unified_text_with_quotes(text, quotes, merge_newlines=True, quote_marker_start='[Quote]\n', quote_marker_end='\n[/Quote]'):
    """For consistency, used by both Post and Quote"""
    text_list = list()
    curr_index = 0
    last_char_index = len(text.rstrip()) - 1
    for quote in quotes:
        quote_text = quote.text_with_quotes(quote_marker_start=quote_marker_start, quote_marker_end=quote_marker_end)
        quote_text = quote_marker_start + quote_text.strip() + quote_marker_end
        if quote.text_index < last_char_index:
            quote_text += '\n'
        if quote.text_index > 0:
            quote_text = '\n' + quote_text
        preceding_text = text[curr_index:quote.text_index]
        if merge_newlines:
            preceding_text = re.sub(r'\n+', '\n\n', preceding_text)
        text_list.append(preceding_text)
        text_list.append(quote_text)
        curr_index = quote.text_index

    following_text = text[curr_index:]
    if merge_newlines:
        following_text = re.sub(r'\n+', '\n\n', following_text)
    text_list.append(following_text)

    return ('\n\n'.join([entry.strip() for entry in text_list if entry is not ''])).strip()
