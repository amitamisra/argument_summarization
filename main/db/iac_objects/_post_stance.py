from collections import namedtuple

import sqlalchemy
from ._sql_connect import SQL_Base, sql_metadata

Post_Stance_Full_ID = namedtuple("Post_Stance_Full_ID", \
        ["dataset_id", "discussion_id", "post_id"])

class PostStance(SQL_Base):
    __table__ = sqlalchemy.Table("post_stances", sql_metadata, autoload=True)

    def __init__(self, dataset_id: int, discussion_id: int, \
            post_id: int, discussion_stance_id: int, topic_id: int, \
            topic_stance_id: int):
        self.dataset_id = dataset_id
        self.discussion_id = discussion_id
        self.post_id = post_id
        self.discussion_stance_id = discussion_stance_id
        self.topic_id = topic_id
        self.topic_stance_id = topic_stance_id

