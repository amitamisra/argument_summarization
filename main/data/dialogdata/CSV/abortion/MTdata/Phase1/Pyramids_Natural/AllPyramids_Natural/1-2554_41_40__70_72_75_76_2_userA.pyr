<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author2 argues that studies containing statistics about why women have abortions are not accurate because the studies are self-reporting. Therefore judgments about why women have abortions can't be made. Specifically, Author2 argues that no determination can be made whether women had abortions because of the physical damage of pregnancy, since these studies are flawed. Author2 also notes that women have abortions for multiple reasons, not just one.</line>
  <line></line>
  <line>Author1 states that the only way an observer can know why a women has an abortion is by her self-reporting. No one other than the woman can know why she decided to have an abortion. Therefore studies listing the reasons women have abortions must be taken as valid. Physical damage is not a reason that women cite.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 states women say why they need an abortion, and a doctor can't do that for them. He claims Author2 said the reasons women give are invalid because it is self-reporting, but that's the only way to do it. The reasons never include physical damage, so that's not why they choose it. Abortion supporters who list these as reasons for allowing abortion are incorrect as to why women think they need them. Self reporting is the only way to know reasons. </line>
  <line></line>
  <line>Author2 said he is arguing the statistics are not accurate because self reporting is not seen as accurate, reasons are also not known, not reported, and there are often multiple reasons. At best, it would be accurate reports of inaccurate information.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 feels that pro-choice people use the possibility of physical harm to the mother as a justification for keeping abortion legal.  While such risk to the mother is real, it is not a reason women ever give for why they've chosen abortion.  People arguing for abortion rights on that basis are out of touch with the real reasons women have abortions.  Author2 says that whatever reason a woman gives for having an abortion is justified.  The real reasons behind women's choices are personal, and can't be assessed meaningfully, because self-reporting is notoriously inaccurate.  Not all states collect this information, or it is left up to the woman to volunteer the information, and thus these statistics cannot be known accurately.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 argues that because self reporting is the only way a woman has to report why she is having an abortion, the available statistics on reasons for abortion are as accurate as they can be. They touch on the fact that other posters have listed anatomical damage from child birth as a reason for abortions, but no women have self reported this, so it can not be considered a reason for abortion. Author2 argues that self reporting is in itself inaccurate. They do concede that the statistics are accurate, but they are reporting inaccurate information. They point out that it's been proven in studies on breast cancer that self reporting does not provide accurate results, and believes this carries over to abortion as well.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 says they are discussing the reason for having an abortion, and a medical examiner can't tell a woman why she's having one. Author2 agrees but asks the point. Author1 replies Author1 is arguing the reasons are invalid because it's self reporting, but that's the only way to give a reason. Physical damage from pregnancy was never reported as a reason for abortion. Author2 says he is arguing statistics being accurate because of self reporting. He says whatever the reason are abortion is justified. Author1 says the statistics accurately report the reasons women give for having abortions and asks how this is inaccurate. It serves the intended purpose. Author2 says Author1 got him. It's accurate reporting, but self reporting can skew the results of studies.</line>
 </text>
 <scu uid="35" label="Physical damage is not a reason for abortion.">
  <contributor label="Physical damage is not a reason that women cite">
   <part label="Physical damage is not a reason that women cite" start="745" end="792"/>
  </contributor>
  <contributor label="The reasons never include physical damage">
   <part label="The reasons never include physical damage" start="1032" end="1073"/>
  </contributor>
  <contributor label="it is not a reason women ever give for why they've chosen abortion">
   <part label="it is not a reason women ever give for why they've chosen abortion" start="1733" end="1799"/>
  </contributor>
  <contributor label="but no women have self reported this">
   <part label="but no women have self reported this" start="2631" end="2667"/>
  </contributor>
  <contributor label="Physical damage from pregnancy was never reported as a reason for abortion">
   <part label="Physical damage from pregnancy was never reported as a reason for abortion" start="3373" end="3447"/>
  </contributor>
 </scu>
 <scu uid="31" label="The only way an observer can know why a women has an abortion is by her self-reporting.">
  <contributor label="Author1 states that the only way an observer can know why a women has an abortion is by her self-reporting">
   <part label="Author1 states that the only way an observer can know why a women has an abortion is by her self-reporting" start="480" end="586"/>
  </contributor>
  <contributor label="but that's the only way to do it...Self reporting is the only way to know reasons">
   <part label="but that's the only way to do it" start="998" end="1030"/>
   <part label="Self reporting is the only way to know reasons" start="1229" end="1275"/>
  </contributor>
  <contributor label="it is left up to the woman to volunteer the information">
   <part label="it is left up to the woman to volunteer the information" start="2184" end="2239"/>
  </contributor>
  <contributor label="Author1 argues that because self reporting is the only way a woman has to report why she is having an abortion">
   <part label="Author1 argues that because self reporting is the only way a woman has to report why she is having an abortion" start="2322" end="2432"/>
  </contributor>
  <contributor label="but that's the only way to give a reason">
   <part label="but that's the only way to give a reason" start="3331" end="3371"/>
  </contributor>
 </scu>
 <scu uid="25" label="The studies are self-reporting which is not seen as accurate.">
  <contributor label="because the studies are self-reporting">
   <part label="because the studies are self-reporting" start="124" end="162"/>
  </contributor>
  <contributor label="because it is self-reporting...because self reporting is not seen as accurate">
   <part label="because it is self-reporting" start="968" end="996"/>
   <part label="because self reporting is not seen as accurate" start="1338" end="1384"/>
  </contributor>
  <contributor label="because self-reporting is notoriously inaccurate">
   <part label="because self-reporting is notoriously inaccurate" start="2089" end="2137"/>
  </contributor>
  <contributor label="Author2 argues that self reporting is in itself inaccurate">
   <part label="Author2 argues that self reporting is in itself inaccurate" start="2720" end="2778"/>
  </contributor>
  <contributor label="because it's self reporting">
   <part label="because it's self reporting" start="3302" end="3329"/>
  </contributor>
 </scu>
 <scu uid="24" label="The studies containing statistics about why women have abortions are not accurate.">
  <contributor label="Author2 argues that studies containing statistics about why women have abortions are not accurate...these studies are flawed">
   <part label="Author2 argues that studies containing statistics about why women have abortions are not accurate" start="26" end="123"/>
   <part label="these studies are flawed" start="372" end="396"/>
  </contributor>
  <contributor label="He claims Author2 said the reasons women give are invalid...Author2 said he is arguing the statistics are not accurate">
   <part label="He claims Author2 said the reasons women give are invalid" start="910" end="967"/>
   <part label="Author2 said he is arguing the statistics are not accurate" start="1279" end="1337"/>
  </contributor>
  <contributor label="thus these statistics cannot be known accurately">
   <part label="thus these statistics cannot be known accurately" start="2245" end="2293"/>
  </contributor>
  <contributor label="they are reporting inaccurate information">
   <part label="they are reporting inaccurate information" start="2834" end="2875"/>
  </contributor>
  <contributor label="Author2 says he is arguing statistics being accurate because of self reporting">
   <part label="Author2 says he is arguing statistics being accurate because of self reporting" start="3449" end="3527"/>
  </contributor>
 </scu>
 <scu uid="38" label="Abortions supporters use the possibility of physical harm to the mother as a justification for keeping abortion legal.">
  <contributor label="Author1 feels that pro-choice people use the possibility of physical harm to the mother as a justification for keeping abortion legal">
   <part label="Author1 feels that pro-choice people use the possibility of physical harm to the mother as a justification for keeping abortion legal" start="1558" end="1691"/>
  </contributor>
  <contributor label="Abortion supporters who list these as reasons for allowing abortion are incorrect as to why women think they need them">
   <part label="Abortion supporters who list these as reasons for allowing abortion are incorrect as to why women think they need them" start="1109" end="1227"/>
  </contributor>
  <contributor label="They touch on the fact that other posters have listed anatomical damage from child birth as a reason for abortions">
   <part label="They touch on the fact that other posters have listed anatomical damage from child birth as a reason for abortions" start="2515" end="2629"/>
  </contributor>
 </scu>
 <scu uid="37" label="A doctor can't decide if the woman needs an abortion or not.">
  <contributor label="a doctor can't do that for them">
   <part label="a doctor can't do that for them" start="877" end="908"/>
  </contributor>
  <contributor label="medical examiner can't tell a woman why she's having one">
   <part label="medical examiner can't tell a woman why she's having one" start="3150" end="3206"/>
  </contributor>
 </scu>
 <scu uid="39" label="At best, the studies would be accurate reports of inaccurate information.">
  <contributor label="At best, it would be accurate reports of inaccurate information">
   <part label="At best, it would be accurate reports of inaccurate information" start="1466" end="1529"/>
  </contributor>
  <contributor label="It's accurate reporting">
   <part label="It's accurate reporting" start="3769" end="3792"/>
  </contributor>
 </scu>
 <scu uid="53" label="Physical damage can not be considered a reason for abortion.">
  <contributor label="so it can not be considered a reason for abortion">
   <part label="so it can not be considered a reason for abortion" start="2669" end="2718"/>
  </contributor>
  <contributor label="so that's not why they choose it">
   <part label="so that's not why they choose it" start="1075" end="1107"/>
  </contributor>
 </scu>
 <scu uid="56" label="The available statistics on reasons for abortion are accurate.">
  <contributor label="the available statistics on reasons for abortion are as accurate as they can be...They do concede that the statistics are accurate">
   <part label="the available statistics on reasons for abortion are as accurate as they can be" start="2434" end="2513"/>
   <part label="They do concede that the statistics are accurate" start="2780" end="2828"/>
  </contributor>
  <contributor label="Author1 says the statistics accurately report the reasons women give for having abortions...asks how this is inaccurate">
   <part label="Author1 says the statistics accurately report the reasons women give for having abortions" start="3584" end="3673"/>
   <part label="asks how this is inaccurate" start="3678" end="3705"/>
  </contributor>
 </scu>
 <scu uid="46" label="Whatever the reason are, abortion is justified.">
  <contributor label="He says whatever the reason are abortion is justified">
   <part label="He says whatever the reason are abortion is justified" start="3529" end="3582"/>
  </contributor>
  <contributor label="Author2 says that whatever reason a woman gives for having an abortion is justified">
   <part label="Author2 says that whatever reason a woman gives for having an abortion is justified" start="1913" end="1996"/>
  </contributor>
 </scu>
 <scu uid="30" label="Women have abortions for multiple reasons.">
  <contributor label="Author2 also notes that women have abortions for multiple reasons, not just one">
   <part label="Author2 also notes that women have abortions for multiple reasons, not just one" start="398" end="477"/>
  </contributor>
  <contributor label="there are often multiple reasons">
   <part label="there are often multiple reasons" start="1432" end="1464"/>
  </contributor>
 </scu>
 <scu uid="60" label="He agrees but asks the point.">
  <contributor label="Author2 agrees but asks the point">
   <part label="Author2 agrees but asks the point" start="3208" end="3241"/>
  </contributor>
 </scu>
 <scu uid="54" label="He got him.">
  <contributor label="Author2 says Author1 got him">
   <part label="Author2 says Author1 got him" start="3739" end="3767"/>
  </contributor>
 </scu>
 <scu uid="43" label="It's been proven in studies on breast cancer that self reporting does not provide accurate results.">
  <contributor label="They point out that it's been proven in studies on breast cancer that self reporting does not provide accurate results">
   <part label="They point out that it's been proven in studies on breast cancer that self reporting does not provide accurate results" start="2877" end="2995"/>
  </contributor>
 </scu>
 <scu uid="26" label="Judgments about why women have abortions can't be made.">
  <contributor label="Therefore judgments about why women have abortions can't be made">
   <part label="Therefore judgments about why women have abortions can't be made" start="164" end="228"/>
  </contributor>
 </scu>
 <scu uid="47" label="No determination can be made whether women had abortions because of the physical damage of pregnancy.">
  <contributor label="Specifically, Author2 argues that no determination can be made whether women had abortions because of the physical damage of pregnancy">
   <part label="Specifically, Author2 argues that no determination can be made whether women had abortions because of the physical damage of pregnancy" start="230" end="364"/>
  </contributor>
 </scu>
 <scu uid="32" label="No one other than the woman can know why she decided to have an abortion.">
  <contributor label="No one other than the woman can know why she decided to have an abortion">
   <part label="No one other than the woman can know why she decided to have an abortion" start="588" end="660"/>
  </contributor>
 </scu>
 <scu uid="40" label="Not all states collect the information for why women have an abortion.">
  <contributor label="Not all states collect this information">
   <part label="Not all states collect this information" start="2140" end="2179"/>
  </contributor>
 </scu>
 <scu uid="50" label="People arguing for abortion rights on that basis are out of touch with the real reasons women have abortions.">
  <contributor label="People arguing for abortion rights on that basis are out of touch with the real reasons women have abortions">
   <part label="People arguing for abortion rights on that basis are out of touch with the real reasons women have abortions" start="1802" end="1910"/>
  </contributor>
 </scu>
 <scu uid="48" label="Reasons for abortions are also not known, not reported.">
  <contributor label="reasons are also not known, not reported">
   <part label="reasons are also not known, not reported" start="1386" end="1426"/>
  </contributor>
 </scu>
 <scu uid="55" label="Self reporting can skew the results of studies.">
  <contributor label="self reporting can skew the results of studies">
   <part label="self reporting can skew the results of studies" start="3798" end="3844"/>
  </contributor>
 </scu>
 <scu uid="34" label="Studies listing the reasons women have abortions must taken as valid.">
  <contributor label="Therefore studies listing the reasons women have abortions must be taken as valid">
   <part label="Therefore studies listing the reasons women have abortions must be taken as valid" start="662" end="743"/>
  </contributor>
 </scu>
 <scu uid="49" label="The health risk to the mother is real.">
  <contributor label="While such risk to the mother is real">
   <part label="While such risk to the mother is real" start="1694" end="1731"/>
  </contributor>
 </scu>
 <scu uid="44" label="The inaccuracy of studies on breast cancer for self reporting carries over to abortion as well.">
  <contributor label="believes this carries over to abortion as well">
   <part label="believes this carries over to abortion as well" start="3001" end="3047"/>
  </contributor>
 </scu>
 <scu uid="52" label="The real reasons behind women's choices are personal and can't be assessed meaningfully.">
  <contributor label="The real reasons behind women's choices are personal, and can't be assessed meaningfully">
   <part label="The real reasons behind women's choices are personal, and can't be assessed meaningfully" start="1999" end="2087"/>
  </contributor>
 </scu>
 <scu uid="61" label="The reasons behind abortions are invalid.">
  <contributor label="Author1 replies Author1 is arguing the reasons are invalid">
   <part label="Author1 replies Author1 is arguing the reasons are invalid" start="3243" end="3301"/>
  </contributor>
 </scu>
 <scu uid="58" label="The statistic serves the intended purpose.">
  <contributor label="It serves the intended purpose">
   <part label="It serves the intended purpose" start="3707" end="3737"/>
  </contributor>
 </scu>
 <scu uid="45" label="Two people are discussing the reason for having an abortion.">
  <contributor label="Author1 says they are discussing the reason for having an abortion">
   <part label="Author1 says they are discussing the reason for having an abortion" start="3076" end="3142"/>
  </contributor>
 </scu>
 <scu uid="36" label="Women say why they need an abortion.">
  <contributor label="Author1 states women say why they need an abortion">
   <part label="Author1 states women say why they need an abortion" start="821" end="871"/>
  </contributor>
 </scu>
</pyramid>
