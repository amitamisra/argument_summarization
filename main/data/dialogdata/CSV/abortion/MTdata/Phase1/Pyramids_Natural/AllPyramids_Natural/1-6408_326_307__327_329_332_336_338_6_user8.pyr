<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 asks if there is a test to determine whether or not an unborn child is alive. Author2 says that there is, and they prove the embryo is legally dead because it lacks 'mental existence.' Author1 questions how something can be dead if it's growing. Author2 clarifies that he didn't say the unborn child was dead, just that it was legally dead. Author2 states that the unborn child, like a plant, can grow but it does not think. Author1 counters by saying that a plant is indeed alive. Author2 notes that while a plant may be alive, it lacks a mental life. A plant is no more mentally alive than a kidney. In Author2's opinion, personhood requires sentience. Author1 cites the dictionary definition of 'alive.'</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 asked what test there has been for unborn fetuses. He asked how something that is dead can grow. He refutes that a plant is dead, and said it is alive. He quotes a dictionary definition that defines alive as having life or living. </line>
  <line></line>
  <line>Author2 claimed there have been tests, and they proved an embryo can't think, thus it is dead legally. He denies saying it was dead. He gives the example of a plant that can grow, but because it doesn't have a brain or mental existence is considered dead legally. He asked if a plant has a brain. He cites the example of kidneys, which are alive, but not people. In order to be a person, a thing must be sentient.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author2 states many tests have shown that an embryo does not have mental existence, and therefore, is legally dead.  He denies having said that an embryo is dead.  He says that plants are also technically legally dead, because they have no brain or mental existence.  He agrees that plants are alive.  Being alive doesn't automatically make something a person.  Things must be sentient to be people.  Author1 asks how something dead can grow, and states that plants are alive.  He provides a dictionary definition of &quot;alive&quot; as meaning &quot;having life; living.&quot;</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 believes that embryos are alive. He points out that something that is not alive would not be capable of growing, and compares a fetus with a plant. He then quotes the dictionary definition of the word alive. Author2 contends that while an embryo is technically alive, it is considered legally dead. He concedes that both embryos and plants are living things, but that because they do not have brain function, they are not considered legally alive. He questions why disconnecting brain dead people is not considered murder. He points out that kidneys are also living things, but that does not make them human beings, because they lack sentience, which is necessary for something to be considered a legally living human being.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 says Author2 has applied a test to see if there is a person in a coffin. He asks where the test for the unborn is. Author2 replies that there has already been many tests. These tests have proven that an embryo does not have mental existence and because of this they are legally dead. Author1 asks Author2 how something that is dead can grow. Author2 says that he never said it was dead but a plant is legally dead because it has no existence and no brain, but it still grows. Author1 replies that a plant is very much alive. Author2 asks if Author1 is saying a plant has a brain and can think. His kidney's are alive but that doesn't make them people.</line>
 </text>
 <scu uid="1" label="He denies having said that an embryo is dead.">
  <contributor label="He denies having said that an embryo is dead">
   <part label="He denies having said that an embryo is dead" start="1567" end="1611"/>
  </contributor>
  <contributor label="Author2 says that he never said it was dead">
   <part label="Author2 says that he never said it was dead" start="3146" end="3189"/>
  </contributor>
  <contributor label="Author2 clarifies that he didn't say the unborn child was dead">
   <part label="Author2 clarifies that he didn't say the unborn child was dead" start="280" end="342"/>
  </contributor>
  <contributor label="He denies saying it was dead">
   <part label="He denies saying it was dead" start="1112" end="1140"/>
  </contributor>
 </scu>
 <scu uid="2" label="Plants are alive.">
  <contributor label="He agrees that plants are alive...states that plants are alive">
   <part label="He agrees that plants are alive" start="1718" end="1749"/>
   <part label="states that plants are alive" start="1897" end="1925"/>
  </contributor>
  <contributor label="Author1 counters by saying that a plant is indeed alive...Author2 notes that while a plant may be alive">
   <part label="Author1 counters by saying that a plant is indeed alive" start="459" end="514"/>
   <part label="Author2 notes that while a plant may be alive" start="516" end="561"/>
  </contributor>
  <contributor label="He refutes that a plant is dead...said it is alive">
   <part label="He refutes that a plant is dead" start="873" end="904"/>
   <part label="said it is alive" start="910" end="926"/>
  </contributor>
  <contributor label="Author1 replies that a plant is very much alive">
   <part label="Author1 replies that a plant is very much alive" start="3280" end="3327"/>
  </contributor>
 </scu>
 <scu uid="3" label="Things must be sentient to be a person.">
  <contributor label="In Author2's opinion, personhood requires sentience">
   <part label="In Author2's opinion, personhood requires sentience" start="636" end="687"/>
  </contributor>
  <contributor label="In order to be a person, a thing must be sentient">
   <part label="In order to be a person, a thing must be sentient" start="1372" end="1421"/>
  </contributor>
  <contributor label="Things must be sentient to be people">
   <part label="Things must be sentient to be people" start="1812" end="1848"/>
  </contributor>
  <contributor label="which is necessary for something to be considered a legally living human being">
   <part label="which is necessary for something to be considered a legally living human being" start="2689" end="2767"/>
  </contributor>
 </scu>
 <scu uid="4" label="How can something be dead if it is growing.">
  <contributor label="Author1 asks how something dead can grow">
   <part label="Author1 asks how something dead can grow" start="1851" end="1891"/>
  </contributor>
  <contributor label="Author1 questions how something can be dead if it's growing">
   <part label="Author1 questions how something can be dead if it's growing" start="219" end="278"/>
  </contributor>
  <contributor label="He asked how something that is dead can grow">
   <part label="He asked how something that is dead can grow" start="827" end="871"/>
  </contributor>
  <contributor label="Author1 asks Author2 how something that is dead can grow">
   <part label="Author1 asks Author2 how something that is dead can grow" start="3088" end="3144"/>
  </contributor>
 </scu>
 <scu uid="5" label="Kidneys are alive but that doesn't make them people.">
  <contributor label="His kidney's are alive but that doesn't make them people">
   <part label="His kidney's are alive but that doesn't make them people" start="3398" end="3454"/>
  </contributor>
  <contributor label="He points out that kidneys are also living things, but that does not make them human beings">
   <part label="He points out that kidneys are also living things, but that does not make them human beings" start="2567" end="2658"/>
  </contributor>
  <contributor label="He cites the example of kidneys, which are alive, but not people">
   <part label="He cites the example of kidneys, which are alive, but not people" start="1306" end="1370"/>
  </contributor>
 </scu>
 <scu uid="6" label="Is there a test to determine whether or not an unborn child is alive.">
  <contributor label="Author1 asks if there is a test to determine whether or not an unborn child is alive">
   <part label="Author1 asks if there is a test to determine whether or not an unborn child is alive" start="26" end="110"/>
  </contributor>
  <contributor label="Author1 asked what test there has been for unborn fetuses">
   <part label="Author1 asked what test there has been for unborn fetuses" start="768" end="825"/>
  </contributor>
  <contributor label="He asks where the test for the unborn is">
   <part label="He asks where the test for the unborn is" start="2877" end="2917"/>
  </contributor>
 </scu>
 <scu uid="7" label="A dictionary defines the word alive.">
  <contributor label="He quotes a dictionary definition that defines alive as having life or living">
   <part label="He quotes a dictionary definition that defines alive as having life or living" start="928" end="1005"/>
  </contributor>
  <contributor label="Author1 cites the dictionary definition of 'alive.'">
   <part label="Author1 cites the dictionary definition of 'alive.'" start="689" end="740"/>
  </contributor>
  <contributor label="He then quotes the dictionary definition of the word alive">
   <part label="He then quotes the dictionary definition of the word alive" start="2192" end="2250"/>
  </contributor>
  <contributor label="He provides a dictionary definition of &quot;alive&quot; as meaning &quot;having life; living.&quot;">
   <part label="He provides a dictionary definition of &quot;alive&quot; as meaning &quot;having life; living.&quot;" start="1928" end="2008"/>
  </contributor>
 </scu>
 <scu uid="8" label="There have been many tests to determine whether an embryo is alive.">
  <contributor label="Author2 replies that there has already been many tests">
   <part label="Author2 replies that there has already been many tests" start="2919" end="2973"/>
  </contributor>
  <contributor label="Author2 claimed there have been tests">
   <part label="Author2 claimed there have been tests" start="1009" end="1046"/>
  </contributor>
  <contributor label="Author2 says that there is">
   <part label="Author2 says that there is" start="112" end="138"/>
  </contributor>
 </scu>
 <scu uid="9" label="Why is disconnecting brain dead people not considered murder.">
  <contributor label="He questions why disconnecting brain dead people is not considered murder">
   <part label="He questions why disconnecting brain dead people is not considered murder" start="2492" end="2565"/>
  </contributor>
 </scu>
 <scu uid="13" label="Embryos do not have mental existence.">
  <contributor label="These tests have proven that an embryo does not have mental existence">
   <part label="These tests have proven that an embryo does not have mental existence" start="2975" end="3044"/>
  </contributor>
  <contributor label="Author2 states many tests have shown that an embryo does not have mental existence">
   <part label="Author2 states many tests have shown that an embryo does not have mental existence" start="1450" end="1532"/>
  </contributor>
  <contributor label="they proved an embryo can't think">
   <part label="they proved an embryo can't think" start="1052" end="1085"/>
  </contributor>
  <contributor label="it lacks 'mental existence">
   <part label="it lacks 'mental existence" start="190" end="216"/>
  </contributor>
 </scu>
 <scu uid="14" label="Embryos are legally dead.">
  <contributor label="because of this they are legally dead">
   <part label="because of this they are legally dead" start="3049" end="3086"/>
  </contributor>
  <contributor label="therefore, is legally dead">
   <part label="therefore, is legally dead" start="1538" end="1564"/>
  </contributor>
  <contributor label="thus it is dead legally">
   <part label="thus it is dead legally" start="1087" end="1110"/>
  </contributor>
  <contributor label="they prove the embryo is legally dead...just that it was legally dead">
   <part label="they prove the embryo is legally dead" start="144" end="181"/>
   <part label="just that it was legally dead" start="344" end="373"/>
  </contributor>
  <contributor label="it is considered legally dead">
   <part label="it is considered legally dead" start="2312" end="2341"/>
  </contributor>
 </scu>
 <scu uid="15" label="Does a plant have a brain.">
  <contributor label="He asked if a plant has a brain">
   <part label="He asked if a plant has a brain" start="1273" end="1304"/>
  </contributor>
  <contributor label="Author2 asks if Author1 is saying a plant has a brain and can think">
   <part label="Author2 asks if Author1 is saying a plant has a brain and can think" start="3329" end="3396"/>
  </contributor>
 </scu>
 <scu uid="16" label="Plants are technically legally dead.">
  <contributor label="He says that plants are also technically legally dead">
   <part label="He says that plants are also technically legally dead" start="1614" end="1667"/>
  </contributor>
  <contributor label="is considered dead legally">
   <part label="is considered dead legally" start="1245" end="1271"/>
  </contributor>
  <contributor label="a plant is legally dead">
   <part label="a plant is legally dead" start="3194" end="3217"/>
  </contributor>
 </scu>
 <scu uid="17" label="Kidneys lack sentience.">
  <contributor label="because they lack sentience">
   <part label="because they lack sentience" start="2660" end="2687"/>
  </contributor>
 </scu>
 <scu uid="18" label="Embryos are alive.">
  <contributor label="Author1 believes that embryos are alive...Author2 contends that while an embryo is technically alive">
   <part label="Author1 believes that embryos are alive" start="2036" end="2075"/>
   <part label="Author2 contends that while an embryo is technically alive" start="2252" end="2310"/>
  </contributor>
 </scu>
 <scu uid="19" label="Plants lack a mental life.">
  <contributor label="it lacks a mental life">
   <part label="it lacks a mental life" start="563" end="585"/>
  </contributor>
  <contributor label="but because it doesn't have a brain or mental existence">
   <part label="but because it doesn't have a brain or mental existence" start="1189" end="1244"/>
  </contributor>
  <contributor label="they have no brain or mental existence">
   <part label="they have no brain or mental existence" start="1677" end="1715"/>
  </contributor>
  <contributor label="it has no existence and no brain">
   <part label="it has no existence and no brain" start="3226" end="3258"/>
  </contributor>
 </scu>
 <scu uid="20" label="Plants can grow.">
  <contributor label="it still grows">
   <part label="it still grows" start="3264" end="3278"/>
  </contributor>
  <contributor label="He gives the example of a plant that can grow">
   <part label="He gives the example of a plant that can grow" start="1142" end="1187"/>
  </contributor>
 </scu>
 <scu uid="21" label="A plant is no more mentally alive than a kidney.">
  <contributor label="A plant is no more mentally alive than a kidney">
   <part label="A plant is no more mentally alive than a kidney" start="587" end="634"/>
  </contributor>
 </scu>
 <scu uid="22" label="Being alive doesn't automatically make something a person.">
  <contributor label="Being alive doesn't automatically make something a person">
   <part label="Being alive doesn't automatically make something a person" start="1752" end="1809"/>
  </contributor>
 </scu>
 <scu uid="23" label="A test sees if there is a person in a coffin.">
  <contributor label="Author1 says Author2 has applied a test to see if there is a person in a coffin">
   <part label="Author1 says Author2 has applied a test to see if there is a person in a coffin" start="2796" end="2875"/>
  </contributor>
 </scu>
 <scu uid="24" label="Something that is not alive would not be capable of growing.">
  <contributor label="He points out that something that is not alive would not be capable of growing">
   <part label="He points out that something that is not alive would not be capable of growing" start="2077" end="2155"/>
  </contributor>
 </scu>
 <scu uid="25" label="Both embryos and plants are living things.">
  <contributor label="He concedes that both embryos and plants are living things">
   <part label="He concedes that both embryos and plants are living things" start="2343" end="2401"/>
  </contributor>
 </scu>
 <scu uid="26" label="Both embryos and plants do not have brain function.">
  <contributor label="but that because they do not have brain function">
   <part label="but that because they do not have brain function" start="2403" end="2451"/>
  </contributor>
  <contributor label="it does not think">
   <part label="it does not think" start="440" end="457"/>
  </contributor>
 </scu>
 <scu uid="27" label="Both embryos and plants are considered legally alive.">
  <contributor label="they are not considered legally alive">
   <part label="they are not considered legally alive" start="2453" end="2490"/>
  </contributor>
 </scu>
 <scu uid="28" label="Comparison between a fetus and a plant.">
  <contributor label="compares a fetus with a plant">
   <part label="compares a fetus with a plant" start="2161" end="2190"/>
  </contributor>
  <contributor label="Author2 states that the unborn child, like a plant, can grow">
   <part label="Author2 states that the unborn child, like a plant, can grow" start="375" end="435"/>
  </contributor>
 </scu>
</pyramid>
