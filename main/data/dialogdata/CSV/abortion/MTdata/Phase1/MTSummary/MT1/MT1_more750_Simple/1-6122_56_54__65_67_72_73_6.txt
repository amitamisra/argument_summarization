
----------
D0
----------
Author1 says Prudence's death is related to the abortion argument.  Prudence died because her family could not afford to have her dead fetus medically removed.  Bush is responsible for Prudence's death.  Bush cut funding for international women's healthcare.  Bush cut funding so it would not be used to pay for abortions.  People who call themselves pro-life are actually anti-woman.  Pro-lifers oppose funding that would improve women's healthcare worldwide.  Worldwide, one woman dies every minute as a result of pregnancy.

Author2 says Prudence's death is sad, but is not related to abortion.  Prudence had not sought an abortion.  Prudence died due to insufficient access to women's healthcare in Africa.  The argument over abortion would not have affected a case like Prudence's.  Bush should earmark more money for women's healthcare excluding abortion.  Pro-life activists would all agree that women need better healthcare worldwide.  It is an invalid argument to use the story of a woman who did not want an abortion to argue for abortion.


----------
D1
----------
Author1 asks how how others would feel if they died the same way Prudence did. Author2 says he wouldn't want to die at all. He doesn't see how this relates to abortion. He doesn't think the article deals with abortion. There's no evidence she wanted an abortion. It deals with medical care. It seems to be more about inequality of the sexes in Africa. He thinks everyone should be able to have good medical care. Author1 says her death is related to abortion because Bush drastically cut funding for international healthcare using the excuse he might pay for abortions. Catholics get upset anytime reproductive healthcare is talked about. Women are considered dirt in the world especially by republicans and religions. Author2 thinks that this is a big stretch. The problem should be with Bush, not with pro-lifers. Author2 wouldn't have a problem with more money being sent for healthcare. He still doesn't think the story has to do with abortion. Author1 says women die every minute from pregnancy. Author2 doesn't agree with Author1's statements.


----------
D2
----------
Author1 argues that Prudence died due to the abortion issue. Abortion has reduced international maternal healthcare funding from the US. President Bush cut US overseas healthcare for women. The Roman Catholic Church does not like reproductive healthcare. Women die because they are not valued. Prudence died because women are not valued. Religious and right wing people do not value women. One woman each minute dies due to pregnancy.
Author2 argues that people do not want to die. The article linked is not about abortion. Prudence should not be related to the article. Prudence did not have an abortion. Prudence is irrelevant and off topic. The family did not have money. The money would have been to remove a fetus. Prudence died because she was poor. General medical care in the third world needs to be improved. Bush should have given more money for women's health. Pro-life does not mean anti-women. Not all pro life people hate woman. The story had nothing to do with abortion. The story should not have been used as an argument.


----------
D3
----------
Author1 asked how Author2 would like to die like this woman. Would it be worth it to stop abortions? The death of the woman IS tied to abortion. President Bush cut funding for international maternal health care. He was afraid it was paying for abortions. The Catholic church hates abortions. Author1 blames religion and politicians. They are responsible for women's poor care. Author1 asked how allowing women to die from pregnancy is pro-life.


Author2 said the story doesn't say if the woman wanted an abortion. It said the woman couldn't pay to have a dead fetus removed. The story is about women in other countries who can't afford health care. He agrees women should have better care. They should not have to use midwives. The fetus was already dead. That isn't the same as abortion where the fetus is still alive. Pro-lifers should not be blamed for funding decisions. Women not being cared for properly is different from abortion.


----------
D4
----------
Author1 is Pro-Choice.  Life begins at birth, not conception.  President Bush has cut federal funding providing maternal healthcare in third world countries.  The article shows Prudence may not have died if she had received better healthcare in relation to her pregnancy.  A woman should have the right to choose.  Women are not held in high regard in many countries.  As a result of this fact, women are not seen as deserving of good medical care.  One woman every minute around the world dies due to a lack of proper medical care.

Author2 is Pro-Life.  Life begins at conception.  The United States of America could help with maternal healthcare in other countries, but it shouldn't include abortion.  The article about Prudence was irrelevant because it does not support that she wanted an abortion or received a poor quality abortion.  It is not proven a woman dies every minute due to poor medical care.  Women all over the world deserve better medical care, not just maternal.

