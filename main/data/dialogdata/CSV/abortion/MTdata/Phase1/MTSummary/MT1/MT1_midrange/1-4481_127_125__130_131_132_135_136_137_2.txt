
----------
D0
----------
Author2 notes that a majority of Americans don't support partial birth abortion, i.e. abortion in the third trimester. Author2 also notes that approximately 90% of abortions occur in the first or second trimester. Author1 argues that, even if only 10% of abortions take place in the third term, it's still a major problem. Author2 clarifies his position. Author2 points out that many people who are otherwise pro-choice are nevertheless opposed to late-term abortion. Author1 argues that these people take a pragmatic position on the unborn rather than basing their views on principle. Author2 notes that some people may be pragmatic, but Author2 personally opposes third trimester abortion on principle. Author2 states that there are also people who adopt pro-life positions on a pragmatic basis.


----------
D1
----------
Author1 said partial birth abortion creates the biggest difference. He sees murdering viable fetuses as a problem, even if Author2 doesn't. He apologizes if misunderstanding, but it seemed Author2 wasn't concerned with the 10 % of late abortions. He questions whether others are pragmatic instead of principled. 

Author2 said the majority of people don't agree with late term abortions. This isn't a problem since 90% occur in the first two trimesters, and 88% in the first trimester. He questions how Author1 assumed he didn't see it as a problem. Most people support early abortion and disapprove of late abortion, and because less than 10% are late term, this isn't a conflict for people who support pro-choice who still have principles about late term.


----------
D2
----------
Author1 states the biggest difference comes with partial birth abortions.  Author1 feels Author2 doesn't see the problem with murdering a viable fetuses.  Author1 is concerned Author2 can seem to live with the 10% of abortions being late-term.  Author1 questions whether people's positions are based on pragmatism and instead of principle.  Author2 states the majority of the country doesn't agree with late-term abortions, because most abortions happen within the first two trimesters.  Most of the country does support early abortions.  Disagreeing with late-term abortions doesn't conflict with a person being pro-choice.  Author2 doesn't think what was stated can be interpreted wrong.  Author2 feels some people are pro-choice because it is pragmatic, but feels so are some pro-life people.


----------
D3
----------
Author1 believes that the biggest source of conflict in the debate is over partial birth abortion. The state that third trimester abortions are murder, since the fetus is viable. He argues that anyone who considers themselves pro-choice but disagrees with late term abortions is being pragmatic as opposed to principled.  Author2 believes that most people who are pro-choice are against late term abortions, and points out that because most abortions occur in the first and second trimester, this is not a "big deal" overall. He does note feel that there is a conflict between the two stances, and argues that people are being both principled and pragmatic when they support the right to an abortion but oppose the idea of late term abortion.


----------
D4
----------
Author1 thinks the biggest difference comes with partial abortion. Author2 says partial abortion or not, the majority doesn't agree with late-term abortions. 90 percent happen in the first two trimesters, so this isn't a problem. Author1 says Author2 may not see a problem with murder but he does. Author2 isn't sure how Author1 got that from what he said. Author1 apologizes if he didn't understand but thinks Author1 is saying he can live with 10 percent that are late-term. Author2 says they were speaking of who is pro-choice or pro-life, and since less than 10 percent are late-term, it's not a conflict with being pro-choice. Author1 thinks this is saying most opinions are based on pragmatism, not principle. Author2 replies that isn't what he's saying.

