
----------
D0
----------
Author2 asks Author1 at what point Author1 believes a fetus becomes a person. Author2 states that Author2 is not anti-abortion, but would not support abortion if the fetus would suffer pain during the procedure. Author2 claims that if a fetus is developed enough to suffer, it is sentient. And that should give anyone pause when they consider permitting abortion. Author2 points out Author1 is also unwittingly supporting a universal.

Author1 says that many religious traditions have different views on when a fetus becomes a person. In Author1's view, he as a man does not have the right to decide when a woman should have an abortion. She should make that determination in her best interest. Author1 cautions Author2 not to assume Author2's values are universal.


----------
D1
----------
Author1 said when a fetus becomes a person depends on what people believe, and different religions have different ideas on that. Because of the unclear consensus, the law leaves the decision up to pregnant women. Author1 is concerned when a fetus can feel pain, and that decision shouldn't be left to a woman's whims on being pregnant. When a fetus becomes sentient is also debated. 

Author2 said universal beliefs are applied by what people believe in. He questions why Author1 shouldn't be able to have an opinion on a sentient humans suffering. If a fetus can feel pain, it is sentient, and abortion shouldn't be performed. Author1 is claiming women or ideologies should be able to determine, instead of whether a fetus suffers pain.


----------
D2
----------
Author1 states that different religions have different views on when a fetus transitions to personhood, with some saying conception, and others saying birth.  Since there is no one definitive answer to the question, it makes sense to leave it up to each individual woman to decide.  Author1 says that as a man, he is not entitled to answer the question.  He is critical of those who claim "universal truths," when they really just mean their own personal values should be applied to everyone.  Author2 says the question of when a fetus deserves consideration as a person is not a religious question.  If the fetus is developed enough that it can suffer and feel pain, it should not be permissible to terminate.


----------
D3
----------
Author1 feels that there is no one correct answer about abortion. He believes that there is no hard and fast point in time at which a fetus becomes a human, and he lists different religions with different opinions on the matter. He states that because many different groups have different opinions on the matter, the correct choice is to leave the decision up to each individual woman. Author2 feels that abortion is wrong if the fetus will suffer. He argues that different opinions on the issue do not matter, because it is objectively wrong to terminate a pregnancy past the point at which a fetus can feel pain. He believes this to be a universal belief that should be universally respected.


----------
D4
----------
Author1 thinks there is a difference between a fetus and a person and Author2 asks when a fetus became a person. Author1 thinks it depends on morals and beliefs. Author2 says he wasn't talking about religious belief. He is talking about pain. He isn't against abortion but thinks the fetus would suffer during termination. Author1 says he isn't entitled to answer that question. Author2 thinks he is saying he should have no opinion on the suffering of a sentient being, a fetus. Author1 agrees because sentient is a matter of discussion. He thinks women are able to decide for themselves what is best for them and Author2 is saying his opinions should be universal. Author2 thinks if a fetus can suffer it's an individual.

