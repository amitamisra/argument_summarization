
----------
D0
----------
Author1 states his opinion that a fetus in the early stage of development isn't human. Author2 asks if that means that Author1 is opposed to abortion in the third trimester, since the fetus can survive outside of the womb. Author1 opines that he would likely not support abortion unless the mother's life was endangered. In response to Author2's question, Author1 says that he would be opposed to abortion in the third trimester unless there were serious physical deformities, mental damage or other exceptional circumstances. Author2 thanks Author1 for the answer to his question. Author2 notes that Author1's answer would have to be dependent on the seriousness of the deformity or damage. Given the power of medical science, Author2 thinks the baby should get a chance.


----------
D1
----------
Author1 does not believe a fetus in the beginning is a human. Author1 believes that unless the mother's life was endangered, Author1 would be against a late term abortion. Author1 clarifies that as long as there were no physical problems with the baby such as deformities, mental damage, or other circumstances, then Author1 would not support a late term abortion. 


Author2 asked if Author1 would be against an abortion then if the fetus was old enough to be considered a human baby that could survive outside the womb during the third trimester. Whether deformities should be considered would depend on the severity because of the advances in medical science to treat them, and the baby's right to a chance to live.


----------
D2
----------
Author1 says it is his opinion that early stage fetuses aren't really human.  Author2 asks if he ever considers, "What if I'm wrong?"  Author2 asks if Author1 is against 3rd trimester abortions, given that 3rd trimester fetuses are often capable of living outside the womb.  Author1 says that yes, he would be opposed to 3rd trimester abortions, with exceptions for cases in which the life of the mother is in jeopardy, or the fetus shows certain physical deformities or mental damage.  Author2 seems to be in agreement on the issue, but adds that the severity of the defect should be taken into account, as great gains have been made in medical science, and a baby deserves a chance to live.


----------
D3
----------
Author1 states that they are of the opinion that early stage fetuses are not actually human. They recognize that this is only their opinion, and go on to clarify that they would be opposed to late term abortions unless the mother's health was at risk or the child was severely deformed. Author2 points out that Author1 is only stating an opinion, and asks about his opinion on late term abortion. When it is clarified that Author1 is opposed, Author2 agrees with them, but states that it would depend on the severity of the deformity in question. They state that because of the many advancements that science has made in these areas, the child should at least be given a chance at life.


----------
D4
----------
Author1 thinks the early stages of a fetus aren't human. Author2 says this is just Author1's opinion and asks what if he is wrong. He asks if Author1 is against third trimester abortions because the baby can survive outside the womb. Author1 says he states it was his opinion and says unless the mother's life were in danger he would likely be against them. Author2 asks if the mother's life isn't in danger and the baby could survive if Author1 would be against abortion. Author1 replies that he would be except for cases of severe damage or deformity, or some exceptional circumstances. Author2 thanks Author1 for that and thinks it would depend on how severe the damage was as there have been gains in science.

