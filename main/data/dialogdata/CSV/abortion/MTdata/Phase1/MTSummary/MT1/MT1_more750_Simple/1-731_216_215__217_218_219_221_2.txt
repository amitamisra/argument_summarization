
----------
D0
----------
Author1 argues that women shouldn't be talked to about abortion clinics. It is easy to persuade a woman to have an abortion. There are no disabled people born with tails. All successful births do not look like embryos. Humans have the same recognizable cellular structure. Once that structure is developed in the womb it is a human. Killing a human is wrong. Abortions shouldn't happen once the cells have developed.
Author2 argues that not all humans appear the same. Toddlers don't look like adults. Deformed people don't look like normal humans. We should still treat them the same. Abortions should not be based on visual appearance. People have been born with severe deformities. People with severe deformities are treated less than human. That is wrong. The elephant man was one of those people. We should not treat people with deformities differently. Abortions can be dangerous to the mother. Down's syndrome patients are the genetic same as normal people. We still think of them as humans. We should not treat people differently based on looks or genes.


----------
D1
----------
Author1 says embryos are more similar to mollusks than humans. He thinks it is wrong to counsel women going into abortion clinics. Author2 says that a toddler is underdeveloped. It still has 16 years to go. He asks about deformed children and if we can kill them. Just because someone doesn't look human doesn't mean they aren't. Author1 says he is talking about cell structure. He isn't talking about looks. Author2 says that everyone has tails. He asks if the only way a zygote will be human is if someone looks at it. Author1 again says he is talking about cellular structure. He asks if Author2 really thinks birth control is unsafe and an abortifacient. He asks if he's researched the claims. Author2 says that this doesn't matter. The infant only weights a few pounds and adults weigh over a hundred. People with Down's Syndrome have too many or few chromosomes. These things don't mean they don't have a right to live. A defective cell structure isn't a reason to kill someone in Author2's opinion.


----------
D2
----------
Author1 believes that abortion is not the same as killing a human. Fetuses are not yet fully formed or developed. An embryo in the womb is not like a baby or toddler. It should not be compared to a fully developed person. It has a much simpler structure. Fetuses are more like simple animals than people. Author1 also thinks it's wrong to confront people going into abortion clinics. These women are innocent. It is a lie to say abortion causes cancer or that contraception causes trauma.
Author2 believes that there is no distinction between a fetus and a human. It does not matter that is not fully developed yet. Toddlers are also not fully grown. Some people might say disabled people are not fully developed. Aborting a fetus is comparable to killing a toddler or disabled person.  All humans should have the same right to life. There should not be a line between born and unborn humans. Author2 also believes abortions are dangerous for women. Ending a pregnancy can cause physical problems for the woman.


----------
D3
----------
Author1 asserts that fetuses do not look human. The American Life League is wrong to counsel women going into abortion clinics. He believes that fetuses are like organisms. He stresses that he is referring to their cellular structure. He asks Author2 if he believes their claims that emergency contraceptives are unsafe abortifacients. Abortion causes breast cancer & severe psychological trauma. He questions Author2 on the validity of those claims. Author1 asks him if he researched it to see if the claims were true or not.

Author2 states that deformed humans do not look human. Toddlers are not fully developed. The American Life League offers post abortion treatment centers as well. He believes that these organisms are human. It does not matter that a zygote does not look human. A toddler does not look like an adult. They are both human. Our cellular structure is always different in the cycle of life. He states that he does not know what the effects of abortion are. Author2 insists that abortions are dangerous. These premature procedures cause problems.


----------
D4
----------
Author1 said a fetus is a human when it is born. As a fetus though, it is a simple organism. It is similar to a mollusk. Trying to dissuade women walking into abortion clinics is wrong. Author1 said they are not talking about looks. They are talking about cellular structure. Author1 scolds Author2 for telling lies to people going into abortion clinics. They ask if Author2 believes the false claims. Contraception is supposedly unsafe.. It causes cancer and psychological trauma. Author1 asked if Author2 has researched the truth about those claims.

Author2 said the toddler is undeveloped. It still has years to grow. Some deformed children don't look human. Not looking human doesn't mean someone isn't human. Author2 cites the differences of an infant to an adult. Does that make them not human? People with Down's Syndrome have different cellular structure. Does that make them not human? Can people with different cellular structure be killed then? Abortion is dangerous because the uterus is opened too early.

