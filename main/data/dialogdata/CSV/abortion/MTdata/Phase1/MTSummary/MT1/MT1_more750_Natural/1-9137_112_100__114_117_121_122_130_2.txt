
----------
D0
----------
Author1 argues that the rights of the mother need to be protected. Under certain circumstances, abortion is able to be considered, because abortion is not cut and dry as it seems to be made out to be. Killing a baby is not what abortion always is, because the fetus has to go through a development period. Abortion is not a get out of jail free card, because the mother has rights as well as the child, but she is also responsible for carrying the child. Killing children after they are born is not acceptable, because past the third trimester it is a living breathing person. Providing education on safe sex, abstinence, STD's, and AIDS education are all good things that can reduce teen pregnancy and the need for abortions. 
Author2 argues that killing your baby is not acceptable. It is in fact a crime. Under extreme cases, abortion may save the life of the mother, but that planned parenthood in schools would cause more issues than it is worth. It would be too evil.


----------
D1
----------
Author1 said mother's rights have to be protected. Abortion should be allowed under certain circumstances. The mother is responsible for carrying the child, so she has to have some rights too. He advocates clear careful thinking before abortion. PP in schools would be a good thing because it would teach kids about sex education. This was neglected during the Bush years and he claims teen pregnancy doubled in eight years. He said there are enough laws regulating abortion, and people need to be able to make free choices. 

Author2 asked why the right to kill a baby should be protected. The mother is responsible for being pregnant. He agrees abortion may help save some mother's lives. He doesn't like the easy availability, or the idea of planned parenthood being in schools. He thinks PP in schools will lead students to available abortions. He asked how a child born c-section to prevent death  is viable, and yet a fetus is also viable but not protected from death. He proposes no abortions after 3 months.


----------
D2
----------
Author1 believes the rights of the mother should be reserved, that under certain circumstances abortion should be considered. Throwing a child in the trash is wrong because it is already born, abortion saves the life of the mother in extreme cases. Author1 states that he is unaware that some people want to put a non-profit in a school. There will be no abortions on campus, sex education is all that will be offered. Author1 tells Author2 that his measures are extreme, and that there are enough laws to regulate abortion. People need to make informed choices without feeling judged.

Author2 states that Author1 promotes a mother's right to kill her baby, and abortion clinics are too available. Some people want Planned Parenthood in schools, and the liberal media does not cover it well. It will offer easy access to off campus abortions, and Octomom had premature babies. Author2 asserts that Author1 would consider premature babies to be fetuses. A baby in the womb is a viable baby, abortions past the third trimester should be banned.


----------
D3
----------
Author1 says we have to protect the rights of the mother. Author2 says he means the right to kill the baby and that isn't a right. It's legalized crime. Author1 says it isn't so cut and dry. He supports abortion under certain circumstances, and the mother has rights as she is responsible for the child. Author2 agrees she is responsible, and if the mother's life is in danger it is different. The problem is the way it's set up. He asks if Author1 supports planned parenthood in schools. Author1 thinks Author2 is trying to blur the issue. Past the third trimester the baby is a breathing person. He asks what planned parenthood would do in schools and thinks abortion should be used with care and caution. Author2 says it would offer easy access to abortions. He asks if a child that is born early that is viable should be considered independent. He thinks all abortions after 3 months should be illegal. Author1 thinks this extreme. He thinks there are enough laws to govern it already.


----------
D4
----------
Author1 argues that the rights of the mother to have an abortion are important, and that abortion should be considered under certain circumstances. He argues that because of fetal development the issue is not simply killing the baby, and that the mother should have the right to decide since she is the one responsible for the child. 
He argues that there are already enough laws on the books for regulating abortions, and is in favor of providing sex education in schools, including practicing safe sex, abstinence, STD " s, and AIDS education. 

Author2 calls abortion the mother's "right to kill her baby" and states it is not a right it is a crime against nature. He agrees that under extreme cases abortion may save the life of the mother, but argues that it is not limited to mothers in danger.
He asserts that planned parenthood in schools, will offer easy access to abortions. He next points out that children can be born prematurely and still survive, so all abortions after 3 months should be outlawed.

