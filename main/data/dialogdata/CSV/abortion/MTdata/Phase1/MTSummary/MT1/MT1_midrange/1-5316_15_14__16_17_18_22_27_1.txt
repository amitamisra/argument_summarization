
----------
D0
----------
Author1 challenges the idea that most abortions happen because birth control fails. Author1 believes it is unlikely that statistics will be found to support that argument. Author1 argues that when women have sex without birth control, any abortion is simply for convenience. Author1 says that women shouldn't use abortion to replace birth control. Author2 counters that Author1 is trivializing abortion and disrespecting women. Author1 says that the fetus deserves the right to life. Author2 states that is simply a moral rather than logical judgment. The fetus only deserves to live if it has rights. Author1 believes the fetus is alive. Author2 agrees, but does not believe the fetus is sentient. Author2 references a picture of a fetus holding a doctor's finger. Author1 calls it 'propaganda.'


----------
D1
----------
Author1 refutes abortion is used because birth control fails, and doubts women used birth control. He denies he said it's strictly the woman's fault, both partners should make sure birth control is used, and not use abortion for convenience. Abortion is the woman's option, and she should take precautions if she didn't want one, and it shouldn't be used as birth control. He cites pictures of a fetus as being sentient, thus deserving of rights. 

Author2 said Author1 shouldn't be allowed to decide what is moral for someone else. The decision could affect her whole life, and a child's. The fetus should not be equal to the woman's rights. S showed disrespect in how he spoke of women. Fetuses don't have automatic rights.


----------
D2
----------
Author1 argues that abortion is being used as just another method of birth control, and that is irresponsible.  It is not the case that most non-rape-victims who seek abortions were using birth control that failed.  It is irresponsible to not have one or the other sex partner use birth control, if pregnancy is not an acceptable outcome.  A fetus is not on equal footing with the mother, but it is more deserving of consideration than is a strep infection.  Author2 states that Author1 is making a moral judgement and not everyone shares the same viewpoint.  Author1 is investing the fetus with rights equal to the mother's.  Author1 is trivializing the impact an unwanted pregnancy could potentially have.


----------
D3
----------
Author1 says he doubts it possible to find statistics saying abortions aren't for convenience but because birth control failed, or that it's even used by most women seeking an abortion. He didn't imply it's only the woman's responsibility. He said non-rape victims willingly engaged in activity that can lead to pregnancy. Abortion shouldn't be a form of birth control. Author2 asks if this is true because Author1 decided it's wrong. Placing the fetus as equal to the mother is a moral judgement and her decision will effect the rest of her life and her children's. She isn't obligated to share Author1's morals. Author1 says they're not equal but are deserving of a chance. Author2 replies this is a moral judgement. Author1 realizes not all agree.


----------
D4
----------
Author1 contends abortion is used as birth control by people who make irresponsible choices to engage in unprotected sex. Author1 compares the disregard for abortion to taking medication for a bacterial infection like strep. He shares a photo of a 21-week-old fetus holding a surgeon's finger during prenatal surgery as proof of the difference. Author2 believes Author1 is trivializing the use of abortion, which affects a woman's life and future reproduction, and disagrees that it's used simply as another form of birth control. Author2 feels this shows a moral judgment that the fetus has rights equal to the woman, and asks Author1 to prove that this is a logical conclusion. Author2 agrees that a fetus is alive but not that it is sentient.

