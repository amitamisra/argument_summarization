
----------
D0
----------
Author1 thinks a woman shouldn't be forced to see the fetus image before being allowed an abortion. He says contraception is as bad as abortion in some religions. The law that's being discussed also has to do with religion. He continues that only in religion is 2,4 or 8 cells considered a person. If it wasn't a religious position there would be a number of states making conception "the point when one has rights". Author1 believes it should be up to the woman to decide without coercion. He wonders who might be sponsoring this type of legislation?

Author2 says contraceptives and abortion are two entirely different things. He says he doesn't believe in religion. He doesn't think it has anything to do with this discussion. He claims there are ethical and moral perspectives outside of religion that would require a woman look at the "potential child" she is about to abort. He thinks people grown up enough for sex should face up to the consequences of that behavior.


----------
D1
----------
Author1 asks if it should be required to submit to a prostate exam and provide a semen sample before buying condoms. Author2 says if it was a requirement before buying condoms he would still have a choice. The fetus doesn't. Author1 thinks contraception is as bad to some as abortion. Author2 says so is eating pork to some. He asks what religion has to do with the conversation. Author1 says religion has everything to do with it. Author2 thinks there are ethical and moral perspectives outside of religion that would require a woman to look at the potential child she is about to destroy. Author1 says potential child is a meaningless term. Author2 doesn't agree. He says if the fetus was considered a person the woman would not be allowed an abortion. Author1 asks why her rights come second. Author2 questions what rights. Author1 replies her right not to be subjected to procedures to satisfy someone else. Author2 says some think abortion is an invasive procedure. People should use more caution beforehand. There is contraception.


----------
D2
----------
Women should not be forced to look at images of fetuses. Abortions should not have ethical questions in them. Birth control and contraception disgust some religious people. Abortion disgusts some religious people. Religion affects abortion laws. Fetuses are not potential children. A fetus might turn into a child. A fetus is not yet a child. Religion is the only time a few cells are called a person. Some states want conception to grant rights. Women should not be forced to look at their fetus before abortion. Unnecessary medical procedures should not be taken without consent. The law should not force invasive procedures. 
Author2 argues contraception is different from abortion. Religion does not apply in an abortion debate. Ethics and morals are different from religion. Ethics apply to abortion. Ethics says people should look at what they destroy. Fetuses are potential children. Fetuses should be treated like potential children. Women could not get abortions if fetuses were treated right. Womens rights do not enter the debate. People think that fetuses have no rights.


----------
D3
----------
Author1 says women having an abortion shouldn't be forced to do something. They shouldn't have to see an image of a fetus. Religion is the only one who sees a group of cells as something else. It isn't a potential child. What something COULD be does not define what it is. Women's rights should not be taken away. They should not be given to something that is not a person. No one has the right to force someone to do something without a good reason.

Author2 says a fetus is being forced to have a procedure during an abortion. A fetus is a potential child. It might not be a person yet. But it has the potential to become a person. Author2 says women should have to look at an image of the fetus. There are ethical and moral reasons to do this. A woman should see what she is destroying. Women might be more careful not to have unwanted pregnancies. It is immoral to say a fetus doesn't even deserve being looked at.


----------
D4
----------
Author1 is Pro-Choice.  Women should not be made to view ultrasound images in order to receive an abortion.  Life begins at birth.  Abortions are not performed after the fetus reaches over eight cells.  Should a man have to provide a semen sample to buy contraception?  Women should not have to undergo invasive procedures to be granted the right to have an abortion.  Abortion laws are often attacked in a religious capacity.  Abortion is a woman's choice and she should make the decision.

Author2 is Pro-Life.  Abortion is immoral.  Women should be made to view an ultrasound image of the fetus prior to being permitted to have an abortion.  This concept would force the woman to see what she would be destroying is a potential child.  Those who are in support of Pro-Choice view fetuses as parasites to be destroyed.  A fetus is not a parasite.  Seeing ultrasound images prior to abortion may remind the woman to use contraception next time she has sex.

