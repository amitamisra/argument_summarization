
----------
D0
----------
Author1 wants to know why Author2 thinks a "woman's decisions to have an abortion is arbitrary". He also asks to see proof that "infant abandonment is happening more often". He goes as far as to ask Author2 for a numbered list of "arbitrary reasons" and explanation of such. He asks again for proof, other than more mention in the media, that more women are abandoning babies. He says the media is liberally biased and can't quite believe the numbers as being accurate. He provides a link to a CDC website that claims the rate of abortions in the US is going down. He also believes that 40,000,000 seems like a low number.

Author2 claims he has answered Author1's questions more than once and that Author1 is a blind liberal that has more compassion for a murderer on death row than an unborn baby. Author2 goes on to say that he thinks a lot of women "take the easy way out" with abortion. He feels sorry for Author1 but he isn't re-listing his reasons.


----------
D1
----------
Author1 asked why Author2 said women's reasons for abortion were arbitrary, and proof that child abandonment happens more often. Author1 asked what are the reasons women feel they must have an abortion. Just because child abandonment is given more attention in the news doesn't mean it happens more often, and Author1 asked for proof. Author1 is from Canada and assumes Author1 is from the U.S., and the abortion rate is different in Canada. According to the CDC, the rate of abortions in the U.S. is dropping. The number of abortions mentioned also is spread out over many years and a very large population. Author1 asked again exactly what the frivolous reasons are for abortion. 

Author2 said Author1 is probably one of those people who don't care about a baby in the womb, but protests murderers being executed. Women see abortion as more convenient. There seems to be more news reports on children being abandoned.  Author2 just knows what the news reports. Forty million babies have been killed by abortion since 1972.


----------
D2
----------
Author1 argues that women's decisions to have abortions are not arbitrary. Infant and child abandonment may or may not be happening more often but there are no sources provided to tell. Abortion is not used as an easy way out for most people and most women do not treat abortion as a get out of jail free. The reasons for most abortions are not arbitrary and are pretty concrete in some cases. Abortion now has a bigger presence in the media but that does not mean there are more abortions happening. There must be some statistics in order to prove a number, not just pulled from thin air. The abortion rates in different countries is different due to different circumstances.
Author2 argues that some people are filled with compassion for murderers and rapists but can be fine with killing babies in the womb. Abortion is seen as an easy way out for a large number of reasons, and abandonment is very common. The media says there are many frivolous abortions.


----------
D3
----------
Author1 thinks Author2 either doesn't have an answer to his questions or can't express them in English. Author2 says he has answered his questions. He thinks Author1 is a liberal that has no compassion for a baby in the womb, but has it for murderers and rapists. Author1 asks why Author2 thinks the decision to have an abortion is arbitrary. He asks how he can justify that abandonment is happening more often. Author2 thinks that some women take the easy way out by having an abortion and you can't pick up the paper without hearing about infant abandonment. It didn't used to be that way. Author1 questions abortion being an easy way out and asks how it is arbitrary to do so. Also, media attention to things doesn't mean it's happening more often. We simply hear of it more often. Author2 says he is going by what he hears and 40,000,000 murdered babies isn't nothing. Author1 says there should be official statistics or records to verify the numbers and asks which country Author2 is talking about.


----------
D4
----------
Author1 is Pro-Choice and feels abortion should remain legal.  He does not agree with Author2's opinion advising more women abandon infants these days and challenges Author2 to provide proof of this statement.  Author1 argues Author2's statement that abortion numbers are rising, citing the Center for Disease Control's website as stating numbers are actually decreasing.  Author1 also advises he is from Canada so there is a difference in their numbers from the United States. He feels a lot of the numbers regarding abandoned children are inflated by the media presence.   He feels Author2 has not answered questions to support his theories.  

Author2 is Pro-Life and believes women who choose to abort are taking the easy way out.  He advised there have been 40,000,000 abortions in the United States since 1972.  He feels abortion is murder.  He feels women who choose to have abortions do so for frivolous reasons.  Author2 feels those who support abortion do not have any compassion for a child in the womb.

