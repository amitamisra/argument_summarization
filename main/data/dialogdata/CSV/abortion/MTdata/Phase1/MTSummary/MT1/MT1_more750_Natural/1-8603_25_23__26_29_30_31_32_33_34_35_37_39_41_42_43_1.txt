
----------
D0
----------
Author1 asks Author2 to prove his last sentence. Author2 asks how many infants or children Author1 has adopted or taken in. Author1 hasn't but would if his brother, sister or their children needed. He would do it without feeling the need to exhibit any moral superiority as he thinks Author2 seems to be doing. Author2 says he is only speaking as one that has. Author1 thinks this doesn't make him a fount of knowledge. Author1 thinks Author2 is basking in someone else's glory. Author2 asks if Author1 or others have adopted any of the babies they fought so hard to save, or if the concern for them ends at birth. Author1 says Author2 is citing someone else to qualify him to ask that question. Author2 says his adopted 37 year old son may have a different opinion and again asks how many children Author1 has adopted. Author1 doesn't think the argument that he should have to adopt the baby he saved is a valid one. It's like saying he should fund dialysis for a kidney.


----------
D1
----------
Author2 is accusing Author1 of arguing for the sake of saving a life via abortion but not following through with the care of said child after birth. Author2 has exposed Author1 for not taking in any of the survivors of the would be abortion. Author1 retorts by exposing that Author2 has referenced a story to which Author2 themselves were not a party. Author2 comes back with that even though they did reference another's story that they have actually performed the task being discussed, the task being adoption. Author1 reiterates Author2's previous post which is meant to illustrate how Author2 is extorting credit from someone else's selflessness. Author2 goes on to say how they actually have adopted in the past. Author1 satirizes the claim by retorting that his grandmother had joined the foreign legion. Author2 congratulates Author1's grandmother and attempts to restate the original assertion that none of the restrictionists have adopted any of the babies they fought so hard to save. Author1 challenges Author2's integrity and then they go on to jab each other sarcastically.


----------
D2
----------
Author1 states that she would provide a home if her brother or sister needed one, she would not feel the need to act morally superior. She points out an example where Author2 talked about adopting or taking a boy in, but he was just basking in someone else's glory. Author1 believes that he took credit for another person's good deed, and that Author2 is being hypocritical. He had an invalid argument when he stated that Author1 should adopt the fetus that she saves.

Author2 asks her how many children she has adopted or taken in on her own dime, and he states that he has adopted a child. He wonders if she misspoke when Author1 said that she has not adopted any children. He reiterates to her that he really did adopt his child. He asks Author1 if she ever adopted any of the fetuses that she fought to save. He believes that her concern for the fetus ends at its birth, and Author2 then tells Author1 that she needs to get her facts straight.


----------
D3
----------
Author1 wants proof! Says she'd take in her brother's or sister's kids if need be. Then she says "haven't we all?" as if she had. Author1 turns the question around on Author2, sarcastically, then goes on about Author2 being a "keyboard hero". Author1 all but calls Author2 a liar to her face, says that her "adopted 37 year old son is as real as my foreign legionnaire grandmother." Finally the real discussion - or argument - Adopting the fetus that Author1 wants to save from abortion. And it's gone again in a flurry of "you are a silly woman too!" And something about grandma smells like a sea lion?

Author2 wants to know how many kids Author1 has taken in or adopted "on your own dime?" She says that she has an adopted 37 year old son. She asks if Author1 has adopted any of the babies she fought to save when they were just fetuses or "does concern for the little dears end at birth?" she finishes sarcastically. Author2 wishes Author1's grandma well.


----------
D4
----------
Author1 said he would take in any of his family or their children that needed a home. He accuses Author2 of acting superior. Author1 said haven't we all, and that most people have or will, but know that doesn't make them superior in knowledge. He says his wife's brother is dying, and his wife's sister has offered to take in his son. Author1 doubts that Author2 even has an adopted 37 year old son. He dismisses the argument that he should adopt a fetus he saved as invalid.

Author2 asked how many children Author1 has taken in or adopted. She says she has done so. Author2 accuses Author1 of lying by saying haven't we all, when he just said he hadn't taken any in. Author2 claims to have an adopted son who is 37. She asked again if Author1 adopted any of the babies he fought so hard for when they were fetuses, or whether his concern for unborn children ended once they were born, and he just gave token charity donations.

