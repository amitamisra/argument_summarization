
----------
D0
----------
Author1 argues that women don't have to do anything particular to care for the fetus when they're pregnant other than avoiding overeating and overexertion. Author2 says that Author1 could only hold that view because he is a man. Author2 accuses him of ignorance of the dangers of pregnancy. Author1 disdains her argument and calls her 'self-righteous.' Author1 contends that birth involves relatively little danger, and that driving at night is far more dangerous. Author2 challenges Author1 to produce statistics in support of his claim. Author1 cites statistics to the effect that there is a 1% of dying in a car accident, while there is a 0.00005% chance of dying in childbirth. Author2 requests that Author1 provide the source of his statistics.


----------
D1
----------
Author1 said caring for a fetus only requires a woman eats and doesn't over-exert herself. He said billions of births have happened without danger. He claims the risk of death is higher for driving at night than having a baby. He cites traffic deaths in one year as 41,611 meaning you have a 1% chance of dying in a car accident, whereas only 5 out of every 100,000 births are fatal for mothers. Since women have a small number of children, that makes the risk lower. 

Author2 rebuked that only a man would say that, and Author1 obviously doesn't understand what pregnancy is like and the possible complications that can happen. She asked where he got his statistics and his sources.


----------
D2
----------
Author1 argues that caring for a fetus in the womb is no more trouble than caring for one's big toe.  Billions of human births have taken place -- they were not all replete with danger.  You have a higher chance of death while driving at night than giving birth.  The chance of dying in a car accident during one's life is about 1%.  The maternal mortality rate is 5.1/100,000 births.  Unless you have 20,000 [sic] kids, your chances of dying in a car accident are greater.  Author2 states that only a man could conclude that pregnancy is like caring for a toe. Author1 is totally clueless about pregnancy and possible complications.  Author2 questions Author1's statistics, asking for a source.


----------
D3
----------
Author1 asks what care is needed for a fetus in the womb other than a woman eating and not over-exerting herself. It's like saying a woman shouldn't care for her toe if she doesn't want to. Author1 thinks only a man could say that and Author2 is clueless about pregnancy and possible complications. Author1 thinks this is another "walk a mile in my shoes" ideas. Only a self-righteous woman would argue that every birth is replete with danger. Statistically speaking, driving at night is more risky than giving birth. Author2 asks from what statistic Author1 drew that conclusion. Author1  replies from the number of traffic deaths and maternal mortality rates, and unless you have 20,000 kids driving is more risky. Author2 asks for sources.


----------
D4
----------
Author1 says pregnancy requires little care on the part of the woman other than eating and not doing anything too strenuous. Pregnancy doesn't pose any more risk to a woman than caring for a body part like a toe. Author1 quotes statistics of the risk of dying in a car accident as about 1% in a person's lifetime as opposed to a much lower chance of maternal mortality, which is 5.1 in 100,000 births. Author2 contends that only a man would say pregnancy is not a health risk to women, and accuses Author1 of being ignorant about the possibility of complications. She asks for proof that there's a greater chance of dying in a car accident. When Author1 quotes numbers, Author2 asks for a source.

