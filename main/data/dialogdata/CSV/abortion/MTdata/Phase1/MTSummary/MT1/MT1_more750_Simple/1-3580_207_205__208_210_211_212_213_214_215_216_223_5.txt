
----------
D0
----------
Author1 says abortion doesn't just withhold support from a fetus. It removes it in a way causing death. Why can't women withhold support from a week old baby? If she can do it to a fetus she should be able to do the same with a newborn. A newborn can't survive if no one cares for it. If no one can be found, a quick death is preferable. Someone should be forced to care for it. That's better than killing it. The force is permissible. It is paid for by taxes to the welfare system. He would support a small tax to pay for it.

Author2 agreed forcing a painful death on a child is wrong. The child could not survive alone. A quick death was preferable. He asked how you could force someone. Jail might be the only way. He pays taxes willingly. Some people don't. Force may not work though. Child neglect is illegal. Some people still do it though.


----------
D1
----------
Author1 is Pro-Life.  If it is alright to have an abortion, why is it illegal to withhold care from a child?  Should people euthanize children they no longer wish to care for?  People should be forced to care for children via a taxing that contributes to unwanted children's care.  If folks would give up a cup of coffee or dinner out once a week, that money would help to care for unwanted children.  It is reasonable to force people to care for unwanted children by tax.

Author2 is Pro-Choice.  Care should not be withheld from a child, causing a lingering and painful death, a child could be euthanized; however.  What should the penalty be for someone who does not want to care for a child?  The thread of going to jail is not enough to force people to care for children.  A tax may not work to force people to care for children as there are many people who do not pay taxes without fear.


----------
D2
----------
Author1 argues that abortion is not removing support. Abortion is the removal of a fetus. Abortion is trying to kill a fetus. Fetuses should not be different from born children. Why can we kill a fetus and not a child. Starving a child is the same as aborting a fetus. Newborns need help from somewhere. It is better to force someone to care for a child. Children's lives should be preserved. The state should force people to care for children. Laws do not stop people from bad behavior. Bad behavior does not stop new laws.
Author2 argues that abortion seems harmful. Fetuses should be cared for. It would be best to care for babies outside the womb until they are ready. Euthanizing children is better than starving them to death. Taxes are not forcing others to act. Taxes are how we participate in the government. Child neglect laws do not stop neglect. Child neglect punishment needs to be harder. Child neglect punishment is not effective enough.


----------
D3
----------
Author1 states that abortion is the "active removal of the fetus ... intended to bring about its death." He asks why a fetus and not a living child? If the mother has the right to abort what's the difference if she decides to starve her born child? Author1 says since "newborn babies can't survive without the aid of another and if one can't be found... quick disposal seems to be the next best option, right?" Author2 throws the question back. Author1 concedes. He says it would be better to "make someone take care of it" than to euthanize it. When asked how much force to use? Author1 replies, "the same amount it takes to get my taxes that pay for this welfare state. 

Author2 catches on to Author1's line of questioning. She answers back "quick disposal seems to be the next best option". Then suggests euthanasia when someone couldn't be found. Author2 ends by telling Author1 hire a new accountant or take a class.


----------
D4
----------
Author1 thinks abortion is more than just withholding support from the developing fetus. It's the removal of the fetus from it's natural environment using a procedure intended to bring about it's death. He questions if she has the right to remove her support from the child at any stage. Does she has the right to withhold nutrition and hydration from her 1 week old baby? Author2 says there is no point once removed from the mother's body. He thinks it would be wonderful if we were able to simply remove the fetus from the mother's body and put it in an artificial womb. If the fetus cannot survive being removed from its mother's body then quick disposal seems to be the next best option. Author1 questions if the child doesn't get care would it be preferable to euthanize the child rather than let it starve. He thinks someone should be forced to care for it. Author2 asks why and how. Author1 replies with welfare. He thinks this reasonable. Author2 questions why it is reasonable.

