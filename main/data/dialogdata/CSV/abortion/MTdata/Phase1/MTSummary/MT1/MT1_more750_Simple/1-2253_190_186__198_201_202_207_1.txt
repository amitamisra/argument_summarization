
----------
D0
----------
Author1 is Pro-Life.  Abortions should not be performed regardless of the status of the fetus.  A fetus is not just a lump of tissue, but a person.  Life begins at conception.  If it looks like a person, it's a person.  Four D ultrasounds show the person-like features of a fetus.  Just because a fetus is confirmed to be deformed does not mean it should be aborted.  A fetus should be viewed as a person.

Author2 is Pro-Choice.  Life begins at birth.  A fetus does not show physical features or brain function until the point at which abortion is no longer an option.  Are people with deformities not people?  Many anti-abortion websites cannot be trusted because of who runs them.  Only medical websites should be trusted when researching the development of a fetus into a child.  Potential never outweighs actual.  The lump of tissue a fetus is until further developed has potential to be a child but is not one yet.


----------
D1
----------
Author1 argues that people look like people. Therefore objects look like people are people. People who look like humans should be treated with dignity. People who don't look like humans should be treated like humans. People should not be aborted. Mental and physical disabilities should not be cause for killing. Abortions are wrong in these cases.
Author2 argues that people can not look like people. People may sometimes be different. Pictures of disabled people do not appear to be human. Anti abortion propaganda is bad. Anti abortion propaganda uses photographs sneakily. Abortion propaganda uses pictures of women late in pregnancy. fetuses late in pregnancy look as if they are human. Abortion propaganda tries to mislead people. Propaganda uses pictures of a zef instead of a fetus. A fetus is different than a zef. Propaganda tries to mislead people. Abortion is not a bad thing. Abortion is not killing babies. Abortion is typically early in a pregnancy. Abortion is not typically evil.


----------
D2
----------
Author1 showed a link to an ultrasound picture. He clarified what he meant. It "usually" is a person if it looks like one. He doesn't support aborting retarded or deformed fetuses. He showed a picture of a person in the womb. He wanted to know if Author2 believed it's a person. He asked if she believed ultrasounds showed a person. They show physical features of babies. Author1 showed new pictures. He accused Author2 of using the lump of tissue argument.

Author2 said Author1's argument is if it looks like a person it is one. She argues people with deformities or injuries do not look like people sometimes. She accuses Author1 of using biased materials. Author1 is using the pictures to say they are people because they resemble people. Author2 does not doubt 4D ultrasounds. These are propaganda pictures used by pro-life websites. They are lumps of tissue with potential. Potential doesn't mean something is actual.


----------
D3
----------
Author1 states a picture of a person is usually a person. He believes that severely disfigured people should not be aborted. Mentally undeveloped people should not be aborted. An ultrasound shows human features of a fetus. This means the fetus is a person. Medical establishments and medical websites use photos showing fetuses that look like people.  

Author2 states the argument that if a person does not look like a person it isn't, means a disfigured person isn't one. The argument would also mean a fertilized egg isn't a person. Pro-life websites are biased. They mislabel their pictures. Their sites are filled with lies. The information on pro-life websites can not be trusted.  90% of abortions are performed in the first trimester. A lump of tissue in the early stages is tissue with potential. It is not an actual person.


----------
D4
----------
Author2 thinks Author1's argument is it looks like a person. That makes it a person. He doesn't agree. A fertilized egg or deformed person wouldn't a person because it doesn't look like one if that were true. He thinks the link posted is biased. Author1 says his argument is it usually is a person when it looks like a person. He questions if Author2 denies a medical ultrasound shows the physical features of babies in the womb. Author2 asks why show the pictures if he isn't saying it's a person because it looks like one. He thinks the pictures are misleading. The pictures aren't labeled. Author1 thinks Author2 is one of few pro-choicers that hasn't progressed past the lump of tissue argument. Most realize there is more to the debate than that. Author2 thinks it's clearly Author1's skills that are lacking. Author2 asks if Author1 would like to admit the pictures are nonsense so they can move on. Author1 talks about trisomy conditions. Author1 thinks it is tissue with potential. Potential never outweighs actual, though.

