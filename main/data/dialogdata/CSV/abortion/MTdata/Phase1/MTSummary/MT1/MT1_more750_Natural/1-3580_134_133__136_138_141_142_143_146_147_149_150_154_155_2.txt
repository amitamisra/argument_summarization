
----------
D0
----------
Author1 says "it's a baby, a human, a life" then goes on to say most women have abortions because they don't want to have a baby. He believes an abortion is like an "undo" button and equates abortions with contraceptives. He states that the "line between getting accidentally pregnant and not getting accidentally pregnant is very thin". He thinks the only way to reduce the number of abortions is to reduce the number of unwanted pregnancies. He then suggests either "more birth control or less sex". He says with sex there is always the risk of pregnancy, if that happens it can be undone via abortion.

Author2 believes abortion is murder and not "ok" unlike using contraception. He also believes that from conception is when the fetus is a person, that only at a few weeks old it looks like a small human is proof. Author2 suggests that banning abortions would be the best way to make sure less are done. He says once pregnant, a life is created that no one has the right to end.


----------
D1
----------
Author1 is Pro-Choice and feels abortion is not an act of killing.  Author1 is in favor of women using abortion as birth control per the statement made, advising abortion is just another form of contraception as they both produce the same result.  Author1 does not feel any woman enjoys having an abortion and advises the process is not a comfortable one.  Author1 feels abortion is a better solution and producing an unwanted child noting the numbers are 40,000,000 since 1972.
Author2 is Anti-abortion and does not agree with using abortion as an alternative form of birth control.  He feels abortion is the act of killing a child and should be banned.  Author2 believes life begins at conception, not at birth.  He feels people engaging in sex are knowingly risking getting pregnant, even if contraception is used and should accept the consequences of their actions.  He compares the issue to chancing being hit by a car when crossing a street.  Just because it shouldn't happen doesn't mean it won't.


----------
D2
----------
Author1 said most women have an abortion because they don't want to have a baby. The abortion is like an "undo button." They don't enjoy abortions. They want to have sex, and use contraceptives to not get pregnant. If they don't use contraceptives for some reason or they don't work and they get pregnant, then they have an abortion. Thus, an abortion is just like a contraceptive. He said contraception is the same thing, stopping a fetus from being developed. They are 2 methods with the same results. He asked if Author2 would want more unwanted pregnancies..

Author2 says it is murder, not an undo button. He asked how Author1 would feel if someone had wanted to undo him. Contraception is different because it isn't murder of a fetus that is already started. He says conception is when it becomes a person no matter how small it is. Author2 believes abortion should be banned. He says yes he would rather have more people born because human life should be valued.


----------
D3
----------
Author1 says it's a baby. It doesn't matter what its called, the sentiment remains the same. He thinks most women have abortions because they don't want to have a baby. They have sex and don't want to get pregnant, so they abort the baby. He doesn't think they enjoy the abortions, though. They should use contraception. Author2 thinks abortion is murder. Author1 asks how it is different than using a condom. Author2 says if you killed someone in their sleep they wouldn't know it either, but it doesn't make it alright. Author1 thinks there is no difference in contraception and abortion. Author2 thinks life begins at conception. Author1 says the root of the problem is having sex with little discretion. Author2 thinks banning abortion would reduce the number of them. You should accept the consequences of your actions. You can't kill people to fix your problems. Author1 questions if you are on your own when you choke at KFC because you took the risk. Author2 says there are no disclaimers to protect you from choking.


----------
D4
----------
Author1 argues that the name of what you call a fetus is irrelevant, because most people have abortions simply to get rid of the baby. They enjoy sex and treat an abortion like an undo button where they can just ignore condoms and contraceptives. There is no difference between an abortion and contraceptives, both result in a lack of a child. There is no dilemma in abortions because the fetus is never conscious and is removed before it is truly alive, and abortion is just another way to get rid of a pregnancy. People should have sex with a little more discretion. To reduce abortions you need to either have better birth control or less sex, but both of those don't work. 
Author2 argues that murder is abortion, and that is different from wearing a condom. Ethics applies even if you kill someone while they are not aware. You can identify if its a person by using DNA tests. Banning abortion is a good idea to reduce abortions. Risks are in everything.

