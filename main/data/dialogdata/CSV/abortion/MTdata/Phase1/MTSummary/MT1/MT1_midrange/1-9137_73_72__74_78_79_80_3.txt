
----------
D0
----------
Author1 speaks of her childhood full of serious illnesses, and asks whether Author2's God afflicted her with those illnesses. She implies that this may be the reason why she does not have religious belief. Author2 argues that, even with all of Author1's health issues, it was better to be alive than dead. Author2 states that abortion shouldn't be considered merely because a child may suffer. Author1 denies that she was talking about abortion. Author2 notes that Author1 said that she would abort a baby rather than see it have similar pain and suffering. Author1 says she will likely die of a heart attack and that it would be unfair to have a child. Author2 says the child should have a chance and Author1 might survive.


----------
D1
----------
Author1 cites the health problems he had from a young age.  He said he never mentioned abortion. He claims to have a short life span and how that would affect a child. 

Author2 asked if Author1 preferred to be dead, or if it would be better if his mother had killed him. He asked didn't good moments outweigh bad. He asked if people should kill babies if there was a chance they may suffer, or if they weren't perfect. He claimed the topic of the thread was abortion, and Author1 said he would kill his baby so it didn't suffer like him. He said a baby deserved the good times available. He said at least the baby would be alive and adopted.


----------
D2
----------
Author1 was very sick as a child and it wasn't until after she was adopted they discovered she had bad asthma and constant ear infections which made her almost deaf.  Author1 believes she is more mature and open minded because Author1 doesn't have a religion.  Author1 wonders if they die where would that leave a hypothetical child?  Author2 feels that no matter what someone suffers, they would not have been better off if their mother would have killed them.  A person deserves those good times to enjoy.  Author2 argues that should people in poor nations kill their babies because they will grow up poor and hungry. Author2 states if Author1 dies, the child would still be alive and adopted.


----------
D3
----------
Author1 descries being very ill as a child, with many different serious maladies.   She questions whether a kind and just God would allow such a life.  She says she will likely start having heart attacks before the age of 30, and asks where that would leave her hypothetical child.  Author2 replies it would leave the child alive, which is better than dead.  The child could be adopted, and live a life.  Author2 says that a life of suffering has good times, too, which make up for the bad.  It isn't right to deny a person life because of potential suffering.  It is better to allow the child a chance at life than to kill it.


----------
D4
----------
Author1 thinks Author2's God makes girls sick from the age of two. Author1 talks about what all she went through as a child. She thinks Author2's God makes it okay to insult her beliefs. She doesn't have the beliefs Author2 does, but doesn't insult Author2. Author2 asks if Author1 would prefer to be dead considering all the the suffering and trauma she went through. Author2 asks if Author1 can say it's better to kill a baby rather than take a chance it will suffer. Author1 says Author2 is taking it too far. Author2 thinks that Author1 is saying she would kill her baby so it wouldn't have asthma. Author1 refuses to argue. Asks where the child would be if she dies young. Author2 replies alive.

