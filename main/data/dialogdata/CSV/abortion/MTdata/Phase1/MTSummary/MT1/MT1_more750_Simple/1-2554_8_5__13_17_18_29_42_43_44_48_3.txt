
----------
D0
----------
Author1 argues that pregnancy can be good. Pregnancy can also be bad. Pro lifers do not always think pregnancy is good. Pro lifers wonder why pregnancy is viewed as bad. Not all pregnancies are hard to go through. Some pregnancies are just good things and are just happy. Unfounded generalizations are not good things. Healthy safe babies and mothers are good things. There is a risk in being pregnant. There is also risk in walking down the street. It is better to be pregnant and not need or have an abortion. If the mother is in danger an abortion is okay. Abortions should not be used when the mothers life is not in danger.

Author2 argues that pregnancies are typically wonderful. Pregnancies are not often wonderful though. People who are not having wonderful pregnancies should be able to end them. Bringing a child into the world is not wonderful. It is too dangerous to say that they are always good. Risk to the mother should be able to be escaped from.


----------
D1
----------
Author1 doesn't remember hearing a pro-lifer say that pregnancy was the best thing in the world. He sees pro-lifers wondering why pregnancy is portrayed as horrible by people who describe it as a life threatening condition. It may be for some. It isn't true of all. Author2 says it is not wonderful for everyone. Author1 says a healthy baby being born is wonderful as long as the mother is safe too. Author2 says children are not wonderful for everyone. You can't speak or decide for every woman. Just because you survive pregnancy doesn't mean it doesn't harm your body. Author1 says the same is true of walking on a sidewalk. Author2 says if you don't want the risk you shouldn't have to take it. He thinks Author1 is trying to force risk on people. Author2 asks if he should be able to kill another driver if he was driving towards him. Author1 says if the mother's life is in danger abortion should be allowed. Author2 thinks this means he would kill another to avoid risk.


----------
D2
----------
Author1 feels abortion is wrong no matter what the situation.  Pregnancy is not a horrible experience.  Labeling pregnancy as a horrible experience is like calling it terminal cancer.  Risky pregnancies are less prevalent in this day and age.  A risky pregnancy is less likely than a car jumping a curb and killing someone walking on the sidewalk.  A birth is wonderful when both the baby and mother are healthy.

Author2 is Pro-choice and feels women should be able to choose.  Some women do have bad experiences during pregnancy.  A woman should be able to choose not to participate in a pregnancy which could put her life in danger.  If comparing risks during pregnancy to cars jumping curbs, should people not be allowed to walk on sidewalks to avoid the danger?  Pregnancy, even without added risks, is detrimental to a woman's body.  It should be a woman's choice whether or not to move forward with a pregnancy.  A mother at risk should be able to choose.


----------
D3
----------
Author1 wonders why pregnancy is generally portrayed so horrible. As "unbelievably hard" or "life threatening". His wife's experience was the opposite. He thinks a healthy baby is wonderful as long as the mother is safe too.  He thinks pregnancy is about as risky as walking on the sidewalk in this day and age. He also believes abortions where the mother's life is at risk should be allowed.

Author2 thinks that if a woman doesn't want the opportunity to be pregnant then it is not wonderful to her. He states that pregnancy is always a physical detriment to the woman's body. He says neither he nor Author1 can speak for how the pregnancy is perceived by the woman. He compares pregnancy with being hit by a car. "We don't know how badly the car will hurt you, much like in pregnancy we have no way of knowing how much it will harm the woman."


----------
D4
----------
Author1 asked why pro-choice people describe pregnancy as difficult or life threatening. They should describe it as "for some women." His wife told him it was not hard to go through. For some women it is not a difficult thing. Having a healthy baby is a wonderful thing. He does agree in one instance women should be allowed to have abortions. If their health is in danger it is okay.


Author2 says it is not always a wonderful thing to be pregnant. Some women do not want to be. Author1 shouldn't be able to make decisions for all women. Pregnancy causes physical harm to a woman's body. It may be a small harm or a big one. But there is always some kind of harm for all women. He says no one knows what risk each woman faces. . He says neither of them are women. So they can't say what women feel about it.

