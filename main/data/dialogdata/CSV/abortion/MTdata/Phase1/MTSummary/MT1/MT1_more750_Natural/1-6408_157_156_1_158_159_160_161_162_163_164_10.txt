
----------
D0
----------
Author1 says Author2 stated he would force a pregnancy to term. This means they both use force. Author2 was talking about abortion in the early trimesters being a responsible answer to unwanted pregnancy and it being an advantage of letting the woman choose whereas Author1 would force her. Author1 thinks this is a double standard. Author2 thinks he gave an acceptable compromise and his view is being misrepresented. Author1 replies he isn't misrepresenting it. Author2 said he would use force after claiming that the use of force was wrong. He thinks this is a double standard. Author2 states he said Author1 was wrong for using force in that situation, but that doesn't mean that it is wrong to use it in any situation. He also didn't claim it was alright when he did it, but not when others did. He doesn't see any hope of resolving the issue, so he thinks it is best to drop it and move on to other issues that aren't inane. Author1 asks how he was wrong in his argument.


----------
D1
----------
Author1 argues that, in order to make an argument that using force is wrong, you can't make the argument in the other direction too: that your use of force is right. Double standards are bad and should not be allowed in a debate. Force cannot be good for one person's argument and bad for another persons argument. 
Author2 argues that abortion in the early trimesters is acceptable, and that is good because a woman can then make choices about her body and that is a good thing. The use of force by the government, in order to stop a woman from being able to make choices about her body, especially concerning abortions. The government should not be able to stop a woman from choosing what to do with her body. Force is no wrong in all instances, though, and sometimes force is appropriate. There are times when force is justifiable in order to keep order and protect the typical life of people.


----------
D2
----------
Author1 said that Author2 said he would force someone to continue pregnancy later in term, yet said Author1 would be wrong to force someone to pregnancy in earlier terms. Author1 said saying Author1 is wrong to force someone then is wrong. Author1 continues to argue that use of force in one situation means use of force in another isn't wrong. 

Author2 clarifies that abortion in early term is a responsible solution for unwanted pregnancy, and gives the woman the choice. He would allow abortion to be outlawed in the third trimester. Author2 assumed that just because he said force was wrong early on, that he meant it is wrong to force someone. He feels force has to be justified through law. He supports freedom unless there are justifications for the use of force. If Author1 could show justification for force in early trimesters, he would be willing to change his view. Author1 only has his views, not justification, for use of force. They both have to have justification for their uses of force.


----------
D3
----------
Author1 starts out by pointing out how Author2 uses force in their argument and then poses the question of why did Author2 bother to state how Author1 uses "force" if not to state it was wrong. Author2 believes abortion to be a viable option for dealing with an unwanted pregnancy especially in the first trimester, but in the third in which case a pregnancy should be forced. Author1 exposes Author2's double standard on the use of force in comparison between a forced birth at term vs. a termination of the pregnancy. It is at this point where Author2 starts to reject continuing the debate, to which Author1 restates Author2's argument in the form of a double standard. Author2 continues to reply to these jabs claiming that they want their argument to be properly understood before ending the discussion. Author1 continues to defend their view of the pace of the debate up to this point and Author2 concludes that their debate has degenerated into a bias argument of semantics and they will agree to disagree.


----------
D4
----------
Author1 claims to have been condemned by Author2 for wanting to use force to make women carry their fetuses to term, and yet Author2 supports such force for women in their third trimester.  Author1 calls this a double-standard, where force is acceptable for Author2, but not Author1.  Author1 does not agree that he misinterpreted anything, or jumped to any conclusions.  He says that Author2 has not given any other argument for why Author1 is wrong, but is now acknowledging that the use of force is acceptable, as a way to make a woman carry to term.

Author2 says that his views are being misrepresented, and that he did not argue that it was always wrong to force a woman to complete a pregnancy.  Any such force would have to have good justification, which he believes would not apply to early pregnancies.  He suggests they focus on what justifications there might be to force a woman to complete her pregnancy, and not on the issue of force being the main difference.

