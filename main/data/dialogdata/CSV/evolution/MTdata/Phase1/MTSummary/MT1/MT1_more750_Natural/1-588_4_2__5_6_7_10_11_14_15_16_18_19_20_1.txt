
----------
D0
----------
Author1 states that their argument hinges on proof found in a source material which Author2 already considers invalid, which is not a fair way to approach the debate. Author2 elaborates that the referenced source material is a work of fantasy and not science, so their point is valid. Author1 and Author2 come to an agreement on the flawed nature of their debate but decide to press on. Author1 restates their first point, attempting to bring an understanding to the grounds of this debate that the bible won't be allowed as a scientific source for their argument, thus a bias is already in place. Author2 agrees to that point and requests evidence based on empirical scientific evidence. Author1 points out how the bible has been used in the past as witness testimony and a historical document, which gives it a shred of scientific validity. Author2 points out that none of the authors were actually a witness to any of the events they were writing about. Author1's rebuttal is that God was the witness and by-proxy author.


----------
D1
----------
Author1 believes that the evidence for creation is in the Bible. People who believe in evolution will not accept that as evidence. We need a common ground for understanding. The Bible is testimony. Historians use the Bible as an historical document. God inspired the Bible's books. He rejects the other writer's description of a testimony from God because the writer is an atheist. Science only deals with a limited part of the universe.

Author2 states that the Bible is not scientific evidence. The Bible is scripture, not empirical evidence. He asks which author of the Bible was alive during the creation of the world. He believes that the theory of creation is unverifiable. One could claim God to be the author of any text. He gives an example of how a theory of creation can be created. A turtle creates the Earth by laying an egg. All of the organisms of the world instantly grow on the earth. This is comparable to eyewitness testimony from God. Turtleism destroys the theory of both evolution and creationism.


----------
D2
----------
Author1 claims he can't discuss creationism and is forced to discuss evolution because the other commenters dismiss his evidence from the Bible as fiction. Author1 claims he is unfairly forced to debate with only what evidence Author1 and the other commenters decide is acceptable. He questions why swearing on a Bible in court is considered evidence of truth. Historians often use it as history notes. So it is accepted as evidence by courts and historians. He said God was there when creation began, and God told the authors of the Bible what to write. 


Author2 advises that Author1 will have to find other evidence besides the Bible because it is not considered scientific. Author2 said Author1 basically just admitted there isn't any scientific evidence of creationism. He said scientific evidence is what commenters want from creationists. He points out none of the authors of the books in the Bible were alive when creation happened, so they are just telling stories of what they think or were told by others.


----------
D3
----------
Author1 believes the evidence for creation is in the Bible and we need a common ground for understanding. Author2 says the Bible is not scientific evidence. To support some Theory of Creation, you need to do it without the Bible. Author1 expected this and says he's forced to point out all of the flaws in there argument. Author2 sees this as admitting there's no scientific evidence of any theory of creation. Author1 replies because Author2 doesn't accept the Bible, he must use evidence that he does accept and Author2 agrees with this. Author1 sees the Bible as testimony and Author2 doesn't agree. Author2 thinks he's shown there's nothing to back up Author1's argument but Author1 says all Author2 has done is state that he doesn't accept the evidence which brings them back to square one. Author1 says science deals with a limited part of the universe and science can't explain everything. He asks if other planets were there before we discovered them, if so, there are things that are before they are, or were scientific.


----------
D4
----------
Author1 states that the evidence for creation is in the bible, which the "evolutionaries" don't accept as evidence.  This is why creationists point out flaws in the evidence for evolution rather than giving evidence in favor of creationism.  The bible is the eyewitness testimony of God.  Things exist that are beyond science's capacity to describe.  Evolutionists fail to understand the big picture, and therefore, creationists limit themselves to discussing the limited part of the universe that science deals with in debates like these.  Things ARE before they ARE SCIENTIFIC (by which Author1 means before they are discovered by science).

Author2 confirms that the bible cannot be used as scientific evidence.  The bible is scripture, and not empirical evidence.  The authors of the bible were not present during the creation of life on Earth.  Anyone can claim that any text was authored by God.  Author2 has not denied any creationist evidence -- none has been offered.

