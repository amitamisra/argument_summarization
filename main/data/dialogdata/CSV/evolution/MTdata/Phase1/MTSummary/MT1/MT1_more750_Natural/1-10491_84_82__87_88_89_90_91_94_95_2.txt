
----------
D0
----------
Author1 believes that God is all powerful and eternal, and has no limitations.  God cannot cease to exist, in the same way infinity can not cease to exist.  This is not a limitation of His power, but rather an illustration of the limitations of language.  To think there could be a limit on infinity indicates a lack of comprehension of the concept of infinity.  Language is an imperfect medium.  When the bible says it is impossible for God to cease to exist, it does not mean He's limited in what He can do, but rather that the concept of not existing isn't applicable.

Author2 argues that If something is "impossible" for God to do, it means He is limited.  He is incapable of something, and therefore not omnipotent.  Author1 doesn't understand how language works if he thinks "impossible" doesn't mean absolutely impossible.  Impossible isn't the same as "chooses not to."  If God just chooses not to lie, then the bible is lying by saying it's impossible.


----------
D1
----------
Author1 argues that god is eternal and all powerful, but not in just the way that the words mean. God cannot simply cease to exist, because he is more than just the words. Scriptures define what god is, but the infinite cannot be limited, cease to exist, or otherwise end. The language in the scripture is more than just language, but rather it is the word of god and extends past our language. An infinite being cannot cease to exist, because that is not something than an infinite being would do. Language is an imperfect medium, and the promises that a deity makes is essentially permanent and forever. Omnipotent beings do not have limitations because of a literal meaning of one word in English. 
Author2 argues that god is limited as defined by the bible. He cannot cease to exist, look upon evil, change, or stop existing. God is either lying or not telling the truth, and either way that is wrong. The bible puts limitations on god. The bible is clear.


----------
D2
----------
Author1 claims the translations don't show God is limited because he can't do something, but because he has promised to do something. Just like infinite never ends, so does God. He claimed infinity was limiting because it couldn't stop existing. Author2 is limited in understanding because of his logic and limited language. He claims impossible doesn't mean that God can't if he wanted, but because of his promise he never would. 

Author2 said he showed God is limited: he can't lie, look at evil, change, or stop existing. He berates Author1 for using the excuse someone doesn't understand when they disagree with him. He accuses him of twisting biblical quotes that disprove his stance. Infinity can't end so it is limited. If something is an eternal being, of course it can't stop existing. The quote says it is impossible for God to lie, and impossible means he is incapable of doing it. Author2 is ignoring the definition, and admitting he isn't viewing the word as totally impossible, which then contradicts the meaning.


----------
D3
----------
Author1:1- says gods ability isn't limited. God cannot cease to exist in the same way that infinite cannot cease to exist. If you think that this is a limitation on infinite then you do not understand or comprehend infinite. Author2 replies Author1 asked where the Bible limited God, and he showed him that it stated that God can't lie, can't look upon evil, can't change, and can't cease to exist. Author1 is providing anything to back up his claim. Also, infinity is limited in the sense that it can not end. Author1 says something that is infinite cannot be seen as limited because it cannot cease to exist and Author2 is using scriptures that don't back him up. Author2 replies an eternal being can't cease to exist. If it was possible for him to cease to exist, he wouldn't be eternal. And since an inability to do something is a limitation, his point is proven. Author1 doesn't see this as a limitation and thinks Author2 simply doesn't understand and is too literal. Author2 doesn't agree.


----------
D4
----------
Author2 is arguing that the God of the Bible is limited, i.e. not omnipotent. Author2 points to examples in the Bible of certain descriptions of God that would imply limitation: that God can't cease to exist, can't change, can't lie and so on. In Author2's view, God is limited by being unable to do these things.

Author1 believes that Author2 is looking at the issue from an incorrect perspective. Author1's position is that God is an infinite being and is capable of anything God wills. Author2 argues that the Bible uses certain language so that God can be better understood in human terms, but that should not be taken as limiting God in any way. If he 'cannot' cease to exist, that's because infinity is incompatible with the concept of nonexistence. If God 'cannot' lie, that is representative of his promise not to lie to mankind.

