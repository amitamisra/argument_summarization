
----------
D0
----------
Author1 would like to know how many origin of life theories there are. Author2 states that there is one, which is a spin off of Darwinism that affects non-biological substances. Author1 doesn't believe Author2 knows the answer to their question. Author2 is confused about which question Author1 wanted answered. Author2 is also pointing out how easy it is to make the connection between extreme leftist ideals and religious ideals based on their inability to logically defend their own mistakes. There is then a brief back and forth between Author2 and Author1 where they are confused about which subject they are actually having a dialogue about. Author2 finalizes the dialogue by explaining how the number of amino acids given a simple protein. Then Author2 goes on to state that base pair mutations are not more significant than smaller mutations because the odds for novelty doesn't increase. Author2 also doesn't think Author1 should be surprised of the defensive status of the argument when trying to challenge the theory of his God.


----------
D1
----------
Author1 states that we do not know how life on Earth began.  There is more than one theory that has been proposed to explain life.  Author1 asks whether Author2 believes the scientific community has only one theory to explain the emergence of life.  Author1 does not know how many scientific theories on the topic there are.

Author2 states that all theories for how life began are variations on Darwinism.  Leftists and religious people are similar, in that they don't feel the need to defend their facts.  The real reason they don't defend them is because they don't have the ability.  This is a primitive way of saying "it's a matter of faith."  It is impossible that life formed from chemicals through random chance, because of the astronomical odds against it.  Author1 claims that these odds are invalid because there are many different ways that life could have been formed.  This kind of reasoning is called "an appeal to infinite odds."


----------
D2
----------
Author1 says we don't know how life began. There are many theories. He asks if Author2 thinks there is only one scientific theory. Author2 says there is only one theory, a modification of Darwinism. It's pathetic to graft natural selection and mutation to non-biological, non-reproducing situations. Author1 thinks the correct answer is I don't know. Author2 replies it's arrogant to dismiss an argument based on personal authority. Author2 thought he was replying to how many types of theories he thinks there are. Author1 says he was wrong. Author2 says the only thing he calculated was lower limit on the odds that any particular 20 amino acids are coded for randomly, and he was correct. He thinks Author1 is arguing there was lots of way for life to form, so the calculation are false. That is ignorant. Author1 asks what Author2 thinks he's wrong about. Author2 replies it's a fallacy as his point is about where information is coming from and Author1 brought up the 20 amino acid number. Involving mutations, odds for novelty don't increase.


----------
D3
----------
Author1 states that it is unknown how life began, and asked if Author2 believes the one scientific theory, or believes there are more theories. He thinks Author2 should just say he doesn't know. He shouldn't try to answer things if he doesn't know. 

Author2 said their is only one type of theory which is based on Darwinism. He berates Author1 for being dismissive of arguments based on  his own superior feelings. He said the question was if there's more than one theory, and he does know the answer and said so. He explains he calculated the odds of amino acids coding randomly. He supports another person's argument that it's impossible for life to form randomly based on these chances of randomness. Author1 is being illogical by saying there are many ways for life to form. While Author1 may see it as a debatable theory, the odds make that an unlikely theory. Author1 brought up the 20 amino acids. There are bigger mutations that have occurred, but the odds for novelty don't increase.


----------
D4
----------
Author1 and Author2 are discussing when life first began.  Author1 feels there is virtually no way to know the true answer because there are too many different scientific probabilities to arrive at one.  He poses the question to Author2 as to how many scientific probabilities he thinks there are.  He then advises his opinion is Author2 is wrong in his answer.  He feels the answer should be, "I don't know."

Author2 feels the only "type" theory is a modification of Darwinism.  He does not support this theory; however, and compares it to something developing on the backs of crystals, which was a running joke in his family when something couldn't be proven or explained.  Author2 feels Author1's reluctance to accept his answer and feeling he should have answered, "I don't know," is as good as saying "It's a matter of faith."  He also advises he answered the question correctly because he was asked what he believes and his answer is what he believes.  He advises Author1 is incorrect.

