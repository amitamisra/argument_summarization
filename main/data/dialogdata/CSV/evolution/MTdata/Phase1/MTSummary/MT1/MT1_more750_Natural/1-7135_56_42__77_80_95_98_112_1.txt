
----------
D0
----------
Author1 argues that evidence which contradicts evolution is the worst thing to an atheist. Atheists have a set of beliefs that is similar among all of them. Atheists want to have backing for their beliefs. Atheists from before Darwin didn't have this, and they don't exist today. The observations that those previous atheists based their beliefs on are not valid reasons for arguments, and they are no longer known. Atheists are based primarily on Darwinism. This is evidenced in how quickly atheists are annoyed by Christians. The original teachings of Christ are not atheist.
Author2 argues that all atheists are not based on the darwinian beliefs. Atheists are not sharing one specific set of beliefs. All you need is to not believe in any god. There were many atheists who existed before Darwin and his theory of evolution. There were atheists even during ancient civilizations. When Christ gave his teachings, the disciples are the ones who added in more and more elements that made it into a religion, not Christ himself.


----------
D1
----------
Author1 is discussing atheist talking points. He said types of previous atheists before Darwin were probably illiterate. Today most people can read and would look for validity of their beliefs. He mentions how Christians easily recognize a fellow Christian. He questions why these observations against religion aren't popular talking points today, and said he's never heard them. 


Author2 questions it being Darwinian evolution. He claims this isn't true because there were atheists centuries before Darwin. Not believing in God doesn't mean you have to believe in something else. He cites that Darwin wasn't published until 1850, and Thomas Paine was a famous atheist in the 1700s. Atheists back then based their beliefs on observations, and science didn't allow for non-material explanations. Another famous atheist, Paley, spoke before Darwin's time. Thus, atheism is an older theory that spans time and civilizations, so clearly can't be attributed to Darwin. He said there are Christian atheists who reject the Roman church added theology to Christ's teachings and embrace the original philosophy.


----------
D2
----------
Author1 thinks it is unpleasant for atheists to have Charles Darwin's work challenged, believing that his work is central to the atheist belief structure. A point which Author2 corrects by saying that there were many Atheists before Darwin published his work on evolution, that the two are not linked. Author1 reiterates that the atheists that existed before Darwin were most likely illiterate and likens them to simple minded animals. Author2's rebuttal to the jab is simply to listing off a few prominent, well read, atheists that predate Darwin. One such atheist was Thomas Paine who wrote 'The Age of Reason'. Author1 concedes that point but interjects that none of the talking points of atheist today seem to predate Darwin's work. Author2 suggests that the Roman Church interjected many of their views into the teachings of Christ, like the authoritarian structure and ethics. Author2 also makes the claim that Christ originally was an atheist and as were his teachings. Author1 doesn't believe that point and requests some proof via reference.


----------
D3
----------
Author1 believes in creation over evolution.  He feels all atheists follow Darwin's teachings of evolutionary theory.  Author1 disagrees with Author2's statement of pre-Darwin atheists believing as they did due to empirical observations.  He feels these are not used as talking points by modern day atheists, so they are not relevant.  He feels today's atheists cite Darwin more than any other source for their reasons for not believing in Gods.  Author1 challenges Author2 to cite a link to prove the original teachings were atheistic.  

Author2 advises atheism has been a practice since before Darwinian Evolution was a theory.  He feels it is quite possible to be an atheist without being and evolutionist.  He uses Thomas Paine as an example of one of these pre-Darwin atheists.  Author2 advises when the Disciples of Christ were dispersed to foreign lands, the original teachings of Christ were laden with theological elements and it was nearly impossible to locate the original lessons therein.  He feels the original teachings were atheist in origin.


----------
D4
----------
Author1 thinks for atheists, the most unpleasant and unwelcome thing imaginable is for evidence to be presented that contradicts their beliefs. They believe in darwinian evolution as the SS believes in the Bible. Author2 says there were atheists before Darwin. Also, we don't know the religious beliefs of SS. Author1 replies atheists, any person, will want to have some backing for their beliefs. The atheists that existed before Darwin didn't have anything. Author2 says they were convinced on the basis of many many empirical observations. Science was a well organized enterprise before Darwin and it did not admit of non-material explanations well before his time. Author1 asks why those points aren't more prominent today. He has a hard time being convinced of the teachings of Christ and characteristics of God. Author2 says the addition of OT concepts and Roman concepts has made it hard to find the original teachings. Christian atheists get back to the basics and true teachings of Christ. Author1 asks if the original teachings of Christ were atheist in their context.

