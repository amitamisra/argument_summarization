
----------
D0
----------
Author1 asks where did the designer came from and if Author2 understands that chemical reactions would go through a natural selection processes and with a filter, rules, and some random variation, you can develop complexity very easily without design. Author2 asks why the creator needs a creator and where DNA comes from. Author1 says because they are discussing origins and DNA comes from RNA. Author1 replies God is important because they are discussing origins and asks where the information comes from, as DNA comes from RNA doesn't cut it. Author2 says it has been shown that complex organic chemicals spontaniously organise themselves in the proper environment. Author1 thinks Author1 is assuming DNA is a language and it's not, also the path to DNA didn't happen all at once. He includes a link to protocells. Author2 states cell division is a complex cellular process and asks how a simpler cell division mechanism functioned, and how did it lead to the process today. He asks if any remnants are evident today and if it's irreducibly complex.


----------
D1
----------
Author1 is a believer of evolution and natural selection.  He advises chemistry can happen under the right circumstances creating complex organisms, even without design.  Author1 advises DNA is developed from RNA and this is an established fact.  He feels people tend to develop the thought, "God did it," when something cannot be explained.  Author1 advises there are scientific studies noting DNA internally carries a language of three billion genetic letters.  

Author2 is a believer of creation.  He wonders why the standard question seems to revolve around the creator and why folks seem to think the creator needs a creator.  He feels knowing where the creator came from is irrelevant to the origin of life.  Author2 elaborates advising the discussion pertains to the origin of life on earth and this is the reason God's origin is not relevant.  Author2 disagrees with Author1's statement regarding genetic letters advising it is merely chemistry.  He offers a website to back up his argument DNA path changes not being instantaneous.


----------
D2
----------
Author1 would like to know where the creator comes into the evolution equation, proposing that evolution is the sum of its parts sans a creator. Author2 doesn't see the point in questioning where the creator came from. Author2 then poses the question of where did the structure of DNA come from. Author1 explains how DNA comes from RNA which is a well known process. Author2 elaborates how the discussion is on origins. Claiming that the organization of these amino acids isn't just by chance. Author1 explains how there is good science to back up how these structures happen naturally without intervention. Author2 refutes that claim stating the opposite. Then goes on to claim that there is a hidden language in the "human DNA molecule". Author1 then links some scientific evidence to support their argument. Author2 claims Author1 is going backwards, stating that they had previously discusses how simple cell division is different from the why our cells work today. Author2 poses the questions to Author1 whether or not our current cells show the previous method.


----------
D3
----------
Author1 is arguing that the origin of life is easily explained through the mechanism in which organic molecules seem to organize themselves naturally. Amino acids being formed naturally, and the development of protocells show that increasingly complex molecules tend to be able to organize and interact with each other in increasing more complex ways as their own complexity increases. Author1 posits that through a logical extrapolation of given data, it is easy to see how the spontaneous formation of amino acids could eventually lead to single cell organisms being formed.

Author2 is not convinced by this. Author2 claims that while it can be seen that these complex organic molecules do form naturally and eventually proto and simple cells may form, it can not be shown how early cell-like structures developed the mechanisms required to divide (reproductive capacity being an integral definition of 'life'). Author2 seems to be proposing that intelligent design is a valid theory for how these seemingly lifeless assortments of organic molecules have been able to organize themselves into things that are considered alive.


----------
D4
----------
Author1 questions what Author2's basis is, and where would the creator come from then. He asked if Author2 admits to chemistry, and says chemistry reactions with filters, rules, and variations could create a complex thing without any design.  DNA comes from RNA in a definitive design. Lab demonstrations have shown in the right conditions chemicals organizing themselves. He argues DNA is chemistry, not a language, and did not happen all at the same time. 



Author2 asked why a creator would need someone to create it, and it's origin doesn't matter.to the origin of life debate. He questioned what DNA's origin was, and even if it was a chemical reaction, where did the design come from. The subject is origins of life, and although Author1's theory isn't impossible, the DNA comment isn't really possible. Even if it was an accident, where did all the information in the cell come from? He disagrees lab experiments have demonstrated that premise. Scientists studying DNA have discovered it contains a complex language of three billion genetic letters.

