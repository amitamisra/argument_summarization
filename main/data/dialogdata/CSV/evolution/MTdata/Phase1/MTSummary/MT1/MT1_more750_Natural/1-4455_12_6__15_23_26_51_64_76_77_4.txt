
----------
D0
----------
Author1 questions what scientific breakthroughs have come from evolution. He's curious because Dr. Larson was unhappy a non-scientific theory like ID was compared to a theory that's accomplished so many breakthroughs in science. He said creationism also presents an understanding of the origin of life, so it's similar to evolution, but Dr. Larson claimed it was different in that way. He surmises they have different views on what the doctor is referring to. It is important for evolution to be scientific. 


Author2 denies it matters about breakthroughs, because evolution provides explanations. He speculates Dr. Larson was referring to the scientific method, not evolution, when he made that statement. But even if evolution only provided understanding of life origins, that would still be an accomplishment. He claims the two philosophies are different because creationism is based on a belief with no supporting physical evidence, and evolution is based on observation and interpretation of physical evidence. He disagrees evolution has to be proven scientific because it is a product of the scientific process.


----------
D1
----------
Author1 agrees that they need to do more research before broaching the subject of evolution in more detail. Author1 also believes the referenced clip of Dr. Larson speaking to be on the subject of evolution, which he attributes to many medicinal breakthroughs in recent years. Author2 has a differing opinion on that subject, believing Dr. Larson was referring to the scientific method, not evolution specifically when speaking in the clip. Author1 goes on to state that there are many theories that similarly explain life over time and space, evolution being the one that can be proven scientifically. Author2 would like to scientifically challenge evolution, as would be right by the scientific method. Author2 would like to point out that the clear separation in evolution vs faith based interpretations is their scientific relevance. Finally Author1 would like to know Author2's opinion on whether or not speciation is reversible. There was an example of speciation given which was the field mouse vs. the house mouse. Author1 would like to know if those two could breed and bear offspring.


----------
D2
----------
Author1 says Mendel didn't predate Darwin at all. He asks what scientific breakthrough evolution has given us. Author2 thinks evolution is worthy knowledge alone as an explaination of the history of change in life on this planet. Author1 asks because Dr. Larson was upset the theory was put on the same level as non-scientific theories. He was the one that emphasized this. Author2 isn't convinced Larson was attributing all the breakthroughs in medicine and biotechnology over the past hundred years to evolution but more too the scientific method as a whole. In this regard he is correct to be upset. Author1 doesn't agree. Author2 doesn't think that it's unreasonable to assert that Dr Larson was refering to the scientific method as a whole as the source of the breakthoughs he gives. Author1 thought he was speaking about evolution, but could be wrong. It's important evolution proves to be scientific. Author2 says it's part of the scientific process. It's different from faith. Author1 thinks that theory should be proven and he is curious if speciation is reversable.


----------
D3
----------
Author1 argues that there have been very few, if any, scientific breakthroughs due to the understanding of the evolutionary process. It has only led towards cruelty against animals, specifically medical testing on mice and monkeys. Why should evolutionary theory not be tested and confirmed that it works, the same way that all faith based theories are tested and argued over? Evolution does not seem to be any different from a theological view of the universe, as both provide an understanding of life over time and space. A theory that doesn't need to be proven is strange and unusual. 
Author2 argues that evolution is worth knowing just for the understanding of how history and change. Evolution does not need to be proven as a scientific theory, because it was made from the scientific method in the first place. Dr. Larson was referring to the scientific method, not the theory of evolution, when he stated that was the source of the breakthroughs. Evolution is inherently a part of the scientific method.


----------
D4
----------
Author1 is taking a skeptical position on whether the theory of evolution is scientific. He claims that in a video clip, a scientist states that evolution is responsible for "all of the breakthroughs in medicine and biotechnology. . .over the past hundred years." If so, what are those breakthroughs? Author1 disdains the idea that evolution can be scientific merely for giving an explanation of the history of life on Earth, as mystical systems make the same claims, and many of those are necessarily wrong. For evolution to be scientifically valid, it must be responsible for specific advances.

Author2 disagrees with Author1's fundamental premise. Author2 states that the scientist quoted above was referencing the scientific method generally, not evolution specifically, and was lamenting that non-scientific approaches (such as intelligent design) were being given equal treatment. Author2 contends that evolution's explanation of the development of life on Earth is sufficiently impressive, regardless of specific advances. Evolution is scientific because it is the product of the scientific method. Systems based on faith or mysticism are not.

