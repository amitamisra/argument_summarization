
----------
D0
----------
Author1 originally refers to a previous point in which science disregards the possibility for a creator. Author2 claims Author1 will neither listen to, nor make substantial argument against, any of the points that have stated. Author2 then puts the responsibility on the elected shoulders to educate themselves in order to make decisions. Author1 summarizes the study presented originally by saying it removes the possibility of there being a creator. Author2 states that filling the gaps in the theory isn't an attempt to disprove those who need those gaps to make their beliefs work, its just good science. Author1 suggests they have different definitions of simple logic, and then accuses Author2 of waiting to answer their own questions instead of conducting themselves in a debating manner. Author2 states that their definition of simple logic is based in reality. Then Author2 goes on to explain that they have to answer their own questions because Author1 refuses to answer anything, only responding in thinly-veiled "ad homoninem" attacks, which should be clear to everyone reading by this point.


----------
D1
----------
Author1 appears to be a believer in creationism.  He feels science is presupposed to there not being a God, or in his words, creator.  He believes the belief in a scientific theory of evolution is being naturalistic.  Author1 does not really discuss much in the way of the forum topic.

Author2 does not specify his beliefs; however, he does make reference to Kenneth Miller's book.  Author2 feels the reference to science's trying to close gaps as being a way to disprove the existence of a higher power is false.  He believes those who believe in a higher power use those gaps to support the existence.  Author2 feels Author1 is closed minded to the opinion of those who believe in science.  Author2 feels much of the confusion regarding these topics are a result of a lack of knowledge of the public.  He feels more of the responsibility belongs to the politicians as the public elects them with the assumption they will educate themselves on the issues at hand.


----------
D2
----------
Author2 believes through the reading of Dr. Kenneth Miller's book Finding Darwin's God? will prove the existence of a creator or higher power. Scientific study closing gaps does not disprove the existence of a higher power. People pretend to be dumb when it comes to certain issues and we would rather elect representatives who educate themselves on these issues in order to make the decision for us as to how we should think. It is due to willful ignorance and close-mindedness as to why we believe scientific research is out to close gaps in order to prove that there is no existence of a creator or higher power. 
Author1 contends that science presupposes no existence of a creator in its quest to make all of their discoveries. He refutes that Dr. Kenneth Miller's book Finding Darwin's God is an attempt for scientific studies to prove in the existence of no higher power or creator by closing gaps in the fossil records. Simple logic presupposes the non existence of neither a higher power nor creator.


----------
D3
----------
Author1 says science presupposes no creator in it's naturalistic quest to make all its discoveries. Author2 thinks Author1 refuses to give any weight to his points, even when unable to provide any reason not to. He says, just because you don't like certain facts doesn't make them false. He questions Tiktaalik being a victory for atheism and says to ask Catholics what they think of the matter. Regarding certain issues, you elect representatives to educate themselves on the matter and make the decision for you. The U.S. is not a democracy, it's a republic. Author1 replies scientific study attempts to close gaps in the fossil record to show evidence of no God. Simple logic tells us that this study presupposed no creator. He thinks Author2 is showing himself to be liberal by discussing politics. Author2 says filling gaps isn't an attempt to disprove God. He thinks Author1 is being closed-minded and thinks evolution equals atheism. Author1 says they have different points of logic. Author2 thinks his definition conforms to reality and that Author1 never does.


----------
D4
----------
Author1 mocks science for making assumptions that there is no creator. He points out the post states the scientific study said it was trying to show no higher power by eliminating gaps in fossil records. He speculates Author2 is a liberal. He said they differ about definitions of logic. 

Author2 said Author1 refuses to consider points he makes. He recommends Dr. Miller's book. He said just because Author1 doesn't like certain facts doesn't make them false. He discusses politics briefly. He said just because people don't know things that go on doesn't mean those that do are excused from not speaking up. He mentions a fish fossil found that could be an example of evolution. He said filling in gaps in records is not deliberately trying to disprove a higher power. He mocks that Author1 doesn't believe Miller really believes in some of the things he writes, and says Miller's beliefs refutes Author1's assertion. He said his definition agrees with reality, whereas Author1's doesn't, due to his lack of knowledge.

