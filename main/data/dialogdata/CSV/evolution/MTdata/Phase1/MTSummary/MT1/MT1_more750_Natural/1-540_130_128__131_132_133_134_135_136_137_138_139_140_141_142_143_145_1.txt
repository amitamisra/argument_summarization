
----------
D0
----------
Author1 contends that one weakness in evolutionary theory is the lack of consensus among experts in classifying fossils. Since evolutionists cannot unanimously agree on the evolutionary pattern of a species based on a sampling of skulls, then something must be wrong with the underlying theory. He also believes that some skulls considered by evolutionists to be transitional links between apes and humans are misclassified and are simply small human skulls. Because of theses inconsistencies, he believes evolutionary theory to be false and creationism to be correct.

Author2 refutes the assertion about evolutionary theory lacking consensus, pointing out the difficulty in constructing an evolutionary record with only a few sample fossils spanning hundreds of generations. He criticizes the creationist concept of "kinds," which divides life into distinct, unambiguous groups, and argues that even creationists cannot agree on which are human skulls and which are ape skulls. Since creationist theory states that humans and apes are different kinds, such ambiguity should not exist. This, he states, points to a major flaw in creationist thought.


----------
D1
----------
Author1 said different experts would all classify the skulls differently. He suggests some small skulls are just small humans. He questions what the definition of kind is. Author2 is the one who used the term, he just asked for a definition of it. He said Author2 assumes they are chimps, when he mentioned missing link, but doesn't think even that means man descended from chimps.  

Author2 said classifying the different skulls would be difficult if evolution were true because species would evolve and change over time. Scientists have limited numbers of only recent species to use for classifying. If creationism were true, then it shouldn't be difficult to classify things because there would only be one kind of each thing. Humans would be one kind and chimps another kind. How does that explain then the different skulls that had characteristics of both of them? The skulls can't even be agreed upon as to classification by creationists. He says size is not the only thing used in classification. Creationists use inconsistent definitions of kind.


----------
D2
----------
Author1 is not a believer of evolution.  He does not believe humans are descended from apes.  He does not feel the smaller skulls to which they are referring are not apes but in fact are smaller human skulls.  He does not believe there is such a thing as the "missing link."  Author1 questions the skulls' classification as they are not actually labeled as a "kind."  Author1 is unable to classify the skulls or find anything labeling them as a certain species.

Author2 is a believer of evolution.  He feels creationist experts are lacking in the fact they are unable to agree which sample skulls were human and which were apes.  Author2 feels the skulls which they are discussing are actually more primitive versions of human skulls showing ape-like qualities.  He feels this is proof that humans actually did evolve from apes.  Author2 feels the term "kind" is a creationist term used to debunk evolution by requesting every type be classified.  Author2 feels Author1 is avoiding questions.


----------
D3
----------
Author1 thinks a dozen experts would have a dozen different results. Author2 thinks the difficulty in seperating the various fossil skulls into species would be what you would expect if evolution had occurred. The evolution of an ancestral species into one or more species would be very confusing with only a few specimens from hundreds of generations. This sort of confusion shouldn't be possible if you accept creationism. Homo sapiens are one kind, chimpanzees are another kind. Despite different opinions from different experts, none of them would claim the Australopithecus africanus skulls are chimpanzee skulls, or that the Homo habilis skulls are modern human skulls. Author1 thinks links may just be small human skulls. Author2 thinks he is clueless. There is more to it than size. Author1 doesn't believe we came from apes, but some will think smaller skulls are primitive. Author2 asks which are classified wrong. Author2 thinks Author1 is mistaken about skulls and questions the definition of "kind," as creationists speak of humans being a kind. He asks again about skulls classified wrong.


----------
D4
----------
Author1 believes that scientists disagree if asked to work independently. Author2 believes that even working together, creationists disagree on t he same subject matter. The subject matter in this case being ancestral skull classification. Author1 proposes that perhaps the small skulls we think are ancestral are actually just small human skulls, as is possible in the range of size in humans. Author2 explains that there are many more characteristics at hand than mere size. Author1 was merely pointing out how many people who view smaller versions of the same skull would perceive it to be primitive. Author2 then references the source material and poses the question to Author1 which skulls are actually mis-classified. Author1 and Author2 then go into the topic of the definition of the word 'kind' and how it is used by creationists to describe the relationship of apes to humans. This derailment goes on for the rest of the dialogue; Author2 attempts to have his question about the skulls answered, and Author1 continues to accuse Author2 of ignoring the 'kind' topic.

