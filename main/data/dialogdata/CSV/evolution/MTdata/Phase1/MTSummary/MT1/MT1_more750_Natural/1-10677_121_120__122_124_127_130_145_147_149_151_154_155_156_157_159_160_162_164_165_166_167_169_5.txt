
----------
D0
----------
Author1 poses the question of whether something can evolve into a less advanced state. Author2 believes that genetic patters are at the heart of change and isolation is only a factor. Author1 poses the question of whether the blind river dolphin, which could once see and is now blind, is an example of an un-advancement. Author2 believes it is, stating that philosophically that loss could also be considered a gain. Author1 states that dolphins used to live on land but returned to sea, which Author2 agrees with but questions the amount of divergence required to be considered a new species. Author1 and Author2 then get into an argument of semantics on whether or not Author1 contradicts themselves with the statement. Author2 states that we have manipulated many genetic traits in animals but it is not considered evolution. Author1 concludes that evolution gives a good explanation for everything we observe. Author2 wants proof of that statement, which garners a response that it is common sense. Author1 believes it is not common sense and requires an answer.


----------
D1
----------
Two subjects are discussing the topic of evolution.  Author1 questions whether or not a species can un-advance.  As in evolve to a lesser form rather than a greater.  He uses the South Asian River Dolphin as an example.  These mammals used to be sighted, but over the years have devolved into blindness.  He poses the question if this is un-advancement.  Author1 agrees, this is a devolving of a species and advises it is due to the loss of their eco-system.  He also feels it could be due to a genetic defect.  Author1 notes dolphins used to live on land but evolved into sea living mammals and asks Author2 if he disagrees with this fact.  Author2 advises he cannot disagree with something which has been proven a fact.  He does; however, pose the question of whether or not they were a dolphin species during their time on land or if they were another species which created a hybrid which now lives in only water.


----------
D2
----------
Author1 asked if something can reverse advancement into a less advanced form. He cites the example  of South Asian river dolphins that have become blind. He questioned if Author2 believed dolphins used to live on land and evolved into sea creatures. He asked why in dog breeding you can select for certain traits but not others.

Author2 said that's happened, but it didn't evolve, and it's rare. He admits that is reversing advancement in order to survive to a changed environment, but doesn't bode well for the species if the environment changes in the future. He said it's also possible it was caused by genetic drift. He believes dolphins were a different species before. He cites the example of brown and polar bears which have been considered separate species, but is now questioned with the discovery of a hybrid. He said they weren't dolphins on land, and it wasn't evolution that was responsible. He said no one has tried to construct novel systems in dog breeding, they just manipulate current genetic engineering.


----------
D3
----------
Author1 argues that certain species are capable of losing extra systems that they no longer need. For example some species have lost their sight, some have returned from walking on land. The intelligence of dolphins, and their sonic systems, cannot be explained by evolution alone. Breeding dogs makes sense but larger changes cannot be explained by evolution. Evolution can support everything but we just don't understand. 
Author2 argues that species are now defined by separation, because that is not how the evolutionary theory works. Evolution allows for systems to be lost, when they are no longer needed.The loss of a system may in fact help species in the short term but hurt them in the long term. Different species evolve over time. Dolphins did not go from land to water, but rather a new species entirely. Formal logic is required to participate as debates. Intelligence takes a different path when species diverge. The odds of spontaneous generation of a new biological system are very low, and we can't simply breed dogs for new systems.


----------
D4
----------
Author1 asks if it's possible that something can evolve in to a less advanced form. Author2 says since isolation allows divergence we're cutting to the chase and defining species by isolation. That removes it further from any underlying genetic pattern. It obviously happened but it did not evolve. A tautology is not false because it is a tautology. Author1 asks about south Asian river dolphins which once had sight. Author2 asks if they were a different species before contact. Author1 sees this as an agreement but Author2 says they weren't dolphins when on land. Author1 questions if Author2 is saying the sonic system and intelligence of dolphins can't be explained by evolution and Author2 says it can't. Author1 questions how selection is possible but changes from lower to higher intelligence isn't. Author2 replies the scale involved is unimportant except to the odds of spontaneous generation. Author1 thinks evolution gives reasonable explanations to everything. Author2 asks for support for that and says you can't know the unsupportable. Author1 says we can believe in what we see.

