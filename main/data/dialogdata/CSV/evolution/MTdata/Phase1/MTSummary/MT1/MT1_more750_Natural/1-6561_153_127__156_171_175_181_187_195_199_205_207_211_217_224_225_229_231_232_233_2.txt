
----------
D0
----------
The debate centers around the proof and the burden of proof. Both debaters adhere to their side of the argument and lay the burden of proof on the opposition. Author1, a creationist, states that in order for them to change their viewpoint they would need to be shown overwhelming evidence to support the claim, that evidence must be 100% verifiable factual. Author1 states that they will be a creationist until someone with credibility makes an "amazing discovery" to prove evolution true. Author1 also makes the claim that if there weren't some sort of god-like being in existence then the majority of the world wouldn't be religious.

Author2, a Darwinist, makes the assertion that because there are few truths in this world that can be verified with 100% certainty, and to demand that level of certainty in presented evidence would be "asinine". Author2 also makes several assertions that Author1 does not have the ability to prove or even provide evidence for any of Author1's claims in the existence of a god.


----------
D1
----------
Author1 states he is a creationist, and would only change his mind if evolution was to have a discovery that showed it to be absolutely correct. He questioned why so many people believe in a creator if creators are just myths, and few are atheists. He refuses to consider other views without proof positive evidence. He is giving up the debate because they will just continue to argue who is wrong or right and never concede to the other. 

Author2 calls asking for 100% proof or else belief in a non-scientific deity isn't logical. He claims 2/3 of people reject the Christian God. He questioned how only one person believed the planets revolved around the sun, and it took a long time for others to believe it. He states in science nothing is ever fully complete and known because there are always new discoveries. He argues Author1's arguments have a lack of basics and general knowledge, indicative of a seventh grader, and suggests Author1 should spend time studying biology.


----------
D2
----------
Author1 believes in creationism until there is certain proof it's false. He asks why so many believe in God if there isn't one. Author2 asks what evidence would convert Author1. He thinks requiring 100% proof is asinine. If the Christian God is real, why do so many people reject it? Author1 says there are facts for each side. Author2 asks Author1 to explain the human plantaris muscle with a creationist worldview. Author1 says they each have their opinion. He has his own opinion and he won't believe evolution until it's proven and Author2 sees this as saying he will never be convinced unless something amazing happens. Author2 thinks Author1 doesn't know the basics of science and Author2 says that he does. Both agree they aren't getting anywhere with the debate. Author2 thinks that Author1 needs to study biology more and Author1 says that he's already decided what he is going to major in and he wasn't taught the Biblical version of biology. Furthermore, science doesn't really interest him, but psychology does to an extent.


----------
D3
----------
Author1 argues that creationism is the preferable, or at least an equally viable, explanation for the origin of life unless science makes a breakthrough that puts the theory of evolution beyond any doubt. Author1 notes that the majority of humanity believes in a creator god, and doubts that this is purely a matter of coincidence. 

Author2 contends that having one's default position be that of theism is illogical, as theism cannot claim to rest on more rational foundations than the scientific method. Author2 notes that a majority of humanity disbelieves in the Judeo-Christian God, so that mere number of adherents cannot support the argument. Beyond that, Author2 does not recognize it as a sound argument. Was the heliocentric theory untrue, Author2 asks, until a majority of humanity believed it? Author2 states that science does not put any contention beyond doubt absolutely; new information is always allowed to challenge existing theories. If Author1 requires absolute proof, Author1 is stating that Author1 cannot be convinced under any real circumstances.


----------
D4
----------
Author1 believes in creation.  Author1 advises he will stick with that theory until a scientist or other credited person is able to provide proof beyond a doubt that no higher power exists.  He feels there are facts which support both creation and evolution.  He feels no amount of argument would settle the debate due to those facts supporting both theories.

Author2 is not a believer of creation.  He notes that two out of every three people reject the notion of there being a Christian God.   He feels it is ludicrous for Author1 to expect 100% proof of the non-existence of God as this is virtually impossible.  He cites the fact that when it was discovered the planets revolved around the sun, Galileo was the only person who believed this theory but that didn't make it untrue.  Author2 disagrees with Author1's statement of there being facts to support both theories.  He feels Author1's belief is incorrect.  He feels Author1 doesn't understand science enough to make a judgement.

