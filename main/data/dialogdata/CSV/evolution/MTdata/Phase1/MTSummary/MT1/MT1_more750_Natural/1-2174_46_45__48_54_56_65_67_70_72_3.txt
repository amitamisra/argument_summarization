
----------
D0
----------
Author1 thinks it would of been nice if the science teachers had taught evolution from the view point we dont know, instead of we do know. Author2 thinks that Author1 has no basis for saying evolution is wrong and spouting religious spam. Author1 replies he may have no basis to say that evolution is wrong, but thinks it's safe to say we don't know. Author2 says we don't know for certain, but the evidence points to it happening. However, speciation has been observed and that alone kills the Creationist theory of unchanging animals. Author1 thinks until you can come up with solid evidence, it will only remain a theory. It's not a bad thing, it's only that when it is presented to our schools as being fact, that it's a problem. Author2 replies if you accept speciation, you accept species can adapt to their environment. It's presented in textbooks as the Theory of Evolution. It's taught as the best explanation we have. Author1 stresses that it's a theory, but science is great tool for understanding.


----------
D1
----------
Author1 criticizes evolution on the grounds that it is a "theory." Author1 takes issue with evolution being presented as a fact when it is not certain that it is true. Author1 believes that, since evolution cannot be proven, supporters of evolution are merely accepting its existence on faith, the grounds on which they often criticize theists. In Author1's view, since scientists have not been present on Earth long enough to observe evolution on the level that they claim exists, they have no grounds to claim that it is fact.

Author2 argues that, while it is impossible for humanity to have witnessed the entirety of evolution of life on Earth, scientists make reasoned observations based on what they can witness; the theory of evolution is the product of those observations. Author2 points out that, if Author1 accepts speciation (as Author1 does), it follows that species would evolve over time as Darwin surmised. Author2 also notes that evolution is presented not as fact, but as theory. It is simply the best theory available at the present time.


----------
D2
----------
Author1 believes evolution should be taught as a possibility instead of certainty for those who choose creationism instead. He accuses Author2 of using irrelevant examples of species change, and points out it can only occur within a species. Author2 is claiming a certainty when only time can decide a certainty. It should be presented as just a theory in education. He admits science is valuable in determining some things, but with unseen things that can depend on long periods of time, there is uncertainty. 


Author2 said Author1 is incorrect that evolution is incorrect. While unforeseen things have an uncertainty, evidence shows the probability. The uncertainty is why it's still called a theory. Speciation, the creation of new species, has happened and that disproves the creationist theory of unchanging animals already created. Believing in speciation at all is acknowledging the possibility of evolution. He details speciation and how it works. He states science observes many unforeseen things, but we believe in them because of the evidence of them.


----------
D3
----------
Author1 poses the argument that since evolution is only a "theory" that it has no right to tout itself as representing hard facts. There is one important thing to note about Author1's argument, which is their incorrect understanding of the word "theory" in the context of science. Author1 also compares the theory of evolution to the "Law" of Gravity, which is a poor comparison, because Gravity is actually scientifically classified as a theory, not a law. Author1 claims that the observable insights of speciation does not lend any credibility to the claims made by Darwin's Theory of Evolution.

Author2 claims that speciation absolutely lends credibility to the Theory of Evolution. Author2 states that in order to understand that evolution is a factually occurring process one must only take their understanding of speciation and reason that it could occur on a much longer time frame. Other than this, Author2 makes no other serious claims, and instead spends most of the dialog poking holes in Author1's argument, in the form of pointing out Author2's comparisons as poorly constructed.


----------
D4
----------
Author1 states that they would have found religion faster if their teacher had taught evolution as a theory instead of a fact. Author2 accuses Author1 of making spam posts instead of debating. Author1 makes the rebuttal that we don't know if evolution is correct. Author2 states that evolution is a theory because it has not been observed, but we have observed adaptations and those adaptations alone disprove creation. Author1 states that if creationism is disproved by fact, evolution is actually a theory and cant disprove it. Author2 explains further how the theory of evolution is really just "speciation" over a long period of time. Author2 also explains that evolution is taught as the best possible answer still in theory. Author1 goes on to point out that in order for evolution to work scientists need to take a step of faith. Author2 requests a better analogy out of Author1 and then states that scientists use theory to explain gaps in evidence, not faith. Author1 closes out by saying that science needs faith for evolution to work.

