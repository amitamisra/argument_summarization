
----------
D0
----------
Author1 starts out by responding to some previous posts, explaining how scientific law comes before scientific theory. Author1 states that we have a good idea of how the bee's communication system works and how it evolved. Author1 then states that there were Nobel Prizes awarded for reading the biotic code. Author1 also believes that although the theories dealing with gravity are weak, they are still scientifically relevant. Author2 is slightly confused at the contradictory language used by Author1 in their response, classifying it as gibberish. Author1 clarifies that they meant that Author2 was artificially creating scientific laws to bolster their argumentative position. Author2 states that Scientific laws are not something that can be invented, at least not by humans. They simply exist. Author1 accuses Author2 of making a fake law, and Author2 claims no such law can be created. Author2 says they didn't do that, and if you can't have a contradiction then it is a law. Author1 claims to have already contradicted every law stated by Author2 and that's why they aren't laws.


----------
D1
----------
Author1 believes evolution is the only viable answer. A scientific theory never becomes a scientific law. Scientific laws grow up into scientific theories. Humans are capable of creating a biotic code. It took a few millennia to discover it. Since the discovery, we have used evolutionary principles to design new codes. It only took a few decades. He tells the writer that he invents scientific laws to bolster his position. He never stated that scientific laws do not exist. There are no green apples that would violate the scientific law that states apples can only be red. He uses that as an example of a fake scientific law. He tells the writer that he is not the only person that can invent one.

Author2 defines the word viable. He insults the other writer by calling his argument gibberish. To say we can genetically engineer DNA and produce life is not just gibberish, it is a lie. He believes that humans cannot invent scientific laws. Scientific laws exist whether you are aware of them or not.


----------
D2
----------
Author1 points to the communication system of bees as an example of the process of evolution. He argues that not only is evolution a possible explanation for how something like that could develop, it is currently the only viable scientific explanation. Author1 also accuses Author2 of confusing scientific theories with so-called scientific laws. He points out that gravity, although widely accepted as truth, is still only a theory. He points out that it took hundreds of years to discover and understand the existence of genetics, but now we are able to create new genes.

Author2 argues that copying existing genetic material and creating new genes are not the same thing. As a result, it is false to say we can genetically modify DNA or create new life. Additionally, he claims that since we do not yet fully understand all genes in the genetic code, we are not able to say that evolution is the only viable explanation. He argues that any scientific claim that cannot be disproved or contradicted should be considered a scientific law.


----------
D3
----------
Author1 discusses bee's communication systems and said evolution is the only current valid answer for how it's evolved. He mentions evolution scientists who have won the Nobel prize for discovering how cells read the biotic code. He discusses how scientific theories don't become laws, but instead the laws inspire the theories. He agrees scientific theories are as good as it gets. He condemns Author2s statements as gibberish because they are so conflicting. He reiterates Author2 was inventing scientific laws. He makes up a law to demonstrate. 

Author2 berates Author1 for not being willing to debate. He recommends Author1 needs a dictionary because he doesn't understand word's meanings, and defines several words for him. He points out how Author1 contradicts himself constantly with his statements. He denies he is making up scientific laws because they can't be invented, they already existed or didn't. He denies Author1 created a fake law, because something either isn't a law or it is, and that is just not a law, it's not a fake one.


----------
D4
----------
Author1 thinks evolution is the only currently viable answer. A scientific theory never becomes a scientific law. Scientific laws grow up into scientific theories. Scientific theories are as good as it gets. Author2 thinks Author1 believes anyone that doesn't believe in evolution is stupid and that everything he had to say is gibberish. To say we can genetically engineer DNA and produce life is not just gibberish it is a lie. He thinks Author1 is confused and Author1 thinks he is the one that's confused. Author1 replies he clearly stated that Author2 was inventing scientific laws to boost his position, not that they exist. Author2 says scientific laws can not be invented, at least not by people. They exist no matte if you are aware of them or not. Author1 says there are no green apples because it would violate the law that apples are red as an example of fake laws that he can make up and Author2 replies there is no such thing as a fake law. It either exists or it doesn't.

