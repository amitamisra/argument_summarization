MTall
contains all dialogs from gun control.
more750: contains dialogs with more than 750  words
mid range : contains dialogs with less than 750 word count.

do not select more than one pair between same authors.
select dialog with maximum word count.



MT1_750 task :was only simple summaries from records 1 to 16 in original file Data_Pkg/CSV/gun-control/MTdata/MTAll/MTAll_more750.csv.


MT1 Mid task: summary both simple and natural
MTdata/MTSummary/AllMTMidNaturalSummary
contains summaries from MT1Mid task.We put 24 dialogs, but only finally selected 20 as others do not contain any relevant information.
 Use as Input for pyramid

Files from 1 to 5 ====Johnnie
1-342_9_8__12_13_15_16_1.txt1-546_20_19__24_27_28_29_33_34_1.txt1-546_140_139__141_142_143_144_145_146_2.txt1-546_230_229__231_232_233_234_235_3.txt1-671_3_2__4_5_6_7_8_9_10_11_13_14_1.txt

Files from 11===15  Johnnie
1-8490_55_54__56_57_58_63_1.txt1-8530_32_31__33_37_39_40_2.txt
1-8711_67_65__68_70_72_73_74_2.txt1-8711_88_86__90_91_101_107_3.txt1-8833_40_39__41_42_43_44_45_1.txt

Files from 6-10  Carol
1-690_2_1__3_4_5_7_1.txt1-764_66_63__67_70_74_75_2.txt1-1543_41_40__44_47_49_51_1.txt1-8099_19_16__23_24_27_31_2.txt1-8143_42_37__45_50_55_56_1.txt



Files 16-20 Asiya
1-8970_4_3__8_9_12_15_16_17_1.txt1-9057_129_126__131_134_138_142_3.txt1-9294_9_6__10_13_18_19_1.txt1-10145_4_3__5_9_21_29_1.txt1-10756_37_35__39_42_43_44_1.txt

MTmid pyramid allocation is done.

MT2 More 750 TASK
get both simple and natural summaries
use rows from 17 t0 26 hence 10 rows. 30 dialogs.

done by Asiya/ checked by me
AllMT_more750_Batch_1and2_1_10

1-173_68_67__69_70_74_77_79_81_87_1.txt1-483_2_1__3_5_6_7_8_10_11_12_13_19_24_31_42_1.txt1-737_78_77__80_81_82_83_84_1.txt1-1103_28_27__29_30_31_32_33_34_1.txt1-3320_45_44__46_47_48_49_1.txt1-3449_16_6__18_19_20_21_23_24_1.txt1-8003_38_37__39_40_41_42_43_45_47_49_1.txt1-8543_111_109__114_131_132_135_139_147_148_1.txt1-8543_123_121__129_130_133_134_2.txt1-8543_150_143__151_153_156_160_162_164_3.txt

done by Johnnie moved to carol and asiya
AllMT_more750_Batch3_11_15
1-8635_36_33__38_40_41_46_49_53_56_1.txt/ checked 1-8786_12_11__13_14_15_22_25_26_27_30_31_32_33_34_35_36_37_38_39_1.txt/ checked1-8860_7_6__8_9_12_15_16_17_1.txt/ checked

By Asiya checked by me1-9036_154_152__155_157_158_159_160_161_162_163_4.txt1-9059_22_15__23_28_43_70_76_3.txt
1-546_140_139__141_142_143_144_145_146_2.txt

Pyramids Checked and Revised by ME in both drives
Done by Carol 
AllMT_more750_Batch4_16_20
1-9235_99_98__100_101_102_103_4_user8.pyr1-9554_8_7__9_19_21_42_45_46_49_57_64_95_3_user8.pyr1-9580_33_28__35_37_40_44_50_55_58_65_68_74_5_user8.pyr1-9600_4_1__6_9_10_12_14_18_19_22_24_26_28_1_user8.pyr1-9615_38_37__41_44_46_47_51_1_user8.pyr


Done by Johnnie
AllMTMidSummaryBatch1_1_5/checked by me
1-342_9_8__12_13_15_16_1_user7.pyr1-546_20_19__24_27_28_29_33_34_1_user7.pyr1-546_140_139__141_142_143_144_145_146_2_user7.pyr1-546_230_229__231_232_233_234_235_3_user7.pyr1-671_3_2__4_5_6_7_8_9_10_11_13_14_1_user7.pyr


AllMTMidSummaryBatch2_6_10/checked by me
1-690_2_1__3_4_5_7_1_user8.pyr1-764_66_63__67_70_74_75_2_user8.pyr1-1543_41_40__44_47_49_51_1_user8.pyr1-8099_19_16__23_24_27_31_2_user8.pyr1-8143_42_37__45_50_55_56_1_user8.pyr




TO BE CHECKED
AllMtmore750 batch5_6 by Jianhong
checked by me 
1-10931_30_25__35_38_46_48_51_52_54_2.txt1-10931_104_103__105_106_107_108_109_110_111_112_113_118_119_3.txt
1-10121_22_21__23_28_30_34_37_1.txt
1-9841_142_140__147_149_151_153_157_158_6.txt
1-9836_25_22__27_34_35_37_39_1.txt1-9836_92_88__94_101_103_108_110_115_2.txt1-9841_196_194__199_202_204_205_206_207_208_209_210_4.txt1-9874_30_3__32_40_41_55_59_82_86_91_92_2.txt1-9884_5_4__7_9_11_13_18_1.txt1-10996_14_10__17_19_22_25_27_28_1.txt
