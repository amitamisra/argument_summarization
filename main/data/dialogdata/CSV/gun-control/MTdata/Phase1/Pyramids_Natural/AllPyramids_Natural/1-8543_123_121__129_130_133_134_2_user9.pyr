<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 says injuries are reduced by regulation. He says no one said anything about if guns didn't exist except Author2. He accuses Author2 of misstating what other people say.</line>
  <line></line>
  <line>Author2 says saying if something didn't exist isn't realistic. Guns are well made and last a long time. He ask what Author1 means by &quot;sufficient regulation.&quot; He says regulations haven't stopped school shootings, the only thing that has is when the shooter is killed or injured. He was simply pointing out how untrue it would be to imagine if guns didn't exist. He says Author1 DID make a reference to guns not existing when he said if it was like in Australia and the UK where the killer couldn't have bought a gun, the 32 victims would be safe and alive. He says this proves Author2 thinks there should be a total ban on handguns like in the UK. He says that would only be possible if all handguns were destroyed. He says the system failed because the person wasn't entered in the system.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>1 believes that the point is not that some people are dangerously mentally ill, but that guns should be regulated sufficiently enough to minimize gun fatalities. He does not think guns will someday be made totally unavailable or that better guns will someday be made that have been designed to be less deadly. He does not think guns will someday cease to exist. He simply believes that better regulation is most likely to lead to less gun violence and premature death.  Author2 believes that guns will always be available, just as they are now. Sufficient regulations are all very well, but when someone is actually shooting innocent people, the only thing that will solve the problem is another shooter to bring the first one down. Regulations have not stopped active school shooters. On occasion though, shooting them has. That has sometimes minimized shooting fatalities. People of all sorts will always be able to get guns, unless every single gun is somehow destroyed or taken out of circulation somehow. That is extremely unlikely to happen.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 states that lethal lead patterns are minimized by sufficient regulation, but Author2 tells Author1 that modern guns are of superior quality. He believes that guns will always exist, and Author2 wants Author1 to define sufficient regulation. Author1 refutes by stating that the quality of manufacture does not lessen the lethality of a gun. Author2 stresses his weariness of the argument that people would be safer if guns did not exist, and he gives various examples of dangerous things that exist. Author2 wonders how Author1 came to the conclusion that 32 people would have been saved if their killer did not have gun access. Author1 mentioned the UK where nobody can own a handgun, so Author2 wonders if Author1 wants guns to not exist at all. The killer got access because he was not in the system, meaning that he could pass any background check like most Americans. He believes that unless Author1 thinks the system should be reformed, then he must want to deny people their right to own guns in the first place.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 says the point isn't sickness patterns, but lethal lead patterns, which occurrence is minimized by regulation. Author2 thinks Author1's party keeps diverting off the side and confusing the issue. He thinks the point is the claim that if this didn't exist it doesn't work in the real world and should be abandoned. They shouldn't whine about guns because they are going to continue to exist and may work for hundreds of years. Furthermore, no school shooting has been stopped by regulation, only when the shooter has been killed or incapacitated. Author1 thinks Author2's version of reality must be different and asks where the if guns didn't exist claim was made. Author2 says he's pointing out the flaws of the argument and compares guns to money. If money didn't exist then no one would be poor. Author1 asks why he keeps repeating it and unlike Author2, he understands the meanings of the words he uses. Author2 quotes Author1 if killers didn't have guns people would be safe. That means destroying guns unless he's talking reform.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 argues that deaths can be reduced by regulation of guns. Guns do not become less lethal when they are not made as well as other guns. Author1 argues that regulations of guns will decrease violence and that more gun regulation is necessary. School shootings would happen less often if there were sufficient regulation of guns. </line>
  <line></line>
  <line>Author2 argues that guns will always exist, because old guns are still around and work just fine. Modern guns are made of higher quality materials and will still be around many more years from now. It is impossible to remove all guns from society. Gun regulation should not be increased. Gun violence has not decreased due to regulations, but rather guns have helped to end school shootings while they are in progress. Since guns do exist, we should not be worried about hypothetical situations which will not occur, and should instead provide guns to as many people as possible to protect ourselves. A total ban on guns is impossible in the US.</line>
 </text>
 <scu uid="143" label="Casualties are reduced by gun regulation">
  <contributor label="Author1 says injuries are reduced by regulation">
   <part label="Author1 says injuries are reduced by regulation" start="26" end="73"/>
  </contributor>
  <contributor label="but that guns should be regulated sufficiently enough to minimize gun fatalities...He simply believes that better regulation is most likely to lead to less gun violence and premature death">
   <part label="but that guns should be regulated sufficiently enough to minimize gun fatalities" start="1101" end="1181"/>
   <part label="He simply believes that better regulation is most likely to lead to less gun violence and premature death" start="1383" end="1488"/>
  </contributor>
  <contributor label="Author1 states that lethal lead patterns are minimized by sufficient regulation">
   <part label="Author1 states that lethal lead patterns are minimized by sufficient regulation" start="2096" end="2175"/>
  </contributor>
  <contributor label="but lethal lead patterns, which occurrence is minimized by regulation">
   <part label="but lethal lead patterns, which occurrence is minimized by regulation" start="3198" end="3267"/>
  </contributor>
  <contributor label="Author1 argues that deaths can be reduced by regulation of guns...Author1 argues that regulations of guns will decrease violence and that more gun regulation is necessary...School shootings would happen less often if there were sufficient regulation of guns">
   <part label="Author1 argues that deaths can be reduced by regulation of guns" start="4220" end="4283"/>
   <part label="Author1 argues that regulations of guns will decrease violence and that more gun regulation is necessary" start="4362" end="4466"/>
   <part label="School shootings would happen less often if there were sufficient regulation of guns" start="4468" end="4552"/>
  </contributor>
 </scu>
 <scu uid="147" label="Guns last a long time">
  <contributor label="Guns are well made and last a long time">
   <part label="Guns are well made and last a long time" start="267" end="306"/>
  </contributor>
  <contributor label="but Author2 tells Author1 that modern guns are of superior quality">
   <part label="but Author2 tells Author1 that modern guns are of superior quality" start="2177" end="2243"/>
  </contributor>
  <contributor label="and may work for hundreds of years">
   <part label="and may work for hundreds of years" start="3548" end="3582"/>
  </contributor>
  <contributor label="because old guns are still around and work just fine...Modern guns are made of higher quality materials and will still be around many more years from now">
   <part label="because old guns are still around and work just fine" start="4600" end="4652"/>
   <part label="Modern guns are made of higher quality materials and will still be around many more years from now" start="4654" end="4752"/>
  </contributor>
 </scu>
 <scu uid="157" label="Guns will always be available">
  <contributor label="He does not think guns will someday be made totally unavailable...He does not think guns will someday cease to exist...Author2 believes that guns will always be available, just as they are now...unless every single gun is somehow destroyed or taken out of circulation somehow. That is extremely unlikely to happen">
   <part label="He does not think guns will someday be made totally unavailable" start="1183" end="1246"/>
   <part label="He does not think guns will someday cease to exist" start="1331" end="1381"/>
   <part label="Author2 believes that guns will always be available, just as they are now" start="1491" end="1564"/>
   <part label="unless every single gun is somehow destroyed or taken out of circulation somehow. That is extremely unlikely to happen" start="1949" end="2067"/>
  </contributor>
  <contributor label="He believes that guns will always exist">
   <part label="He believes that guns will always exist" start="2245" end="2284"/>
  </contributor>
  <contributor label="They shouldn't whine about guns because they are going to continue to exist">
   <part label="They shouldn't whine about guns because they are going to continue to exist" start="3472" end="3547"/>
  </contributor>
  <contributor label="Author2 argues that guns will always exist...It is impossible to remove all guns from society">
   <part label="Author2 argues that guns will always exist" start="4556" end="4598"/>
   <part label="It is impossible to remove all guns from society" start="4754" end="4802"/>
  </contributor>
 </scu>
 <scu uid="149" label="Regulations have not reduced gun violence">
  <contributor label="He says regulations haven't stopped school shootings">
   <part label="He says regulations haven't stopped school shootings" start="362" end="414"/>
  </contributor>
  <contributor label="Sufficient regulations are all very well...Regulations have not stopped active school shooters">
   <part label="Sufficient regulations are all very well" start="1566" end="1606"/>
   <part label="Regulations have not stopped active school shooters" start="1754" end="1805"/>
  </contributor>
  <contributor label="Furthermore, no school shooting has been stopped by regulation">
   <part label="Furthermore, no school shooting has been stopped by regulation" start="3584" end="3646"/>
  </contributor>
  <contributor label="Gun violence has not decreased due to regulations">
   <part label="Gun violence has not decreased due to regulations" start="4844" end="4893"/>
  </contributor>
 </scu>
 <scu uid="150" label="Shooting the shooter has stopped school shootings">
  <contributor label="the only thing that has is when the shooter is killed or injured">
   <part label="the only thing that has is when the shooter is killed or injured" start="416" end="480"/>
  </contributor>
  <contributor label="but when someone is actually shooting innocent people, the only thing that will solve the problem is another shooter to bring the first one down...On occasion though, shooting them has...That has sometimes minimized shooting fatalities">
   <part label="but when someone is actually shooting innocent people, the only thing that will solve the problem is another shooter to bring the first one down" start="1608" end="1752"/>
   <part label="On occasion though, shooting them has" start="1807" end="1844"/>
   <part label="That has sometimes minimized shooting fatalities" start="1846" end="1894"/>
  </contributor>
  <contributor label="only when the shooter has been killed or incapacitated">
   <part label="only when the shooter has been killed or incapacitated" start="3648" end="3702"/>
  </contributor>
  <contributor label="but rather guns have helped to end school shootings while they are in progress">
   <part label="but rather guns have helped to end school shootings while they are in progress" start="4895" end="4973"/>
  </contributor>
 </scu>
 <scu uid="152" label="If the killers didn't have guns, people would not be hurt">
  <contributor label="He says Author1 DID make a reference to guns not existing when he said if it was like in Australia and the UK where the killer couldn't have bought a gun, the 32 victims would be safe and alive">
   <part label="He says Author1 DID make a reference to guns not existing when he said if it was like in Australia and the UK where the killer couldn't have bought a gun, the 32 victims would be safe and alive" start="565" end="758"/>
  </contributor>
  <contributor label="Author2 wonders how Author1 came to the conclusion that 32 people would have been saved if their killer did not have gun access">
   <part label="Author2 wonders how Author1 came to the conclusion that 32 people would have been saved if their killer did not have gun access" start="2603" end="2730"/>
  </contributor>
  <contributor label="Author2 quotes Author1 if killers didn't have guns people would be safe">
   <part label="Author2 quotes Author1 if killers didn't have guns people would be safe" start="4065" end="4136"/>
  </contributor>
 </scu>
 <scu uid="151" label="It is untrue to imagine if guns didn't exist">
  <contributor label="Author2 says saying if something didn't exist isn't realistic...He was simply pointing out how untrue it would be to imagine if guns didn't exist">
   <part label="He was simply pointing out how untrue it would be to imagine if guns didn't exist" start="482" end="563"/>
   <part label="Author2 says saying if something didn't exist isn't realistic" start="204" end="265"/>
  </contributor>
  <contributor label="Since guns do exist, we should not be worried about hypothetical situations which will not occur">
   <part label="Since guns do exist, we should not be worried about hypothetical situations which will not occur" start="4975" end="5071"/>
  </contributor>
  <contributor label="He thinks the point is the claim that if this didn't exist it doesn't work in the real world and should be abandoned">
   <part label="He thinks the point is the claim that if this didn't exist it doesn't work in the real world and should be abandoned" start="3354" end="3470"/>
  </contributor>
 </scu>
 <scu uid="154" label="A total ban is only possible if all handguns are destroyed">
  <contributor label="That means destroying guns">
   <part label="That means destroying guns" start="4138" end="4164"/>
  </contributor>
  <contributor label="all handguns were destroyed">
   <part label="all handguns were destroyed" start="890" end="917"/>
  </contributor>
 </scu>
 <scu uid="169" label="A total ban on guns is impossible">
  <contributor label="total ban on guns is impossible in the US">
   <part label="total ban on guns is impossible in the US" start="5159" end="5200"/>
  </contributor>
  <contributor label="He says that would only be possible">
   <part label="He says that would only be possible" start="851" end="886"/>
  </contributor>
 </scu>
 <scu uid="153" label="Ban handguns">
  <contributor label="He says this proves Author2 thinks there should be a total ban on handguns like in the UK">
   <part label="He says this proves Author2 thinks there should be a total ban on handguns like in the UK" start="760" end="849"/>
  </contributor>
  <contributor label="Author1 mentioned the UK where nobody can own a handgun, so Author2 wonders if Author1 wants guns to not exist at all">
   <part label="Author1 mentioned the UK where nobody can own a handgun, so Author2 wonders if Author1 wants guns to not exist at all" start="2732" end="2849"/>
  </contributor>
 </scu>
 <scu uid="148" label="Define &quot;sufficient regulation&quot;">
  <contributor label="He ask what Author1 means by &quot;sufficient regulation.&quot;">
   <part label="He ask what Author1 means by &quot;sufficient regulation.&quot;" start="308" end="361"/>
  </contributor>
  <contributor label="and Author2 wants Author1 to define sufficient regulation">
   <part label="and Author2 wants Author1 to define sufficient regulation" start="2286" end="2343"/>
  </contributor>
 </scu>
 <scu uid="156" label="Mentally ill people are not the danger">
  <contributor label="1 believes that the point is not that some people are dangerously mentally ill">
   <part label="1 believes that the point is not that some people are dangerously mentally ill" start="1021" end="1099"/>
  </contributor>
  <contributor label="Author1 says the point isn't sickness patterns">
   <part label="Author1 says the point isn't sickness patterns" start="3150" end="3196"/>
  </contributor>
 </scu>
 <scu uid="145" label="Misstate what other people say">
  <contributor label="He accuses Author2 of misstating what other people say">
   <part label="He accuses Author2 of misstating what other people say" start="147" end="201"/>
  </contributor>
  <contributor label="Author1 asks why he keeps repeating it and unlike Author2, he understands the meanings of the words he uses">
   <part label="Author1 asks why he keeps repeating it and unlike Author2, he understands the meanings of the words he uses" start="3956" end="4063"/>
  </contributor>
 </scu>
 <scu uid="163" label="Reform the system">
  <contributor label="He believes that unless Author1 thinks the system should be reformed">
   <part label="He believes that unless Author1 thinks the system should be reformed" start="2976" end="3044"/>
  </contributor>
  <contributor label="unless he's talking reform">
   <part label="unless he's talking reform" start="4165" end="4191"/>
  </contributor>
 </scu>
 <scu uid="160" label="The quality of manufacture does not lessen the lethality of a gun">
  <contributor label="Author1 refutes by stating that the quality of manufacture does not lessen the lethality of a gun">
   <part label="Author1 refutes by stating that the quality of manufacture does not lessen the lethality of a gun" start="2345" end="2442"/>
  </contributor>
  <contributor label="Guns do not become less lethal when they are not made as well as other guns">
   <part label="Guns do not become less lethal when they are not made as well as other guns" start="4285" end="4360"/>
  </contributor>
 </scu>
 <scu uid="155" label="The system failed because the person wasn't entered in the system">
  <contributor label="He says the system failed because the person wasn't entered in the system">
   <part label="He says the system failed because the person wasn't entered in the system" start="919" end="992"/>
  </contributor>
  <contributor label="The killer got access because he was not in the system, meaning that he could pass any background check like most Americans">
   <part label="The killer got access because he was not in the system, meaning that he could pass any background check like most Americans" start="2851" end="2974"/>
  </contributor>
 </scu>
 <scu uid="144" label="Wonder if guns didn't exist">
  <contributor label="He says no one said anything about if guns didn't exist except Author2">
   <part label="He says no one said anything about if guns didn't exist except Author2" start="75" end="145"/>
  </contributor>
  <contributor label="Author1 thinks Author2's version of reality must be different and asks where the if guns didn't exist claim was made">
   <part label="Author1 thinks Author2's version of reality must be different and asks where the if guns didn't exist claim was made" start="3704" end="3820"/>
  </contributor>
 </scu>
 <scu uid="165" label="Confuse the issue">
  <contributor label="Author2 thinks Author1's party keeps diverting off the side and confusing the issue">
   <part label="Author2 thinks Author1's party keeps diverting off the side and confusing the issue" start="3269" end="3352"/>
  </contributor>
 </scu>
 <scu uid="164" label="Deny people their right to own guns">
  <contributor label="then he must want to deny people their right to own guns in the first place">
   <part label="then he must want to deny people their right to own guns in the first place" start="3046" end="3121"/>
  </contributor>
 </scu>
 <scu uid="167" label="Gun regulation should not be increased">
  <contributor label="Gun regulation should not be increased">
   <part label="Gun regulation should not be increased" start="4804" end="4842"/>
  </contributor>
 </scu>
 <scu uid="158" label="Guns will never be less deadly">
  <contributor label="or that better guns will someday be made that have been designed to be less deadly">
   <part label="or that better guns will someday be made that have been designed to be less deadly" start="1247" end="1329"/>
  </contributor>
 </scu>
 <scu uid="166" label="If money didn't exist then no one would be poor">
  <contributor label="Author2 says he's pointing out the flaws of the argument and compares guns to money. If money didn't exist then no one would be poor">
   <part label="Author2 says he's pointing out the flaws of the argument and compares guns to money. If money didn't exist then no one would be poor" start="3822" end="3954"/>
  </contributor>
 </scu>
 <scu uid="159" label="People of all sorts will always be able to get guns">
  <contributor label="People of all sorts will always be able to get guns">
   <part label="People of all sorts will always be able to get guns" start="1896" end="1947"/>
  </contributor>
 </scu>
 <scu uid="161" label="People will not be safer without guns">
  <contributor label="Author2 stresses his weariness of the argument that people would be safer if guns did not exist, and he gives various examples of dangerous things that exist">
   <part label="Author2 stresses his weariness of the argument that people would be safer if guns did not exist, and he gives various examples of dangerous things that exist" start="2444" end="2601"/>
  </contributor>
 </scu>
 <scu uid="168" label="Provide guns to as many people as possible to protect ourselves">
  <contributor label="and should instead provide guns to as many people as possible to protect ourselves">
   <part label="and should instead provide guns to as many people as possible to protect ourselves" start="5073" end="5155"/>
  </contributor>
 </scu>
</pyramid>
