<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 tells Author2 that the Second Amendment protects a preexisting right, and that the RKBA used to be connected with the militia service. Author2 wants to know when a physically incapable person that could not take part in the militia was ever barred from owning a firearm. Author2 believes that Author1 thinks regular people should not own firearms if they cannot serve the state. Author1 asks why ownership of weapons by those outside the militia would prove that such ownership is protected by the RKBA, and Author2 answers that Author1 has argued that the right to keep and bear arms only applies to the militia. Author2 states that Author1 believes that certain females were barred from owning firearms because it was not in the interest of the government. Author2 asserts that Author1 needs to prove that women and those over 46 were not allowed to be armed because they could not serve in the militia. Author1 clarifies for Author2 that he never argued that there was a complete ban on firearms used for non-militia purposes.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 sites some information eluding to the fact the 2nd Amendment was not designed to protect a preexisting right to bear arms.  He advises the basis of the amendment was in support of people who had been involved with the militia in the sixteen and seventeen hundreds and that weapons owned by people who were not members were subject to reasonable regulations.  He sites several books to support his theory. </line>
  <line></line>
  <line>Author2 does not support the argument that the right to keep and bear arms was established for the militia or militia ready persons only and requests proof of this fact.  He supports his ideas by citing the fact that people who were considered unable to serve the militia would have lost their weapons had this been the case.  He uses retired militia members as an example advising if the RKBA were only to protect militia groups, then those retired would no longer be able to hold weapons as they would no longer be able to serve the state if needed.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 states Heller found that the English Bill of Rights allowed people to keep guns for self defense. A court brief argued he was wrong. Author1 says the right to bear arms was connected with militia so the second amendment wasn't protecting ordinary citizen rights. He says just because people owned guns doesn't prove they were protected to do so. He says he wasn't saying there was a ban on people from owning guns.</line>
  <line></line>
  <line></line>
  <line>Author2 says to prove people who were not able to serve in militias were not allowed to own weapons. He says Author1 is arguing that only the militia were protected in owning guns, and that means anyone unable to serve in a militia would then be excluded from owning guns. He says the fact people were allowed to own guns shows that they were protected to do so, or people outside of militia members wouldn't have been allowed to. He says this shows Author1's statements that the second amendment doesn't protect citizen rights to bear arms are false.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 argues that the Magna Carta was approved in 1215 and that people had typically been allowed to keep weapons in the UK with their typical laws. The second amendment is claimed to protect a preexisting right, but instead the right to keep and bear arms (RKBA) was only ever connected with military service. The RKBA is not a preexisting condition as presented in the second amendment, and although there is no ban against owning weapons outside a militia, the right is only guaranteed for the purpose of having a militia. There has never been a complete ban on firearms for people not in a militia, but rather weapons can be reasonably regulated by the states. </line>
  <line>Author2 argues that there is no proof that weapons were only guaranteed for militia members and not for all citizens. Since the RKBA was not guaranteed for anyone other than milita members, it is reasonable to assume that is should be illegal for anyone not in a militia should not own guns. But there is no ban like that.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 says Heller explored the rights origins, the 1689 English Bill of Rights explicitly protected a right to keep arms for self-defense. By 1765, Blackstone was able to assert the right to keep and bear arms. The English Historians Brief says that Heller was wrong. The RKBA was connected with militia service, so pointing out that the RKBA is a preexisting right doesn't support Author2's argument.</line>
  <line>Author2 asks where it's been established that any person who was incapable of taking part in the militia was barred from owning a firearm. Author1 asks how people owning guns outside the militia would prove it's protected by the RKBA. Author2 says Author1 argues that the right to keep and bear arms exists to guarantee that only the militia may be armed, so he should prove it. Author1 replies that a lack of ban on firearms by those outside of the militia does not mean that ownership was considered to be protected. Author2 thinks it does. Author1 says he never said there was a weapons ban for non-military purposes.</line>
 </text>
 <scu uid="9" label=" people who were not able to serve in militias were not allowed to own weapons needs to be proofed.">
  <contributor label="Author2 says to prove people who were not able to serve in militias were not allowed to own weapons">
   <part label="Author2 says to prove people who were not able to serve in militias were not allowed to own weapons" start="2510" end="2609"/>
  </contributor>
  <contributor label="requests proof of this fact">
   <part label="requests proof of this fact" start="1647" end="1674"/>
  </contributor>
  <contributor label="Author2 asserts that Author1 needs to prove that women and those over 46 were not allowed to be armed">
   <part label="Author2 asserts that Author1 needs to prove that women and those over 46 were not allowed to be armed" start="793" end="894"/>
  </contributor>
  <contributor label="not for all citizens">
   <part label="not for all citizens" start="3853" end="3873"/>
  </contributor>
  <contributor label="so he should prove it">
   <part label="so he should prove it" start="4867" end="4888"/>
  </contributor>
 </scu>
 <scu uid="5" label="The right to keep and bear arms only applies to the militia.">
  <contributor label="Author2 answers that Author1 has argued that the right to keep and bear arms only applies to the militia">
   <part label="Author2 answers that Author1 has argued that the right to keep and bear arms only applies to the militia" start="542" end="646"/>
  </contributor>
  <contributor label="Author2 does not support the argument that the right to keep and bear arms was established for the militia or militia ready persons only">
   <part label="Author2 does not support the argument that the right to keep and bear arms was established for the militia or militia ready persons only" start="1506" end="1642"/>
  </contributor>
  <contributor label="He says Author1 is arguing that only the militia were protected in owning guns">
   <part label="He says Author1 is arguing that only the militia were protected in owning guns" start="2611" end="2689"/>
  </contributor>
  <contributor label="Author2 argues that there is no proof that weapons were only guaranteed for militia members...Since the RKBA was not guaranteed for anyone other than milita members">
   <part label="Author2 argues that there is no proof that weapons were only guaranteed for militia members" start="3757" end="3848"/>
   <part label="Since the RKBA was not guaranteed for anyone other than milita members" start="3875" end="3945"/>
  </contributor>
  <contributor label="Author2 says Author1 argues that the right to keep and bear arms exists to guarantee that only the militia may be armed">
   <part label="Author2 says Author1 argues that the right to keep and bear arms exists to guarantee that only the militia may be armed" start="4746" end="4865"/>
  </contributor>
 </scu>
 <scu uid="3" label="People who cannot serve the state should not own firearms.">
  <contributor label="Author2 believes that Author1 thinks regular people should not own firearms if they cannot serve the state">
   <part label="Author2 believes that Author1 thinks regular people should not own firearms if they cannot serve the state" start="305" end="411"/>
  </contributor>
  <contributor label="weapons owned by people who were not members were subject to reasonable regulations">
   <part label="weapons owned by people who were not members were subject to reasonable regulations" start="1372" end="1455"/>
  </contributor>
  <contributor label="that means anyone unable to serve in a militia would then be excluded from owning guns...people outside of militia members wouldn't have been allowed to">
   <part label="that means anyone unable to serve in a militia would then be excluded from owning guns" start="2695" end="2781"/>
   <part label="people outside of militia members wouldn't have been allowed to" start="2876" end="2939"/>
  </contributor>
  <contributor label="it is reasonable to assume that is should be illegal for anyone not in a militia should not own guns">
   <part label="it is reasonable to assume that is should be illegal for anyone not in a militia should not own guns" start="3947" end="4047"/>
  </contributor>
 </scu>
 <scu uid="22" label="The RKBA is not a preexisting condition by the 2nd Amendment.">
  <contributor label="The RKBA is not a preexisting condition as presented in the second amendment">
   <part label="The RKBA is not a preexisting condition as presented in the second amendment" start="3402" end="3478"/>
  </contributor>
  <contributor label="so pointing out that the RKBA is a preexisting right doesn't support Author2's argument">
   <part label="so pointing out that the RKBA is a preexisting right doesn't support Author2's argument" start="4422" end="4509"/>
  </contributor>
  <contributor label="Author1 sites some information eluding to the fact the 2nd Amendment was not designed to protect a preexisting right to bear arms">
   <part label="Author1 sites some information eluding to the fact the 2nd Amendment was not designed to protect a preexisting right to bear arms" start="1091" end="1220"/>
  </contributor>
  <contributor label="so the second amendment wasn't protecting ordinary citizen rights">
   <part label="so the second amendment wasn't protecting ordinary citizen rights" start="2289" end="2354"/>
  </contributor>
 </scu>
 <scu uid="2" label="The RKBA was connected with militia service.">
  <contributor label="that the RKBA used to be connected with the militia service">
   <part label="that the RKBA used to be connected with the militia service" start="108" end="167"/>
  </contributor>
  <contributor label="Author1 says the right to bear arms was connected with militia">
   <part label="Author1 says the right to bear arms was connected with militia" start="2226" end="2288"/>
  </contributor>
  <contributor label="but instead the right to keep and bear arms (RKBA) was only ever connected with military service...the right is only guaranteed for the purpose of having a militia">
   <part label="but instead the right to keep and bear arms (RKBA) was only ever connected with military service" start="3304" end="3400"/>
   <part label="the right is only guaranteed for the purpose of having a militia" start="3551" end="3615"/>
  </contributor>
  <contributor label="The RKBA was connected with militia service">
   <part label="The RKBA was connected with militia service" start="4377" end="4420"/>
  </contributor>
 </scu>
 <scu uid="12" label="There has never been a complete ban on firearms for people not in a militia.">
  <contributor label="there is no ban against owning weapons outside a militia...There has never been a complete ban on firearms for people not in a militia...But there is no ban like that">
   <part label="There has never been a complete ban on firearms for people not in a militia" start="3617" end="3692"/>
   <part label="there is no ban against owning weapons outside a militia" start="3493" end="3549"/>
   <part label="But there is no ban like that" start="4049" end="4078"/>
  </contributor>
  <contributor label="Author1 says he never said there was a weapons ban for non-military purposes">
   <part label="Author1 says he never said there was a weapons ban for non-military purposes" start="5054" end="5130"/>
  </contributor>
  <contributor label="Author1 clarifies for Author2 that he never argued that there was a complete ban on firearms used for non-militia purposes">
   <part label="Author1 clarifies for Author2 that he never argued that there was a complete ban on firearms used for non-militia purposes" start="940" end="1062"/>
  </contributor>
  <contributor label="He says he wasn't saying there was a ban on people from owning guns">
   <part label="He says he wasn't saying there was a ban on people from owning guns" start="2439" end="2506"/>
  </contributor>
 </scu>
 <scu uid="11" label="People in the UK are allowed to keep weapons with the laws.">
  <contributor label="that people had typically been allowed to keep weapons in the UK with their typical laws">
   <part label="that people had typically been allowed to keep weapons in the UK with their typical laws" start="3150" end="3238"/>
  </contributor>
  <contributor label="Author1 states Heller found that the English Bill of Rights allowed people to keep guns for self defense">
   <part label="Author1 states Heller found that the English Bill of Rights allowed people to keep guns for self defense" start="2085" end="2189"/>
  </contributor>
  <contributor label="Author1 says Heller explored the rights origins, the 1689 English Bill of Rights explicitly protected a right to keep arms for self-defense">
   <part label="Author1 says Heller explored the rights origins, the 1689 English Bill of Rights explicitly protected a right to keep arms for self-defense" start="4107" end="4246"/>
  </contributor>
 </scu>
 <scu uid="18" label="People owned guns doesn't prove they were protected to do so.">
  <contributor label="He says just because people owned guns doesn't prove they were protected to do so">
   <part label="He says just because people owned guns doesn't prove they were protected to do so" start="2356" end="2437"/>
  </contributor>
  <contributor label="Author1 asks how people owning guns outside the militia would prove it's protected by the RKBA...Author1 replies that a lack of ban on firearms by those outside of the militia does not mean that ownership was considered to be protected">
   <part label="Author1 asks how people owning guns outside the militia would prove it's protected by the RKBA" start="4650" end="4744"/>
   <part label="Author1 replies that a lack of ban on firearms by those outside of the militia does not mean that ownership was considered to be protected" start="4890" end="5028"/>
  </contributor>
  <contributor label="Author1 asks why ownership of weapons by those outside the militia would prove that such ownership is protected by the RKBA">
   <part label="Author1 asks why ownership of weapons by those outside the militia would prove that such ownership is protected by the RKBA" start="413" end="536"/>
  </contributor>
 </scu>
 <scu uid="37" label="Retired militia members were never barred from owning a firearm.">
  <contributor label="Author2 wants to know when a physically incapable person that could not take part in the militia was ever barred from owning a firearm">
   <part label="Author2 wants to know when a physically incapable person that could not take part in the militia was ever barred from owning a firearm" start="169" end="303"/>
  </contributor>
  <contributor label="He uses retired militia members as an example advising if the RKBA were only to protect militia groups...those retired would no longer be able to hold weapons as they would no longer be able to serve the state if needed">
   <part label="He uses retired militia members as an example advising if the RKBA were only to protect militia groups" start="1833" end="1935"/>
   <part label="those retired would no longer be able to hold weapons as they would no longer be able to serve the state if needed" start="1942" end="2056"/>
  </contributor>
  <contributor label="Author2 asks where it's been established that any person who was incapable of taking part in the militia was barred from owning a firearm">
   <part label="Author2 asks where it's been established that any person who was incapable of taking part in the militia was barred from owning a firearm" start="4511" end="4648"/>
  </contributor>
 </scu>
 <scu uid="19" label="people owned guns shows that they were protected to do so.">
  <contributor label="He says the fact people were allowed to own guns shows that they were protected to do so">
   <part label="He says the fact people were allowed to own guns shows that they were protected to do so" start="2783" end="2871"/>
  </contributor>
  <contributor label="Author2 thinks it does">
   <part label="Author2 thinks it does" start="5030" end="5052"/>
  </contributor>
  <contributor label="citing the fact that people who were considered unable to serve the militia would have lost their weapons had this been the case">
   <part label="citing the fact that people who were considered unable to serve the militia would have lost their weapons had this been the case" start="1702" end="1830"/>
  </contributor>
 </scu>
 <scu uid="1" label="The Second Amendment protects a preexisting right.">
  <contributor label="Author1 tells Author2 that the Second Amendment protects a preexisting right">
   <part label="Author1 tells Author2 that the Second Amendment protects a preexisting right" start="26" end="102"/>
  </contributor>
  <contributor label="The second amendment is claimed to protect a preexisting right">
   <part label="The second amendment is claimed to protect a preexisting right" start="3240" end="3302"/>
  </contributor>
  <contributor label="He says this shows Author1's statements that the second amendment doesn't protect citizen rights to bear arms are false">
   <part label="He says this shows Author1's statements that the second amendment doesn't protect citizen rights to bear arms are false" start="2941" end="3060"/>
  </contributor>
 </scu>
 <scu uid="16" label="Heller was wrong.">
  <contributor label="A court brief argued he was wrong">
   <part label="A court brief argued he was wrong" start="2191" end="2224"/>
  </contributor>
  <contributor label="The English Historians Brief says that Heller was wrong">
   <part label="The English Historians Brief says that Heller was wrong" start="4320" end="4375"/>
  </contributor>
 </scu>
 <scu uid="23" label="By 1765, Blackstone was able to assert the right to keep and bear arms.">
  <contributor label="By 1765, Blackstone was able to assert the right to keep and bear arms">
   <part label="By 1765, Blackstone was able to assert the right to keep and bear arms" start="4248" end="4318"/>
  </contributor>
 </scu>
 <scu uid="6" label="Certain females were barred from owning firearms.">
  <contributor label="Author2 states that Author1 believes that certain females were barred from owning firearms">
   <part label="Author2 states that Author1 believes that certain females were barred from owning firearms" start="648" end="738"/>
  </contributor>
 </scu>
 <scu uid="7" label="Not in the interest of the government.">
  <contributor label="it was not in the interest of the government">
   <part label="it was not in the interest of the government" start="747" end="791"/>
  </contributor>
 </scu>
 <scu uid="14" label="Several books support the theory.">
  <contributor label="He sites several books to support his theory">
   <part label="He sites several books to support his theory" start="1458" end="1502"/>
  </contributor>
 </scu>
 <scu uid="39" label="The author supports his ideas.">
  <contributor label="He supports his ideas">
   <part label="He supports his ideas" start="1677" end="1698"/>
  </contributor>
 </scu>
 <scu uid="38" label="The basis of the amendment was in support of the militia in the sixteen and seventeen hundreds.">
  <contributor label="He advises the basis of the amendment was in support of people who had been involved with the militia in the sixteen and seventeen hundreds">
   <part label="He advises the basis of the amendment was in support of people who had been involved with the militia in the sixteen and seventeen hundreds" start="1223" end="1362"/>
  </contributor>
 </scu>
 <scu uid="21" label="The Magna Carta was approved in 1215.">
  <contributor label="Author1 argues that the Magna Carta was approved in 1215">
   <part label="Author1 argues that the Magna Carta was approved in 1215" start="3089" end="3145"/>
  </contributor>
 </scu>
 <scu uid="42" label="Weapons can be reasonably regulated by the states.">
  <contributor label="rather weapons can be reasonably regulated by the states">
   <part label="rather weapons can be reasonably regulated by the states" start="3698" end="3754"/>
  </contributor>
 </scu>
 <scu uid="8" label="Women could not serve in the militia.">
  <contributor label="they could not serve in the militia">
   <part label="they could not serve in the militia" start="903" end="938"/>
  </contributor>
 </scu>
</pyramid>
