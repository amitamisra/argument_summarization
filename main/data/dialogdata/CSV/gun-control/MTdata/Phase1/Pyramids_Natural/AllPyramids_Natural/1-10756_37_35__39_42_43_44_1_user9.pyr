<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>S1 is against the controls on guns that California has put in place. They argue that guns should not be so strictly controlled and regulated as they are in California. Other states cannot and should not be controlled by the rulings of a different state. If other states wanted to, they could adopt similar laws. However, gun control does not apply well in real life. If gun laws are stricter, then illegal guns will still manage to bypass those laws, because the guns are illegal. S2 argues that stricter gun control is necessary and would have saved four policeman's lives. Because of the high crime rates in Californian cities, it is a good idea for there to be more gun control in place.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>S1 says S2 is proposing again that the U.S. adopt California's standards. He cites others lack of support, because one state can not determine how another should act. He says states can not ignore issues of their own. S2 says states adopting controls would depend on the voters, and the states.  He states that California has large densely populated cities with high crime rates. He gives a recent example that four cops were killed by a criminal with an AK-47. S1 questions how an example of a prohibited weapon being used serves as a good reason for the ban. S2 says it shows why there is a ban. S2 says if the gun hadn't been available, the damage wouldn't be so great.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>S1 states that S2 made the argument that California should set the standard for the United States, but one state cannot regulate the activities of another state. A state should not ignore home rule issues that may have created a problem that they cannot respond to. S2 believes that other states could adopt controls like California, and he further believes that the reform will not take place because other states do not like the California laws. Four Oakland police officers were gunned down by a hood with an AK-47, which serves as a valid reason for some restrictions in areas like California has. S1 believes that this is not a good example, but S2 believes the cop murder proves that it is a good measure.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>S1 says the argument that the United States should adopt California's standards as national standards has been made several times, but support has been lacking. A single state can't regulate the activities of others, but that doesn't mean a state should ignore home rule issues that have created problems. S2 says states could adopt controls like California if they wanted, but it would depend on voters. He says that people miss that California is a large state with large crime rates, and this is one of the reasons for such restrictions. He gives an example of four officers being gunned down with an AK-47. S1 questions an example of a prohibited weapon being used illegally as an example. S2 thinks the weapon shouldn't have been available.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>S1 notes that S2 has repeatedly suggested that other states should enact gun control laws similar to California's.  He points out that one state can't regulate the rules of another, and that S2's proposal has gained little support.  S1 asks S2 twice if he's really saying that the deaths of four Oakland police officers killed by a (banned) AK-47 shows why California's gun laws are a good idea.</line>
  <line></line>
  <line>S2 asserts that voters in other states could choose to adopt California's gun policies, but concedes that such a move is unlikely in some states.  S2, who says he is a gun owner, believes the killing of the officers in Oakland supports the case for a continued ban on weapons like the one used.</line>
 </text>
 <scu uid="42" label="Four cops were killed by a criminal with an AK-47">
  <contributor label="He gives a recent example that four cops were killed by a criminal with an AK-47">
   <part label="He gives a recent example that four cops were killed by a criminal with an AK-47" start="1124" end="1204"/>
  </contributor>
  <contributor label="four policeman's lives">
   <part label="four policeman's lives" start="577" end="599"/>
  </contributor>
  <contributor label="Four Oakland police officers were gunned down by a hood with an AK-47">
   <part label="Four Oakland police officers were gunned down by a hood with an AK-47" start="1892" end="1961"/>
  </contributor>
  <contributor label="He gives an example of four officers being gunned down with an AK-47">
   <part label="He gives an example of four officers being gunned down with an AK-47" start="2724" end="2792"/>
  </contributor>
  <contributor label="deaths of four Oakland police officers killed by a (banned) AK-47">
   <part label="deaths of four Oakland police officers killed by a (banned) AK-47" start="3237" end="3302"/>
  </contributor>
 </scu>
 <scu uid="31" label="Other states cannot be controlled by the rulings of a different state">
  <contributor label="Other states cannot and should not be controlled by the rulings of a different state">
   <part label="Other states cannot and should not be controlled by the rulings of a different state" start="194" end="278"/>
  </contributor>
  <contributor label="but one state cannot regulate the activities of another state">
   <part label="but one state cannot regulate the activities of another state" start="1543" end="1604"/>
  </contributor>
  <contributor label="A single state can't regulate the activities of others">
   <part label="A single state can't regulate the activities of others" start="2344" end="2398"/>
  </contributor>
  <contributor label="He points out that one state can't regulate the rules of another">
   <part label="He points out that one state can't regulate the rules of another" start="3072" end="3136"/>
  </contributor>
  <contributor label="because one state can not determine how another should act">
   <part label="because one state can not determine how another should act" start="851" end="909"/>
  </contributor>
 </scu>
 <scu uid="32" label="Other states could adopt gun controls like California">
  <contributor label="If other states wanted to, they could adopt similar laws">
   <part label="If other states wanted to, they could adopt similar laws" start="280" end="336"/>
  </contributor>
  <contributor label="S2 believes that other states could adopt controls like California">
   <part label="S2 believes that other states could adopt controls like California" start="1710" end="1776"/>
  </contributor>
  <contributor label="S2 says states could adopt controls like California if they wanted">
   <part label="S2 says states could adopt controls like California if they wanted" start="2489" end="2555"/>
  </contributor>
  <contributor label="S2 asserts that voters in other states could choose to adopt California's gun policies">
   <part label="S2 asserts that voters in other states could choose to adopt California's gun policies" start="3353" end="3439"/>
  </contributor>
 </scu>
 <scu uid="39" label="Propose that the U.S. adopt California's standards">
  <contributor label="S1 says S2 is proposing again that the U.S. adopt California's standards">
   <part label="S1 says S2 is proposing again that the U.S. adopt California's standards" start="744" end="816"/>
  </contributor>
  <contributor label="S1 states that S2 made the argument that California should set the standard for the United States">
   <part label="S1 states that S2 made the argument that California should set the standard for the United States" start="1444" end="1541"/>
  </contributor>
  <contributor label="S1 says the argument that the United States should adopt California's standards as national standards has been made several times">
   <part label="S1 says the argument that the United States should adopt California's standards as national standards has been made several times" start="2183" end="2312"/>
  </contributor>
  <contributor label="S1 notes that S2 has repeatedly suggested that other states should enact gun control laws similar to California's">
   <part label="S1 notes that S2 has repeatedly suggested that other states should enact gun control laws similar to California's" start="2956" end="3069"/>
  </contributor>
 </scu>
 <scu uid="45" label="The cop murder proves that gun control is a good idea">
  <contributor label="S2 says it shows why there is a ban...S2 says if the gun hadn't been available, the damage wouldn't be so great">
   <part label="S2 says if the gun hadn't been available, the damage wouldn't be so great" start="1342" end="1415"/>
   <part label="S2 says it shows why there is a ban" start="1305" end="1340"/>
  </contributor>
  <contributor label="but S2 believes the cop murder proves that it is a good measure">
   <part label="but S2 believes the cop murder proves that it is a good measure" start="2091" end="2154"/>
  </contributor>
  <contributor label="S2 thinks the weapon shouldn't have been available">
   <part label="S2 thinks the weapon shouldn't have been available" start="2877" end="2927"/>
  </contributor>
  <contributor label="S1 asks S2 twice if he's really saying that the deaths of four Oakland police officers killed by a (banned) AK-47 shows why California's gun laws are a good idea...believes the killing of the officers in Oakland supports the case for a continued ban on weapons like the one used">
   <part label="S1 asks S2 twice if he's really saying that the deaths of four Oakland police officers killed by a (banned) AK-47 shows why California's gun laws are a good idea" start="3189" end="3350"/>
   <part label="believes the killing of the officers in Oakland supports the case for a continued ban on weapons like the one used" start="3532" end="3646"/>
  </contributor>
 </scu>
 <scu uid="43" label="An example of a prohibited weapon being used is not a good reason for the ban">
  <contributor label="S1 questions how an example of a prohibited weapon being used serves as a good reason for the ban">
   <part label="S1 questions how an example of a prohibited weapon being used serves as a good reason for the ban" start="1206" end="1303"/>
  </contributor>
  <contributor label="S1 believes that this is not a good example">
   <part label="S1 believes that this is not a good example" start="2046" end="2089"/>
  </contributor>
  <contributor label="S1 questions an example of a prohibited weapon being used illegally as an example">
   <part label="S1 questions an example of a prohibited weapon being used illegally as an example" start="2794" end="2875"/>
  </contributor>
 </scu>
 <scu uid="41" label="California has high crime rates">
  <contributor label="He states that California has large densely populated cities with high crime rates">
   <part label="He states that California has large densely populated cities with high crime rates" start="1040" end="1122"/>
  </contributor>
  <contributor label="high crime rates in Californian cities">
   <part label="high crime rates in Californian cities" start="616" end="654"/>
  </contributor>
  <contributor label="He says that people miss that California is a large state with large crime rates">
   <part label="He says that people miss that California is a large state with large crime rates" start="2588" end="2668"/>
  </contributor>
 </scu>
 <scu uid="38" label="More gun control is good because of the high crime rate in Californian cities">
  <contributor label="Because of the high crime rates in Californian cities, it is a good idea for there to be more gun control in place">
   <part label="Because of the high crime rates in Californian cities, it is a good idea for there to be more gun control in place" start="601" end="715"/>
  </contributor>
  <contributor label="which serves as a valid reason for some restrictions in areas like California has">
   <part label="which serves as a valid reason for some restrictions in areas like California has" start="1963" end="2044"/>
  </contributor>
  <contributor label="and this is one of the reasons for such restrictions">
   <part label="and this is one of the reasons for such restrictions" start="2670" end="2722"/>
  </contributor>
 </scu>
 <scu uid="46" label="States adopting controls would depend on the voters">
  <contributor label="S2 says states adopting controls would depend on the voters, and the states">
   <part label="S2 says states adopting controls would depend on the voters, and the states" start="962" end="1037"/>
  </contributor>
  <contributor label="but it would depend on voters">
   <part label="but it would depend on voters" start="2557" end="2586"/>
  </contributor>
  <contributor label="voters in other states could choose">
   <part label="voters in other states could choose" start="3369" end="3404"/>
  </contributor>
 </scu>
 <scu uid="40" label="States can not ignore home rule issues">
  <contributor label="He says states can not ignore issues of their own">
   <part label="He says states can not ignore issues of their own" start="911" end="960"/>
  </contributor>
  <contributor label="A state should not ignore home rule issues that may have created a problem that they cannot respond to">
   <part label="A state should not ignore home rule issues that may have created a problem that they cannot respond to" start="1606" end="1708"/>
  </contributor>
  <contributor label="but that doesn't mean a state should ignore home rule issues that have created problems">
   <part label="but that doesn't mean a state should ignore home rule issues that have created problems" start="2400" end="2487"/>
  </contributor>
 </scu>
 <scu uid="50" label="Support is lacking for adopting California's gun laws as US standards">
  <contributor label="but support has been lacking">
   <part label="but support has been lacking" start="2314" end="2342"/>
  </contributor>
  <contributor label="and that S2's proposal has gained little support">
   <part label="and that S2's proposal has gained little support" start="3138" end="3186"/>
  </contributor>
  <contributor label="He cites others lack of support">
   <part label="He cites others lack of support" start="818" end="849"/>
  </contributor>
 </scu>
 <scu uid="48" label="Other states will not adopt gun controls like California">
  <contributor label="and he further believes that the reform will not take place">
   <part label="and he further believes that the reform will not take place" start="1778" end="1837"/>
  </contributor>
  <contributor label="but concedes that such a move is unlikely in some states">
   <part label="but concedes that such a move is unlikely in some states" start="3441" end="3497"/>
  </contributor>
 </scu>
 <scu uid="33" label="Gun control does not apply well in real life">
  <contributor label="However, gun control does not apply well in real life">
   <part label="However, gun control does not apply well in real life" start="338" end="391"/>
  </contributor>
 </scu>
 <scu uid="30" label="Guns should not be controlled and regulated as they are in California">
  <contributor label="S1 is against the controls on guns that California has put in place...They argue that guns should not be so strictly controlled and regulated as they are in California">
   <part label="They argue that guns should not be so strictly controlled and regulated as they are in California" start="95" end="192"/>
   <part label="S1 is against the controls on guns that California has put in place" start="26" end="93"/>
  </contributor>
 </scu>
 <scu uid="35" label="Illegal guns will bypass laws if gun laws are stricter">
  <contributor label="If gun laws are stricter, then illegal guns will still manage to bypass those laws, because the guns are illegal">
   <part label="If gun laws are stricter, then illegal guns will still manage to bypass those laws, because the guns are illegal" start="393" end="505"/>
  </contributor>
 </scu>
 <scu uid="51" label="One person is a gun owner">
  <contributor label="S2, who says he is a gun owner">
   <part label="S2, who says he is a gun owner" start="3500" end="3530"/>
  </contributor>
 </scu>
 <scu uid="49" label="Other states do not like the California laws">
  <contributor label="because other states do not like the California laws">
   <part label="because other states do not like the California laws" start="1838" end="1890"/>
  </contributor>
 </scu>
 <scu uid="36" label="Stricter gun control is necessary">
  <contributor label="S2 argues that stricter gun control is necessary">
   <part label="S2 argues that stricter gun control is necessary" start="507" end="555"/>
  </contributor>
 </scu>
 <scu uid="37" label="Stricter gun control would have saved four policeman's lives">
  <contributor label="and would have saved four policeman's lives">
   <part label="and would have saved four policeman's lives" start="556" end="599"/>
  </contributor>
 </scu>
</pyramid>
