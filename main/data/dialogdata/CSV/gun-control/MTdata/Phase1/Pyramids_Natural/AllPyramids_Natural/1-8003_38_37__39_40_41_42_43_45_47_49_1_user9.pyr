<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 feels the US's problem isn't guns but the violent overzealous people who wield them. In the UK (where Author1 is from) cities of comparable size and population have far lower murder and gun violence rates than cities in the US. Guns are not the problem. A country is safer protected by a police force than vigilantes. Zealots like Author2 happily surrender their rights to the Patriot Act but only feel personal rights are infringed on if guns are regulated? Guns regulation does not create a police state. Author2 is ignorant of the history of firearms and/or all of its surrounding issues. </line>
  <line></line>
  <line>Author2 feels criminals and a weak justice system are the problems in America. The real threat is those who easily relinquish their rights. Author1 is incorrectly labeling those who claim their 2A right to self-defense as vigilantes or &quot;gun nuts&quot;. How can Author2 feel safe in a place where only the police and military control firearms like in the book '1984'? The loss of American rights would result in a tyrannical police state.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 points out that British Canada won the war of 1812. Guns are not the major issue in the US, it is the people who don't know how to use guns safely. In Britain people can walk safely through major cities without fear of crime or murder. Guns are rarely used to commit murder in Britain, and the majority of murders are drunken altercations. Vigilantism is not a better alternative than a trained police force. Taking the law into your own hands is the definition of vigilantism, and the claimed defense of US rights is being eroded daily by the Patriot Act. Since there is no popular uprising, it is clear that guns do not help protect anyone's rights.</line>
  <line>Author2 argues that the US didn't lose the War of 1812, and that the issues with gun ownership lie in criminals and a weak justice system. Americans do not blindly surrender their rights. Britain is a police state with their excessive regulations and surveillance. Violent criminals should not be left on the streets.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 believes the problem in America is not guns, it is gun lovers. In his country he can walk though his whole city at night without a qualm. Gun murders are unusual, and so is gun crime. Occasionally someone might get stabbed, but even that is fairly rare. He does not think vigilantism is better than policing. He thinks policing is better. It is sensible and reasonable. Vigilantism is not. As a Canadian, he can see that the root cause of gun violence is unregulated guns. Author2 believes he simply does not want the government to take his rights. He believes that he must not knuckle under to government control, and he especially must not surrender his right to carry a gun. Exercising his rights is not vigilantism. Look at Britain. The authorities there cosset the citizens. They all live under surveillance. They are forbidden to defend themselves with guns. The authorities trample their rights constantly. Freed criminals cause violence, guns do not. When we give up our rights, we create a police state.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 claims guns aren't so much the problem in the US as the people who think they have to have them. Author2 says it is not the pro-gun people who are the problem, but the criminals who misuse guns and the court system who does not properly punish criminals. Author1 says he lives in a large city in Canada, and says they have 6-7 murders per year. Author2 says the problem is not the pro-gun people, but the ones who don't understand why people don't want to surrender their rights and be controlled by the government and police. Author1 ask why is an armed group of citizens better than a police force. Author2 says being able to defend oneself is not the same as being a vigilante. Author1 argues it does because you are taking the law in your own hands. He mentions how personal rights are being violated by the Patriot Act. Author2 argues being unarmed creates a police state. Author1 says Author2 is arguing citizens should be able to act as police.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 talks about Canada burning down the White house and thinks the problem isn't guns, the problem is gun nuts. Author2 says just because we didn't finish the war the first day doesn't mean we lost. It took time to get the British out. Author2 doesn't think we're the problem, the weak justice system is. Author1 thinks Author2 is ignorant of his own history and the War of 1812. He says he can't remember the last time a gun was used in a murder where he lives and the guns aren't the problem, but the US gun culture is. Author2 says if the US has a gun culture because they have shootings then Canada must have a knife culture with all the stabbings. Author1 asks why vigilantism is better than police force. Author2 says defending the 2nd amendment isn't vigilantism. Author1 says it's supporting vigilantism to say it's a surrender of rights to be policed. Author2 doesn't agree and doesn't support a police state. Author1 thinks citizens are acting as police and that's vigilantism.</line>
 </text>
 <scu uid="48" label="US's problem is people who wield guns">
  <contributor label="but the violent overzealous people who wield them">
   <part label="but the violent overzealous people who wield them" start="68" end="117"/>
  </contributor>
  <contributor label="it is the people who don't know how to use guns safely">
   <part label="it is the people who don't know how to use guns safely" start="1188" end="1242"/>
  </contributor>
  <contributor label="it is gun lovers">
   <part label="it is gun lovers" start="2146" end="2162"/>
  </contributor>
  <contributor label="but the criminals who misuse guns">
   <part label="but the criminals who misuse guns" start="3309" end="3342"/>
  </contributor>
  <contributor label="the problem is gun nuts...but the US gun culture is">
   <part label="the problem is gun nuts" start="4219" end="4242"/>
   <part label="but the US gun culture is" start="4627" end="4652"/>
  </contributor>
 </scu>
 <scu uid="47" label="US's problem isn't guns">
  <contributor label="Author1 feels the US's problem isn't guns...Guns are not the problem">
   <part label="Author1 feels the US's problem isn't guns" start="26" end="67"/>
   <part label="Guns are not the problem" start="262" end="286"/>
  </contributor>
  <contributor label="Guns are not the major issue in the US">
   <part label="Guns are not the major issue in the US" start="1148" end="1186"/>
  </contributor>
  <contributor label="Author1 believes the problem in America is not guns...guns do not">
   <part label="Author1 believes the problem in America is not guns" start="2093" end="2144"/>
   <part label="guns do not" start="3047" end="3058"/>
  </contributor>
  <contributor label="Author1 claims guns aren't so much the problem in the US as the people who think they have to have them">
   <part label="Author1 claims guns aren't so much the problem in the US as the people who think they have to have them" start="3141" end="3244"/>
  </contributor>
  <contributor label="Author1 talks about Canada burning down the White house and thinks the problem isn't guns...He says he can't remember the last time a gun was used in a murder where he lives and the guns aren't the problem">
   <part label="Author1 talks about Canada burning down the White house and thinks the problem isn't guns" start="4128" end="4217"/>
   <part label="He says he can't remember the last time a gun was used in a murder where he lives and the guns aren't the problem" start="4512" end="4625"/>
  </contributor>
 </scu>
 <scu uid="65" label="Vigilantism is not a better alternative to a trained police force">
  <contributor label="Vigilantism is not a better alternative than a trained police force">
   <part label="Vigilantism is not a better alternative than a trained police force" start="1436" end="1503"/>
  </contributor>
  <contributor label="He does not think vigilantism is better than policing...He thinks policing is better...Vigilantism is not">
   <part label="He does not think vigilantism is better than policing" start="2355" end="2408"/>
   <part label="He thinks policing is better" start="2410" end="2438"/>
   <part label="Vigilantism is not" start="2471" end="2489"/>
  </contributor>
  <contributor label="A country is safer protected by a police force than vigilantes">
   <part label="A country is safer protected by a police force than vigilantes" start="288" end="350"/>
  </contributor>
  <contributor label="Author1 ask why is an armed group of citizens better than a police force">
   <part label="Author1 ask why is an armed group of citizens better than a police force" start="3676" end="3748"/>
  </contributor>
  <contributor label="Author1 asks why vigilantism is better than police force">
   <part label="Author1 asks why vigilantism is better than police force" start="4785" end="4841"/>
  </contributor>
 </scu>
 <scu uid="57" label="Must not relinquish rights">
  <contributor label="The real threat is those who easily relinquish their rights">
   <part label="The real threat is those who easily relinquish their rights" start="707" end="766"/>
  </contributor>
  <contributor label="Author2 believes he simply does not want the government to take his rights...He believes that he must not knuckle under to government control...and he especially must not surrender his right to carry a gun">
   <part label="He believes that he must not knuckle under to government control" start="2650" end="2714"/>
   <part label="and he especially must not surrender his right to carry a gun" start="2716" end="2777"/>
   <part label="Author2 believes he simply does not want the government to take his rights" start="2574" end="2648"/>
  </contributor>
  <contributor label="Americans do not blindly surrender their rights">
   <part label="Americans do not blindly surrender their rights" start="1887" end="1934"/>
  </contributor>
  <contributor label="but the ones who don't understand why people don't want to surrender their rights and be controlled by the government and police">
   <part label="but the ones who don't understand why people don't want to surrender their rights and be controlled by the government and police" start="3546" end="3674"/>
  </contributor>
 </scu>
 <scu uid="58" label="Those who claim 2nd Amendment rights are not vigilantes">
  <contributor label="Author1 is incorrectly labeling those who claim their 2A right to self-defense as vigilantes or &quot;gun nuts&quot;">
   <part label="Author1 is incorrectly labeling those who claim their 2A right to self-defense as vigilantes or &quot;gun nuts&quot;" start="768" end="874"/>
  </contributor>
  <contributor label="Author2 says being able to defend oneself is not the same as being a vigilante">
   <part label="Author2 says being able to defend oneself is not the same as being a vigilante" start="3750" end="3828"/>
  </contributor>
  <contributor label="Author2 says defending the 2nd amendment isn't vigilantism">
   <part label="Author2 says defending the 2nd amendment isn't vigilantism" start="4843" end="4901"/>
  </contributor>
  <contributor label="Exercising his rights is not vigilantism">
   <part label="Exercising his rights is not vigilantism" start="2779" end="2819"/>
  </contributor>
 </scu>
 <scu uid="56" label="US's problem is criminals and a weak justice system">
  <contributor label="Author2 feels criminals and a weak justice system are the problems in America">
   <part label="Author2 feels criminals and a weak justice system are the problems in America" start="628" end="705"/>
  </contributor>
  <contributor label="and that the issues with gun ownership lie in criminals and a weak justice system">
   <part label="and that the issues with gun ownership lie in criminals and a weak justice system" start="1804" end="1885"/>
  </contributor>
  <contributor label="but the criminals who misuse guns and the court system who does not properly punish criminals">
   <part label="but the criminals who misuse guns and the court system who does not properly punish criminals" start="3309" end="3402"/>
  </contributor>
  <contributor label="Author2 doesn't think we're the problem, the weak justice system is">
   <part label="Author2 doesn't think we're the problem, the weak justice system is" start="4368" end="4435"/>
  </contributor>
 </scu>
 <scu uid="62" label="Britain has lower rates of crime or murder">
  <contributor label="In the UK (where Author1 is from) cities of comparable size and population have far lower murder and gun violence rates than cities in the US">
   <part label="In the UK (where Author1 is from) cities of comparable size and population have far lower murder and gun violence rates than cities in the US" start="119" end="260"/>
  </contributor>
  <contributor label="In his country he can walk though his whole city at night without a qualm...Gun murders are unusual, and so is gun crime...Occasionally someone might get stabbed, but even that is fairly rare">
   <part label="Gun murders are unusual, and so is gun crime" start="2239" end="2283"/>
   <part label="Occasionally someone might get stabbed, but even that is fairly rare" start="2285" end="2353"/>
   <part label="In his country he can walk though his whole city at night without a qualm" start="2164" end="2237"/>
  </contributor>
  <contributor label="In Britain people can walk safely through major cities without fear of crime or murder">
   <part label="In Britain people can walk safely through major cities without fear of crime or murder" start="1244" end="1330"/>
  </contributor>
 </scu>
 <scu uid="70" label="Britain is a police state">
  <contributor label="Britain is a police state with their excessive regulations and surveillance">
   <part label="Britain is a police state with their excessive regulations and surveillance" start="1936" end="2011"/>
  </contributor>
  <contributor label="Look at Britain. The authorities there cosset the citizens. They all live under surveillance...They are forbidden to defend themselves with guns...The authorities trample their rights constantly">
   <part label="Look at Britain. The authorities there cosset the citizens. They all live under surveillance" start="2821" end="2913"/>
   <part label="The authorities trample their rights constantly" start="2966" end="3013"/>
   <part label="They are forbidden to defend themselves with guns" start="2915" end="2964"/>
  </contributor>
  <contributor label="How can Author2 feel safe in a place where only the police and military control firearms like in the book '1984'?">
   <part label="How can Author2 feel safe in a place where only the police and military control firearms like in the book '1984'?" start="876" end="989"/>
  </contributor>
 </scu>
 <scu uid="66" label="Taking the law into your own hands is the definition of vigilantism">
  <contributor label="Taking the law into your own hands is the definition of vigilantism">
   <part label="Taking the law into your own hands is the definition of vigilantism" start="1505" end="1572"/>
  </contributor>
  <contributor label="Author1 argues it does because you are taking the law in your own hands">
   <part label="Author1 argues it does because you are taking the law in your own hands" start="3830" end="3901"/>
  </contributor>
  <contributor label="Author1 says it's supporting vigilantism to say it's a surrender of rights to be policed...Author1 thinks citizens are acting as police and that's vigilantism">
   <part label="Author1 says it's supporting vigilantism to say it's a surrender of rights to be policed" start="4903" end="4991"/>
   <part label="Author1 thinks citizens are acting as police and that's vigilantism" start="5051" end="5118"/>
  </contributor>
 </scu>
 <scu uid="52" label="The claimed defense of rights is undermined by the Patriot Act">
  <contributor label="Zealots like Author2 happily surrender their rights to the Patriot Act">
   <part label="Zealots like Author2 happily surrender their rights to the Patriot Act" start="352" end="422"/>
  </contributor>
  <contributor label="and the claimed defense of US rights is being eroded daily by the Patriot Act">
   <part label="and the claimed defense of US rights is being eroded daily by the Patriot Act" start="1574" end="1651"/>
  </contributor>
  <contributor label="He mentions how personal rights are being violated by the Patriot Act">
   <part label="He mentions how personal rights are being violated by the Patriot Act" start="3903" end="3972"/>
  </contributor>
 </scu>
 <scu uid="60" label="The loss of rights would result in a tyrannical police state">
  <contributor label="The loss of American rights would result in a tyrannical police state">
   <part label="The loss of American rights would result in a tyrannical police state" start="990" end="1059"/>
  </contributor>
  <contributor label="When we give up our rights, we create a police state">
   <part label="When we give up our rights, we create a police state" start="3060" end="3112"/>
  </contributor>
  <contributor label="Author2 argues being unarmed creates a police state">
   <part label="Author2 argues being unarmed creates a police state" start="3974" end="4025"/>
  </contributor>
 </scu>
 <scu uid="55" label="Know nothing about history">
  <contributor label="Author2 is ignorant of the history of firearms and/or all of its surrounding issues">
   <part label="Author2 is ignorant of the history of firearms and/or all of its surrounding issues" start="541" end="624"/>
  </contributor>
  <contributor label="Author1 thinks Author2 is ignorant of his own history and the War of 1812">
   <part label="Author1 thinks Author2 is ignorant of his own history and the War of 1812" start="4437" end="4510"/>
  </contributor>
 </scu>
 <scu uid="68" label="US didn't lose the War of 1812">
  <contributor label="Author2 argues that the US didn't lose the War of 1812">
   <part label="Author2 argues that the US didn't lose the War of 1812" start="1748" end="1802"/>
  </contributor>
  <contributor label="Author2 says just because we didn't finish the war the first day doesn't mean we lost">
   <part label="Author2 says just because we didn't finish the war the first day doesn't mean we lost" start="4244" end="4329"/>
  </contributor>
 </scu>
 <scu uid="71" label="Violent criminals should not be left on the streets">
  <contributor label="Violent criminals should not be left on the streets">
   <part label="Violent criminals should not be left on the streets" start="2013" end="2064"/>
  </contributor>
  <contributor label="Freed criminals cause violence">
   <part label="Freed criminals cause violence" start="3015" end="3045"/>
  </contributor>
 </scu>
 <scu uid="61" label="British Canada won the war of 1812">
  <contributor label="Author1 points out that British Canada won the war of 1812">
   <part label="Author1 points out that British Canada won the war of 1812" start="1088" end="1146"/>
  </contributor>
 </scu>
 <scu uid="83" label="Canada has a knife culture with all the stabbings">
  <contributor label="then Canada must have a knife culture with all the stabbings">
   <part label="then Canada must have a knife culture with all the stabbings" start="4723" end="4783"/>
  </contributor>
 </scu>
 <scu uid="80" label="Citizens should be able to act as police">
  <contributor label="Author1 says Author2 is arguing citizens should be able to act as police">
   <part label="Author1 says Author2 is arguing citizens should be able to act as police" start="4027" end="4099"/>
  </contributor>
 </scu>
 <scu uid="84" label="Do not support a police state">
  <contributor label="Author2 doesn't agree and doesn't support a police state">
   <part label="Author2 doesn't agree and doesn't support a police state" start="4993" end="5049"/>
  </contributor>
 </scu>
 <scu uid="63" label="Guns are rarely used to commit murder in Britain">
  <contributor label="Guns are rarely used to commit murder in Britain">
   <part label="Guns are rarely used to commit murder in Britain" start="1332" end="1380"/>
  </contributor>
 </scu>
 <scu uid="67" label="Guns do not help protect anyone's rights">
  <contributor label="Since there is no popular uprising, it is clear that guns do not help protect anyone's rights">
   <part label="Since there is no popular uprising, it is clear that guns do not help protect anyone's rights" start="1653" end="1746"/>
  </contributor>
 </scu>
 <scu uid="54" label="Guns regulation does not create a police state">
  <contributor label="Guns regulation does not create a police state">
   <part label="Guns regulation does not create a police state" start="493" end="539"/>
  </contributor>
 </scu>
 <scu uid="81" label="It took time to get the British out">
  <contributor label="It took time to get the British out">
   <part label="It took time to get the British out" start="4331" end="4366"/>
  </contributor>
 </scu>
 <scu uid="79" label="Large city in Canada has low murder rate">
  <contributor label="Author1 says he lives in a large city in Canada, and says they have 6-7 murders per year">
   <part label="Author1 says he lives in a large city in Canada, and says they have 6-7 murders per year" start="3404" end="3492"/>
  </contributor>
 </scu>
 <scu uid="73" label="Policing is sensible and reasonable">
  <contributor label="It is sensible and reasonable">
   <part label="It is sensible and reasonable" start="2440" end="2469"/>
  </contributor>
 </scu>
 <scu uid="78" label="Pro-gun people are not the problem">
  <contributor label="Author2 says it is not the pro-gun people who are the problem...Author2 says the problem is not the pro-gun people">
   <part label="Author2 says it is not the pro-gun people who are the problem" start="3246" end="3307"/>
   <part label="Author2 says the problem is not the pro-gun people" start="3494" end="3544"/>
  </contributor>
 </scu>
 <scu uid="53" label="Rights are infringed on if guns are regulated">
  <contributor label="but only feel personal rights are infringed on if guns are regulated?">
   <part label="but only feel personal rights are infringed on if guns are regulated?" start="423" end="492"/>
  </contributor>
 </scu>
 <scu uid="75" label="The cause of gun violence is unregulated guns">
  <contributor label="As a Canadian, he can see that the root cause of gun violence is unregulated guns">
   <part label="As a Canadian, he can see that the root cause of gun violence is unregulated guns" start="2491" end="2572"/>
  </contributor>
 </scu>
 <scu uid="64" label="The majority of murders in Britain are drunken altercations">
  <contributor label="and the majority of murders are drunken altercations">
   <part label="and the majority of murders are drunken altercations" start="1382" end="1434"/>
  </contributor>
 </scu>
 <scu uid="82" label="The US has a gun culture because they have shootings">
  <contributor label="Author2 says if the US has a gun culture because they have shootings">
   <part label="Author2 says if the US has a gun culture because they have shootings" start="4654" end="4722"/>
  </contributor>
 </scu>
</pyramid>
