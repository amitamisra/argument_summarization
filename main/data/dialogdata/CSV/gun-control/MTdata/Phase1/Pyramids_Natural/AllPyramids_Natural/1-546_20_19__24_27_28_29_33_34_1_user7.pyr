<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>S2 tells S1 that no one is safe on a plane even if they have a gun, and that a good citizen will not play fair with terrorists. S1 asserts that passengers cannot stop a terrorist from hijacking a plane, and it would not make him feel secure knowing the passengers had guns. The plane provides enough security, but S2 tells S1 that this is trading your Second Amendment rights. S1 refutes this by stating that these are carefully planned attacks, and that bringing guns on planes poses far more threats than securities. S2 states that self-defense should not be the government's decision, because the terrorist win when you trade your freedom. S1 believes that people should not be put in danger to save oneself.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>S1 posts about ten terrorists simply ... S2 says it's not simple. He argues terrorists can't see who is armed or who is behind them. Citizens would be striking from behind with concealed weapons. People should be allowed control over their destiny by being able to fight back. Rights should not be taken away, except for certain groups like crazy people. S1 says the terrorists will outnumber the citizens. He says airlines are good at safety. He says others shouldn't be able to decide his fate by their fighting back. S2 says S1 is giving up his rights. S1 says guns are more a security risk than protection. S2 says giving up rights means the terrorists win. S1 says guns put people in danger.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>S2 asks what's so simple about terrorists having guns and shooting someone with one. They say that terrorists can't tell who is carrying guns. They say it's hard to watch your back even when armed, but that good citizens won't play fair with a terrorist. They suggest that someone could use tactics to fight a terrorist. They think gun ownership is about controlling your destiny. They don't think lack of trust is enough to infringe on rights. S1 says terrorists would also use strategy. They say terrorists are trained and plan, and that regular people don't have chance. S2 says self defense is a personal decision the government shouldn't take away. S1 says guns endanger lives and compares it to carrying a bomb.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>S1 sees the situation as a clear argument for gun ownership. S2 doesn't agree because there is no fairness in dealing with a terrorist. S2 does not believe that rights should be restricted based on trust as the responsibility for one's destiny lies with the individual himself. According to S2, rights are waived only for convicted criminals. S1 disagrees as the planes are for business purposes, and guns shouldn't be allowed. S2 is offended by the acceptance of gun ownership restriction when many ancestors perished if only to ensure that the 2nd Amendment stands. S1 disapproves the claim once again by pointing out that the presence of weapons only increases instability instead of providing security. S2 sees this as a restriction of an individual right.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>S1 says stay open minded. S2 says people have a right to make their own defense when under attack. Only criminals and those declared insane should lose the right to defend themselves.S1 says trained and determined terrorists would overwhelm armed citizens. And a plane full of people with guns wouldn't be safe. Let the airline do security. S2 says S1 is giving up his Second Amendment rights. S1 says citizens with guns would be more dangerous than terrorists. S2 says giving up gun rights is letting the terrorists win. S1 says carrying guns puts thousands in danger. The world has changed, and this extremism is not what the founding fathers had in mind. S2 says so if an airline allows guns, just choose another.</line>
 </text>
 <scu uid="52" label="Giving up Second Amendment rights">
  <contributor label="S2 says S1 is giving up his Second Amendment rights">
   <part label="S2 says S1 is giving up his Second Amendment rights" start="3363" end="3414"/>
  </contributor>
  <contributor label="S2 tells S1 that this is trading your Second Amendment rights">
   <part label="S2 tells S1 that this is trading your Second Amendment rights" start="340" end="401"/>
  </contributor>
  <contributor label="S2 does not believe that rights should be restricted based on trust...many ancestors perished if only to ensure that the 2nd Amendment stands...S2 sees this as a restriction of an individual right">
   <part label="many ancestors perished if only to ensure that the 2nd Amendment stands" start="2729" end="2800"/>
   <part label="S2 sees this as a restriction of an individual right" start="2941" end="2993"/>
   <part label="S2 does not believe that rights should be restricted based on trust" start="2370" end="2437"/>
  </contributor>
  <contributor label="Rights should not be taken away...S2 says S1 is giving up his rights">
   <part label="S2 says S1 is giving up his rights" start="1285" end="1319"/>
   <part label="Rights should not be taken away" start="1042" end="1073"/>
  </contributor>
  <contributor label="They don't think lack of trust is enough to infringe on rights">
   <part label="They don't think lack of trust is enough to infringe on rights" start="1870" end="1932"/>
  </contributor>
 </scu>
 <scu uid="56" label="Gun ownership is about controlling destiny">
  <contributor label="They think gun ownership is about controlling your destiny...S2 says self defense is a personal decision the government shouldn't take away">
   <part label="They think gun ownership is about controlling your destiny" start="1810" end="1868"/>
   <part label="S2 says self defense is a personal decision the government shouldn't take away" start="2063" end="2141"/>
  </contributor>
  <contributor label="as the responsibility for one's destiny lies with the individual himself">
   <part label="as the responsibility for one's destiny lies with the individual himself" start="2438" end="2510"/>
  </contributor>
  <contributor label="People should be allowed control over their destiny by being able to fight back">
   <part label="People should be allowed control over their destiny by being able to fight back" start="961" end="1040"/>
  </contributor>
  <contributor label="S2 says people have a right to make their own defense when under attack">
   <part label="S2 says people have a right to make their own defense when under attack" start="3048" end="3119"/>
  </contributor>
  <contributor label="S2 states that self-defense should not be the government's decision">
   <part label="S2 states that self-defense should not be the government's decision" start="545" end="612"/>
  </contributor>
 </scu>
 <scu uid="63" label="Guns are more a security risk than protection">
  <contributor label="S1 says guns are more a security risk than protection...S1 says guns put people in danger">
   <part label="S1 says guns are more a security risk than protection" start="1321" end="1374"/>
   <part label="S1 says guns put people in danger" start="1427" end="1460"/>
  </contributor>
  <contributor label="it would not make him feel secure knowing the passengers had guns...bringing guns on planes poses far more threats than securities">
   <part label="it would not make him feel secure knowing the passengers had guns" start="233" end="298"/>
   <part label="bringing guns on planes poses far more threats than securities" start="481" end="543"/>
  </contributor>
  <contributor label="And a plane full of people with guns wouldn't be safe...S1 says citizens with guns would be more dangerous than terrorists...S1 says carrying guns puts thousands in danger">
   <part label="And a plane full of people with guns wouldn't be safe" start="3279" end="3332"/>
   <part label="S1 says citizens with guns would be more dangerous than terrorists" start="3416" end="3482"/>
   <part label="S1 says carrying guns puts thousands in danger" start="3544" end="3590"/>
  </contributor>
  <contributor label="S1 disapproves the claim once again by pointing out that the presence of weapons only increases instability instead of providing security">
   <part label="S1 disapproves the claim once again by pointing out that the presence of weapons only increases instability instead of providing security" start="2802" end="2939"/>
  </contributor>
  <contributor label="S1 says guns endanger lives">
   <part label="S1 says guns endanger lives" start="2143" end="2170"/>
  </contributor>
 </scu>
 <scu uid="67" label="Terrorists would also use strategy">
  <contributor label="S1 says terrorists would also use strategy...They say terrorists are trained and plan...and that regular people don't have chance">
   <part label="S1 says terrorists would also use strategy" start="1934" end="1976"/>
   <part label="and that regular people don't have chance" start="2020" end="2061"/>
   <part label="They say terrorists are trained and plan" start="1978" end="2018"/>
  </contributor>
  <contributor label="S1 asserts that passengers cannot stop a terrorist from hijacking a plane...S1 refutes this by stating that these are carefully planned attacks">
   <part label="S1 asserts that passengers cannot stop a terrorist from hijacking a plane" start="154" end="227"/>
   <part label="S1 refutes this by stating that these are carefully planned attacks" start="403" end="470"/>
  </contributor>
  <contributor label="S1 says the terrorists will outnumber the citizens">
   <part label="S1 says the terrorists will outnumber the citizens" start="1120" end="1170"/>
  </contributor>
  <contributor label="S1 says trained and determined terrorists would overwhelm armed citizens">
   <part label="S1 says trained and determined terrorists would overwhelm armed citizens" start="3205" end="3277"/>
  </contributor>
 </scu>
 <scu uid="53" label="Airlines are good at safety">
  <contributor label="He says airlines are good at safety">
   <part label="He says airlines are good at safety" start="1172" end="1207"/>
  </contributor>
  <contributor label="The plane provides enough security">
   <part label="The plane provides enough security" start="300" end="334"/>
  </contributor>
  <contributor label="Let the airline do security">
   <part label="Let the airline do security" start="3334" end="3361"/>
  </contributor>
 </scu>
 <scu uid="64" label="Giving up rights means the terrorists win">
  <contributor label="S2 says giving up rights means the terrorists win">
   <part label="S2 says giving up rights means the terrorists win" start="1376" end="1425"/>
  </contributor>
  <contributor label="the terrorist win when you trade your freedom">
   <part label="the terrorist win when you trade your freedom" start="622" end="667"/>
  </contributor>
  <contributor label="S2 says giving up gun rights is letting the terrorists win">
   <part label="S2 says giving up gun rights is letting the terrorists win" start="3484" end="3542"/>
  </contributor>
 </scu>
 <scu uid="58" label="Only criminals and those declared insane should lose the right to defend themselves">
  <contributor label="except for certain groups like crazy people">
   <part label="except for certain groups like crazy people" start="1075" end="1118"/>
  </contributor>
  <contributor label="According to S2, rights are waived only for convicted criminals">
   <part label="According to S2, rights are waived only for convicted criminals" start="2512" end="2575"/>
  </contributor>
  <contributor label="Only criminals and those declared insane should lose the right to defend themselves">
   <part label="Only criminals and those declared insane should lose the right to defend themselves" start="3121" end="3204"/>
  </contributor>
 </scu>
 <scu uid="48" label="Terrorists having guns and shooting is not simple">
  <contributor label="S2 says it's not simple">
   <part label="S2 says it's not simple" start="806" end="829"/>
  </contributor>
  <contributor label="S2 doesn't agree">
   <part label="S2 doesn't agree" start="2295" end="2311"/>
  </contributor>
  <contributor label="S2 asks what's so simple about terrorists having guns and shooting someone with one">
   <part label="S2 asks what's so simple about terrorists having guns and shooting someone with one" start="1489" end="1572"/>
  </contributor>
 </scu>
 <scu uid="49" label="Trained and determined terrorists would overwhelm armed citizens">
  <contributor label="good citizens won't play fair with a terrorist">
   <part label="good citizens won't play fair with a terrorist" start="1696" end="1742"/>
  </contributor>
  <contributor label="there is no fairness in dealing with a terrorist">
   <part label="there is no fairness in dealing with a terrorist" start="2320" end="2368"/>
  </contributor>
  <contributor label="a good citizen will not play fair with terrorists">
   <part label="a good citizen will not play fair with terrorists" start="103" end="152"/>
  </contributor>
 </scu>
 <scu uid="70" label="People should not be put in danger to save oneself">
  <contributor label="S1 believes that people should not be put in danger to save oneself">
   <part label="S1 believes that people should not be put in danger to save oneself" start="669" end="736"/>
  </contributor>
  <contributor label="He says others shouldn't be able to decide his fate by their fighting back">
   <part label="He says others shouldn't be able to decide his fate by their fighting back" start="1209" end="1283"/>
  </contributor>
 </scu>
 <scu uid="47" label="Situation is clear for gun ownership">
  <contributor label="S1 posts about ten terrorists simply">
   <part label="S1 posts about ten terrorists simply" start="765" end="801"/>
  </contributor>
  <contributor label="S1 sees the situation as a clear argument for gun ownership">
   <part label="S1 sees the situation as a clear argument for gun ownership" start="2234" end="2293"/>
  </contributor>
 </scu>
 <scu uid="66" label="Someone could use tactics to fight a terrorist">
  <contributor label="They say that terrorists can't tell who is carrying guns...They say it's hard to watch your back even when armed...They suggest that someone could use tactics to fight a terrorist">
   <part label="They suggest that someone could use tactics to fight a terrorist" start="1744" end="1808"/>
   <part label="They say that terrorists can't tell who is carrying guns" start="1574" end="1630"/>
   <part label="They say it's hard to watch your back even when armed" start="1632" end="1685"/>
  </contributor>
  <contributor label="He argues terrorists can't see who is armed or who is behind them...Citizens would be striking from behind with concealed weapons">
   <part label="Citizens would be striking from behind with concealed weapons" start="898" end="959"/>
   <part label="He argues terrorists can't see who is armed or who is behind them" start="831" end="896"/>
  </contributor>
 </scu>
 <scu uid="54" label="Acceptance of gun ownership restriction is offending">
  <contributor label="S2 is offended by the acceptance of gun ownership restriction">
   <part label="S2 is offended by the acceptance of gun ownership restriction" start="2662" end="2723"/>
  </contributor>
 </scu>
 <scu uid="68" label="Compares to carrying a bomb">
  <contributor label="compares it to carrying a bomb">
   <part label="compares it to carrying a bomb" start="2175" end="2205"/>
  </contributor>
 </scu>
 <scu uid="50" label="No one is safe on a plane even if they have a gun">
  <contributor label="S2 tells S1 that no one is safe on a plane even if they have a gun">
   <part label="S2 tells S1 that no one is safe on a plane even if they have a gun" start="26" end="92"/>
  </contributor>
 </scu>
 <scu uid="69" label="Planes are for business purposes">
  <contributor label="S1 disagrees as the planes are for business purposes">
   <part label="S1 disagrees as the planes are for business purposes" start="2577" end="2629"/>
  </contributor>
 </scu>
 <scu uid="61" label="If an airline allows guns, choose another">
  <contributor label="S2 says so if an airline allows guns, just choose another">
   <part label="S2 says so if an airline allows guns, just choose another" start="3680" end="3737"/>
  </contributor>
 </scu>
 <scu uid="71" label="Extremism is not what the founding fathers had in mind">
  <contributor label="S1 says stay open minded...The world has changed, and this extremism is not what the founding fathers had in mind">
   <part label="The world has changed, and this extremism is not what the founding fathers had in mind" start="3592" end="3678"/>
   <part label="S1 says stay open minded" start="3022" end="3046"/>
  </contributor>
 </scu>
</pyramid>
