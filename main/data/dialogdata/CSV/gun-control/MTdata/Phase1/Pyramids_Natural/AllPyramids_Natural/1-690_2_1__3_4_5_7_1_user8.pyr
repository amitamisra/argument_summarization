<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>S1 asks S1 to view threads that start with &quot;I carried a gun&quot; and says that how their actions didn't endanger anyone, increase crime, or violate rights. They ask S2 if it's okay to carry guns. S2 asks if it's okay to carry a nuclear weapon. S1 challenges S2 to find a nuclear weapon for sale. S1 asks how they could get a gun if they were banned. S2 says that since the US has nukes, they should be allowed one too. They say that the goal should be to repeal a ban, not break it. S1 implies nukes aren't comparable since there are fewer vendors. S2 says that the free market may allow nuclear weapons to be bought if there was a demand.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>S1 states that nuclear weapons are banned in the US, which leads S2 to respond that citizens should have the same access to nuclear weapons that the US does. The goal should be to repeal the ban, not try to break it. S1 states that since they cannot be sold, they cannot be banned. Vendors sell to the Department of Defense, and no vendor would sell them to people. S2 clarifies by defining the various bans on guns and gun ownership that applies to S1, but his example is referring to the ban that is placed on owning and bearing nuclear weapons. He imagines a situation in which it would be profitable to sell nuclear weapons to citizens if it became legal to own them.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>S1 received no posts on threads about gun carrying endangering people, increasing crime or violating rights. So he asks rhetorically if it's okay to carry guns. S2 answers it's as okay as a country storing a nuclear weapon in a foreign city. Carrying guns is inherently dangerous.S1 replies that nukes are effectively banned, invalidating the comparison. His unanswered posts prove guns needn't be banned. S2 says no citizen should have nukes. And guns are similarly dangerous. S1 says nobody sells individuals nukes. S2 says he talks about guns with respect to his opponent's argument; about nuclear weapons with respect to his own. Makers would sell nukes, if allowed, for profit and power. So, he asks rhetorically, shouldn't people be allowed to buy them?</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>S1 asks if it's ok that he has a gun because no one has said how it endangers anyone, increases crime, or violate's anyone's rights. S2 asks if it's ok for him to have a nuclear weapon or for France to have one stored in Chicago. S1 says France has the same rights as a US citizen and says finding a nuclear weapon for sale would be more relevant. He asks why ban guns if the same conditions apply. S1 asks what ban he's talking about. He says the only vendor is happy to have it's only customer be the Department of Defense. He agrees with the logic but it doesn't make sense. S2 says he is talking about the ban to own nuclear weapons.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>S1 refers to previous posts about carrying a gun, and the lack of replies as to whether it endangered anyone, increased crime, or violated rights. He asks is it okay to carry a gun then. S2 asks if it is okay for him to own a nuclear weapon. S1 says the question is irrelevant since nuclear weapons are not for sale. S2 states if the government is allowed to own a nuclear weapon, he should be too. If they are banned, he should not be able to own one. S1 argues there is no ban because they are not for sale. He says there is no seller who would sell one. S2 argues sellers would sell them if they were allowed to.</line>
 </text>
 <scu uid="8" label="is it okay to have a nuclear weapon ">
  <contributor label="S2 asks if it is okay for him to own a nuclear weapon">
   <part label="S2 asks if it is okay for him to own a nuclear weapon" start="3027" end="3080"/>
  </contributor>
  <contributor label="S2 asks if it's ok for him to have a nuclear weapon">
   <part label="S2 asks if it's ok for him to have a nuclear weapon" start="2308" end="2359"/>
  </contributor>
  <contributor label="S2 asks if it's okay to carry a nuclear weapon">
   <part label="S2 asks if it's okay to carry a nuclear weapon" start="218" end="264"/>
  </contributor>
  <contributor label="So, he asks rhetorically, shouldn't people be allowed to buy them?">
   <part label="So, he asks rhetorically, shouldn't people be allowed to buy them?" start="2081" end="2147"/>
  </contributor>
 </scu>
 <scu uid="7" label="if it's okay to carry guns">
  <contributor label="They ask S2 if it's okay to carry guns">
   <part label="They ask S2 if it's okay to carry guns" start="178" end="216"/>
  </contributor>
  <contributor label="So he asks rhetorically if it's okay to carry guns">
   <part label="So he asks rhetorically if it's okay to carry guns" start="1497" end="1547"/>
  </contributor>
  <contributor label="S1 asks if it's ok that he has a gun">
   <part label="S1 asks if it's ok that he has a gun" start="2175" end="2211"/>
  </contributor>
  <contributor label="He asks is it okay to carry a gun then">
   <part label="He asks is it okay to carry a gun then" start="2987" end="3025"/>
  </contributor>
 </scu>
 <scu uid="3" label="No one says that gun carrying endangers anyone, increases crime or violates rights.">
  <contributor label="S1 refers to previous posts about carrying a gun, and the lack of replies as to whether it endangered anyone, increased crime, or violated rights">
   <part label="S1 refers to previous posts about carrying a gun, and the lack of replies as to whether it endangered anyone, increased crime, or violated rights" start="2840" end="2985"/>
  </contributor>
  <contributor label="S1 asks S1 to view threads that start with &quot;I carried a gun&quot; and says that how their actions didn't endanger anyone, increase crime, or violate rights">
   <part label="S1 asks S1 to view threads that start with &quot;I carried a gun&quot; and says that how their actions didn't endanger anyone, increase crime, or violate rights" start="26" end="176"/>
  </contributor>
  <contributor label="S1 received no posts on threads about gun carrying endangering people, increasing crime or violating rights">
   <part label="S1 received no posts on threads about gun carrying endangering people, increasing crime or violating rights" start="1388" end="1495"/>
  </contributor>
  <contributor label="no one has said how it endangers anyone, increases crime, or violate's anyone's rights">
   <part label="no one has said how it endangers anyone, increases crime, or violate's anyone's rights" start="2220" end="2306"/>
  </contributor>
 </scu>
 <scu uid="10" label="Nuke sellers would sell if they were allowed.">
  <contributor label="S2 says that the free market may allow nuclear weapons to be bought if there was a demand">
   <part label="S2 says that the free market may allow nuclear weapons to be bought if there was a demand" start="571" end="660"/>
  </contributor>
  <contributor label="He imagines a situation in which it would be profitable to sell nuclear weapons to citizens if it became legal to own them">
   <part label="He imagines a situation in which it would be profitable to sell nuclear weapons to citizens if it became legal to own them" start="1237" end="1359"/>
  </contributor>
  <contributor label="Makers would sell nukes, if allowed, for profit and power">
   <part label="Makers would sell nukes, if allowed, for profit and power" start="2022" end="2079"/>
  </contributor>
  <contributor label="S2 argues sellers would sell them if they were allowed to">
   <part label="S2 argues sellers would sell them if they were allowed to" start="3397" end="3454"/>
  </contributor>
 </scu>
 <scu uid="21" label=" ban to own nuclear weapons.">
  <contributor label="S2 says he is talking about the ban to own nuclear weapons">
   <part label="S2 says he is talking about the ban to own nuclear weapons" start="2753" end="2811"/>
  </contributor>
  <contributor label="his example is referring to the ban that is placed on owning and bearing nuclear weapons">
   <part label="his example is referring to the ban that is placed on owning and bearing nuclear weapons" start="1147" end="1235"/>
  </contributor>
  <contributor label="about nuclear weapons with respect to his own">
   <part label="about nuclear weapons with respect to his own" start="1975" end="2020"/>
  </contributor>
 </scu>
 <scu uid="2" label="Citizens should have the same access to nuclear weapons that the US does.">
  <contributor label="which leads S2 to respond that citizens should have the same access to nuclear weapons that the US does">
   <part label="which leads S2 to respond that citizens should have the same access to nuclear weapons that the US does" start="742" end="845"/>
  </contributor>
  <contributor label="S2 says that since the US has nukes, they should be allowed one too">
   <part label="S2 says that since the US has nukes, they should be allowed one too" start="372" end="439"/>
  </contributor>
  <contributor label="S2 states if the government is allowed to own a nuclear weapon, he should be too">
   <part label="S2 states if the government is allowed to own a nuclear weapon, he should be too" start="3157" end="3237"/>
  </contributor>
 </scu>
 <scu uid="9" label="Nobody would sell nukes.">
  <contributor label="He says there is no seller who would sell one">
   <part label="He says there is no seller who would sell one" start="3350" end="3395"/>
  </contributor>
  <contributor label="S1 says nobody sells individuals nukes">
   <part label="S1 says nobody sells individuals nukes" start="1866" end="1904"/>
  </contributor>
  <contributor label="no vendor would sell them to people">
   <part label="no vendor would sell them to people" start="1018" end="1053"/>
  </contributor>
 </scu>
 <scu uid="6" label="The question is irrelevant since nuclear weapons are not for sale.">
  <contributor label="S1 says the question is irrelevant since nuclear weapons are not for sale">
   <part label="S1 says the question is irrelevant since nuclear weapons are not for sale" start="3082" end="3155"/>
  </contributor>
  <contributor label="S1 implies nukes aren't comparable since there are fewer vendors">
   <part label="S1 implies nukes aren't comparable since there are fewer vendors" start="505" end="569"/>
  </contributor>
  <contributor label="S1 replies that nukes are effectively banned, invalidating the comparison">
   <part label="S1 replies that nukes are effectively banned, invalidating the comparison" start="1668" end="1741"/>
  </contributor>
 </scu>
 <scu uid="13" label="A country stores nuclear weapon in a foreign city.">
  <contributor label="S2 answers it's as okay as a country storing a nuclear weapon in a foreign city">
   <part label="S2 answers it's as okay as a country storing a nuclear weapon in a foreign city" start="1549" end="1628"/>
  </contributor>
  <contributor label="or for France to have one stored in Chicago">
   <part label="or for France to have one stored in Chicago" start="2360" end="2403"/>
  </contributor>
 </scu>
 <scu uid="29" label="Defines bans on guns and gun ownership.">
  <contributor label="S2 clarifies by defining the various bans on guns and gun ownership that applies to S1">
   <part label="S2 clarifies by defining the various bans on guns and gun ownership that applies to S1" start="1055" end="1141"/>
  </contributor>
  <contributor label="S2 says he talks about guns with respect to his opponent's argument;">
   <part label="S2 says he talks about guns with respect to his opponent's argument;" start="1906" end="1974"/>
  </contributor>
 </scu>
 <scu uid="16" label="Finding a nuclear weapon for sale would be more relevant.">
  <contributor label="says finding a nuclear weapon for sale would be more relevant">
   <part label="says finding a nuclear weapon for sale would be more relevant" start="2460" end="2521"/>
  </contributor>
  <contributor label="challenges S2 to find a nuclear weapon for sale">
   <part label="challenges S2 to find a nuclear weapon for sale" start="269" end="316"/>
  </contributor>
 </scu>
 <scu uid="12" label="If guns are banned, he should not be able to own one.">
  <contributor label="S1 asks how they could get a gun if they were banned">
   <part label="S1 asks how they could get a gun if they were banned" start="318" end="370"/>
  </contributor>
  <contributor label="If they are banned, he should not be able to own one">
   <part label="If they are banned, he should not be able to own one" start="3239" end="3291"/>
  </contributor>
 </scu>
 <scu uid="5" label="The goal should be to repeal a ban, not to break it.">
  <contributor label="They say that the goal should be to repeal a ban, not break it">
   <part label="They say that the goal should be to repeal a ban, not break it" start="441" end="503"/>
  </contributor>
  <contributor label="The goal should be to repeal the ban, not try to break it">
   <part label="The goal should be to repeal the ban, not try to break it" start="847" end="904"/>
  </contributor>
 </scu>
 <scu uid="11" label="There is no ban because  guns are not for sale.">
  <contributor label="S1 argues there is no ban because they are not for sale">
   <part label="S1 argues there is no ban because they are not for sale" start="3293" end="3348"/>
  </contributor>
  <contributor label="S1 states that since they cannot be sold, they cannot be banned">
   <part label="S1 states that since they cannot be sold, they cannot be banned" start="906" end="969"/>
  </contributor>
 </scu>
 <scu uid="14" label="Vendors sell to the Department of Defense.">
  <contributor label="Vendors sell to the Department of Defense">
   <part label="Vendors sell to the Department of Defense" start="971" end="1012"/>
  </contributor>
  <contributor label="He says the only vendor is happy to have it's only customer be the Department of Defense">
   <part label="He says the only vendor is happy to have it's only customer be the Department of Defense" start="2611" end="2699"/>
  </contributor>
 </scu>
 <scu uid="20" label="France has the same rights as a US citizen.">
  <contributor label="S1 says France has the same rights as a US citizen">
   <part label="S1 says France has the same rights as a US citizen" start="2405" end="2455"/>
  </contributor>
 </scu>
 <scu uid="15" label="Guns are dangerous.">
  <contributor label="Carrying guns is inherently dangerous...And guns are similarly dangerous">
   <part label="Carrying guns is inherently dangerous" start="1630" end="1667"/>
   <part label="And guns are similarly dangerous" start="1832" end="1864"/>
  </contributor>
 </scu>
 <scu uid="22" label="Guns should be banned if the same conditions apply.">
  <contributor label="He asks why ban guns if the same conditions apply">
   <part label="He asks why ban guns if the same conditions apply" start="2523" end="2572"/>
  </contributor>
 </scu>
 <scu uid="26" label="He agrees with the logic.">
  <contributor label="He agrees with the logic">
   <part label="He agrees with the logic" start="2701" end="2725"/>
  </contributor>
 </scu>
 <scu uid="19" label="No citizen should have nukes.">
  <contributor label="S2 says no citizen should have nukes">
   <part label="S2 says no citizen should have nukes" start="1794" end="1830"/>
  </contributor>
 </scu>
 <scu uid="17" label="Nuclear weapons are banned in the US.">
  <contributor label="S1 states that nuclear weapons are banned in the US">
   <part label="S1 states that nuclear weapons are banned in the US" start="689" end="740"/>
  </contributor>
 </scu>
 <scu uid="27" label="The logic doesn't make sense.">
  <contributor label="it doesn't make sense">
   <part label="it doesn't make sense" start="2730" end="2751"/>
  </contributor>
 </scu>
 <scu uid="18" label="Unanswered posts prove guns needn't be banned.">
  <contributor label="His unanswered posts prove guns needn't be banned">
   <part label="His unanswered posts prove guns needn't be banned" start="1743" end="1792"/>
  </contributor>
 </scu>
 <scu uid="25" label="What ban is asked.">
  <contributor label="S1 asks what ban he's talking about">
   <part label="S1 asks what ban he's talking about" start="2574" end="2609"/>
  </contributor>
 </scu>
</pyramid>
