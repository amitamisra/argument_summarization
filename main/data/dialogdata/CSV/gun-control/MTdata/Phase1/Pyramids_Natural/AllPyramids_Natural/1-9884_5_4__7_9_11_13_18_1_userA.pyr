<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]*\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>Author1 states most firearms aren't recovered. About 63% of guns recovered in Toronto originated in the US and a large number have their serial numbers removed. According to Royal Mounted Police 2,637 guns were reported and 925 were linked to a dealer, and 3/4 of those originated in the US. Author2 asks how they were linked to the US if the serial numbers had been removed. Author1 says those weren't part of the traceable group unless related to ballistics. Author2 thinks this is ridiculous. Failed ballistics tests aren't traceable. Author1 says comprehensive statistics are scarce, but recent cases point to the problem. His point is even with the serial numbers removed, some can still be traced. Author2 says he doesn't see anything that suggests ballistics determined anything and there's no mention of testing the slugs. Furthermore, BD can only compare rounds linked to a crime, so no random match is possible. Even that is negligible. Author1 says he never suggested anything about ballistics databases, Author2 did. He initially used the term ballistics rather than forensics.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>Author1 states that most firearms used in crimes are not recovered, and approximately 63% of the traceable handguns recovered in Toronto originated in the US. A large proportion of the firearms recovered in crimes have had their serial numbers removed, but Author1 wonders how it was determined that they originated in the U.S. The serial numbers were removed, but Author2 tells Author1 that they were not part of the traceable group unless it was through ballistics. Author2 believes that ballistics traces are not possible, and he ponders the usefulness of a long gun registry. Author1 outlines an article that details a case where Ottawa police traced a gun, and he states that a gun can be traced without the serial numbers. Author1 admits that it may not have been ballistics, he does not support the long gun registry. Author2 tells Author1 that the article did not state that ballistics databases determined anything, and that ballistics databases match rounds to the gun they came from. Author1 mentions that he was referring to forensics instead of ballistics.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>Author1 says most guns used for crimes aren't recovered.  He says in Toronto, 63% of the guns came from America. Over 2600 guns were recovered by police in Canada, and 925 came from a dealer, of which 3/4 came from the USA. Author2 questions how if the serial numbers were removed, they were sure those guns had come from the USA. Author2 says they were through ballistics, but Author2 argues it is impossible to trace guns that way. He questions what good is the long gun registry. Author1 details a case in which a gun was used in a crime. He says although the serial numbers were missing, they were able to trace the gun to a shop in Maine. A person bought eight guns and sold them to a Canadian who smuggled them. He said this showed guns could still be traced. Author2 still questions how ballistics would help identify the guns. He points out the specifics of this, and Author1 admits he should have said forensics instead of ballistics.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>Author1 believes the long gun registry is not for him. He knows most guns used in crimes are not recovered, and quotes statistics to back up his position. Many aren't traceable except through ballistics. Some horrible Canadian gun crimes involve guns smuggled from the United States. Most are not traced, but some can be even though the serial numbers are gone. They may not have been traced with ballistics, he does not know. He says he used the wrong word when he said ballistics. He meant the authorities used forensics to trace guns. Author2 strongly believes the long gun registry is useless. He wonders how the authorities could trace guns without serial numbers to the US. He believes ballistics traces are impossible. People only think it possible because of movies they have seen. Ballistics cannot trace guns. Striations on bullets don't work because the gun bores that make the striations change shape readily. Anyway, ballistics databases only compare what is in the database. New guns are not. Most guns are not. It is useless.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>Author1 argues that firearms are moving into and out of the US at too high rates. Many guns are being illegally trafficked and moved, and a large number of the are untraceable because their serial numbers were removed. A large portion of guns in Canada come from the US illegally. Guns can be traced without their serial number in some cases. This problem is getting worse, as people have been smuggling guns and drugs from Maine and Ohio into Canada. A gun registry would not help this problem. </line>
  <line></line>
  <line>Author2 asked how the guns were traced, without their serial numbers or other information. No ballistics database has helped to put a criminal behind bars, or track weapons that cross borders. Long gun registries would not help track this. Guns are not tested by ballistics or by registries in order to identify them, unlike TV show dramas. Random matches are simply not possible in registries. People confuse the abilities of tv shows with real life, and we can't do much of what is shown on TV.</line>
 </text>
 <scu uid="7" label=" how the guns were traced without their serial numbers.">
  <contributor label="Author2 asks how they were linked to the US if the serial numbers had been removed">
   <part label="Author2 asks how they were linked to the US if the serial numbers had been removed" start="318" end="400"/>
  </contributor>
  <contributor label="Author2 questions how if the serial numbers were removed, they were sure those guns had come from the USA">
   <part label="Author2 questions how if the serial numbers were removed, they were sure those guns had come from the USA" start="2464" end="2569"/>
  </contributor>
  <contributor label="He wonders how the authorities could trace guns without serial numbers to the US">
   <part label="He wonders how the authorities could trace guns without serial numbers to the US" start="3809" end="3889"/>
  </contributor>
  <contributor label="Author2 asked how the guns were traced, without their serial numbers or other information">
   <part label="Author2 asked how the guns were traced, without their serial numbers or other information" start="4777" end="4866"/>
  </contributor>
  <contributor label="Author1 wonders how it was determined that they originated in the U.S">
   <part label="Author1 wonders how it was determined that they originated in the U.S" start="1400" end="1469"/>
  </contributor>
 </scu>
 <scu uid="9" label="A gun can be traced wiouth the serial numbers in some cases.">
  <contributor label="His point is even with the serial numbers removed, some can still be traced">
   <part label="His point is even with the serial numbers removed, some can still be traced" start="653" end="728"/>
  </contributor>
  <contributor label="he states that a gun can be traced without the serial numbers">
   <part label="he states that a gun can be traced without the serial numbers" start="1809" end="1870"/>
  </contributor>
  <contributor label="He says although the serial numbers were missing, they were able to trace the gun to a shop in Maine...He said this showed guns could still be traced">
   <part label="He says although the serial numbers were missing, they were able to trace the gun to a shop in Maine" start="2782" end="2882"/>
   <part label="He said this showed guns could still be traced" start="2958" end="3004"/>
  </contributor>
  <contributor label="Most are not traced, but some can be even though the serial numbers are gone">
   <part label="Most are not traced, but some can be even though the serial numbers are gone" start="3495" end="3571"/>
  </contributor>
  <contributor label="Guns can be traced without their serial number in some cases">
   <part label="Guns can be traced without their serial number in some cases" start="4560" end="4620"/>
  </contributor>
 </scu>
 <scu uid="2" label="About 63% of guns recovered in Toronto came from the US.">
  <contributor label="About 63% of guns recovered in Toronto originated in the US">
   <part label="About 63% of guns recovered in Toronto originated in the US" start="73" end="132"/>
  </contributor>
  <contributor label="approximately 63% of the traceable handguns recovered in Toronto originated in the US">
   <part label="approximately 63% of the traceable handguns recovered in Toronto originated in the US" start="1215" end="1300"/>
  </contributor>
  <contributor label="He says in Toronto, 63% of the guns came from America">
   <part label="He says in Toronto, 63% of the guns came from America" start="2298" end="2351"/>
  </contributor>
  <contributor label="A large portion of guns in Canada come from the US illegally">
   <part label="A large portion of guns in Canada come from the US illegally" start="4498" end="4558"/>
  </contributor>
  <contributor label="Some horrible Canadian gun crimes involve guns smuggled from the United States">
   <part label="Some horrible Canadian gun crimes involve guns smuggled from the United States" start="3415" end="3493"/>
  </contributor>
 </scu>
 <scu uid="14" label="Ballistics traces are not possible.">
  <contributor label="Author2 believes that ballistics traces are not possible">
   <part label="Author2 believes that ballistics traces are not possible" start="1611" end="1667"/>
  </contributor>
  <contributor label="Author2 thinks this is ridiculous...Failed ballistics tests aren't traceable">
   <part label="Failed ballistics tests aren't traceable" start="522" end="562"/>
   <part label="Author2 thinks this is ridiculous" start="487" end="520"/>
  </contributor>
  <contributor label="Author2 argues it is impossible to trace guns that way...Author2 still questions how ballistics would help identify the guns">
   <part label="Author2 argues it is impossible to trace guns that way" start="2618" end="2672"/>
   <part label="Author2 still questions how ballistics would help identify the guns" start="3006" end="3073"/>
  </contributor>
  <contributor label="He believes ballistics traces are impossible...Ballistics cannot trace guns">
   <part label="He believes ballistics traces are impossible" start="3891" end="3935"/>
   <part label="Ballistics cannot trace guns" start="4001" end="4029"/>
  </contributor>
  <contributor label="Guns are not tested by ballistics or by registries in order to identify them">
   <part label="Guns are not tested by ballistics or by registries in order to identify them" start="5017" end="5093"/>
  </contributor>
 </scu>
 <scu uid="1" label="Most guns used in crimes aren't recovered.">
  <contributor label="Author1 states most firearms aren't recovered">
   <part label="Author1 states most firearms aren't recovered" start="26" end="71"/>
  </contributor>
  <contributor label="Author1 states that most firearms used in crimes are not recovered">
   <part label="Author1 states that most firearms used in crimes are not recovered" start="1143" end="1209"/>
  </contributor>
  <contributor label="Author1 says most guns used for crimes aren't recovered">
   <part label="Author1 says most guns used for crimes aren't recovered" start="2240" end="2295"/>
  </contributor>
  <contributor label="He knows most guns used in crimes are not recovered">
   <part label="He knows most guns used in crimes are not recovered" start="3266" end="3317"/>
  </contributor>
  <contributor label="a large number of the are untraceable">
   <part label="a large number of the are untraceable" start="4417" end="4454"/>
  </contributor>
 </scu>
 <scu uid="11" label="Ballistics databases only compare rounds linked to a crime.">
  <contributor label="Furthermore, BD can only compare rounds linked to a crime...Even that is negligible...Author1 says he never suggested anything about ballistics databases, Author2 did">
   <part label="Furthermore, BD can only compare rounds linked to a crime" start="857" end="914"/>
   <part label="Even that is negligible" start="948" end="971"/>
   <part label="Author1 says he never suggested anything about ballistics databases, Author2 did" start="973" end="1053"/>
  </contributor>
  <contributor label="Anyway, ballistics databases only compare what is in the database...New guns are not. Most guns are not. It is useless">
   <part label="Anyway, ballistics databases only compare what is in the database" start="4133" end="4198"/>
   <part label="New guns are not. Most guns are not. It is useless" start="4200" end="4250"/>
  </contributor>
  <contributor label="No ballistics database has helped to put a criminal behind bars, or track weapons that cross borders">
   <part label="No ballistics database has helped to put a criminal behind bars, or track weapons that cross borders" start="4868" end="4968"/>
  </contributor>
  <contributor label="that ballistics databases match rounds to the gun they came from">
   <part label="that ballistics databases match rounds to the gun they came from" start="2072" end="2136"/>
  </contributor>
 </scu>
 <scu uid="18" label="Long gun registries would not help track the guns.">
  <contributor label="A gun registry would not help this problem...Long gun registries would not help track this">
   <part label="Long gun registries would not help track this" start="4970" end="5015"/>
   <part label="A gun registry would not help this problem" start="4731" end="4773"/>
  </contributor>
  <contributor label="he ponders the usefulness of a long gun registry...he does not support the long gun registry">
   <part label="he ponders the usefulness of a long gun registry" start="1673" end="1721"/>
   <part label="he does not support the long gun registry" start="1925" end="1966"/>
  </contributor>
  <contributor label="He questions what good is the long gun registry">
   <part label="He questions what good is the long gun registry" start="2674" end="2721"/>
  </contributor>
  <contributor label="Author1 believes the long gun registry is not for him...Author2 strongly believes the long gun registry is useless">
   <part label="Author1 believes the long gun registry is not for him" start="3211" end="3264"/>
   <part label="Author2 strongly believes the long gun registry is useless" start="3749" end="3807"/>
  </contributor>
 </scu>
 <scu uid="27" label="Many guns aren't traceable except through ballistics.">
  <contributor label="Many aren't traceable except through ballistics">
   <part label="Many aren't traceable except through ballistics" start="3366" end="3413"/>
  </contributor>
  <contributor label="Author2 says they were through ballistics">
   <part label="Author2 says they were through ballistics" start="2571" end="2612"/>
  </contributor>
  <contributor label="Author2 tells Author1 that they were not part of the traceable group unless it was through ballistics">
   <part label="Author2 tells Author1 that they were not part of the traceable group unless it was through ballistics" start="1508" end="1609"/>
  </contributor>
  <contributor label="Author1 says those weren't part of the traceable group unless related to ballistics">
   <part label="Author1 says those weren't part of the traceable group unless related to ballistics" start="402" end="485"/>
  </contributor>
 </scu>
 <scu uid="15" label="Referring to forensics instead of ballistics.">
  <contributor label="Author1 mentions that he was referring to forensics instead of ballistics">
   <part label="Author1 mentions that he was referring to forensics instead of ballistics" start="2138" end="2211"/>
  </contributor>
  <contributor label="He initially used the term ballistics rather than forensics">
   <part label="He initially used the term ballistics rather than forensics" start="1055" end="1114"/>
  </contributor>
  <contributor label="Author1 admits he should have said forensics instead of ballistics">
   <part label="Author1 admits he should have said forensics instead of ballistics" start="3116" end="3182"/>
  </contributor>
  <contributor label="He says he used the wrong word when he said ballistics...He meant the authorities used forensics to trace guns">
   <part label="He meant the authorities used forensics to trace guns" start="3694" end="3747"/>
   <part label="He says he used the wrong word when he said ballistics" start="3638" end="3692"/>
  </contributor>
 </scu>
 <scu uid="3" label="A large number of guns have their serial numbers removed.">
  <contributor label="a large number have their serial numbers removed">
   <part label="a large number have their serial numbers removed" start="137" end="185"/>
  </contributor>
  <contributor label="A large proportion of the firearms recovered in crimes have had their serial numbers removed...The serial numbers were removed">
   <part label="A large proportion of the firearms recovered in crimes have had their serial numbers removed" start="1302" end="1394"/>
   <part label="The serial numbers were removed" start="1471" end="1502"/>
  </contributor>
  <contributor label="their serial numbers were removed">
   <part label="their serial numbers were removed" start="4463" end="4496"/>
  </contributor>
 </scu>
 <scu uid="6" label="3/4 of the guns came from the USA.">
  <contributor label="3/4 of those originated in the US">
   <part label="3/4 of those originated in the US" start="283" end="316"/>
  </contributor>
  <contributor label="which 3/4 came from the USA">
   <part label="which 3/4 came from the USA" start="2435" end="2462"/>
  </contributor>
 </scu>
 <scu uid="5" label="925 guns were linked to a dealer.">
  <contributor label="925 were linked to a dealer">
   <part label="925 were linked to a dealer" start="250" end="277"/>
  </contributor>
  <contributor label="925 came from a dealer">
   <part label="925 came from a dealer" start="2408" end="2430"/>
  </contributor>
 </scu>
 <scu uid="24" label="An article details a case where Ottawa police traced a gun.">
  <contributor label="Author1 outlines an article that details a case where Ottawa police traced a gun">
   <part label="Author1 outlines an article that details a case where Ottawa police traced a gun" start="1723" end="1803"/>
  </contributor>
  <contributor label="Author1 details a case in which a gun was used in a crime">
   <part label="Author1 details a case in which a gun was used in a crime" start="2723" end="2780"/>
  </contributor>
 </scu>
 <scu uid="12" label="Ballistics databases did not determine anything.">
  <contributor label="Author2 tells Author1 that the article did not state that ballistics databases determined anything">
   <part label="Author2 tells Author1 that the article did not state that ballistics databases determined anything" start="1968" end="2066"/>
  </contributor>
  <contributor label="Author2 says he doesn't see anything that suggests ballistics determined anything">
   <part label="Author2 says he doesn't see anything that suggests ballistics determined anything" start="730" end="811"/>
  </contributor>
 </scu>
 <scu uid="33" label="Comprehensive statistics are scarce but point to the problem.">
  <contributor label="quotes statistics to back up his position">
   <part label="quotes statistics to back up his position" start="3323" end="3364"/>
  </contributor>
  <contributor label="Author1 says comprehensive statistics are scarce, but recent cases point to the problem">
   <part label="Author1 says comprehensive statistics are scarce, but recent cases point to the problem" start="564" end="651"/>
  </contributor>
 </scu>
 <scu uid="30" label="Many guns are being illegally trafficked.">
  <contributor label="Many guns are being illegally trafficked and moved...This problem is getting worse, as people have been smuggling guns and drugs from Maine and Ohio into Canada">
   <part label="Many guns are being illegally trafficked and moved" start="4361" end="4411"/>
   <part label="This problem is getting worse, as people have been smuggling guns and drugs from Maine and Ohio into Canada" start="4622" end="4729"/>
  </contributor>
  <contributor label="A person bought eight guns and sold them to a Canadian who smuggled them">
   <part label="A person bought eight guns and sold them to a Canadian who smuggled them" start="2884" end="2956"/>
  </contributor>
 </scu>
 <scu uid="21" label="No random match is possible.">
  <contributor label="so no random match is possible">
   <part label="so no random match is possible" start="916" end="946"/>
  </contributor>
  <contributor label="Random matches are simply not possible in registries">
   <part label="Random matches are simply not possible in registries" start="5118" end="5170"/>
  </contributor>
 </scu>
 <scu uid="4" label="Over 2600 guns were recovered by police in Canada.">
  <contributor label="According to Royal Mounted Police 2,637 guns were reported">
   <part label="According to Royal Mounted Police 2,637 guns were reported" start="187" end="245"/>
  </contributor>
  <contributor label="Over 2600 guns were recovered by police in Canada">
   <part label="Over 2600 guns were recovered by police in Canada" start="2353" end="2402"/>
  </contributor>
 </scu>
 <scu uid="28" label="People confuse the abilities of TV shows with real life.">
  <contributor label="People only think it possible because of movies they have seen">
   <part label="People only think it possible because of movies they have seen" start="3937" end="3999"/>
  </contributor>
  <contributor label="unlike TV show dramas...People confuse the abilities of tv shows with real life...we can't do much of what is shown on TV">
   <part label="People confuse the abilities of tv shows with real life" start="5172" end="5227"/>
   <part label="we can't do much of what is shown on TV" start="5233" end="5272"/>
   <part label="unlike TV show dramas" start="5095" end="5116"/>
  </contributor>
 </scu>
 <scu uid="32" label="Pointing out the specifics of gun tracing.">
  <contributor label="He points out the specifics of this">
   <part label="He points out the specifics of this" start="3075" end="3110"/>
  </contributor>
  <contributor label="Striations on bullets don't work because the gun bores that make the striations change shape readily">
   <part label="Striations on bullets don't work because the gun bores that make the striations change shape readily" start="4031" end="4131"/>
  </contributor>
 </scu>
 <scu uid="25" label="The gun tracing may not been ballistics.">
  <contributor label="Author1 admits that it may not have been ballistics">
   <part label="Author1 admits that it may not have been ballistics" start="1872" end="1923"/>
  </contributor>
  <contributor label="They may not have been traced with ballistics, he does not know">
   <part label="They may not have been traced with ballistics, he does not know" start="3573" end="3636"/>
  </contributor>
 </scu>
 <scu uid="34" label="Firearms are moving into and out of the US at too high rates.">
  <contributor label="Author1 argues that firearms are moving into and out of the US at too high rates">
   <part label="Author1 argues that firearms are moving into and out of the US at too high rates" start="4279" end="4359"/>
  </contributor>
 </scu>
 <scu uid="22" label="There's no mention of testing the slugs.">
  <contributor label="there's no mention of testing the slugs">
   <part label="there's no mention of testing the slugs" start="816" end="855"/>
  </contributor>
 </scu>
</pyramid>
