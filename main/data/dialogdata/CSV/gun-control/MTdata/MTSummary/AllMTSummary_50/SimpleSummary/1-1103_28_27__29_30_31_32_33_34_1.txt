
----------
D0
----------
Author1 thinks owning a gun for self defense shows you have no problem violating someone's life. He doesn't think criminal's loose the right to life because it supports the idea that rights are given by society and may be taken away. Author2 asks for proof that criminals have the right to commit a crime against law-abiding citizens. He thinks gun laws don't effect criminals because they disregard laws. Author1 says it's pointless because it would be produced by a society that has the power to remove rights. He thinks gun control laws effect criminals. Author2 thinks this is a typical reaction from one that wants to take away rights. Author1 asks for evidence that people have a right to have guns. He wants something not made up by the government. Author2 thinks the intent of the 2nd amendment was to preserve and guarantee rights. It doesn't grant them. Author1 thinks criminals don't break laws. They become criminals after they broke them. Author2 says the UK doesn't have the 2nd amendment. The laws provide punishment for criminals.


----------
D1
----------
Author1 believes that, by owning and carrying a gun, you are claiming the right to end someone's life. Whether it's a criminal or not, taking a life is not a decision to be taken lightly. It's taking the law into your own hands. Rights and laws are created by a society. They are not intrinsically provided. All laws affect all citizens. Rights aren't imposed by legislation but by their being upheld or suppressed by the majority. Criminals are not criminals until they've broken a law. Written law creates criminals more than deters them from committing crimes. 

Author2 feels that when criminals are violating a person's rights in the commission of the crime they're assuming the risk of being punished or killed. Criminals operate outside of society's laws. Gun control legislation does not bar criminals from obtaining firearms. It is only a restriction and hassle of those purchasing them legally. The 2nd Amendment's purpose was to preserve and guarantee these rights, not to grant pre-existing rights. Laws primarily serve to allocate punishments for crimes committed.


----------
D2
----------
Author1 thinks that if you own a gun for self defense it is a violation of everybody else's right to life. Including criminals. He continues without proof to the contrary. He says that this society is powerful. That it can give and take away rights. That a person is at the mercy of the mob mentality.  He questions whether it is all laws or just gun laws that criminals disregard. Why even have laws at all then?  
Author2 believes that criminals are the ones who break the laws. It's not the law abiding citizens.  That the gun laws won't hurt the criminals, because they still will get guns.  He goes on to claim that the 2nd amendment wasn't made to take away rights. but to preserve the preexisting rights of the people to keep and bear arms.  Laws are there not for prevention of crime but to ensure punishment when it happens.


----------
D3
----------
Author1 argues that owning a gun means you're willing to hurt others. Killing someone deprives them of the chance to live. The right to live is the most important right. Criminals still have the right to live. Gun control laws should be implemented. Documentation would help with gun control laws. Gun control would reduce gun crime. Gun control laws would effect everyone. There is no inherent right to own guns. The government created the right to own guns. The Second Amendment is the only thing that grants the right to guns. The right to live is more basic than the right to own guns. If society's opinion changes the Constitution can too. 
Author2 argues gun control laws do not effect criminals. Gun control only effects law abiding citizens. Gun control will not stop criminals from getting guns. The right to own guns is preexisting. The right to own guns was not granted by the Constitution. Laws against violent crime punish the criminals. Gun control laws punish the wrong people.


----------
D4
----------
Author1 says owning a gun is not respecting another person's right to life. They have that right even if they attempt to take your life. Denying them that right says rights are not guaranteed. Author2 says criminals don't have a right to commit a crime. Criminals don't obey laws. Gun control doesn't affect them. Author1 questions why have laws if they don't have any effect. He says they do affect criminals. Author2 argues gun control laws don't affect criminals who don't obey them. They do affect law-abiding citizens. Author1 questions why Author2 thinks he has a right to a gun. A government made up law shouldn't be a right. . Author2 says the second amendment doesn't grant the pre-existing right to bear arms. It preserves and protects a right that already existed. Author1 says it doesn't matter if it was pre-existing. Society can change laws if opinions support doing so. Author2 states laws don't prevent crimes. They provide punishment for when crimes occur. Gun control laws affect law-abiding citizens. They don't affect criminals.

