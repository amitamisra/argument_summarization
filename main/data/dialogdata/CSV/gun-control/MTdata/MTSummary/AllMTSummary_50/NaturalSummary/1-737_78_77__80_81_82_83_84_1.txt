
----------
D0
----------
Author1 is in support of people owning guns.  He believes it is a person's right to be allowed to protect himself/herself when necessary.  He calls to question the different crimes, including gang rapes of teenage girls which have happened in Australia, where Author2 is from.  Author1 responds to Author2's mention of Jeffery Dahmer and Ted Bundy advising these men did not use guns in their killings.  He cites a few other crimes from Australia advising guns are not the issue, but instead crime is.  
Author2 feels guns should be more regulated and admits he looks down on those who choose to own them.  Author2 brings attention to some different catastrophic events which have occurred in the United States such as the Uni-bomber and the D.C. Sniper.  He also mentions both Bundy and Dahmer, serial killers from the US.  Author2 is offended by Author1's mention of the gang rapes in Australia as he feels Author1 is blaming this activity on a lack of guns in public hands.


----------
D1
----------
Author1 and Author2 are talking about what free speech rights are.

Author1 says that he is holding a gun, which probably makes Author2 angry. He brings up rape gangs in Australia that have been attacking teen girls. He says three of those US serial killers did not use guns. He knows there's crime, that's why he carries a gun. He mocks whether cops respond well to crime in Australia. He says forget about the gangs, what about the brutal home invasions in the news. 

Author2 mocks Author1 for having a gun. He accuses him of jumping from subject to subject. He jokes if the US is crime free and brings up the names of several famous serial killers. Author2 points out the Australian rape gangs did not use guns. He says some of the US serial killers didn't use guns either. He says they do still have crime, but less of it. He says he didn't say guns were the problem, it was Author1 who said they had gangs because they didn't have guns.


----------
D2
----------
Author1 defines free speech, and states that yelling fire in a theater when there is none is a lie. This deception causes panic and injury, and the perpetrator is not free of responsibility if it causes damage. Author2 believes that Author1 is contradicting himself, because Author1 said that yelling fire is a lie that is not protected by free speech provisions. Author2 tells Author1 that he then said that it is defined by the court, and Author1 responds by stating that Author2 wants to mold society until it fits his vision.  Author2 stresses that Author1 is wavering between natural rights and the rights that courts devise. Author1 talks about gun control, and the fact that the USA has crime which is why Author1 carries a gun. Author1 tells Author2 that he is glad that Australian police have stopped crime, and its citizens feel safe without guns. Author2 states that Australia has crime, but they have less crime. Author2 agrees with Author1 that guns are not the issue, that the issue is crime in both countries.


----------
D3
----------
Author1 states that free speech is important, we are allowed to use it to express ourselves, but we are also held accountable to what happens as a result of what we say. Author2 is anti-gun and wishes to bully others into following his/her/Australia's example. Australia has lots of violence that could potentially be abated with laxer firearm restrictions. Author1 acknowledges America can be dangerous, that's why he/she carries a gun. The problems with America aren't due to guns but to criminals, the same as in every country -- Australia included. 

Author2 states that Author1 is contradicting his/her own arguments and technically is arguing the exact same point Author2 is. Author1 has "very little knowledge about law, history, culture etc." America has lots of violence as well, many murders being committed with a firearm or explosives. Author1's stating crimes in Australia occur (and go unpunished) because of lack of gun availability shows that Author1 has no empirical knowledge about Australian (or American) crime statistics. Author1 is an over-zealous gun nut whose passion is obfuscating facts.


----------
D4
----------
Author1 argues that free speech consists of communications, economic activity, artistic activity, and political positions. Lies are allowed to be said, but the consequences for telling a lie are not protected by the Constitution. Society does not need to mold itself to any one person. Most people who own guns just want to be left alone. Rape gangs were common in Australia and the police did not react. Gun control does not apply if people didn't use guns. The USA has crime, and so people can protect themselves by arming themselves. Police cannot instantly react to crimes in progress to protect everyone. People need a way to protect themselves. Brutal home invasion cannot be stopped without guns. 
Author2 argues that lies are not protected by the courts, and that gun control helps to reduce crime. In the US there is lots of crime and no gun control: gun control would help the United States. Australia has less crime overall and per capita, but people commit crimes even without guns, guns just increase the damage people cause.

