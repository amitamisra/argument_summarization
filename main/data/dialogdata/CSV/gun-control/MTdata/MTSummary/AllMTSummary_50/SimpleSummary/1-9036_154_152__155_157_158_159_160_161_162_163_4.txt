
----------
D0
----------
Author1 says he isn't making judgements. He doesn't have an opinion on gun laws in America. He's neither pro nor con on the issue. He is angry at being lumped in with another person. That person has been comparing the US and Britain. He himself has not expressed an opinion. He thinks Author2 should be allowed to have a gun. He says Author2 is a very nice person. He says guns have made America great.

Author2 says Author1 has communication problems. He makes fun of him being a Mensa member. He says Author1 admitted not being really smart. Author2 says he's humble. He blames vision problems for his poor communication. He discusses correct definitions. He accuses Author1 of jumping to conclusions. He says he should be allowed to keep his rights and his gun. He thanks Author1 for accepting others views. He is glad Author1 admits his mistakes. He likes having a conversation with him. He considers him a friend.


----------
D1
----------
Author1 starts off defending himself.  He never said he was pro or con about gun laws. He continues critically and restates he never made any claims pro or con about the gun laws.  He says his "MENSA membership accomplishment lies at the bottom of his drawer". It is ironic.  He believes Author2 should be able to keep his "pop gun". He ends it saying "God, guns and guts have made America great."
Author2 starts off criticizing. He reminds Author1 said on his profile he was "not to smart" 
He goes on calling Author1 a troll because he doesn't have a thesis. As it progresses though they end up seeing eye to eye and making friends with each other.  All have their egos (and gun rights) in tact.  Author2 ends with a blessing of peace for Author1 .


----------
D2
----------
Author1 states that he should not be compared to someone that judges US gun laws by making comparisons to Britain. Author2 states that he is ashamed that Author1 claims to be a member of Mensa. Author1 asserts that he does not have an opinion either way in regards to the gun laws of the USA. Author1 tells Author2 that he never expressed an opinion about gun laws. Author1 insists that his Mensa certificate is proof that a meaningless certificate can be obtained. That is why he mentioned the Mensa certificate to Author2. Author2 debates with Author1 over a typo that Author1 made. Author1 tells Author2 that Author2 should be able to keep his pop gun. Author2 continues to mention the typo to Author1. He mentions a vote on keeping their rights and his popgun. Author2 continues to insult Author1. Author1 responds by saying that he is proud to have his shortcomings pointed out by Author2. Author2 states that he took advantage of a simple error to lighten the mood and the seriousness of the debate.


----------
D3
----------
Author1 believes that people should definitely get to keep their guns. However, he has no personal desire to debate the current gun laws. Religious faith, widespread gun ownership and bottomless courage are the true foundations of American greatness. He certainly believes that. At the same time, he declines to compare the gun laws in America to those in Britain. He firmly stands on his right to have no opinion on the matter. He is not a troll, though. Refusing to debate these or related matters does not make him one. Author2 thinks people contributing to this forum should take a side. Or they should offer a new viewpoint on a question. If they refuse to do so, perhaps they are trolling. Author1 may think he is acting like an intellectual, but he is not. His Mensa membership is irrelevant. His mentioning it is bragging. Yet they both agree people have rights. They both agree that people should keep their guns. So the two are friends and allies. Allies may be essential in the future.


----------
D4
----------
Author1 is annoyed that he's being lumped in with Penfold. Penfold's making judgements about the American gun laws by comparing them to Britain. He was under the impression that Limey meant English. Author2 says Author1 has more communication problems than that. He's ashamed that Author1 claims to be a Mensa member. Author1 says Author2 should be ashamed of his lack of reading skills. He shouldn't be ashamed of Author1's Mensa certificate. Author2 replies Author1 has admitted he isn't too smart. He says he has glaucoma. He's going to occasionally spell words wrong. He would make an exception between inate and inane, though. Author1 says he has no opinion as to the gun laws of the US. He didn't express one either. If Author2 has a problem with Penfold he should take it up with him. He is sorry Author2 has glaucoma. He only mentions Mensa because it's true. Author2 jumping to the wrong conclusion isn't because of his medical condition. Author1 and Author2 think each other nice people. They trade compliments. They also trade quips.

