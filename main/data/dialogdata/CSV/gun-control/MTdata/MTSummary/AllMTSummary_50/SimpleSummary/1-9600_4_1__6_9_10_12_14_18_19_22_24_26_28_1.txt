
----------
D0
----------
Author1 wants to discuss guns being scarce in the UK. They are being rented to both sides of a conflict. The fact that DC now allows guns hasn't deterred crime at all.  He claims it is a significant reduction?  He is happy not to be an owner of a firearm. He might blame the armed citizens and the availability of guns for others criminal activity. 
Author2 claims that the fact there are guns to be rented out in the UK at all is a failure of the "gun grab" laws. He points out "Churchhill sank most of the French Fleet  at the start of WWII" as proof "we can take care of our own". Then there came the Brit disarmament plan.  He claims that is why rulers want unarmed populations. They can be enslaved easily.  "Free men are armed!"  He claims Author1's points are insignificant.


----------
D1
----------
Author1 believes that guns are rare in England. Gang members are renting guns.  Author1 believes British people must rent. This means the law is working. Gang members must find a gun to rent. This added difficulty is effective. Author1 believes laws cannot be totally effective. Author1 quotes a report on gun availability in the UK. The report found gun shared by opposing gang members. Author1 thinks Author2 has an anti-gun agenda. Author1 says "2A" is not increasing crime by criminal refugees.  He is not Republican. Author1 dislikes being linked with gun rights opposition. He disagrees with some of their views.
Author2 states that it is illegal to possess, borrow or loan a firearm in the UK. Author2 says restricting gun ownership is ineffective. In the UK guns are easy to obtain. Author2 values allegiance. Gun ownership is part of being free. Author2 believes that Author1 blames guns for crime. He believes Author1 is for gun control. Author1 believes the constitution promises "Republican rights".


----------
D2
----------
Author1 argues gun control laws are good. UK gun control laws are decreasing guns on the street. Rival gangs have to rent guns from the same source. The decrease in guns is good. Gun control laws take time to have effect. DC's laws are too new to see an effect. No law is perfect. Gun control law is good for the citizens. Crime might increase from criminal refugees. More guns leads to more crime. Less guns causes less crime. 

Author2 argues that gun control laws are bad. There is still gun violence in the UK. Criminals can still get guns. Citizens cannot get guns to defend themselves. Gun control laws hurt lawful citizens. Citizens should be armed. More guns does not increase crime. Gun rights should be protected by the government. The UK gun laws have failed. Only tyrants don't want citizens to have guns. Armed citizens are  not responsible for armed crime. The UK elite do not trust their citizens.


----------
D3
----------
Author1 shows how scarce guns are in the UK now. Criminals and gangs have to share or rent guns. Author2 says this shows gun control laws have failed. Author1 says criminals have less access. This proves gun control laws work. He says the DC laws allowing guns haven't reduced crime. Author2 says guns are still available in the UK. It doesn't matter if they have to share or rent them. The laws haven't stopped crimes. Author1 says no law is 100% effective. He says there has been a significant reduction. This proves they are successful. Author2 argues people without guns are not free people. They are slaves to their government. Author1 says Author2 is ignoring the fact DC laws didn't stop crime. Author1 cites guns used in crimes were recovered. People selling guns were caught. These laws stopped crimes. He says it is logical to blame availability. If guns are available, criminals will buy and use them.


----------
D4
----------
Author1 thinks rival gangs are forced to share guns. He says analysis suggests guns are so scarce in the UK that they're rented out to both sides. Author2 thinks that this is admitting failure. He asks if it's against the law to possess, borrow or loan a firearm in the UK. Author1 says that's his point. Author2 says it's his. Author1's was just admitting failure. Author1 thinks a gang member having to rent a gun to shoot someone is not a failure. Anyone that thinks so should be equally lampooning the recent DC statistics thread because newly allowed guns haven't dropped the crime rate to 0. Author2 says it must be a handicap to go next door to get one. Author1 thinks no law is 100% successful. Author2 says if you turn on your allies you can't trust them. Author1 claims an increase in crime due purely to criminal refugees should not be treated as due to 2A. Author2 thinks that Author1 will blame citizens and the availability of guns for their criminal actions.

