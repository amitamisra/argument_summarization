
----------
D0
----------
Author1 is against the controls on guns that California has put in place. They argue that guns should not be so strictly controlled and regulated as they are in California. Other states cannot and should not be controlled by the rulings of a different state. If other states wanted to, they could adopt similar laws. However, gun control does not apply well in real life. If gun laws are stricter, then illegal guns will still manage to bypass those laws, because the guns are illegal. Author2 argues that stricter gun control is necessary and would have saved four policeman's lives. Because of the high crime rates in Californian cities, it is a good idea for there to be more gun control in place.


----------
D1
----------
Author1 says Author2 is proposing again that the U.S. adopt California's standards. He cites others lack of support, because one state can not determine how another should act. He says states can not ignore issues of their own. Author2 says states adopting controls would depend on the voters, and the states.  He states that California has large densely populated cities with high crime rates. He gives a recent example that four cops were killed by a criminal with an AK-47. Author1 questions how an example of a prohibited weapon being used serves as a good reason for the ban. Author2 says it shows why there is a ban. Author2 says if the gun hadn't been available, the damage wouldn't be so great.


----------
D2
----------
Author1 states that Author2 made the argument that California should set the standard for the United States, but one state cannot regulate the activities of another state. A state should not ignore home rule issues that may have created a problem that they cannot respond to. Author2 believes that other states could adopt controls like California, and he further believes that the reform will not take place because other states do not like the California laws. Four Oakland police officers were gunned down by a hood with an AK-47, which serves as a valid reason for some restrictions in areas like California has. Author1 believes that this is not a good example, but Author2 believes the cop murder proves that it is a good measure.


----------
D3
----------
Author1 says the argument that the United States should adopt California's standards as national standards has been made several times, but support has been lacking. A single state can't regulate the activities of others, but that doesn't mean a state should ignore home rule issues that have created problems. Author2 says states could adopt controls like California if they wanted, but it would depend on voters. He says that people miss that California is a large state with large crime rates, and this is one of the reasons for such restrictions. He gives an example of four officers being gunned down with an AK-47. Author1 questions an example of a prohibited weapon being used illegally as an example. Author2 thinks the weapon shouldn't have been available.


----------
D4
----------
Author1 notes that Author2 has repeatedly suggested that other states should enact gun control laws similar to California's.  He points out that one state can't regulate the rules of another, and that Author2's proposal has gained little support.  Author1 asks Author2 twice if he's really saying that the deaths of four Oakland police officers killed by a (banned) AK-47 shows why California's gun laws are a good idea.

Author2 asserts that voters in other states could choose to adopt California's gun policies, but concedes that such a move is unlikely in some states.  Author2, who says he is a gun owner, believes the killing of the officers in Oakland supports the case for a continued ban on weapons like the one used.

