
----------
D0
----------
Author1 believes Author2 is stating opinions, not facts. He thinks Author2 is being silly and he doesn't accept the parallel. Author2 says what he stated is a fact, and a loaded firearm must have a magazine in the firearm in order for it to be considered loaded. Author1 states the LAPD alleges that one of 16 handguns locked under the bed was loaded, and it doesn't say if the weapon was a revolver or an auto. Furthermore, just having the ammo and weapon together is illegal, and the LAPD knows what loaded means. Author2 doesn't agree and gives an example of when the LAPD was wrong, and it's not illegal for a weapon and ammo to be stored together. He asks about bullets in chambers.


----------
D1
----------
Author1 defends an action by the LAPD, claiming that it was not actually police brutality. He argues that the police were just doing their jobs. Author2 argues that the LAPD behaved inappropriately because the firearm during the incident was unloaded. Author1 rejects this argument, saying that the LAPD claimed that one of the firearms was actually loaded. He further states that even having ammunition and firearm together is illegal, so it doesn't matter if the firearm was actually loaded. Author2 shifts the topic to the quality of the LAPD. He cites several instances where the police incorrectly identified weapons. He uses this as evidence that the police may have incorrectly identified the firearm in question as being loaded, which would make their behaviors police brutality.


----------
D2
----------
Author1 says Author2 is using opinion not fact. Author1 says that trucks and guns are not a good parallel because trucks have many uses and guns just shoot. Author2 says that a gun has to have a magazine in it to be loaded, so they were expressing fact not opinion. Author1 says he's sure the LAPD knows what loaded means and asks for contrary evidence. Author2 says the LAPD is dumb and says something similar happened to someone else. Author2 say California police can't spot illegal weapons anyway and that most guns chamber a round, and even without a magazine the gun may still have a bullet. Author1 asks Author2 if a gun can be empty with the slide foward. Author2 says yes.


----------
D3
----------
Author1 believes police acted appropriately in the situation, and Author2 disagrees. Author2 is arguing that for a gun to be loaded, it must have the magazine in the gun. Author1 is arguing that the police surely know the difference between a fully loaded gun or not. Author2 is providing examples of when police made mistakes regarding firearms. Author2 points out that it is not illegal to have guns and ammo stored together. He also points out that when you take a magazine out, there can still be a bullet loaded in the chamber. Author1 asks if you can take the round out and have an empty chamber with the clip still in and the slide forward, and Author2 says yes.


----------
D4
----------
Author1 says he hears opinion, not fact. The police acted properly. Running over someone is different--trucks don't only shoot. Author2 says a gun must hold a magazine to be loaded. Author1 replies one gun was loaded. He trusts the police to recognize loaded guns. Having guns and ammo together is itself illegal, and any gun might be loaded. Author2 says cops make mistakes. They misidentified a knife and have let people go with illegal weapons. Having guns and ammo is legal. The facts aren't known. And a gun without a magazine could hold a bullet.Author1 asks if you can have the magazine in but no bullet in the chamber, like he saw in a movie. Author2 says yes, if the slide is forward.

