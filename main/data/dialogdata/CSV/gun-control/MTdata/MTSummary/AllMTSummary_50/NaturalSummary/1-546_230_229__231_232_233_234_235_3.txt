
----------
D0
----------
Author1 asks if there was a case of something that ever happened. They say that people like to use that quote when talking about unarmed passengers. They think people are vilifying people defending themselves. Author2 says there is no difference between dying in a crash and dying during defense. They say that the danger is from mistakes of intent. Author1 says there are no incidents of Author2's claims, that they are just chanting anti gun ideas. They say Author2 wants to ban right to own guns. Author2 says they don't want to ban, just restrict. Author1 accuses Author2 of using undocumented statements to limit or ban the right to bear arms. Author2 says they think and that regular people can't defend planes anyway.


----------
D1
----------
Author1 feels that law-abiding citizens should be able to carry guns, and use them in self-defense.  He says there are no reported incidents of the sort that Author2 brings up( referring to a quote by Author2 that is not included in this dialogue).  Author1 feels that Author2's argument vilifies law-abiding citizens, and that Author2's real goal is to limit or ban the right to bear arms.

Author2 claims not to want to ban the right, but to restrict it.  He doesn't think the 9/11 hijacking was a unique scenario, and can't be compared to other situations. Author2 feels that the possibility of an armed, untrained gun-owner confusing an ordinary citizen for an attacker is too high to risk.


----------
D2
----------
Author1 is in favor of having law abiding citizens carrying guns onto passenger aircraft. Guns would be helpful in the hands of civilians on an aircraft, in order to defend against the threat of terrorism and hijacking of planes. Author1 accuses Author2 that there is no evidence that having citizens carry guns onto planes has resulted in any injuries, and that it could only make flights safer. Guns are a beneficial invention. Author2 believes that anyone carrying a gun on a plane would be a greater risk to those arround them, because they might be mistaken for a terrorist. Guns should not be allowed on planes, as the risks would be too high and the reward too unsure. Gun control would be a good idea.


----------
D3
----------
Author1 is arguing the benefits of people being armed and able to defend themselves against terrorists on airplanes. Author2 says that either way results in death, which is the same. He questions how confusing it could be trying to identify who is a terrorist and who is a citizen. Author1 points out Author2 has no facts on incidents, yet condemns them. Author2 says he is not asking for a ban, just restrictions. Author1 retorts that Author2 used undocumented statements, and condemns law abiding citizens, in an effort to taking away rights of others. Author2 questions how a citizen could protect a plane. Author1 says practice makes gun owners capable of when to shoot. Author1 shows quotes showing how gun owners are treated unfairly.


----------
D4
----------
Author1 feels that people's rights to defend themselves are vilified and some people would rather die by religious zealots flying them into a building then chance death in a rescue attempt by citizens. Author2 says death is the same no matter which way it happens. He thinks the question is of the dangers in confusing citizens and terrorists. Author1 thinks it's a typical tactic of people wishing to ban guns to chant negatives about law-abiding citizens with guns while having no reported incidents to back it up. Author2 says he doesn't want guns banned, just restricted. Author1 thinks law-abiding citizens are being vilified by people that wish to limit or ban guns. Author2 questions how a citizen can gain the skills to protect a plane.

