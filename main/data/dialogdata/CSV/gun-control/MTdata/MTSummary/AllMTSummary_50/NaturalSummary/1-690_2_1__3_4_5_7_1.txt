
----------
D0
----------
Author1 asks Author1 to view threads that start with "I carried a gun" and says that how their actions didn't endanger anyone, increase crime, or violate rights. They ask Author2 if it's okay to carry guns. Author2 asks if it's okay to carry a nuclear weapon. Author1 challenges Author2 to find a nuclear weapon for sale. Author1 asks how they could get a gun if they were banned. Author2 says that since the US has nukes, they should be allowed one too. They say that the goal should be to repeal a ban, not break it. Author1 implies nukes aren't comparable since there are fewer vendors. Author2 says that the free market may allow nuclear weapons to be bought if there was a demand.


----------
D1
----------
Author1 states that nuclear weapons are banned in the US, which leads Author2 to respond that citizens should have the same access to nuclear weapons that the US does. The goal should be to repeal the ban, not try to break it. Author1 states that since they cannot be sold, they cannot be banned. Vendors sell to the Department of Defense, and no vendor would sell them to people. Author2 clarifies by defining the various bans on guns and gun ownership that applies to Author1, but his example is referring to the ban that is placed on owning and bearing nuclear weapons. He imagines a situation in which it would be profitable to sell nuclear weapons to citizens if it became legal to own them.


----------
D2
----------
Author1 received no posts on threads about gun carrying endangering people, increasing crime or violating rights. So he asks rhetorically if it's okay to carry guns. Author2 answers it's as okay as a country storing a nuclear weapon in a foreign city. Carrying guns is inherently dangerous.Author1 replies that nukes are effectively banned, invalidating the comparison. His unanswered posts prove guns needn't be banned. Author2 says no citizen should have nukes. And guns are similarly dangerous. Author1 says nobody sells individuals nukes. Author2 says he talks about guns with respect to his opponent's argument; about nuclear weapons with respect to his own. Makers would sell nukes, if allowed, for profit and power. So, he asks rhetorically, shouldn't people be allowed to buy them?


----------
D3
----------
Author1 asks if it's ok that he has a gun because no one has said how it endangers anyone, increases crime, or violate's anyone's rights. Author2 asks if it's ok for him to have a nuclear weapon or for France to have one stored in Chicago. Author1 says France has the same rights as a US citizen and says finding a nuclear weapon for sale would be more relevant. He asks why ban guns if the same conditions apply. Author1 asks what ban he's talking about. He says the only vendor is happy to have it's only customer be the Department of Defense. He agrees with the logic but it doesn't make sense. Author2 says he is talking about the ban to own nuclear weapons.


----------
D4
----------
Author1 refers to previous posts about carrying a gun, and the lack of replies as to whether it endangered anyone, increased crime, or violated rights. He asks is it okay to carry a gun then. Author2 asks if it is okay for him to own a nuclear weapon. Author1 says the question is irrelevant since nuclear weapons are not for sale. Author2 states if the government is allowed to own a nuclear weapon, he should be too. If they are banned, he should not be able to own one. Author1 argues there is no ban because they are not for sale. He says there is no seller who would sell one. Author2 argues sellers would sell them if they were allowed to.

