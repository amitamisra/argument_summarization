
----------
D0
----------
Author1 believes that teaching little kids to shoot guns is wrong. The idea is actually funny in the abstract. However, the reality is horrible. Growing up in a serious gun culture is very unhealthy. Kids raised that way are more likely to become dangerous. He is from Australia. There, people feel calmer than Americans do. At least, some Americans have worked themselves into a fearful frenzy. Australians, on the other hand, are not preparing to defend themselves against highly unlikely savage attacks. Guns are rare. That is good. Author2 believes that children must learn to defend themselves. The earlier they learn the better for them. Early learning teaches young boys and girls respect for guns. Practiced gun users will be safer throughout their lives. He has read many stories about Australia. He thinks Australia has many guns. They are often brought into the country illicitly. They are used in gun violence. The country is not as safe as some may think it is. It is unrealistic to deny the many dangers in the world.


----------
D1
----------
Author1 jokes kids should be armed at birth. He jokes about Author2's kids becoming school shooters. He says they are not afraid there. He says there's lots of space between their land free of guns and Author2's home where people are afraid. 

Author2 says he started teaching his kids to shoot at 4-5. He says it teaches them respect for firearms. He notes Author2 lives in Australia. Author2 has to be afraid if he even says the word gun he will go to jail. He says people here understand firearms. He says Australians are not free from gun crime. He cites several gun related violence articles. There was a hostage situation. There are 3 shootings a week in Sydney. Neighborhoods are in fear. There were 34 shootings in two years in Fairfield. He says the number of shootings in the past year was up to 157 from 129 the year before. He list several other shooting statistics. He mocks Author1 for saying he is not afraid of gun violence.


----------
D2
----------
Author1 tells Author2 that kids should be taught how to handle a gun. Author2 replies that all of his kids are crack shots. Author1 sarcastically commends Author2 for being an example to all responsible law abiding parents. He then tells Author2 that his kids are prepared for a killing spree at school. Author2 proceeds to insult Author1. He tells Author1 that his kids respect firepower. Author2 then makes a joke about his kids. Author1 responds by saying that the joke was unforgivable. Author1 makes several sarcastic remarks about kids packing and handling guns. Author2 tells Author1 that America is different from Australia. Author1 must be living under a rock in Australia. Author2 states that Americans are not paranoid about guns. Americans understand firearms. Author1 tells Author2 that Australia is the land of no guns. America is the land of the gun and the home of the fearful. Author2 corrects Author1 by reading headlines from Australian news reports. The news reports state that Australian criminals are getting guns from overseas. Australia is dealing with gun crime.


----------
D3
----------
Author1 is from Australia. He uses sarcasm to argue against Author2. Children should not be given guns. Guns are restricted in Australia. Children around guns might commit crimes. Training children to use guns is irresponsible. Parents should not let their children near guns. School shootings are due to children familiar with guns. It is better to have fewer guns around. In the US people have guns because they fear others. Australians don't need guns because they don't fear others. Gun control laws are good for everyone.

Author2 argues children should use guns. The earlier they understand guns the better. More practice with guns increases safety. Self defense is important for children and adults. Children can be trusted with guns. Gun control is a bad idea. Australia shows gun control doesn't work. Criminals can still get guns. Criminals import guns from overseas to use. Shootings and murders still take place in Australia. Gun control laws only make citizens in more danger. Everyone should have a gun and be able to use it.


----------
D4
----------
Author1 thinks guards being armed would be a good start. He asks what if the guard is overpowered. He thinks kids should be taught to use guns. Author2 thinks 4 or 5 is a good age to start. Author1 jokes that Author2 is a good example. If they decide to go on a killing spree you wouldn't want them to miss anyone. Author2 says he means teaching them respect for guns. If they went to a gun free zone and cut loose and missed he would shoot them himself. Author1 says the punishment should fit the offence. If after all those years they miss it's unforgivable. He asks if Author2 approves of kids packing guns in lunchboxes. Author2 says here we understand guns and aren't paranoid about the word. Author1 should sleep well knowing we have it under control. Author1 says they sleep well knowing they don't have anything to get under control. Author2 cites an article about Australians getting guns from overseas. He also cites the crime statistics associated with gun use in Australia.

