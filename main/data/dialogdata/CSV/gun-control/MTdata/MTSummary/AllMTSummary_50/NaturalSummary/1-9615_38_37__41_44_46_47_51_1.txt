
----------
D0
----------
Author1 says his wife would object to him having a husband. While he doesn't know what his response would be, he thinks he should follow Christ's example when the disciples responded to a violent mob that came to take him and not respond with violence. Author2 says Author1 shouldn't have a family if he isn't going to protect them and using Christ is a cop out. He says God used more violence and destroyed Sodom and Gomorrah. Also, Jesus was sent as a sacrifice. Author1 says Jesus commanded us to follow him. The acts of vengeance in the old testament are God's alone. Author2 thinks unless the Bible says not to defend yourself the position is nothing but a cop out. The correct answer is to keep your family safe. Author1 quotes "put away your sword, Peter." He says money is not worth taking someone's life. Author2 thinks Author1 is confusing Catholicism and Christianity. God ordered people to wipe out non-believers. Author1 says Catholics are the majority of Christians. Christianity expresses belief in the saints.


----------
D1
----------
Author1 states that if one follows the example that Jesus has set, that person will not respond violently to a situation. Author1 believes that when others are being harmed it is more difficult to not attack or kill someone, but one should follow the example that Jesus set by being non-violent. Author2 responds by stating that using Christ is a cop out, and then he insults Author1. Author1 asserts that Christ commanded that we follow him, and Author2 should not insult him just because he cannot defend his perversion of Christian belief. Author2 states that he needs to see where God said that a person must remain defenseless in the face of danger, and that one should protect their loved ones. Author1 proceeds to quote several passages of the bible that pertains to saints and resisting evil, which causes Author2 to retort that he is confusing Christianity with Catholicism. God commanded Christians to kill without mercy, then Author1 states that Catholics represent the majority of Christians. All major Christian denominations recognize the existence of saints.


----------
D2
----------
Author1 says he is not sure how he would respond to violence. He says Christ did not respond with violence, so he shouldn't either. He says he would not respond violently to theft, although he is unsure how he would respond if they were harming others. Author2 argues God used violence against people. He says Jesus was a sacrificial lamb which is why he did not react violently, but Author1 and his family are not sacrificial lambs. Author1 says the Bible says people are supposed to follow Christ's example. Vengeance belongs to God alone. Author2 says Author1 should prove where it says in the Bible that God says not to protect yourself if someone is trying to kill you. He says people shouldn't even have to think about what they would do, they should always say they would protect their family. Author1 quotes where the Bible says render to no one evil for evil. Author2 says God ordered his followers to kill entire cities just for being non-believers.


----------
D3
----------
Author1 believes that we should follow Christs example and not react violently to anyone. Even when people are assaulting us or taking our belongings violence is not the answer. Christ should be used as an example in all things. Vengance is not something that we should try to do, but rather we should be willing to lose our life for other people peacefully. When being threatened by an attacker it is better to give them what they want than to react with violence. Even when people are evil we should not be evil back to them. Christians are made up of Catholics, Anglicans, and Eastern Orthodox, who all represent 2 billion people.
Author2 argues that one should be prepared to defend one's family. Pacifism is not justified. God himself used force and killed many people. Force and violence should be used to defends one's self and family. The Bible does not anywhere instruct people to not defend themselves, and being this pacifist is not safe for your loved ones.


----------
D4
----------
Author1 believes it is always wrong for a person to use violence to defend his possessions. Refraining from defending his or her loved ones would be harder for anyone to do. Nevertheless, it is right to follow the example Jesus showed humanity. Jesus commanded his followers to be peaceable, so that is what everyone should do. The Bible says that only God can take revenge. He quotes several more passages that command peaceful nonresistance. He also explains the largest denominations of Christianity, and his understanding of the nature and standing of the Christian saints. Someone who shows he does not know these distinctions is displaying his ignorance. That is something people should try not to do. Author2 believes people should not have families if they are unwilling to protect them. He says God Himself uses violence, and Jesus was meant to be a sacrifice. That possible sacrifice is no excuse for cowardice. People must defend their loved ones. He also points out that certain Christians massacred thousands, which was not very saintly of them.

