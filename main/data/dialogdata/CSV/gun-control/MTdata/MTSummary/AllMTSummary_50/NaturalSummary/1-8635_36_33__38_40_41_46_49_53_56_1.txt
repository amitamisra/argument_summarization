
----------
D0
----------
Author1 says we've never banned ownership of weapons. It is being suggested that we consider this in the article. He thinks Author2 has reading comprehension difficulties and thinks if a law doesn't seem to produce the intended effects then no law will. Author2 thinks Author1 is showing ignorance. California has its own AWB, and the state has forced licensed owners of specific firearms to turn them in for destruction, even though the specific weapons weren't illegal under the law. He's showing how his argument can be interpreted another way. If current laws aren't being enforced, new ones won't be either. Author1 says at the time the Attorney General assured gun owners that rifles were legal and not subject to the confiscation order. A man owning one was arrested for possessing an illegal firearm. The Supreme Court said the rifles are illegal. Author2 thinks California law is more reason to prevent such an occurrence from happening. Author1 asks how authorities would find out the weapons were turned over. Author2 says they can traced back to buyers.


----------
D1
----------
Author1 tells Author2 that the ownership of certain weapons have never been banned, in which Author2 replies that California forced licensed owners of specific firearms to turn them in for destruction. Author1 talks about converted SKS rifles, and how the owners were told they were not subject to the gun confiscation order. A converted rifle owner got arrested in Santa Clara for possessing an illegal firearm which led to his prosecution. Author1 then tells Author2 about the case reaching the Supreme Court, and that they ruled that the rifle was illegal. Laws that attempt to address straw purchases are unenforceable, and Author2 tells Author1 that it is impossible to prove that an SKS was converted. He gives him examples of how difficult it is to catch the straw purchasers. Author1 ponders the sense of confiscating an SKS since it easily converts back to stock format, and that filling out a form does not combat the problem. Author2 tells Author1 that the way to combat the problem is to aggressively prosecute people who buy illegal guns.


----------
D2
----------
*Author1 states that the US has never banned automatic weapons, despite what individual states have decided. This is no threat to the Constitutional rights of US RKBA, state-level decisions are protected by the Constitution. There'd' be no logical reason to confiscate SKS rifles that could be switched back.

Author2 states that California has its own automatic weapon ban in place that forced licensed owners to turn over any such firearms. It's impossible to prove an SKS was converted to accept detachable magazines because it is so easily converted back to stock settings. If a banning can happen in California, it would be possible for it to move to other states or federal levels. It's impossible to pin illegal firearm possession on someone but not if you monitor straw purchasers.  

*Author2 is referring to a 1996 case where a man was told his converted rifle was legal by the then Attorney General. The man was later arrested for the same firearm, stating it was an illegal weapon. SCOTUS then officially ruled and determined it was illegal.


----------
D3
----------
Author1 argues that weapons have never been banned, but that it is a topic worthy of discussion. Certain laws are slow to produce effects, but that does not mean that no laws will have a positive result. California has laws banning specific types of weapons, and weapons that can be converted into that type of weapon have been confiscated and destroyed before. The state Supreme Court has decided that the rifles which were able to be converted into those illegal types of weapons are also illegal weapons. Straw purchase laws, where one person buys a gun for another, are also mostly impossible to enforce. Those types of laws should not be put into place because there is always a way to weasel out of it. 
Author2 argues that California has declared that certain weapons are legal, but then have confiscated those weapons and destroyed them. Since current laws are not being enforced correctly, new ones will not be either. Bulk purchases of weapons are monitored and are usually caught. Liberal courts do not combat crime.


----------
D4
----------
Author1 says the article is just suggesting banning these weapons. Author2 argues that California has an automatic weapon ban. He says owners were forced to turn them in, even though they weren't illegal weapons. He says current laws are not obeyed, so new ones wouldn't be either. Author1 refers to a case involving a gun that was banned, but which was able to be converted. The attorney General said at the time they were not illegal. However, a man was later arrested for owning one and the Supreme Court ruled the gun was illegal. Author2 says that is what he was referring to. He says it is necessary to prevent this happening to others. Because guns were banned and confiscated in California, it could happen anywhere. He says it is difficult to catch criminals breaking the laws. People who buy guns for others make it difficult to catch law-breakers. He says if the courts were stricter with punishment when they caught people breaking laws, it might be less likely to happen.

