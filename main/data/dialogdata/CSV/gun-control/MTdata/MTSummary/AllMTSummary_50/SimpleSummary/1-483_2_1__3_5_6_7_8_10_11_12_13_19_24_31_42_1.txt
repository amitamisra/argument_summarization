
----------
D0
----------
Author1 mentions the US's bans and restrictions on guns. Similar circumstances in Canada, England, and Australia hint that the US could be headed towards an eventual firearm buy-back or confiscation. These are the only two ways to get guns off of the street. A confiscation is essentially creating a police state. A buy-back will significantly damage the economy. By melting down guns they are wasting money and resources. With the US economy in its current state and government spending so vast, we need to be creating jobs and financial gains. Author1 acknowledges the Canadian government has a buy-back program and does not seize them as previously stated. But its trade-in rates compared to the FMV of the guns is abysmal. 

Author2 is from Canada and argues that Canada has not simply confiscated its guns, as Author1 suggested. It put a restriction on importing and/or buying new ones. You are allowed to keep the weapons you own. This only applies to handguns anyway. Hunting weapons aren't under these restrictions. Illegal weapons have no FMV anyway.


----------
D1
----------
Author1 argues that gun banning is bad. One way to ban guns is a buy back. The government will offer money for guns. The other way is confiscation. The government will simply take all the guns. Other countries use these methods. If they ban guns in the US one of those will happen. Government buy backs waste government money. The government will not get any use out of a buy back. The money they spend doesn't help anyone. Guns will go to waste. Gun buy back programs don't offer enough money. The values are estimated too low. It is basically stealing. People accept lower prices because they are scared.
Author2 argues that there are other alternatives. In Canada they have banned the sale of new guns. It is still legal to own guns. It is still legal to sell hunting weapons too. Only handguns are banned. Anyone can keep their old guns. No sales are allowed. This strategy works over time. It also costs the government less money.


----------
D2
----------
Author1 asks what happens after guns are banned. He thinks a confiscation or buy back. Author2 questions why those. Author1 thinks that's the way the UK and Canada gets rid of guns. He thinks that is what would happen here. These are the only two ways to get them off the streets. He thinks the first is one step closer to a police state. The second will ruin the economy. Author2 is says Canada hasn't gotten rid of guns. You just can't buy or import new ones. You can't resell old ones but you can keep ones you own. This applies to handguns. Hunting guns are legal. Author1 says he was mistaken. Canada doesn't take guns. He says they buy them back. He doesn't think many would volunteer because the government is paying a low price. Author2 says the amount paid is that much over market value because they can't be sold. Author1 thinks this implies the government should take them. Author2 disagrees. He says the market value is zero. He thinks it is fair.


----------
D3
----------
Author1 says the governments of the UK, Canada and Australia are getting rid of guns.  There are two ways to get guns off the street: confiscation and buyback.  Confiscating guns is a step toward a police state.  Confiscating guns is the government stealing.  To buy back guns at a fair price would cause economic collapse.  Market value of an AR-15 is at least $800.  Few people will volunteer to sell their weapon to the government for $50.  Forced buybacks are stealing by the government.

Author2 says that Canada is not trying to get rid of existing guns.  Canada does not confiscate anyone's gun.  Ownership of existing guns is legal, but you can't buy new handguns.  Hunting guns are still legal to buy in Canada.  The fair market value of a gun drops to $0 when the gun is illegal to sell.  $50 is $50 more than these guns are worth, since they're illegal to sell.


----------
D4
----------
Author1 says guns have been banned. He says confiscation or buy backs will happen. Author2 ask why. Author1 says that is what happened in the UK, Australia, and Canada. He says it is the only way police can get guns off the streets. Confiscation creates a police state. Buy backs are wasting money.  Author2 lives in Canada. He disagrees with this. He says you can keep your old guns. He says you can't buy new handguns. Author1 agrees Canada is buying guns. He says the buybacks are not voluntary. They only pay $50 regardless of value. He says very few gun owners have participated. He gives an example of a boy who sold his $900 gun. He ask if it was really voluntary or forced. Author2 says $50 is above the value of the gun now. The gun is not allowed to be sold to anyone else. The value of the gun is zero. Author1 insists the boy was cheated out of property value. He calls this government theft.

