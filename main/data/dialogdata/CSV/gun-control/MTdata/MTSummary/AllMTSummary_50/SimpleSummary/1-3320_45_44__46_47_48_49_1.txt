
----------
D0
----------
Author1 says you have to leave if you can do it safely. If your safety is immediately threatened you don't have to leave. There has to be an immediate threat to claim self defense. The law realizes sometimes you can not leave safely. The law requiring you to leave was repealed. The court realizes sometimes you can't leave safely. He suggests talking to a lawyer to prove this. Author2 says Author1 is wrong. He says there is no requirement to flee. He says you just have to prove you were in danger. Author1 says you have to flee if you can do it safely. He says he wasn't saying you always have to flee. He says to prove self defense you have to show you couldn't flee. He says a lawyer would confirm this. Author2 makes a sarcastic remark. He says people should call a lawyer when threatened with a weapon instead of using a weapon themselves.


----------
D1
----------
Author1 argues that self defense is a legal court plea. If you want to fight then it is not self defense. Self defense applies if there is an immediate threat. If there is no immediate threat it is not self defense. Fleeing is sometimes not possible. Fleeing should not be required. If it is possible to flee you should. If you harm someone instead of fleeing it is not self defense. Manslaughter, murder, and self defense are different. It is a crime to kill when you could have left. Self defense does not always apply. 
Author2 argues you do not need to flee. If there is a threat you can respond with force. You do not need to wait. You do not need to flee. A threat can be met with force. This is self defense. If there is a threat it is legal to defend yourself. You do not need to consider fleeing. Fleeing is not a requirement in the law anywhere. Self defense takes priority.


----------
D2
----------
Author1 states there is a difference between being required to retreat and the definition of self defense. He says that a being willing participant at any time then it is not self defense. He  continues that if there was an opportunity to get away and lethal force is used it would be difficult to claim self defense.  But best to contact a lawyer to find out the law exactly.
Author2 starts off saying Author1 is wrong. That there is no requirement to attempt flight. Being threatened with great bodily harm is enough. He continues to hold his ground that one doesn't need to try to get away. Self defense is a viable plea. That instead of acting first Author1 might want to call his lawyer.


----------
D3
----------
Author1 believes a person who uses lethal force under SYG should seek legal advice.  He notes laws used to require the proof of an attempted escape to allow for a self defense plea.  There is a difference between the definition of self defense and being able to make a self defense plea.  If at any time you are a part of the altercation, you cannot use self defense as justification for lethal force.  It is difficult to prove self defense if the perpetrator is deceased.  If you don't attempt to flee, you could be guilty of manslaughter or murder.  Use a phone instead.  

Author2 disagrees with Author1's advice in regard to legal counsel.  It is not required to attempt to escape in states having SYG laws.  If lethal force is necessary, it should be used.  Author2 supports SYG laws.  SYG is different from manslaughter or murder because it is self defense.  Self defense does not require an attempt to escape an attacker.


----------
D4
----------
Author1 says good luck trying to use the defense in court. There is a difference in being required to retreat in order for a self defense plea and the definition of self defense. If you are a willing participant in the fight it isn't self defense. You can't flee if there's an immediate threat. There isn't time. It's hard to justify lethal force without an immediate threat. Author2 says there is no requirement to attempt fight. He thinks all of Author1's logic is wrong. Author1 states he never said there was. He said if you have a chance to flee but use lethal force instead it isn't self defense. He says to talk to a lawyer. Author2 thinks Author1 is spinning his advice. He says there is no requirement to fight. All you have to do is show that you were threatened with harm. Author1 says he is just saying you will have a hard time justifying lethal force if you had a chance to escape and didn't. Author2 thinks you don't have to retreat.

