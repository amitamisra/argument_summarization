
----------
D0
----------
Author1 says he is not making any judgements about gun laws in America. He states he is annoyed to be mistaken for having the same views as another person who compares the US and Britain. He says he is neither pro nor con towards the issue. He did not express an opinion. He says he thinks Author2 should be allowed to have his gun. Author1 calls him a gentleman and scholar, and says guns have made the US a great country. 

Author2 says Author1 has communication problems. He mocks him for being a Mensa member. He says he admitted he's not that smart. Author2 claims to be humble. He blames vision problems for his communication problems. He discusses correct definitions. He accuses Author1 of jumping to conclusions. He says he should indeed be allowed to keep his rights and his gun, and thanks Author1 for accepting others views and admitting when he makes mistakes. He says Author1 is a stimulating person to have a conversation with, and he considers Author1 a friend.


----------
D1
----------
Author1 starts off on the defense saying he never made any judgments about the US or England's' gun laws. He continues in the same critical way and restates he never made any claims pro or con about the gun laws.  He goes on to explain that his "MENSA membership accomplishment lies at the bottom of his drawer" and that expresses "irony".  He concedes that Author2 should be able to keep his "pop gun". He ends it in a kum-by-ya moment with Author2 saying God, guns and guts have made America great.
Author2 starts off critically calling Author1 on his profile quote of "not to smart" and basically telling him that if he (Author1) doesn't have a thought on the gun control issue that he (Author1) is on the wrong forum and might be considered a troll.  As it progresses though they end up seeing eye to eye and making friends with each other.  All have their egos (and gun rights) in tact.  Author2 ends with a blessing of peace for Author1


----------
D2
----------
Author1 states that he should not be compared to someone that makes judgments about gun laws in the USA by making comparisons to Britain, then Author2 tells Author1 that he is ashamed that Author1 is a Mensa member. Author1 asserts that he does not have an opinion either way in regards to the gun laws of the USA, then Author1 tells Author2 that he never expressed an opinion. Author1 insists that his Mensa certificate is proof that a meaningless certificate can be obtained, and that is why he mentioned it to Author2. Author2 argues with Author1 over a typo that Author1 made, and Author1 tells Author2 that he should be able to keep his pop gun. Author2 continues to mention the typo, then he mentions a vote on keeping their rights and his popgun. Author2 insults Author1 even further, which causes Author1 to respond by saying that he is proud to have his shortcomings pointed out by Author2. Author2 states that he took advantage of a simple error to lighten the seriousness of the debate.


----------
D3
----------
Author1 believes people should be allowed to keep their guns. On the other hand, he says he has no desire to take a side in any kind of debate concerning America's gun laws. Belief in God, ownership of guns and dauntless courage is what has made America great. That is something he believes. On the other hand, he does not believe in analyzing the gun laws in America by comparing them to the ones in Britain. He says he has a right to have no opinion at all, and states that withholding an opinion does not make him a trouble-making, dissension-sowing troll. Author2 feels that any poster who has no side to take or point of view to offer in the discussion is likely to get labeled a troll, rather than as the intellectual Author1 appears to consider himself to be. On the other hand, he agrees that everyone should keep their gun rights, and particularly their guns. He believes that the two are friends and allies, for the dark days that may come.


----------
D4
----------
Author1 is annoyed that he's being lumped in with Penfold who is making judgements about the American gun laws by comparing them to Britain. He was under the impression that Limey meant English, not British. Author2 says Author1 has more communication problems than that and he's ashamed Author1 claims to be a Mensa member. Author1 says Author2 should be ashamed of his lack of reading skills and prejudice, not his Mensa certificate. Author2 replies Author1 has admitted he isn't too smart. He says he has glaucoma so he's going to occasionally spell words wrong, he would make an exception between inate and inane, though. Author1 says he has no opinion as to the gun laws of the US and didn't express one. If Author2 has a problem with Penfold he should take it up with him. He is sorry Author2 has glaucoma and mentions only Mensa because it's true, but Author2 jumping to the wrong conclusion isn't because of his condition. Author1 and Author2 think each other nice people and trade compliments as well as quips.

