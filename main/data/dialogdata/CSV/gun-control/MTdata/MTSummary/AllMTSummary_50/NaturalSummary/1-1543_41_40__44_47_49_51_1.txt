
----------
D0
----------
Author1 says that no one can prove that gun owners are safer than non gun owners.. They say that if Author2 had a family member die from gun violence it might be more significant to them. Author2 counters by saying that no one can prove non gun owners are safer than gun owners. Author2 says questions Author1's motives and says that if things can't be proven one more or another Author1 wants to infringe on their rights. Author1 says they are not trying to infringe. Author2 claims Author1 is trying to violate their rights using flawed data. Author1 says illogical arguments aren't a violation of rights. Author1 says that the data is not flawed just because it disagrees with Author2. Author2 continues to feel oppressed.


----------
D1
----------
Author1 says no one has been able to prove gun owners are safer than non-gun owners, but you can play with numbers to make the problem seem insignificant. He says you could say only 3,000 people died in 9/11 and make that seem like it's a small problem. Author2 asks if Author1 thinks you shouldn't infringe on rights because you can't prove something. He thinks the same could be said of cars, and to most liberals 3,000 isn't a big number. Author1 says he didn't say it had been proven. Author2 thinks Author1 should have the courage to say he wants to infringe on other's rights. Author1 asks where the Constitution says making illogical arguments violations somebody's rights. Author2 thinks it is a witch hunt.


----------
D2
----------
Author1 points out there is no empirical data suggesting that gun owners are safer than non-gun owners. Author2 (or any staunch pro-gun person) could use different statistics to infer this concept but it wouldn't be truly valid. Comparisons widely vary depending on the perspective terms they're placed in. Personal opinions and emotions will also make vaguely-relative data seem legitimate. Pro-gun perspective is: on 9/11, 3000 people died without the ability to defend themselves.

Author2 posits that if there is no definitive data proving safety with or without handguns, then on what evidence is a ban enforced? You can use statistics to promote your stance with anything. You cannot, though, use skewed statistics to undermine Constitutional rights. People from Author1's side are violating civil rights in disarmament.


----------
D3
----------
Author1 states there are no statistics proving owning a gun makes people safer. He says that numbers can be deceiving as to what creates a problem, citing the number of people killed in 9-11 as a small number to some and a large one to others. He says if Author1 had a personal or family encounter with gun violence, he would feel differently. Author2 says if there is no proof one way or the other,  then why infringe on his rights? Author1 says it is provable, but just hasn't been done. Author2 says illogical statistics are used to violate his rights. Author1 replies making a debate on something is not violating someone's rights. Author2 restates his rights are being violated.


----------
D4
----------
Author1 believes that there is no proof that gun owners are safer than non-gun owners, and that people who have had relatives die from gun violence have a different attitude. Author2 tells Author1 that nobody has proven that non-gun owners are safer than gun owners, so there is no reason to infringe on his rights. Author1 refutes this statement by stressing that he never said it can be proven or not, and Author1 asks Author2 how he is infringing on his rights. Author2 answers Author1 by stating that he is being illogical and he uses revisionist history to violate the constitutional rights of Author2, but Author1 asks Author2 to show him where it states in the Constitution that making an illogical argument violates his rights.

