
----------
D0
----------
Author1 argues that guns do not protect people. When people are being attacked, having a gun makes things worse. A survey in Philadelphia showed that guns increase risk five times. Urban citizens should not own guns. Guns increase danger to yourself and to others. Statistics can often be misleading. Surveys are often done wrong. Statistics should be looked at overall. Sample size affects reliability of statistics. No surveys show anything good happening when you have a gun in a fight. Too small of a sample size should not be applied to all American people. Guns in public makes everyone less safe.
Author2 argues that statistics are misleading. Author1's survey is meaningless because of other data. Conclusions in statistics cannot be trusted. Guns are used for self defense. Self defense is a good thing. People defend themselves from dangerous situations with guns. Guns should not be removed from public. Statistics can always be mistaken. Guns defend people every few seconds from being hurt by others.


----------
D1
----------
Author1 quotes a study done in Philadelphia. The study is about firearm possession and gunshot risk in assaults. The results said people with a gun are shot 4.46 times more often. The study included assault victims only. Having a gun did not protect people. Both speakers agree that statistics are flawed. Author1 trusts his study. Author1 believes there can be many answers to a study. Author1 argues that Author2's study is flawed. In Author2's study people self-reported the incident. This study includes alleged threats. The study does not require evidence. Author1 feels telephone surveys are not accurate. Author1 feels the sample size is too low.
Author2 asks Author1 to review his study. He wants Author1 to see other possible results. He gives an example using another study's results.  Author2 states that every 13 seconds an American defends themselves with a gun. Author2 believes even simple self-reported events may have ended badly without a gun. He believes a gun ended the threat. Author2 says there is no way to know in each report. Author2 believes self-reporting is useful. Author2 likes phone surveys. He thinks they are useful.


----------
D2
----------
Author1 cites an investigation. It examined the relationship between being shot in an assault and possession of a gun at the time. The study concluded that guns did not protect those who possessed them from being shot in an assault. Successful defensive gun use occurs each year. The probability of success may be low for civilian gun users in urban areas. Author1 believes that those users should reconsider their possession of guns. They should understand that regular possession necessitates careful safety countermeasures. Author2 refutes by telling Author1 to consider other possibilities when compiling these statistics. It already has been proven that guns are widely used in self-defense. Author2 also tells Author1 that criminals will still attack when guns are removed from civilians. Author1 believes that his conclusion is flawed.  Both Author2 and Author1 agree that studies and the statistics they produce are not reliable. Author2 insists that he has proven that millions of Americans successfully defended themselves with a gun. Author1 tells Author2 that he cited a tele-survey when he should have used better evidence.


----------
D3
----------
Author1 says we investigated the relationship between being shot and possession of a gun. We enrolled 677 participants that had been shot in an assault and 684 population-based control participants. We adjusted ratios for variables. Individuals with a gun were 4.46 times more likely to be shot than those without one. The ratio increased to 5.45 when the victim could resist. Guns didn't protect people from being shot. Author2 says that men are more likely to be struck by lightening. It's wrong to think criminals will be sympathetic when guns are removed. Author1 thinks this conclusion is flawed. Author2 thinks that Author1's are flawed. Author1 asks to see a study where having a gun produces a favorable outcome. Author2 says he's proven millions defend themselves with guns. Author1 says that would include people that get a gun for a noise. Author2 doesn't see the point. Author1 it doesn't count if no one's there. Author2 says someone could have been there. Author1 says the yearly DGU claim is based on a tele-survey of 1500 Americans.


----------
D4
----------
Author1 cites a survey. It examined if people who were shot and had a gun were safer. The study said people with a gun were 4-5 times more likely to be shot than someone without one. The study said having a gun did not protect people. Author2 argues that studies can be flawed. He uses another study as an example. He says every 13 seconds a person defends themselves with a gun. He says people without guns still get shot. Author1 says all studies have problems. He would like to see a study where having a gun helped. Author2 says he proved people defend themselves with guns. Author1 argues there is a difference. A home invasion is different from an assault in a public place. He questions how the study was done.

