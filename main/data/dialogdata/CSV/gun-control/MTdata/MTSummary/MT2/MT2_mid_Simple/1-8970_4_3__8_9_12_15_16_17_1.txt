
----------
D0
----------
Author1 says an AK-47 sells on the street in Miami for less than a PlayStation video game console. This means it is less expensive to buy an automatic weapon than a toy. Author2 disagrees that this is the case. Author2 wants Author1 to give specific evidence of the price of each. Author2 accuses Author1 of lying to try to distract from the discussion of gun regulations. Author1 says Author2 is the liar. Author1 believes Author2 will call anyone who disagrees with the pro-gun standpoint a liar. Author2 quotes prices of PlayStations and a higher price for the cheapest AK-47 available on the market. Author1 asks for proof that these prices are correct. Author2 insists no one would sell the AK-47 for less than it's worth.


----------
D1
----------
Author1 states than AK-47s are cheaper than Playstations on the street in Miami.  He objects to being called a liar by Author2.  A lie is a deliberate false statement.  Not complying with Author2's demands doesn't prove Author1 is a liar. Author2 should give evidence that Author1 is wrong.

Author2 challenges Author1 to give evidence that supports his claim.  Author1 will be considered a liar if he doesn't provide this evidence.  The cheapest AK-47 made costs $500.  Playstations cost between $5 and $300.  It is up to Author1 to prove what he's saying is true.  Author2 doesn't have to prove that Author1 is wrong.  Author1 should provide links that show that AK-47s can be purchased for under $300.


----------
D2
----------
Author1 says in Miami a Playstation costs more than an AK 47. Author2 asks exactly what the street price for an AK 47 is in Miami. Author1 replies it's less than a Playstation. Author2 says Author1 has made a claim but isn't backing it up. If Author1 can't give the exact price for an AK 47 then the statement is false. Author1 says a lie is a deliberate false statement. He says his statement isn't false. Author2 provides Playstation prices and the price of the cheapest AK 47. The AK 47 is higher than a Playstation. Author1 says Author2 is a liar. He asks the current prices available on Miami streets. Author2 says Author1 has to prove his statement. Author2 has provided his proof already.


----------
D3
----------
Author1 says in Miami guns are cheaper than consoles. Author2 asks what the street price of guns are. Author2 doesn't think Author1 is contributing. Author1 responds less than a PlayStation. Author2 calls Author1 a liar. Author2 again asks for the prices. Author1 says they aren't lying. Author1 says what they're saying is true. Author2 lists various prices. Author1 says Author2 just made numbers up. Author2 says Author1 needs to prove what they said. Author2 thinks Author1 is not contributing.


----------
D4
----------
Author1 says that an AK-47 costs less than a playstation in Miami. Author2 asks what the cost of an AK-47 is. He says Author1 is making it up. Author1 says he does know the price is less. Author2 says to name the price to prove it. Author1 says accusing him of lying is illogical. He says lying is being deliberately untruthful. His statement has not been shown to be untruthful. Author2 quotes prices of playstations and an AK-47. He says Author1 is wrong. Author1 accuses him of lying about the prices. Author2 says Author1 needs to prove he is right. He says he does not present any proof of figures. He says Author1 is denying he is wrong even though he is.

