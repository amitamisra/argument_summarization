
----------
D0
----------
Author1 says they are talking about gun buy back. Author2 says if you don't sell yours they will come get it. Author1 says gun laws were increased so people had to get rid of them, and he doesn't like the idea, but the government isn't taking away all guns. Author2 says they aren't taking away all the Jews. He asks if Author1 trusts by next year any Aussie will have a legal gun. Author1 asks how many nations support eliminating Jews and thinks comparing guns to Jews isn't fair because Jews are people and guns aren't. Also, Jews can't be replaced and it's an aggressive act. Guns won't stop the government. Author1 refers to the US government. Author2 thinks that confiscating guns equals tryanny.


----------
D1
----------
Author1 supports the government taking away some types of guns from citizens, but doesn't fully support a buy back program.  He believes a government will not automatically start oppressing people if they have no guns and also believe a government could oppress it's people, regardless if they have guns or not.  He also believes Author2 is comparing the elimination of Jews to the elimination of guns and doesn't like the comparison.  Author2 corrects Author1 and says he believes Jews were eliminated because their guns were taken away.  He also says if the government can't trust the public with guns, why should the public trust the government with guns.  He says the buy back program policy is simply confiscation.


----------
D2
----------
Author1 says it's just a purchase of citizens guns. Author2 says it is a forced sale. Author1 says not all guns are being banned. Author2 says that is like saying they did not take away all the Jews. He doubts Australians will be able to have any guns next year. Author1 points out the differences between Jews and guns. He says even if everyone had guns, the government could still oppress them. Author2 clarifies he meant they took away guns from the Jews. He argues the congress of 1775 was able to stop a government from oppressing them. Author1 says the world is more connected now and stable, and overthrowing a government would be hard. Author2 points out taking away guns is oppression.


----------
D3
----------
Author1 thinks a new gun law is a gun buyback. Author2 says if you don't sell your gun they will take it. Author1 says they aren't taking all guns away. Author2 compares it to WW2 Germany not taking away 'all the Jews.' Author1 says Author2 loves bringing Jews into conversations and asks how many countries wanted to eliminate Jews. Author2 provides a few answers. Author1 says comparing Jews to guns is ridiculous for multiple reasons. Author1 has no problems with Australian guns being taken away and asks what harm will come from it. Author1 says that even with no gun control, governments can still oppress. Author2 says it's fair because guns were taken from Jews. Author1 says governments could take control of us regardless.


----------
D4
----------
Author1 feels Author2 is merely being argumentative just for argument's sake and providing no factual information to support his/her claims. Author2's comparing this recent legislation to Nazi Germany is far-fetched and unfair to Jews. It is not complete disarmament if you have one firearm taken from you but are free to purchase other firearms. With or without guns the government reigns. 

Author2 is skeptical of a recently-passed decision to increase gun restrictions in Australia. This is not merely forcing citizens to return illegal weapons, it is a thinly-veiled attempt at disarming Australians innocuously. The same thing happened in Nazi Germany when they began confiscating guns from Jews. The government is insinuating if you don't adhere to the buy-back you will be arrested. This is tyranny.

