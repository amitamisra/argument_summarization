
----------
D0
----------
Author1 (an American) mentions multiple cases where police/military officers abused their RKBA in shooting without regard for bystanders and/or killing innocent victims. Australians like Author2 are not prepared/have no solution for preventing police/military officers from going rogue. The government is purchasing the weapons and ammo for these officers with the knowledge they're being used to kill. Citizens need to equally be able to defend themselves in the event of such situations. Regulations don't keep people from becoming violent. Author2 is resorting to sarcasm instead of offering up a valid response to the topic. 

Author2 is from Australia and claims to not have had major issues with rogue citizens discharging firearms or abusing their RKBA. In the event of such an incident, this would be even more reason for law enforcement to be armed. Self-defense is our ultimate right but this does not necessitate lax firearm ownership. Author1 is assuming American issues are prevalent globally. America's issue is gun-crazy radicals. If this situation arose in Australia, it'd be handled/regulated more efficiently than in America.


----------
D1
----------
Author1 argues that police endanger others lives when they engage in shootouts due to reckless firing. The cops ignore people around them when in shootouts and could easily hurt other people. There is no easy solution when the police and military are shooting and killing citizens. Police brutality and disregard for life are too common. Guns are becoming too much of a problem in too many ways. Police officers should practice stricter gun control and have better training. Police officers should not be the only ones armed or else they could cause too much damage when they go out of control. Australia is unprepared for rouge police agents. Government agencies should not have the power to randomly kill its citizens.
Author2 argues that citizens shoot and kill each other often as well. Rogue police officers are rare in Australia, and anywhere. If rogue police and military men become common then the public will demand guns. Socialist countries would be able to provide guns and instruction to all their citizens for free.


----------
D2
----------
Author1 thinks an officer was threatening the lives of everyone when he was engaged in a shootout, and anybody walking by was at risk of being hit. He asks the solution when members of the military police start killing people. Author2 asks if Author1 has examples of civilians killing each other. Author1 wants Author2 to stay on topic and provide an answer for what to to about police killing people. Author2 says if it were a problem he would expect to be given guns by the government and training to use it because Australia is socialist. Author1 thinks this means Author2 doesn't have a solution. Author2 jokes that we have a lot to learn from Australia. Author1 says they are talking about cops, not citizens, who Author2 thinks should be the only ones with guns because only they can be trusted. He says the argument is good until the cops start shooting people and you realize it isn't a good idea after all. Author2 says they are totally unprepared. All that saves them is prayer.


----------
D3
----------
Author1 is discussing a police officer who had a shootout against other police officers. He states how citizens were in danger from stray bullets. He ask how Australians think they would protect themselves if an officer went crazy since they can't own guns. He says there are many examples of authority figures going crazy like that. He chides Author2 for his country being so unprepared. He says giving arms to only the people you think can be trusted like police is foolish because if there is suddenly a time they are not trustworthy, you have no way to defend yourself. He says there is no laws that will stop a murderer who wants to murder. 

Author2 points out citizens go crazy as well. He says they don't have that problem over there with police. He makes jokes about what they would do. He says the chances of an incident happening are very low, they've only had two incidents, and one was a citizen. He says they believe in God and the power of prayer.


----------
D4
----------
Author1 is not in favor of stricter gun laws.  He is using rogue police officers and military as his reasoning behind this.  He cites instances of police brutality ranging from bystander shootings to officers putting down family pets which were not attempting to harm them.  He also speaks of instances wherein SWAT members have left their automatic weapons at crime scenes.  He calls attention to the Port Arthur incident, to which Author2 responds that incident was started by a law abiding citizen, until he began shooting.   He feels government officials are no more trustworthy than regular citizens.  
Author2 is from Australia where the public does not own guns.  He is in favor of stricter gun laws.  Author1 asks Author2 what people in his area would do if the same problems (rogue officers and soldiers) were to arise.  Author2 answers this doesn't happen there, but if it did they would request guns to protect themselves.    Author2 feels God will protect them, not guns.

