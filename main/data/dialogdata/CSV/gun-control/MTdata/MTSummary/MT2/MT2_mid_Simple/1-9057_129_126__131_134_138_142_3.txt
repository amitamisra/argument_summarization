
----------
D0
----------
Author1 mocks a training course Author2 took.  The course had students use fake weapons to simulate real fights.  Author2 is in awe of the course's instructor.  Author2 has claimed to have trained in real life dangerous situations.  The course Author2 took is not at all like real life danger.  Author1 would hope that people training for the military or police force understand the difference between simulated and real life danger.

Author2 is frustrated with Author1 for saying these things.  Author1 never claimed that his training was in real life situations.  He compares his training to the way the military and police forces are trained.  He had only mentioned the training because Doc accused him of not having any.


----------
D1
----------
Author1 raises doubt about Author2's training authenticity. Training with non-dangerous weapons or in simulated situations is not real-life experience. The fear and risk associated with these situations is absent in training situations. It is inaccurate for Author2 to truly claim to be trained in "real-life situations". Author1 likens this to medical emergency situations. Author2 is inflating his/her experience.

Author2 feels Author1 is simply being argumentative by qualifying Author2's fighting experience. Author2 learned under the expertise of an ex-police officer. The whole of the military and police force train in simulated dangerous situations. By all accounts, this provides them adequate experience in dealing with real-life situations. Author2 never declared to train in "real life situations". Author1 has no personal information enough to comment on Author2's abilities..


----------
D2
----------
Author1 tells Doc to be careful because this guy trained under the bullet proof monk for two. Author2 says he isn't bullet proof or a monk. He's a white ex-cop. Author1 says if he says that fighting with rubber knives is like the real thing then he's a charlatan. He asks what he says to do if you get stabbed. Author2 asks if Author1 is saying military and police training doesn't work. Author1 says he hopes they wouldn't say they train in real situations when they don't. Author2 asks if he should say he has never been trained. He thinks Author1 is bashing someone he has never met. He's falsely accusing him of saying he trained in real life situations when he didn't say that.


----------
D3
----------
Author1 jokes about Author2 training under a bulletproof monk. Author2 says he trains under an ex-cop. Author1 thinks Author2's trainer is a charlatan. Author1 does not think that using rubber knives is like a real life situation. Author2 agrees that it isn't like real life. Author2 says that police and military train the same way. Author1 doesn't think it's like military and police training. Author1 say police and military are more mature because they don't look up to a grandmaster. Author1 thinks that people like Author2 feel the need to prove themselves. Author2 insults Author1's reading comprehension. Author2 says Doc Jones called them untrained. Author2 does not like that claim because they do train. Author2 also does not like the unfounded negative opinions about their instructor.


----------
D4
----------
Author1 jokes that a guy trained under the bulletproof monk. Author2 tells Author1 that the guy is not bulletproof. He is not a monk. He is a white ex-cop. Author1 asserts that rubber knives are different than real ones. He believes the trainer is a charlatan. Author2 asks Author1 if he thinks that the training methods of the military and police are bogus. Author1 answers by stating that none of them claim to be trained in real life situations. Author2 states that Author1 accused him of being untrained. That is why Author2 stated that he was trained. Author2 accuses Author1 of insulting his instructor. He never told Author1 that he trained in real life situations. Author2 insists that these false claims are just stupid assumptions.

