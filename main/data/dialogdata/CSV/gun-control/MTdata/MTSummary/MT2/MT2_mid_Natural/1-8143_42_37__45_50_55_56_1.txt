
----------
D0
----------
Author1 takes issue with Author2's interpretation of results from a poll that Author2 created about gun control. Author1 believes Author2 is judging others by Author2's own standards, preconceptions and life experiences. Author1 is trying to convince Author2 that respondents provided truthful answers and that the results prove that Author1's pole hypothesis was simply wrong.  

Author2 believes his life experience and personal standards support his conclusion that gun owners will not be huge advocates for gun control, regardless of what the poll results showed. He thinks respondents could have lied and he refuses to derive any conclusions from the results of his own poll. He thinks his poll's accuracy is questionable because the poll results do not match what logic tells him.


----------
D1
----------
Author1 thinks logic suggests others are being judged by your own standards, and if answers don't fit your preconceptions it doesn't mean the answers are lies, it means your preconceptions are sub-optimal. Author2 thinks it's best to judge everything by the same set of standards rather than have two sets. Reason dictates gun owners will not be advocates for gun control. He knows this from experience and can trust experience or online polls. Author1 thinks everyone's standards need to match and the implication the poll may not be honest simply suggests a lack of experience. Author2 asks how he is biased. If logic says something different the poll may not be accurate. Author1 thinks Author2 is wrong. Author2 thinks that theory can't be definitive.


----------
D2
----------
Author1 says Author2 is not logical judging others according to his standards. Just because answers are not what Author2 wants doesn't mean the answers are wrong, but that Author2's beliefs are. Author2 says it is better to use one set of standards to judge. He says it's reasonable gun owners wouldn't support gun control, and his experience supports that. Author1 says Author2 is accusing people of lying because their experiences do not agree with Author2's, when it is Author2's experience that is insufficient. Author2 denies his poll was biased. Logic on issues makes him question it.  Author1 points out the poll results were completely against Author2's hypothesis. Author2 says that is because a poll is just theory and not definitive.


----------
D3
----------
Author1 thinks Author2 is judging by their own standards and not being logical. They say that Author2 thinks things are lies because they don't fit Author2's preconceptions. Author2 thinks all things should be judged by the same standards. Author2 thinks it is reasonable that gun owners won't support gun control, and has anecdotal evidence to back it up. They insist anecdotal evidence is more important. Author1 says Author2 needs to have their standards match other people and that Author2 was implying that poll respondents weren't lying just because it doesn't match Author2's anecdotal evidence. Author2 says their poll is not biased and logic dictates that poll results other than expected may indicate a problem with the poll. Author1 accuses Author2 of ignoring contrary evidence.


----------
D4
----------
Author1 tells Author2 he's being illogical in the way he interprets his polling data.  He says that Author2 is rejecting the results of his own poll because he doesn't like the answers he's getting.  But if the poll results don't match the hypothesis, it means the hypothesis is wrong, not that everyone who took the poll is lying.

Author2 considers what he does to be valid, saying he's using a single set of standards for everyone.  If his life experience tells him one thing, and a poll tells him the opposite, he's not going to assume the poll is accurate.  Author2 believes the fact that the poll results don't match his own experience casts doubt on the poll's accuracy.

