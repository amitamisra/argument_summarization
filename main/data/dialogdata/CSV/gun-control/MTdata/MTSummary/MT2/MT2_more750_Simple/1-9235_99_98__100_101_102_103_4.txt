
----------
D0
----------
Author1 tells Author2 that the preamble is not the law. It is an introduction to laws that follow. Author2 agrees that is the definition for their existence. The Constitution is the source of those rights that cannot be removed by government. Author1 shares his reason for pointing out that the preamble is not law. Liberals enact social engineering legislation that is outside the scope of the Constitution. Promoting the general welfare does not mean enriching individuals with monetary rewards for doing nothing.  Author1 then asks Author2 for links to Walter William and Thomas Sowell. Author2 gives Author1 a link to Walter William's website. He describes books that were written by scholars of the Constitution. Author2 continues to list links to sites that have historical information about the Constitution. Author1 then thanks Author2 for all of the links that he gave to Author1. Author2 explains why he chose to quote the preamble to the Bill of Rights. The Bill of Rights enumerates in great detail all of the power that Congress is ever allowed to have.


----------
D1
----------
Author1 says the Preamble is not a law. It's an introduction to them. It could be considered a precedent for the laws. He says the authors listed rights. People should have them because they were considered God given rights. He says liberals tend to misinterpret the part that says "promote the general welfare." They use it as an excuse to create social welfare legislation. That isn't covered by the Constitution. He says he would like the links to the author. 

Author2 says the Preamble is a definition of laws. Since it's part of the Constitution it protects those rights from being taken away. He ask if Author1 would like links to an author. He says people are confusing the Declaration of Independence with the Constitution. He says there are many good websites on the Constitution. He list them for Author1. He says there is a reason he quoted the Preamble. Article 1, section 8, details what powers Congress has. If it isn't in there, they don't have the power.


----------
D2
----------
Author1 says the preamble isn't law. It's an introduction of the laws that follow. Author2 agrees. It's a definition of why they exist and as part of the Constitution and the source of those rights. They can't be removed by government. Author2 thinks this is statutory precedent. Author1 says he is careful making the point that the preamble isn't law because liberals like to say general welfare gives them authority to enact legislation outside of the scope of the Constitution. General welfare doesn't mean giving people money for nothing. Author2 says that's in the Declaration of Independence. He provides a link to a website on JFPO. He gives many sites that he believes are good. He apologizes for the delay in responding. He has been busy with dough. Author1 thanks him for the links. Author2 says this is why he quoted the preamble to the BOR. Congress has its powers enumerated in article 1, section 8. Any claim to the contrary is not withstanding. He then talks about his hobbies and why he enjoys them.


----------
D3
----------
Author1 states that the "Preamble" is NOT law. It is an introduction to the Constitution.  He believes that liberals use the "promote the general welfare" part of the Preamble. They use it to "enact social engineering legislation" not covered by the Constitution.  
Author2 agrees with Author1 that the Preamble is not law. He goes on to explain it as being a definition of why the Constitution was written. He says the laws in the Constitution are rights that are given by a higher power (i.e. God). Those rights would exist written or not. He states that these rights can not be diminished or removed by any government branch. He lists some useful links. The links have to do with the Constitution and history.  He mentions that Congress has its powers enumerated in Article 1 Section 8.  "If it isn't enumerated, they don't have it." Congress doesn't have any rights not listed.


----------
D4
----------
Author1 argues that the Preamble of the constitution is not law. The purposes in the Preamble are not justification for laws. Social changes cannot be enacted based on the Preamble. Liberals use the preamble to defend their ideas. Liberal ideas of social engineering or handing money out are not justified. The Preamble merely introduces the Constitution. It is not legal to cite the Preamble as justification. New laws must not stem from the Preamble.
Author2 argues that the Preamble defines why the document exists. The definitions for why the Constitution exists are important too. The Preamble does provide a precedent for laws. Those ideas can't be taken away by any other branch. The Preamble is also part of the Declaration of Independence. Congress's powers are outlined in the Constitution. Article 1 section 8 details what powers Congress has. Congress only has these powers, no others. If the power is not there congress does not have it. Many scholars have published works on the Constitution.

