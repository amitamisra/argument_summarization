
----------
D0
----------
Author1 says that it's not proven owning a gun is safer than not. They say that if Author2 had a family member die of gun violence they might care more. Author2 counters by saying that no one can prove not owning a gun is safer than owning one. Author2 questions Author1's motives. Author2 says it can't be proven one way or another. Author1 says they are not trying to infringe. Author2 claims Author1 is trying to violate their rights using flawed data. Author1 says illogical arguments aren't a violation of rights. Author1 says that the data is not flawed just because it disagrees with Author2. Author2 continues to feel oppressed.


----------
D1
----------
Author1 says no one has been able to prove gun owners are safer than non-gun owners. You can play with numbers to make the problem seem insignificant. He says you could say only 3,000 people died in 9/11 and make that seem like it's a small problem. Author2 asks if Author1 thinks you shouldn't infringe on rights because you can't prove something. He thinks the same could be said of cars. He thinks to most liberals 3,000 isn't a big number. Author1 says he didn't say it had been proven. Author2 thinks Author1 should have the courage to say he wants to infringe on other's rights. Author1 asks where the Constitution says making illogical arguments violations somebody's rights. Author2 thinks it is a witch hunt.


----------
D2
----------
Author1 mentions lack of empirical data linking gun ownership with safety. Author2 and similar people could use many sources of "statistics" to infer the link between the two. Comparisons widely vary depending on the perspective terms they're placed in. In addition, personal opinions, experiences and emotions will solidify shaky information as implicit fact. Pro-gunners view 9/11 as 3000 people needlessly dying unarmed. 

Author2 admits there is no definitive statistics linking gun-ownership and safety. But there's no evidence otherwise either. If no such evidence exists, on what grounds is the ban based? You can skew any statistics to legitimize any position. But skewed data references cannot be used to undermine Constitutional rights. Author1 and similar people are violating citizen civil rights under the guise of disarmament.


----------
D3
----------
Author1 states there are no statistics. He said there is no proof owning a gun makes someone safer. He says numbers are deceiving. What is high to one is small to another. He uses the number killed on 9-11 as an example. He states Author2 would feel differently if his family had been a victim. Author2 says if there is no proof, why violate his rights? Author1 says the issue is provable. He says no one has attempted to do it. Author2 says illogical statistics are used to violate his rights. Author1 says debating something isn't taking away rights. Author2 restates his rights are being violated.


----------
D4
----------
Author1 believes that it is unknown whether gun owners are safer than non- gun owners. People who have had relatives die from gun violence think differently. Author2 tells Author1 that it is unproven that non-gun owners are safer than gun owners. Author2 believes that Author1 is using this argument to infringe on his rights. Author1 knows it is unproven. He further refutes by asking Author2 how he is infringing on his rights. Author2 answers by stating that Author1 is being illogical. He believes Author1 is using revisionist history to violate his constitutional rights. Author2 asks Author1 to show him how he used the Constitution to violate his rights. The Constitution does not state that using an illogical argument is a violation of a person's rights.

