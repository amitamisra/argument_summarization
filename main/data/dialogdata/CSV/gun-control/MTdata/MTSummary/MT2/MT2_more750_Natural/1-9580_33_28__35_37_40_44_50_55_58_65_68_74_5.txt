
----------
D0
----------
Author1 says that you need a warrant to search a home or car, unless you consent to a search. Because the man was within was within 1000 feet of a school, he was expelled for having shotguns in his trunk. There are no exceptions that loaded firarms near schools are illegal. The student was not charged with a crime, but rather expelled from school after the school searched his car. The school asked the cops to search the car. The law allowed the school to search his car. 

Author2 says that the student was not breaking any laws. His shotgun was unloaded and in the car, and he was not breaking any law or else he would have been arrested. There is an exception in California law that says unloaded long guns in the trunk of a car are acceptable. The school district should not have searched his car. The police should not have searched his car, and the school was wrong to expel him for not breaking any laws at all.


----------
D1
----------
Author1 feels a student was rightfully expelled from school after a search of his truck found an unloaded shotgun in the back seat.  The student knowingly had a firearm within 1000 feet from the school, and although he consented to the search of his vehicle, Author1 feels the school district was within its right to act on the expulsion.  He notes the report also indicates there were shotgun shells in the truck which were found to have been bird shot.
Author2 feels the school was wrong for contacting the police to search the young man's truck because it was parked on a public roadway, not in the school parking lot.  He believes the DA's decision not to prosecute the young man indicates the gun was in a closed container; therefore, indicating an exception to the law regarding the distance limit.  He disagrees with the school district's decision to expel the young man due to the fact his vehicle was parked on a public roadway and there was no indication of illegal activity.


----------
D2
----------
Author1 believes expelling the student was proper. He did consent to a search and did have an illegal gun. Within 1000 feet of schools, guns are forbidden. Dog sweeps are legal, because they are not considered searches. The local newspaper says that he opened his vehicle for a search and that the guns were there on the back seat. Guns are illegal near schools whether or not they are loaded. There were shells with them, too, though only birdshot. Even if it was not illegal, the student was not charged, only expelled. Author2 believes the schools should not have the authority to expel students who probably did nothing illegal. The gun was unloaded and the truck was locked. It may well have been in a container. Firearms are allowed where he was parked. And he thinks transporting unloaded long guns is legal. The point is that the school had his truck searched, called the police and expelled him even though he did nothing that was criminal. They should not have such authority.


----------
D3
----------
Author1 is discussing a case- " Student expelled for having unloaded shotguns in truck." He says the student consented to searching his truck. He thinks it was legal to do a search if no guns are allowed to be within 1000 feet of a school. The rule for no firearms  doesn't make any exceptions for them being unloaded. He says there was birdshot ammo in the truck. He notes he was not charged with a crime, just expelled from school. He states the cops searched it, not the school. He says zero tolerance rule means not even if it is legal otherwise.

Author2 says the DA didn't prosecute, then it must be legal. He says there is nothing in the California firearms laws that says having an unloaded shotgun in his truck was illegal. Author2 says there is an exception to the footage rule if they are legally transported unloaded long guns. Author2 questions what gives the school the authority to expel a student over a legal activity.


----------
D4
----------
Author1 thinks a warrant is needed to search a home. The student consented to open his trunk while the truck was parked within 1000 feet of the school. If guns aren't allowed, it's possibly legal to have dogs sweep the zone. Author2 thinks firearms are obviously allowed because it wasn't prosecuted. Author1 says it happened in California and the student was expelled. Author2 says it wasn't indicated if the gun was in a container and he wasn't able to find where it is illegal to have one. Author1 says it's covered in the no guns within 1000 feet of a school part. Author2 says there's an exception for transporting unloaded long guns and nothing indicates his doesn't fit that. Author1 asks to see the specifics. Author2 says in California it's legal. Author1 says he wasn't charged, just expelled. Author2 asks what gives the school the right to search and expel. Author1 replies permission of the owner and the cops did the search, not the school. Author2 thinks this is symantics and asks what gives them authority.

