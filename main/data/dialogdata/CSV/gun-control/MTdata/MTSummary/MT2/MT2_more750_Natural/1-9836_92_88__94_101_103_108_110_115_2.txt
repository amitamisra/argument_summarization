
----------
D0
----------
Author1 thinks the idea of teaching very young children to shoot guns is darkly comic. Young kids who grow up accustomed to using guns might well be still more lethal than those who learn to shoot later. In Australia, where he lives, he believes he observes that people feel safer and secure than folks seem to feel in America. In his country, guns are few and those who desire to bring them into everyone's everyday lives are far away. So most Australians are not as fearful as Americans are. Author2 believes that it is very important for children to learn how to defend themselves as early in life as possible. Learning to use guns while they are still young also teaches boys and girls to respect firearms. They will be safer throughout their lives if they learn to shoot young. Also, he has read many stories about guns being brought into Australia and used in gun violence there, so he believes the country might not be as serene as some people appear to think.


----------
D1
----------
Author1 jokes kids should be armed at birth. He jokes about Author2's kids becoming school shooters. He says they are not afraid there since there is a lot of space between their gun free worry and Author2's home where people are afraid. 

Author2 says he started teaching his kids to shoot at 4-5. He says it teaches them respect for firearms. He says that Author2 lives in Australia, where he has to be afraid if he even says the word gun he will go to jail. He says people here understand firearms. He says Australians are not free from gun crime just because they are illegal. He cites several gun related violence articles. They include a hostage situation, 3 shootings a week in Sydney with neighborhoods in fear, and 34 shootings in two years in Fairfield. He says the number of shootings in the past year was up to 157 from 129 the year before. He list several other shooting statistics, and mocks Author1 for saying he is not afraid of gun violence.


----------
D2
----------
Author1 tells Author2 that kids should be taught how to handle a gun, and Author2 replies that all of his kids are crack shots. Author1 sarcastically commends Author2 for being an example to all responsible law abiding parents. He then tells Author2 that his kids are prepared for a killing spree at school, which causes Author2 to insult Author1. He tells Author1 that his kids respect firepower, then Author2 makes a joke about his kids. Author1 responds by saying that the joke was unforgivable, then Author1 makes sarcastic remarks about kids packing and handling guns. Author2 tells Author1 that America is different from Australia, and that Author1 must be living under a rock in Australia. Author2 states that Americans are not paranoid about guns, and that they understand firearms. Author1 tells Author2 that Australia is the land of no guns, while America is the land of the gun and the home of the fearful. Author2 corrects Author1 by reading headlines from Australian news reports about gun access and gun crimes that Australia is dealing with.


----------
D3
----------
Author1 says that all children should be taught how to handle a gun as soon as they are born. However, Author1 is being sarcastic when he later reveals that he lives in Australia, where guns are restricted. Guns should not be provided to children. Children who are familiar with guns might be more likely to commit school shootings. Parents who give their children guns are irresponsible. It is better to have fewer guns. When you have more guns you are more fearful of others and have more gun violence. 

Author2 argues that children should be trained how to use guns early. More practice with guns leads to safer citizens in the future. Gun training increases personal safety. Children can be trusted with guns. Guns control laws do not work, as shown by the situation in Australia. Criminals are still able to get and use guns to commit crimes even when they are illegal. Criminals have been importing guns from overseas into Australia to use in murders and shootings.


----------
D4
----------
Author1 thinks guards being armed would be a good start but asks what if the guard is overpowered. He thinks kids should be taught to use guns. Author2 thinks 4 or 5 is a good age to start training. Author1 jokes that Author2 is a good example, if they go on a killing spree you wouldn't want them to miss anyone. Author2 says he means teaching them respect for guns, but if they went to a gun free zone and cut loose and missed then he would shoot them himself. Author1 says the punishment should fit the offence. If after all those years they miss it's unforgivable, and asks if Author2 approves of kids packing guns in lunchboxes. Author2 says here we understand guns and aren't paranoid about the word. Author1 should sleep well knowing we have it under control. Author1 says they sleep well knowing they don't have anything to get under control. Author2 cites an article about Australians getting guns from overseas and the crime statistics associated with gun use in Australia.

