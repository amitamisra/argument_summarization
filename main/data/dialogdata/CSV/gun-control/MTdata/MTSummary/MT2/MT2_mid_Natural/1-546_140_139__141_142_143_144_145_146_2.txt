
----------
D0
----------
Author1 had responded to a post someone else put up, and he tries to exclude Author2 from the discussion, saying his message was not addressed to Author2.  He concedes that the forum is not his own private message board, but says his message was for someone who could debate intelligently without using insults (Author2 calls Author1 "gun hater pilot"), and therefore didn't apply to Author2, who he accuses of spamming the board.

Author2 makes four nearly identical posts, badgering Author1 to respond.  Author2 asks how Author1's post relates to gun control.  He says gun haters like Author1 try to disrupt discussions.  He defines infringement, and says you cannot be pro gun rights if you're willing to limit those rights, as Author1 is.


----------
D1
----------
Author1 initially responded to another commentator on this forum. Author2 is being zealously aggressive and falsely labelling (me) as a gun hater or a member of a hate group based on slightly conflicting opinions. Author2 is obnoxiously posting and reposting the same comments over and over again like a spammer.

Author2 feels Author1's comments contradict his/her claims of being pro-guns. You cannot be pro-gun but support limited gun control. Author1 needs to keep their posts to private message boards. Gun control laws infringe on 2nd Amendment rights. The definition of 'infringe' is, in part, "an encroachment on a right or privilege". Author1 resorts to insults and unfair labeling when faced with opposing viewpoints. Author2's behavior in vilifying opponents is much like hate group member behavior.


----------
D2
----------
Author2 asks what the post has to do with gun control. He wonders if the conversation is being disrupted because it can't be won and then gives the definition of infringement. Author1 says it was not addressed to Author2. Author2 repeats his comment and points out that it's not a private message board. Author1 agrees it's not a private board and explains it was in reply to another post with someone able to debate intelligently. He thinks he has been falsely labeled a gun hater. Author2 says people that want gun control laws that infringe on the right to bear arms are gun haters and that's why he was called one. Author1 says he won't call Author1 names and Author2 repeats his statement in reply.


----------
D3
----------
Author1 responds to someone's comment by joking that he will post it at work so that people can laugh at how absurd it is. The comment is directed at another poster on the message board, not Author2. Author1 says Author2 is just being disruptive and spamming the board by saying the same thing over and over. Author2 repeats the same argument several times, that Author1 claims to believe in the right to bear arms, but also believes guns should be regulated. This makes Author1 a gun hater because those two ideas are contradictory. Infringement is defined as encroachment on a right or privilege. If Author1 believes in the right to bear arms, he or she can't also believe in the government's infringement on that right.


----------
D4
----------
Author2 insults Author1 and then asks him what his post has to do with the topic of gun control, then he proceeds to tell him the definition of infringement. Author2 defines infringement as an encroachment or trespass on a right or privilege, then Author2 questions his stance on the right to bear arms because Author1 wants to limit those rights. Author1 states that he was not addressing Author2, which leads Author2 to repeat his stance. Author1 clarifies his statement by saying that it was directed to another person. Author2 insists that Author1 is a gun hater, but Author1 wants to debate without labeling someone as a member of a hate group. Author2 accuses Author1 of vilifying his opponents, then Author2 defines infringement one more time.

