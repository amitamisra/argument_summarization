
----------
D0
----------
Author1 a warrant is required to search a home or car. If you consent to a search you waive that right. The student was near a school. He was expelled for having long guns in his car. He consented to a search. Having guns in your car near a school is illegal. The student was not charged with a crime. The student was instead expelled. The school was ok in asking the cops to search the car. The student broke a zero tolerance policy. 

Author2 argues that the student was not breaking a law. His shotgun was unloaded and stored. He would have been arrested if he had broken a law. Unloaded shotguns in a trunk are allowed near a school. California law has exceptions. The school district should not have searched his car. The school district should not have expelled the student. He was not breaking any laws or doing anything wrong. Law differs depending on the state. He did not break California law or regulations.


----------
D1
----------
Author1 agrees the young man should have been expelled.  The law states firearms are not allowed within 1000 feet of a school.  The student knowingly had a firearm in his vehicle while parked outside the school during a class day.  The law is clear on the distance limit concerning firearms and schools.  A dog sweep was done prior to the search, this is what indicated the possibility of a firearm in the vehicle.

 Author2 feels the school district crossed a line by expelling the student.  The vehicle was parked on a public road, not the school parking lot.  The shotgun was not loaded.  The shotgun was in a case, not exposed.  The student openly agreed to the search, indicating the gun was not for illegal purposes.  He feels the school district violated the young man's rights by searching his vehicle.  Why would there be a dog sweep of a vehicle parked on a public roadway?  If illegal, why were there not charges filed?


----------
D2
----------
Author1 believes expelling the student was right. He had agreed to a search. Guns are in fact forbidden within 1000 feet of a school. Dog sweeps are not searches. Therefore they are likely to be legal. The newspaper says he himself opened his truck for the search. The guns were right there on the back seat. Guns are illegal near schools, whether they are loaded or not. The police also found shells, though they were only birdshot. He was only expelled. He was not charged, even his actions were illegal. Author2 believes the school exceeded its authority. Schools should not be allowed to expel such students. He probably did nothing illegal. The gun was not loaded. The truck was locked. It may have been in a container. Firearms are allowed where he parked. Transporting unloaded long guns is legal. The school called the police. They searched the truck. The school expelled him improperly, since he was never charged. He did nothing criminal. They should not have such authority.


----------
D3
----------
Author1 is discussing a case. A student was expelled for having unloaded shotguns in a truck. The student consented to the search. He thinks the search was legal. No guns are allowed within 1000' of a school. The rule doesn't make exceptions for unloaded guns. He says there was birdshot ammo in the truck. He was not charged with a crime. He was expelled from school. The school did search it. The cops searched it. He says zero tolerance means not even if it is legal. 

Author2 says the DA didn't prosecute. It must be legal. He says CA firearms laws did not say it was illegal. He says there was an exception to the footage rule. Legally transported unloaded long guns were okay. He questions what gives the school the authority to punish a student over a legal activity.


----------
D4
----------
Author1 thinks a warrant is needed to search a home. The student consented to open his trunk while the truck was parked within 1000 feet of the school. If guns aren't allowed then it's possibly legal to have dogs sweep the zone. Author2 thinks firearms are allowed because it wasn't prosecuted. Author1 says it happened in California and the student was expelled. Author2 says it wasn't indicated if the gun was in a container and he wasn't able to find where it is illegal to have one. Author1 says it's covered in the no guns within 1000 feet of a school part. Author2 says there's an exception for transporting unloaded long guns. Nothing indicates his doesn't fit that. Author1 asks to see the specifics. Author2 says in California it's legal. Author1 says he wasn't charged. He was just expelled. Author2 asks what gives the school the right to search. Author1 says permission of the owner. The cops did the search. Author2 thinks this is symantics. He asks what gives the school the authority for this.

