
----------
D0
----------
Author1 says his wife would object to him having a husband. He doesn't know what his response would be. He thinks he should follow Christ's example when the disciples responded to a violent mob that came to take him. He shouldn't respond with violence. Author2 says Author1 shouldn't have a family if he isn't going to protect them. Using Christ is just a cop out. He says God used more violence when he destroyed Sodom and Gomorrah. Also, Jesus was sent as a sacrifice. Author1 says Jesus commanded us to follow him. The acts of vengeance in the old testament are God's alone. Author2 thinks unless the Bible says not to defend yourself the position is nothing but a cop out. The correct answer is to keep your family safe. Author1 quotes "put away your sword, Peter." He says money is not worth taking someone's life. Author2 thinks Author1 is confusing Catholicism and Christianity. God ordered people to wipe out non-believers. Author1 says Catholics are the majority of Christians. Christianity expresses belief in the saints.


----------
D1
----------
Author1 states that one should follow the example that Jesus has set. A person should not respond violently to a situation. Author1 believes that when others are being harmed it is more difficult to not attack or kill someone. One should still follow the example that Jesus set by being non-violent. Author2 responds by stating that using Christ is a cop out. He insults Author1 by calling him a weak pacifist. Author1 asserts that Christ commanded that we follow him. Author2 should not insult him just because he cannot defend his perversion of Christian belief. Author2 states that he needs to see where God said that a person must remain defenseless in the face of danger. One should protect their loved ones. Author1 proceeds to quote several passages of the bible that pertains to saints and resisting evil.  Author2 retorts by stating that he is confusing Christianity with Catholicism. God commanded Christians to kill without showing mercy. Author1 states that Catholics represent the majority of Christians. All major Christian denominations recognize the existence of saints.


----------
D2
----------
Author1 is not sure how he would respond to violence. He says Christ did not respond to violence. He says he shouldn't either. He says he would not respond violently to someone trying to steal from him. He's not sure what he would do if they were hurting other people. Author2 argues God used violence against people. He says Jesus was a sacrificial lamb. That's why Jesus was not violent. He says Author1 is not a sacrificial lamb sent from Heaven. Author1 says the Bible says people are supposed to be like Christ. He says vengeance is for God to do. Author2 says to prove God said don't defend yourself. He says you shouldn't even have to think about it. You should automatically say you would protect your family. Author1 quotes where the Bible says not to respond to evil with evil. Author2 says God ordered his followers to kill whole cities of people. They were killed just for being non-believers.


----------
D3
----------
Author1 argues we should follow Christs examples. Violence is not the answer. Christ tells us not to react violently to anyone. Vengeance is not a good thing. Giving attackers what they want is a good idea. We should give up our lives willingly. Pacifism is good. 2 Billion people are Christian. Catholics, Anglicans, and Eastern Orthodox are all Christians. Muggers should be given what they want. Christ died for us. He was beaten and did not fight back. Even evil people should not be treated with evil. 
Author2 argues that one should defend their family. Pacifism is not justified. We should be willing to fight for what we believe in. God used force. God killed many people. Force should be used to defend yourself. The Bible never tells people to not defend themselves. Being a pacifist is not safe for those around you. God used armies to wipe out millions. We should not follow Christs example. Christ was supposed to be a sacrifice for us. He was not supposed to get you killed.


----------
D4
----------
Author1 believes people should not defend mere possessions with violence. Doing so is wrong. It is against the law of God. Not defending loved ones is harder to do. Yet it is making the right choice. Christian believers should always follow the peaceful example of Jesus. He commanded his followers to be peaceable. Everyone should remember to do that. Only God can take vengeance. That Bible says that. Several more Bible passages counsel nonviolence. He believes it is important to understand which the largest Christian denominations are. He also explains Christian saints. They are people worthy of being with God. Showing confusion about these distinctions displays ignorance. People should not do that.  Author2 believes people must defend their families. Otherwise they should not have them. He points out that God uses violence in the Bible. Also, Jesus came to earth to be sacrificed. That possibly mythical sacrifice is not an excuse for cowardice. People must defend their loved ones. Anyway, certain Christians massacred thousands of innocent people. That was not very saintly.

