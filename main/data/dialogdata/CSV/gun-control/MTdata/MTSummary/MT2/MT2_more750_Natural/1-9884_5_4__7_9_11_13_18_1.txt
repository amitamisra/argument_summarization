
----------
D0
----------
Author1 states most firearms aren't recovered. About 63% of guns recovered in Toronto originated in the US and a large number have their serial numbers removed. According to Royal Mounted Police 2,637 guns were reported and 925 were linked to a dealer, and 3/4 of those originated in the US. Author2 asks how they were linked to the US if the serial numbers had been removed. Author1 says those weren't part of the traceable group unless related to ballistics. Author2 thinks this is ridiculous. Failed ballistics tests aren't traceable. Author1 says comprehensive statistics are scarce, but recent cases point to the problem. His point is even with the serial numbers removed, some can still be traced. Author2 says he doesn't see anything that suggests ballistics determined anything and there's no mention of testing the slugs. Furthermore, BD can only compare rounds linked to a crime, so no random match is possible. Even that is negligible. Author1 says he never suggested anything about ballistics databases, Author2 did. He initially used the term ballistics rather than forensics.


----------
D1
----------
Author1 states that most firearms used in crimes are not recovered, and approximately 63% of the traceable handguns recovered in Toronto originated in the US. A large proportion of the firearms recovered in crimes have had their serial numbers removed, but Author1 wonders how it was determined that they originated in the U.S. The serial numbers were removed, but Author2 tells Author1 that they were not part of the traceable group unless it was through ballistics. Author2 believes that ballistics traces are not possible, and he ponders the usefulness of a long gun registry. Author1 outlines an article that details a case where Ottawa police traced a gun, and he states that a gun can be traced without the serial numbers. Author1 admits that it may not have been ballistics, he does not support the long gun registry. Author2 tells Author1 that the article did not state that ballistics databases determined anything, and that ballistics databases match rounds to the gun they came from. Author1 mentions that he was referring to forensics instead of ballistics.


----------
D2
----------
Author1 says most guns used for crimes aren't recovered.  He says in Toronto, 63% of the guns came from America. Over 2600 guns were recovered by police in Canada, and 925 came from a dealer, of which 3/4 came from the USA. Author2 questions how if the serial numbers were removed, they were sure those guns had come from the USA. Author2 says they were through ballistics, but Author2 argues it is impossible to trace guns that way. He questions what good is the long gun registry. Author1 details a case in which a gun was used in a crime. He says although the serial numbers were missing, they were able to trace the gun to a shop in Maine. A person bought eight guns and sold them to a Canadian who smuggled them. He said this showed guns could still be traced. Author2 still questions how ballistics would help identify the guns. He points out the specifics of this, and Author1 admits he should have said forensics instead of ballistics.


----------
D3
----------
Author1 believes the long gun registry is not for him. He knows most guns used in crimes are not recovered, and quotes statistics to back up his position. Many aren't traceable except through ballistics. Some horrible Canadian gun crimes involve guns smuggled from the United States. Most are not traced, but some can be even though the serial numbers are gone. They may not have been traced with ballistics, he does not know. He says he used the wrong word when he said ballistics. He meant the authorities used forensics to trace guns. Author2 strongly believes the long gun registry is useless. He wonders how the authorities could trace guns without serial numbers to the US. He believes ballistics traces are impossible. People only think it possible because of movies they have seen. Ballistics cannot trace guns. Striations on bullets don't work because the gun bores that make the striations change shape readily. Anyway, ballistics databases only compare what is in the database. New guns are not. Most guns are not. It is useless.


----------
D4
----------
Author1 argues that firearms are moving into and out of the US at too high rates. Many guns are being illegally trafficked and moved, and a large number of the are untraceable because their serial numbers were removed. A large portion of guns in Canada come from the US illegally. Guns can be traced without their serial number in some cases. This problem is getting worse, as people have been smuggling guns and drugs from Maine and Ohio into Canada. A gun registry would not help this problem. 

Author2 asked how the guns were traced, without their serial numbers or other information. No ballistics database has helped to put a criminal behind bars, or track weapons that cross borders. Long gun registries would not help track this. Guns are not tested by ballistics or by registries in order to identify them, unlike TV show dramas. Random matches are simply not possible in registries. People confuse the abilities of tv shows with real life, and we can't do much of what is shown on TV.

