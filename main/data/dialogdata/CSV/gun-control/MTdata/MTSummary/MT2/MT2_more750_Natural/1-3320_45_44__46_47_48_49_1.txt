
----------
D0
----------
Author1 states in order to claim self defense, you have to leave if you can do so safely. However, if your safety is being immediately threatened, you may not be able to leave the scene. He says if there is no immediate threat, you can't claim self defense. The law realizes there are times during an immediate threat where you can not safely retreat and then it is self defense, which is why the statue requiring you to leave was abolished. He suggests seeking legal advice to clarify this. Author2 says he is wrong because there is no requirement to flee, only prove you were in danger. Author1 says he only said you can't prove it was self defense if you had a safe way to flee and didn't do so, not that you have to attempt to flee. He says you should confirm this with a lawyer. Author2 sarcastically says people should try calling a lawyer while facing someone with a weapon instead of using a gun in self defense.


----------
D1
----------
Author1 argues that self defense is only applicable as a court plea if you are do not want to be in the fight. Self defense works if there is an immediate threat to your safety, but not if there isn't an immediate threat. Fleeing from an attacker should not be required before self defense, as sometimes it is not possible. If you have a chance to flee and do not take it, that is no longer self defense. There is a clear difference between manslaughter and murder. If you are able to flee a situation and you instead kill a man, you will be charged with a crime. If fleeing is dangerous, then you can still act in self defense. 
Author2 argues that there is no requirement in the law that you need to flee in order to plead self defense. If you are threatened with a dangerous situation, you do not need to attempt to flee, you can instead immediately respond with force. Defending yourself is legal as self defense.


----------
D2
----------
Author1 states that he believes there is a difference between being required to retreat in order to successfully make a self defense plea and the definition of self defense. He goes on to say the if you were a willing participant at any time self defense is out. He  continues his argument that if there was an opportunity to get away without harm and instead lethal force is used it would be difficult to claim self defense.  But best to contact a lawyer to find out the law exactly.
Author2 starts off saying Author1 is wrong, that there is no requirement to attempt flight only to be threatened with great bodily harm. He continues to hold his ground that one doesn't need to try to get away and self defense is a viable plea but that instead of acting first Author1 might want to call his lawyer.


----------
D3
----------
Author1 is discussing the definition of self defense and if it is a requirement to attempt to flee in states which have Stand Your Ground laws.  He advises in the past there was a requirement to attempt to flee and a person had to prove he/she tried to get away prior to using lethal force against an attacker.  He states by enacting the SYG laws, states have acknowledged the fact that when lethal force is used, there is rarely an opportunity to flee.  He recommended a person seek legal advice if a situation such as this happens.  He also feels it would be hard to justify the use of lethal force if there is any indication there was an opportunity to retreat.
Author2 feels Author1 is given bad advice by suggesting legal counsel.  He notes there is not a requirement of attempted flight when SYG laws are in effect.  He notes the law says all that is needed is to establish the victim was in danger of great bodily harm.


----------
D4
----------
Author1 says good luck trying to use the defense in court. His point is there's a difference in being required to retreat in order for a self defense plea and the definition of self defense. If you are a willing participant in the altercation, it isn't self defense. You can't flee if there's an immediate threat, there isn't time. It's hard to justify lethal force without an immediate threat. Author2 says there is no requirement to attempt fight and he thinks all of Author1's logic is wrong. Author1 states he never said there was. He said if you have a chance to flee but use lethal force instead it isn't self defense. He says to talk to a lawyer. Author2 thinks Author1 is spinning his advice and there's no requirement to fight. All you have to do is demonstrate you were threatened with harm. Author1 says he is just saying you will have a hard time justifying lethal force if you had a chance to escape and didn't. Author2 thinks you don't have to retreat.

