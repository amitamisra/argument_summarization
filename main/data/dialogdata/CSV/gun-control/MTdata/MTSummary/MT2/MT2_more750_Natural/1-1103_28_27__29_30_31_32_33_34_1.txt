
----------
D0
----------
Author1 thinks owning a gun for self defense shows you have no problem violating someone's life. He doesn't think criminal's loose the right to life because it supports the idea that rights are given by society and may be taken away. Author2 asks for proof that criminals have the right to commit a crime against law-abiding citizens. He thinks gun laws don't effect criminals because they disregard laws. Author1 says it's pointless because it would be produced by a society that has the power to remove rights and he thinks gun control laws effect criminals. Author2 thinks this is a typical reaction from one that wants to take away rights. Author1 asks for evidence that people have a right to have guns - other than something made up by the government. Author2 thinks the intent of the 2nd amendment was to preserve and guarantee, not grant, rights. Author1 thinks criminals don't break laws, they become criminals after they broke them. Author2 says the UK doesn't have the 2nd amendment and laws provide punishment for criminals.


----------
D1
----------
Author1 feels that by owning and carrying a gun you are claiming the right to end someone's life. Taking a life is not a decision that should be put in our hands so easily. It's taking the law into your own hands. Rights and laws are created by the people in a society, they're not intrinsically given. All laws affect all citizens. Rights aren't imposed by legislation but by their being upheld or suppressed by the majority. Criminals are not criminals until they've broken a law. The law creates criminals more than it prevents them from committing crime. 

Author2 feels that criminals are violating a person's rights in the commission of the crime, they are assuming the risk of being punished/killed. Criminals operate outside of society's laws. Gun control laws do not keep criminals from obtaining weapons, they only serve as a restriction on those acting within the laws in purchasing them legally. The 2A's purpose was to preserve and guarantee these rights, not grant pre-existing rights. Laws serve to allocate punishment for crimes committed.


----------
D2
----------
Author1 believes that if you own a gun, even for self defense, that it is a violation of everybody else's right to life, including criminals. He continues, not feeling the need to submit proof to the contrary, claiming that this society is powerful enough that it can give and take away rights away as it sees fit.  And that a person alone is at the mercy of the mob mentality.  He questions whether it is all laws or just gun laws that criminals disregard and why even have laws at all then?  
Author2 believes that criminals are the ones who break the laws, not the law abiding citizens.  And that the gun laws won't hurt the criminals, because they still will get guns.  He goes on to claim that the 2nd amendment wasn't made to take away rights  but to preserve the preexisting rights of the people to keep and bear arms.  Laws are there not for prevention of crime but to ensure punishment when it happens.


----------
D3
----------
Owning a gun shows that you have the ability and forethought that you are willing to take another person's right. The right to life is the most important right we have, and criminals do not lose this right because they are willing to take your things. Documentation in gun control is a positive force that would help to reduce gun crime and would effect both typical citizens as well as criminals who break laws. There is no inherent right to own guns other than the fake right that the government has given to you. The right to own guns was falsely given from the amendments to the Constitution. If society's opinion shifts the Constitution can change. 
Author2 believes that gun control laws do not effect criminals, only law abiding citizens. Gun control laws will not stop criminals from getting guns. The right to guns is preexisting and was not granted by the Constitution, that only guaranteed it. Laws against violent crimes punish those who commit them, but gun control would not effectively punish criminals.


----------
D4
----------
Author1 says owning a gun shows someone does not respect another person's right to life- even if that person is attempting to take your life. Denying them that right says rights are not guaranteed. Author2 says criminals do not have a right to commit a crime. Criminals don't obey laws, so gun control doesn't affect them. Author1 questions why have laws if they do not have any effect, and says they do affect criminals. Author2 argues gun control laws do not affect criminals who don't obey them, but do affect law-abiding citizens. Author1 questions why does Author2 think he has a right to a gun just because of a government made up law. Author2 says the second amendment does not grant the pre-existing right to bear arms, but preserves and protects a right that already existed. Author1 says regardless if it was pre-existing, society can change it if opinions support doing so. Author2 states laws do not prevent crimes, they provide punishment for when crimes occur. Gun control laws affect law-abiding citizens, not criminals.

