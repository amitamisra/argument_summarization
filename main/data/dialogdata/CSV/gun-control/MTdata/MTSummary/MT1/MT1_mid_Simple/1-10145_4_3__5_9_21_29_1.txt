
----------
D0
----------
Author1 is chastising Author2. He says Author2 is belittling people just for disagreeing. He disagrees there is a hidden agenda. He does not believe authorities want to seize weapons. He says Author2 is a radical. He says people with those beliefs are a minority. He supports recent rulings. He says because of the constitution they will not make things change very much. Author2 says he is not asking for repeals. He says citizens do not support stricter laws. Author1 points out people want the laws to stay the same. Author2 is in a minority because only 15% want laws repealed. He accuses Author2 of being dishonest. Author2 says gun laws steal rights from people. He says he is defending his citizen's rights.


----------
D1
----------
Author1 and Author2 are talking about something being imaginary. Author1 thinks Author2 is trying to demonize them. Author1 calls Author2 a radical. Author1 says they support recent rulings. Author1 does not believe those rulings will repeal existing gun regulations. Author1 says firearm regulation is constitutional. Author2 says that they do not want laws to be repealed/ Author2 cites a recent Gallup Poll that claims fewer Americans support stricter gun control laws. Author1 thinks they are reading selectively. Author1 says 85% of Americans want gun regulations to stay the same or be stricter. Author2 says that Author1 wants to steal rights. Author2 says second amendment rights are inalienable.


----------
D2
----------
Author1 thinks that Author2 is trying to demonize anyone that doesn't agree with his views. Author1 thinks Author2 believes firearms should be unregulated. He also thinks there is a hidden agenda that will lead to the confiscation of firearms. Author1 believes Author2 is a radical and in the minority of Americans. Author1 agrees with the SCOTUS rulings. He doesn't think Heller or McDonald will lead to a repeal of existing gun laws. Firearm restrictions are constitutional. Author2 says he isn't calling for repeal. He says fewer Americans support strict gun control laws according to CNN polls. Author1 thinks the decline doesn't mean they want laws repealed. 46 percent of people do not want a change in the current laws. He thinks Author2 is being dishonest.


----------
D3
----------
Author1 supports gun ownership laws because they are Constitutional. Author2 disagrees here. Author1 interprets Author2's point of view as a complete disregard for any regulation. Seems like Author2 supports the removal of guns from the hands of private owners. Author2 defends himself by providing poll results obtained by various news corporations which support his claim of American preference for looser restrictions. Author2 does not argue in favor of complete removal of laws. He is for their minimization. Author1 informs Author2 that he is simply misinterpreting the  data. According to Author1, the diminishing support of the people for strict gun laws is not the same as support of removal of all barriers to ownership. Author2 disagrees with Author1. Author2 sees Author1's position as more radical.


----------
D4
----------
Author1 wants gun regulations. More unregulation is dangerous and not helpful. The Supreme Court has made recent rulings. These rulings will not change current laws. A majority of people want the same or more gun control. Getting rid of regulations is a bad idea. Guns are dangerous. Guns should be regulated more, not less. Author2 thinks guns should not be regulated at all. Gun regulation takes away rights. Gun regulation is non-Constitutional. Very few people support stricter gun control. Author2 thinks Author1 is in the minority opinion. The right to own guns is important. The Second Amendment is being violated. Author2 calls Author1 a radical for wanting regulation on guns. Gun regulations should be repealed

