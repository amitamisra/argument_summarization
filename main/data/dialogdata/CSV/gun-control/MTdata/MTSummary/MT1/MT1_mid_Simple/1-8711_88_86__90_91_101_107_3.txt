
----------
D0
----------
Author1 said Canada has lower levels of gun violence. It's not a coincidence. They have a similar level of gun ownership. Gun crime is much lower. People are less dysfunctional. He said Author2 is ignoring mathematical statistics. 

Author2 said Canada has a much lower population. They have 31 million. The U.S. has 300 million. The population is more dense too. The U.S. has over a million gang members. They are responsible for 80% of all crime. He said you can't compare the two countries. They don't have the same features. Population, economic stability, government, justice systems, and number of criminals are too different. So you can't compare the two countries because of that.


----------
D1
----------
Author1 brings up that Canada has lower levels of gun violence. This is not a coincidence. Author2 says that Canada has a much lower population than America. It is not plagued with gang violence like America is. They attribute 80% of crime in America to gangs. Author1 contends that Canada's level of gun ownership is similar to America. Canada and America's levels of gun crime differ. Author2 reiterates their earlier argument of differing population size and density. Because of this they dismiss the comparison. Author1 fires back that this is a dismissal of certain mathematics. They say Author2 uses these to back their argument as well. Author2 backs their original opinion of the two countries being incomparable. They cite a list of differences.


----------
D2
----------
Author1 thinks it's not a coincidence Canada has lower levels of gun violence. Author2 thinks the lower levels of gun violence in Canada are because the population is less. He says that the United States has nine times the population of Canada. Author1 says that the level of gun ownership in Canada is equal to the level in the United States. He also thinks that the Canadian society is less dysfunctional. Author2 does not believe those things matter unless all other factors are equal as well. He thinks that the United States and Canada have too many differences to have an accurate comparison. Author1 thinks this is either dishonest or stupid. He thinks that Author2 is dismissing mathematics in order to justify his different opinion.


----------
D3
----------
Author1 references the disparity between Canada's and the United States' gun violence statistics.  Canada has laxer firearm legislation. Lesser gun control does not necessarily introduce more gun-related violence.  Despite other differences, Canada and America are comparable because of similar gun ownership numbers. Author2 is denying Author1's position its legitimacy by emphasizing other unrelated differences between Canada and America. 

Author2 feels there are far too many differences between Canada and America to logically compare statistics. Differences in government, economic stability, crime rates, and population density are important factors in comparing the two countries. Canada also has 1/10 the population of America, which is steadily increasing daily. America has a large problem with gang culture, unlike Canada, and 80% of American crime results from gang activity.


----------
D4
----------
Author1 argues Canada and the US can be compared. Gun control laws are different in Canada. Canada has less gun crime. Canada and the US both have roughly the same number of guns per person. Canada's system for gun control may be a better system. This kind of comparison is typical in statistics. There is no way to determine the truth without changing US law. Author2 argues that Author1 is wrong. Canada and the US are too different. Canada has much lower population density. Gangs are more of a problem in the US. Too many factors are different. Comparing the two is then not possible. Author2 thinks Canada's system is not better.

