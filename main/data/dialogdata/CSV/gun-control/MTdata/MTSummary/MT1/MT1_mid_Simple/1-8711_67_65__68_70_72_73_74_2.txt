
----------
D0
----------
Author1 argues against licenses. Individual gun licenses are unconstitutional. Guns should not be everywhere. Gun sales should require proof of identity. Proof of identity could be a photograph or fingerprint. Proof of identity would encourage responsibility. Guns should be available to citizens. Guns help with personal safety. Guns are also fun to sport hunt with. Gun control could be taken too far. The Supreme Court supports some gun control. Gun control prevents theft and loss.  There should be some regulation on guns. Author2 wants less gun control. Author2 thinks Author1 just argues for the law. Author1 does not have a personal opinion. If the law changed, Author1 might too. Author2 argues guns save lives.


----------
D1
----------
Author1 feels NATO is unfairly labelling him/her in favor of strict gun control. This is based on statements he/she made regarding photographing and fingerprinting during the sale of firearms. This could be useful in crime investigations, cases of stolen weapons, and fraud. It's a right to own firearms for protection and personal use. It's not an unlimited right. NATO is ignorant of legislation. NATO labels all gun laws unconstitutional. 

Author2 is a third-party commentator in this conversation. Author2 is being aggressive towards NATO in his response. Does Author1 only believe as he/she does simply because it's a law or because it's a personal belief? Other anti-gun people claim to support the 2nd Amendment also. Guns save lives, as indicated by law enforcements' carrying them.


----------
D2
----------
Author1 supports fingerprinting and photographing at the point of sale of guns. He thinks licenses are unconstitutional. He says 200,000 guns are stolen every year. He supports gun ownership but not total rights. Author1 says Author1 will have to take every bit of gun control he wants. They each know where the other stands now. Author1 says that the Heller decision has been explained many times. Safe storage laws are not unconstitutional. Author2 says that they are talking about the 2nd amendment. He asks if the Supreme Court said the right to keep and bear arms didn't exist if people would still support the 2nd amendment. He is skeptical of what people have to say. Author1 and Author2 agree that they don't hate each other.


----------
D3
----------
Author1 defends himself against claims of Author2. Supposedly, Author1 supports a ban on ownership of handguns. Instead, he is in favor of more stringent controls. These are photos and fingerprints at the time of sale. Author2 attempts to lighten the situation. Author1 remains somewhat offended. Author2 continues to argue his point of view from another angle. He presents a hypothetical scenario of a change in the Supreme Court roster that would allow for gun ownership rights to be taken away. He wants to know if Author1 will remain with the same opinion in this case. Will he side with what the current legal structure presents him with? Author1 reassures Author2 that there is no personal discontent since both speakers would have to follow the rule.


----------
D4
----------
Author1 wants a database for handguns. Author1 does not want gun licenses because they are unconstitutional. Author1 says lots of handguns are stolen every year. Author1 thinks the database would make handgun theft go down. Author1 does not support a handgun ban. Author2 says Author1's is clear to them. Author2 says Author1 will have to fight for gun control. Author2 quotes John Paul Jones, "I have not yet begun to fight." Author1 calls Jones an old guy. Author1 says that a person lied about gun storage being unconstitutional. Author1 talks about a pro gun person who likes safe storage. Author2 does not trust people who say they are pro gun people. They are talking about people who do not interpret the second amendment correctly.

