
----------
D0
----------
S1 claims never argued stance based upon religion would be arbitrary. He claims argument is based upon biology. S2 claims argument on biology is void. He  references age difference. S1 Claims debate is really about benefits not marriage itself. He claims heterosexual marriage is based on biology even if reproduction is impossible. S2 wants equality. He claims constitutionality. S1 Refers to S2s arguments about benefits. S2 Claims to have been commenting for years. S1 Doesn't care. He accuses S2 of hypocrisy. S2 refers to heterosexuals that can't breed or won't, can still get married. S1 refers to S2 having legal marriage in one state and not others as will of the people. Again refers to the demand being about benefits. S2 Asks why gays should be different. S1 Says that the idea of homosexuals and heterosexuals are the same is political fiction. He  claims that being gay is like being left-handed. It's too bad that the world is made for right handed heterosexuals. Because you want something doesn't mean it's your right. He claims it's ignorant, insulting & arrogant to believe that.


----------
D1
----------
S1 believes that everyone should live within the bounds of the laws of society. They are free to be changed but not broken. Society's definition of marriage is not based merely on religion. Definition of marriage is based on  biology. S2 argues that people who are not able to reproduce but marry invalidates S1's argument. S1 reiterates that the ultimate intended goal of marriage is to make reproduction easier. Bringing up exceptions   does not change anything. S1 argues that S2's opposition to gay marriage bans stems from not receiving benefits that a married couple does. S2 argues that gay marriage bans are unequal and unjust. S2 emphasizes universal equality and desires to be treated just as heterosexual people who do not reproduce are in terms of the ability to marry.  S1 argues that by placing marriage law in the realm of the states, then the states can determine which type of marriage to recognize. S2 argues that even if this were the case, it would still be unjust. S1 argues that S2 ignores the biological differences involved.


----------
D2
----------
S1 sees marriage as it is today as part of a social contract that is based upon biology and reproduction.  The drive to reproduce is part of the heterosexual bond and marriage is its formalization.  S1 is annoyed with S2 demanding that because his marriage is not universally recognized that there is something unconstitutional in place.  He explains that marriage is left to the States to determine what is legal.  According to the Constitution, Federal government,has no place to change that.
S2 would like to see a change in the definition of marriage.  His marriage license is not universally recognized. He argues that biology and reproduction do not apply when people past the age where childbearing is possible can marry.  He challenges S1 that if marriage truly is about reproduction and family that a married couple must prove that this is a part of their relationship or their marriage would be terminated.  He is offended that others' beliefs can play a role in his life.


----------
D3
----------
S1 argues that is important that a society lives by its laws. They argue that a heterosexual marriage is a formalization of the need to reproduce. They do not believe that the Constitution requires a state to recognize another state's laws because it is up to the states what is recognized and what is not. They argue that heterosexuals and homosexuals are biologically different. Some people just have to deal with the ramifications of being biologically different. 
S2 argues against the idea of marriage being solely for reproduction. He brings up the fact that heterosexuals who are well past birthing age can legally get married without any consequence. They argue that it is unfair that even though they may have legal grounds and license to prove that they are married in one state. If they go to another state that does not recognize their marriage license then that marriage is not accepted. This person believes that it should be up to the federal government and not the state government to recognize a marriage license.


----------
D4
----------
S1 believes that marriage, in it's conception, was made for biological reasons, that marriage exists to allow children to be born more easily. Although this should, in no way force people to have children, or their being capable of having children in the first place make a difference. S1 says that heterosexuals and homosexuals are fundamentally different. Due to the  difference they are treated differently. S1 compares the institution of marriage to the world being built to accommodate right handed people. He is insulted that the world should be forced to be changed for lefties, not believing they have that right. 
    S2 feels that, the federal government and other states, not recognizing his marriage license, is unconstitutional.  Iowa issues the same license for both hetero- and homosexual couples. He feels that there is no difference between a gay couple getting married, and a 70 year old straight couple getting married. He wants equal right to get married. He has a right for  marriage to be recognized. This is similar to couples who either can't have children or choose not to.

