
----------
D0
----------
California Amendment bans gay marriage.
S1 argues that the proponents of Prop 8 need to verify that the California Amendment was enacted legally. 
S1 believes that if DOMA isn't declared unconstitutional than those arguing against Prop 8 don't have a valid platform. 
Federal law gives states the right to ignore other states' laws.
S1 argues that they would have to first strike down the federal law before they could do the same to the California Amendment.

S2 argues that someone is trying to see if the California Amendment violates the 14th Amendment of the Constitution. 
S2 believes that DOMA violates the federal constitution. 
Nothing in DOMA prevents California's court from ruling the 14th amendment requires gay marriage. 


----------
D1
----------
S1 claims that proponents of gay marriage legislation need only to verify the state constitutional amendment was enacted illegally.
The state constitutional amendment bans gay marriage.
If the defense of marriage act is constitutional, then it is legal to ban gay marriage.
S1 argues that gay marriage has a financial impact on all tax payers. 

S2 argues that the state constitution does not supersede the federal constitution.
The question is whether the state constitutional amendment violates the 14th amendment.
S2 suggests that the defense of marriage act will likely be ruled unconstitutional.
The defense of marriage act ruled unconstitutional leads to the question of the legality of the state amendments. 
S2 points out that the defense of marriage act does not prohibit a state from recognizing gay marriage.




----------
D2
----------
S1 thinks Prop 8 needs to be verified as being enacted legally. 
S1 believes that if DOMA isn't unconstitutional there is no point in fighting over it. 
He says it has a financial impact on all tax payers.
Federal law (about DOMA) would have to be struck down before the California law could be considered unconstitutional. 
S2 says that the federal DOMA is unconstitutional.
The discussion of the California law could lead to this recognition. 
S2 points out that gay people pay taxes.
Gay people pay for straight marriages.
Gay people are denied the right to marry themselves. 
He believes the 14th amendment requires gay marriage.


----------
D3
----------
S1 believes DOMA on a state level is unconstitutional.  
S1 argues gay marriage has an impact on all tax payers in a negative way.  
S1 believes the state ban on gay marriage in California was enacted legally.
S1 cites the federal ruling that each state must acknowledge the marriage laws of another state.



S2 provides the argument that DOMA on a federal level could actually be found unconstitutional.  
S2 also argues the impact on taxpayers.
Gay couples pay taxes on heterosexual marriages and activities.
Gay couples are impacted based on fundamental law.
In S2's theory, S1's observance is incorrect.  
S2 also advises a state constitution cannot supersede the federal constitution. 
S2 argues that S1 is incorrect concerning each state's recognition. 


----------
D4
----------
Two people are discussing the Defense of Marriage Act (DOMA). 
S1 believes that the State Constitutional Amendment banning gay marriage is legal.  
He states that the outcome of the courts ruling impacts all taxpayers.
S1 argues that the court cannot rule a California Constitutional Amendment unconstitutional.
Federal Law gives states the right to ignore other state's gay marriage law.  
S2 asserts that the state constitution cannot violate the federal constitution.
Based on what DOMA does it violates the 14th amendment.  
S2 also states that gay people pay taxes too.
S2 wonders why gays must pay for straight marriages.
Gays are not allowed to marry themselves.
DOMA doesn't prohibit states from recognizing gay marriage.

