
----------
D0
----------
S1 identifies the important spiritual situation that homosexuals are in. 
He suggests that it is inappropriate to make jokes about homosexual lifestyles.
No one approves of the deviant behaviors practiced by homosexuals.
It would not be funny if homosexuality was discovered in your own community. 
S2 claims that we do not identify non-homosexuals by the sexual behaviors they engage in.
We should not identify homosexual individuals by the sexual behaviors they engage in. 
He argues that a person's private behaviors are no one else's business.
Sexual behaviors do not define who someone is as a person. 
He defends homosexual behaviors by referencing the deviant behaviors performed by heterosexuals.
He suggests that deviant sexual acts are not the sole province of homosexuals.


----------
D1
----------
S1 and S2 are discussing gay marriage and relationships.
S1 is opposed to the issue.
S2 is for the issue.  
S1 believes those who practice gay relationships are spiritually lacking.
S1 feels S2's sarcasm toward the issue at hand is proof of spiritual lacking.  
S2 feels S1 is too literal.
S1 is obsessed with the issue of gay sex rather than gay marriage.  
S2 advises what happens in the bedroom is private.
What happens in bed is not the issue.  
S1 makes some rude remarks in regard to gay sex involving gerbils.  
S2 feels many heterosexuals also have a history of doing vulgar things as well.
It is unfair to categorize only gay individuals in this manner.  
S1 does not agree with this.


----------
D2
----------
S1 believes that it is a dangerous spiritual situation that S2 is in.
To take homosexuality lightly or amusing in any way is proof that one doesn't care about it. 
They don't believe that anyone should be laughing or joking at what they think is a perverted and unnatural practice that homosexuals engage in.
S2 doesn't believe that sexual activity is what makes up the homosexual community.
People don't, and shouldn't, care about what one does in the bedroom. 
They believe such activities should remain private.
Sex is no one else's business. 
They don't believe that what is done behind closed doors defines a person. 
They don't agree that homosexuals should be classified as deviant.
Heterosexual activities can be likewise deviant.


----------
D3
----------
Two people are discussing homosexuality.  
S1 contends that S2 and Jake fails to comprehend the serious spiritual situation they are in.
They think that anything about their lifestyle is funny but its not.  
He states that their practices are sick and perverted and also unnatural.  
S2 states that it was sarcasm.
He finds it funny that S1 was making jokes about inserting Gerbils into someone's rectum.  
He also claims that sex is all that S1 thinks about.
Sex is the only argument he comes up with.  
He then contends that people do not define their neighbor or the people around them by what they do sexually.
Americans could care less about others' sex life.


----------
D4
----------
S1 makes a comment about gerbils. 
He believes homosexuals have created sick and perverted sexual practices.
Such as sexual practices involving gerbils.
They are in a dangerous spiritual situation because of it. 
He denies joking. 
S2 laughs sarcastically.
S2 questions if sex is all some people think about. 
He thinks no one cares what people do in their bedrooms.
Sex is private.
Sex is not a reflection of who they are as a person.
He states that people do not define their neighbor or mailman based on what they do in their bedroom.
If that were the case there could be many negative comments made about S1. 
Also, S2 says that heterosexuals have been known to have messed up sexual practices as well.

