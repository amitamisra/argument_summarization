
----------
D0
----------
S1 believes that gay couples are no different than incestuous and polygamous relationships.
Gay couple's rights in regard to marriage, are no different than that of incestuous and polygamous relationships. 
S1 does not feel that gay marriage should be prohibited
Gay Marriage is simply different than a normal heterosexual marriage
To recognize difference between gay and heterosexual marriage is not discrimination. 
S1 feels that if gay relationships were to be allowed then not extending the same benefits to all forms of relationships between consenting adults would then be discriminatory. 
S1 feels that gay couples who have children through surrogates have outsourced their children.
S1 should not be responsible for gay couple's children.
   
S2 makes the case that he has been with his partner for 15 years
S2 will be having his own biological children soon. 
S2 feels that his family should be afforded the same rights as a heterosexual couple.
S2's husband should gain automatic custody if he dies, survivor benefits from social security, and others. 
S2 does not see his relationship with his husband as being the same as other forms of non-sanctioned relationships.
Each new form of marriage arrangements would need to be argued on their own merits individually.


----------
D1
----------
S1 believes that gay marriages should not be viewed the same way as heterosexual marriages.
S1 does not disagree with the idea of gay marriage. 
S1 says that gay marriage is a deviation from the traditional union between a man and woman. 
Any sexual relationship that is a deviation from the "norm," like polygamist and incestuous relationships, should also be granted the same rights.
S1 feels that these relationships are all on the same levels.
One should not be afforded rights that the others are not.

S2 identifies as a gay male.
S2 has been in a 15-year-long homosexual relationship.
S2 has a set of twins due soon.
S2 feels his family should be granted the same rights as a heterosexual, married couple.
S2 states that legalizing gay marriage allows for automatic guardianship of children if something happens to the biological parent
Legalizing gay marriage awards survivors benefits from social security if something happens to the biological parent. 
S2 believes that other types of marriage should be looked at based on their own situations and conditions.


----------
D2
----------
S1 states that same-sex marriage should not be prohibited.
S1 believes that a homosexual marriage is different than a heterosexual marriage. 
S1 argues that recognizing this difference is not discrimination. 
S1 claims that children of same-sex couples are comparable to children from incestuous, polygamous, or other sexual deviant relationships. 
S1 makes the argument that if same-sex marriages are legalized, then all types of marriages between consenting adults should be legalized. 
S1 claims that the arguments in favor of same-sex marriage would work equally well for incestuous or polygamous relationships.

S2 identifies himself as a homosexual.
S2 argues that same-sex families deserve the same benefits as heterosexual families. 
S2 questions what difference exists that distinguishes his relationship from that of a heterosexual couple other than the genitals involved.
S2 identifies a variety of benefits received only through marriage, such as social security and guardianship among others. 
S2 takes offense at S1's comment that his children are "outsourced"
S2 rejects the idea that homosexuals are fighting for marriage equality simply because they free handouts from the government.


----------
D3
----------
S1 believes that homosexual marriages should not be prohibited. 
S1 believes that heterosexual marriages and homosexual marriages are not the same. 
S1 believes that if homosexuals are allowed to marry, then any kind of marriage between consenting adults should be allowed.
Not allowing any kind of marriage between consenting adults would be discriminating against the person wanting to enter into the marriage. 
S1 believes that S2 deserves as much protection as an incestuous or polygamous relationship that has children. 
Overall, S1 believes that if homosexual marriages are allowed then it should branch out to other types of potential marriages.

Benefits of social security or retained guardianship of their children are otherwise not available to homosexual couples.
S2 points out that with full civil marriage equality, benefits of social security or retained guardianship of their children are now options.
S2 doesn't see a reason for homosexual couples to have to go through a different process than heterosexuals to gain the same benefits of heterosexual marriage. 
S2 has children.
S2 does not believe that they are any less deserving of government benefits of marriage than heterosexual couples with children.


----------
D4
----------
S1 argues that allowing gay marriage would set a precedent.
Allowing gay marriage would be used to allow marriage for other forms of traditionally socially unacceptable intimate or sexual relationships, such as incest between half brothers or a mother and son, and/or polygamy. 

S2 is in a 15 year male homosexual relationship
S2 is anticipating twin children that are biologically related to him.
S2 challenges S1 to explain why he and his family should not be entitled to the many benefits associated with marriage, such as social security surviver status.  

S1 provides a putative explanation by noting that his children are outsourced. 
One of the parents of the children is not part of the family intended to receive the benefits of marriage.  
S1 also notes that others do not qualify.
S1 likens S2 to a single mother who has many children. She then complains about not enough help from the government.
S1 repeatedly compares gay relationships to other types of relationships in order to justify excluding gays from marriage rights.
S1 considers other types of relationships to be devious.


