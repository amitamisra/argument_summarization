<?xml version="1.0"?>
<!DOCTYPE pyramid [
 <!ELEMENT pyramid (startDocumentRegEx?,text,scu*)>
 <!ELEMENT startDocumentRegEx (#PCDATA)>
 <!ELEMENT text (line*)>
 <!ELEMENT line (#PCDATA)>
 <!ELEMENT scu (contributor)+>
 <!ATTLIST scu uid CDATA #REQUIRED
               label CDATA #REQUIRED
               comment CDATA #IMPLIED>
 <!ELEMENT contributor (part)+>
 <!ATTLIST contributor label CDATA #REQUIRED
                       comment CDATA #IMPLIED>
 <!ELEMENT part EMPTY>
 <!ATTLIST part label CDATA #REQUIRED
                start CDATA #REQUIRED
                end   CDATA #REQUIRED>

]>

<pyramid>
<startDocumentRegEx><![CDATA[[-]*\nD[0-9]\n[-]*\n]]></startDocumentRegEx>
 <text>
  <line></line>
  <line>----------</line>
  <line>D0</line>
  <line>----------</line>
  <line>S1 States that the current definition of marriage specifically excludes homosexuals because the current definition is &quot;two people that are not underage, or opposite genders, same species, not closely related and not already married&quot; and that gays want to be married under current definition. Believes that ability to reproduce is implied in definition. S2 Claims S1's definition isn't only possible, refers to lack of reproductive abilities of an elderly person and a younger person marrying. S1 Refers to differences between cultural definitions of marriage and mating pairs or legal definitions of marriage. S2 Ask who agve S1 right to define cultural meanings. S1 Claims that cultural meaning comes from observation and that legal definition can be decided any way. S2 Asks who gets to make that legal definition. Claims that those that oppose gay marriage are opposing something that harms no one and are trying to limit other's actions. S1 States that in America people vote on laws or those that make them and that is who gets to determine legality of different marriage concepts.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D1</line>
  <line>----------</line>
  <line>S1 argues that marriage has a specific and deeply embedded definition within culture. Marriage is a formalized mating pair. Marriage's legal definition closely mirrors that definition. Homosexual marriage has never been an explicit or implicit part of those definitions. Including it would make the definition of marriage inherently self-contradictory. S2 argues that S1 is exercising a subjective definition. S2 notes that different members of society have different definitions of marriage. Further, there are instances of accepted marriage which violate S1's provided definitions. Instead, love, the emotional connection is what is most important.  S1 argues that legal and cultural definitions are not the same. The cultural definition is what is important. Most associate marriage with producing offspring, hence it is deemed important in the cultural definition. S2 questions S1's authority to determine what is culturally important and whether his cultural definition is appropriate for a legal definition. S1 says appropriateness is irrelevant. Cultural definitions have formed the root of all other definitions. The cultural definition of marriage has always been one male and female in monogamy.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D2</line>
  <line>----------</line>
  <line>S1 is focused on the definition of marriage and the cultural definition, both limiting marriage to heterosexual couples.  The cultural definition includes reproduction, he uses the term mating mating pairs.  He argues that this definition automatically excludes homosexuals because they cannot, without outside help, create a family.  He discusses other marriage outside of the norm-a young woman and older man- as being less meaningful than those that fulfill the cultural definition.  He sees it as the job of elected officials and the citizens to change the legal definition of marriage if that is what the majority want.</line>
  <line>S2 disagrees with and questions the cultural definition given by S1.  He feels that the main factor in marriage is not reproduction but love and commitment between two individuals.  He challenges the cultural definition with the example of a 65 and 68 year old getting married.  He finds it unfair that a group of people are being excluded from an institution on which they pose no threat to those who are enjoying the status quo.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D3</line>
  <line>----------</line>
  <line>S1 argues that the cultural meaning of marriage is a formalization of a mating pair, who create and support a family unit. They believe that in order for homosexuals to be included, the definition and cultural meaning of marriage would have to change to accommodate it. They argue that the most dominant cultural view on marriage is based upon evolutionary sound logic, that a monogamous pair of individuals will reproduce. This person does not believe that the argument stands on equality because they do not believe that equality factors into the evolutionary view of marriage.</line>
  <line>S2 argues against the idea that there is a set, evolutionary definition of marriage and uses the example that people well over the age of reproduction are not only legally capable of getting married, but also suffer no special stigma for doing so. They believe what defines marriage is love between two people who desire a commitment between themselves. They argue that many do not share S1's definition of marriage and that it's an issue of personal belief rather than culture.</line>
  <line></line>
  <line></line>
  <line>----------</line>
  <line>D4</line>
  <line>----------</line>
  <line>S1 states that the legal definition of marriage is a contract between a man and a woman, who are not related, with no regard to love, or the intention of reproduction. S1 maintains, from the basis of observation, that the cultural definition of marriage is a formalization of a mating pair, love and the intension to reproduce being implied. Saying that homosexual couples are not a part of either definition, and that being included in the current definition is a catch-22, as homosexuality is not in sync with evolution. S1 further states that the only connection between, mating couples and marriage, are cultural.</line>
  <line>    S2 defines marriage as a commitment between two people who love one another. Using a hypothetical marriage between a 65 year old woman and a 68 year old man as an example, argues that reproduction has no bearing on the definition of marriage, and that without a harm involved, equality of rights is what is important. S2 believes that personal opinions are subjective and should not be used as the basis to limit the rights of the minority.</line>
 </text>
 <scu uid="68" label="Ability to reproduce is implied in  marriage definition">
  <contributor label="Believes that ability to reproduce is implied in definition">
   <part label="Believes that ability to reproduce is implied in definition" start="318" end="377"/>
  </contributor>
  <contributor label="The cultural definition includes reproduction">
   <part label="The cultural definition includes reproduction" start="2495" end="2540"/>
  </contributor>
  <contributor label="love and the intension to reproduce being implied">
   <part label="love and the intension to reproduce being implied" start="4833" end="4882"/>
  </contributor>
  <contributor label="that a monogamous pair of individuals will reproduce">
   <part label="that a monogamous pair of individuals will reproduce" start="3826" end="3878"/>
  </contributor>
  <contributor label="Most associate marriage with producing offspring, hence it is deemed important in the cultural definition">
   <part label="Most associate marriage with producing offspring, hence it is deemed important in the cultural definition" start="1902" end="2007"/>
  </contributor>
 </scu>
 <scu uid="48" label="Marriage definition excludes homosexuals">
  <contributor label="S1 States that the current definition of marriage specifically excludes homosexuals">
   <part label="S1 States that the current definition of marriage specifically excludes homosexuals" start="26" end="109"/>
  </contributor>
  <contributor label="Homosexual marriage has never been an explicit or implicit part of those definitions...Including it would make the definition of marriage inherently self-contradictory">
   <part label="Homosexual marriage has never been an explicit or implicit part of those definitions" start="1325" end="1409"/>
   <part label="Including it would make the definition of marriage inherently self-contradictory" start="1411" end="1491"/>
  </contributor>
  <contributor label="both limiting marriage to heterosexual couples...He argues that this definition automatically excludes homosexuals">
   <part label="both limiting marriage to heterosexual couples" start="2446" end="2492"/>
   <part label="He argues that this definition automatically excludes homosexuals" start="2581" end="2646"/>
  </contributor>
  <contributor label="Saying that homosexual couples are not a part of either definition...being included in the current definition is a catch-22">
   <part label="Saying that homosexual couples are not a part of either definition" start="4884" end="4950"/>
   <part label="being included in the current definition is a catch-22" start="4961" end="5015"/>
  </contributor>
  <contributor label="They believe that in order for homosexuals to be included, the definition and cultural meaning of marriage would have to change to accommodate it">
   <part label="They believe that in order for homosexuals to be included, the definition and cultural meaning of marriage would have to change to accommodate it" start="3579" end="3724"/>
  </contributor>
 </scu>
 <scu uid="49" label="Marriage definition is &quot;man and woman that are not underage, not related, not already married&quot;">
  <contributor label="the current definition is &quot;two people that are not underage, or opposite genders, same species, not closely related and not already married&quot;">
   <part label="the current definition is &quot;two people that are not underage, or opposite genders, same species, not closely related and not already married&quot;" start="118" end="258"/>
  </contributor>
  <contributor label="Marriage is a formalized mating pair...The cultural definition of marriage has always been one male and female in monogamy">
   <part label="Marriage is a formalized mating pair" start="1226" end="1262"/>
   <part label="The cultural definition of marriage has always been one male and female in monogamy" start="2261" end="2344"/>
  </contributor>
  <contributor label="he uses the term mating mating pairs">
   <part label="he uses the term mating mating pairs" start="2542" end="2578"/>
  </contributor>
  <contributor label="S1 states that the legal definition of marriage is a contract between a man and a woman, who are not related, with no regard to love, or the intention of reproduction...the cultural definition of marriage is a formalization of a mating pair">
   <part label="S1 states that the legal definition of marriage is a contract between a man and a woman, who are not related, with no regard to love, or the intention of reproduction" start="4542" end="4708"/>
   <part label="the cultural definition of marriage is a formalization of a mating pair" start="4760" end="4831"/>
  </contributor>
  <contributor label="S1 argues that the cultural meaning of marriage is a formalization of a mating pair...who create and support a family unit">
   <part label="S1 argues that the cultural meaning of marriage is a formalization of a mating pair" start="3456" end="3539"/>
   <part label="who create and support a family unit" start="3541" end="3577"/>
  </contributor>
 </scu>
 <scu uid="55" label="Marriage definition is a personal belief rather than culture">
  <contributor label="S2 Ask who agve S1 right to define cultural meanings">
   <part label="S2 Ask who agve S1 right to define cultural meanings" start="636" end="688"/>
  </contributor>
  <contributor label="S2 argues that S1 is exercising a subjective definition...S2 questions S1's authority to determine what is culturally important and whether his cultural definition is appropriate for a legal definition">
   <part label="S2 argues that S1 is exercising a subjective definition" start="1493" end="1548"/>
   <part label="S2 questions S1's authority to determine what is culturally important and whether his cultural definition is appropriate for a legal definition" start="2009" end="2152"/>
  </contributor>
  <contributor label="S2 believes that personal opinions are subjective">
   <part label="S2 believes that personal opinions are subjective" start="5482" end="5531"/>
  </contributor>
  <contributor label="They argue that many do not share S1's definition of marriage...it's an issue of personal belief rather than culture">
   <part label="it's an issue of personal belief rather than culture" start="4461" end="4513"/>
   <part label="They argue that many do not share S1's definition of marriage" start="4390" end="4451"/>
  </contributor>
  <contributor label="S2 disagrees with and questions the cultural definition given by S1">
   <part label="S2 disagrees with and questions the cultural definition given by S1" start="2998" end="3065"/>
  </contributor>
 </scu>
 <scu uid="53" label="Reproduction has no bearing on the definition of marriage">
  <contributor label="refers to lack of reproductive abilities of an elderly person and a younger person marrying">
   <part label="refers to lack of reproductive abilities of an elderly person and a younger person marrying" start="426" end="517"/>
  </contributor>
  <contributor label="Further, there are instances of accepted marriage which violate S1's provided definitions">
   <part label="Further, there are instances of accepted marriage which violate S1's provided definitions" start="1633" end="1722"/>
  </contributor>
  <contributor label="Using a hypothetical marriage between a 65 year old woman and a 68 year old man as an example...reproduction has no bearing on the definition of marriage">
   <part label="Using a hypothetical marriage between a 65 year old woman and a 68 year old man as an example" start="5241" end="5334"/>
   <part label="reproduction has no bearing on the definition of marriage" start="5348" end="5405"/>
  </contributor>
  <contributor label="uses the example that people well over the age of reproduction are not only legally capable of getting married, but also suffer no special stigma for doing so">
   <part label="uses the example that people well over the age of reproduction are not only legally capable of getting married, but also suffer no special stigma for doing so" start="4124" end="4282"/>
  </contributor>
  <contributor label="He challenges the cultural definition with the example of a 65 and 68 year old getting married">
   <part label="He challenges the cultural definition with the example of a 65 and 68 year old getting married" start="3180" end="3274"/>
  </contributor>
 </scu>
 <scu uid="70" label="Marriage definition is a commitment between two people who love one another">
  <contributor label="S2 defines marriage as a commitment between two people who love one another">
   <part label="S2 defines marriage as a commitment between two people who love one another" start="5164" end="5239"/>
  </contributor>
  <contributor label="Instead, love, the emotional connection is what is most important">
   <part label="Instead, love, the emotional connection is what is most important" start="1724" end="1789"/>
  </contributor>
  <contributor label="They believe what defines marriage is love between two people who desire a commitment between themselves">
   <part label="They believe what defines marriage is love between two people who desire a commitment between themselves" start="4284" end="4388"/>
  </contributor>
  <contributor label="He feels that the main factor in marriage is not reproduction but love and commitment between two individuals">
   <part label="He feels that the main factor in marriage is not reproduction but love and commitment between two individuals" start="3068" end="3177"/>
  </contributor>
 </scu>
 <scu uid="63" label="Marriage has a specific and deeply embedded definition within culture">
  <contributor label="S1 argues that marriage has a specific and deeply embedded definition within culture...Marriage's legal definition closely mirrors that definition...The cultural definition is what is important...Cultural definitions have formed the root of all other definitions">
   <part label="S1 argues that marriage has a specific and deeply embedded definition within culture" start="1140" end="1224"/>
   <part label="Cultural definitions have formed the root of all other definitions" start="2193" end="2259"/>
   <part label="The cultural definition is what is important" start="1856" end="1900"/>
   <part label="Marriage's legal definition closely mirrors that definition" start="1264" end="1323"/>
  </contributor>
  <contributor label="S1 is focused on the definition of marriage and the cultural definition">
   <part label="S1 is focused on the definition of marriage and the cultural definition" start="2373" end="2444"/>
  </contributor>
  <contributor label="S1 further states that the only connection between, mating couples and marriage, are cultural">
   <part label="S1 further states that the only connection between, mating couples and marriage, are cultural" start="5065" end="5158"/>
  </contributor>
  <contributor label="legal definition can be decided any way">
   <part label="legal definition can be decided any way" start="754" end="793"/>
  </contributor>
 </scu>
 <scu uid="59" label="Gay marriage harms no one">
  <contributor label="Claims that those that oppose gay marriage are opposing something that harms no one">
   <part label="Claims that those that oppose gay marriage are opposing something that harms no one" start="843" end="926"/>
  </contributor>
  <contributor label="without a harm involved">
   <part label="without a harm involved" start="5416" end="5439"/>
  </contributor>
  <contributor label="on which they pose no threat to those who are enjoying the status quo">
   <part label="on which they pose no threat to those who are enjoying the status quo" start="3358" end="3427"/>
  </contributor>
 </scu>
 <scu uid="62" label="Marriage rights of the minority are limited">
  <contributor label="are trying to limit other's actions">
   <part label="are trying to limit other's actions" start="931" end="966"/>
  </contributor>
  <contributor label="should not be used as the basis to limit the rights of the minority">
   <part label="should not be used as the basis to limit the rights of the minority" start="5536" end="5603"/>
  </contributor>
  <contributor label="He finds it unfair that a group of people are being excluded from an institution">
   <part label="He finds it unfair that a group of people are being excluded from an institution" start="3277" end="3357"/>
  </contributor>
 </scu>
 <scu uid="56" label="Cultural meaning comes from observation">
  <contributor label="S1 Claims that cultural meaning comes from observation">
   <part label="S1 Claims that cultural meaning comes from observation" start="690" end="744"/>
  </contributor>
  <contributor label="S1 maintains, from the basis of observation">
   <part label="S1 maintains, from the basis of observation" start="4710" end="4753"/>
  </contributor>
 </scu>
 <scu uid="66" label="Different members of society have different definitions of marriage">
  <contributor label="S2 notes that different members of society have different definitions of marriage">
   <part label="S2 notes that different members of society have different definitions of marriage" start="1550" end="1631"/>
  </contributor>
  <contributor label="S2 Claims S1's definition isn't only possible">
   <part label="S2 Claims S1's definition isn't only possible" start="379" end="424"/>
  </contributor>
 </scu>
 <scu uid="61" label="Elected officials and citizens determine legality of different marriage concepts">
  <contributor label="S1 States that in America people vote on laws or those that make them...that is who gets to determine legality of different marriage concepts">
   <part label="that is who gets to determine legality of different marriage concepts" start="1042" end="1111"/>
   <part label="S1 States that in America people vote on laws or those that make them" start="968" end="1037"/>
  </contributor>
  <contributor label="He sees it as the job of elected officials and the citizens to change the legal definition of marriage if that is what the majority want">
   <part label="He sees it as the job of elected officials and the citizens to change the legal definition of marriage if that is what the majority want" start="2860" end="2996"/>
  </contributor>
 </scu>
 <scu uid="72" label="Most dominant cultural view on marriage is based upon evolutionary sound logic">
  <contributor label="the most dominant cultural view on marriage is based upon evolutionary sound logic...they do not believe that equality factors into the evolutionary view of marriage">
   <part label="the most dominant cultural view on marriage is based upon evolutionary sound logic" start="3742" end="3824"/>
   <part label="they do not believe that equality factors into the evolutionary view of marriage" start="3954" end="4034"/>
  </contributor>
  <contributor label="homosexuality is not in sync with evolution">
   <part label="homosexuality is not in sync with evolution" start="5020" end="5063"/>
  </contributor>
 </scu>
 <scu uid="54" label="There are differences between cultural and legal definitions of marriage">
  <contributor label="S1 Refers to differences between cultural definitions of marriage and mating pairs or legal definitions of marriage">
   <part label="S1 Refers to differences between cultural definitions of marriage and mating pairs or legal definitions of marriage" start="519" end="634"/>
  </contributor>
  <contributor label="S1 argues that legal and cultural definitions are not the same">
   <part label="S1 argues that legal and cultural definitions are not the same" start="1792" end="1854"/>
  </contributor>
 </scu>
 <scu uid="65" label="Appropriateness is irrelevant">
  <contributor label="S1 says appropriateness is irrelevant">
   <part label="S1 says appropriateness is irrelevant" start="2154" end="2191"/>
  </contributor>
 </scu>
 <scu uid="73" label="Argument does not stand on equality">
  <contributor label="This person does not believe that the argument stands on equality">
   <part label="This person does not believe that the argument stands on equality" start="3880" end="3945"/>
  </contributor>
 </scu>
 <scu uid="71" label="Equality of rights is what is important">
  <contributor label="equality of rights is what is important">
   <part label="equality of rights is what is important" start="5441" end="5480"/>
  </contributor>
 </scu>
 <scu uid="69" label="Gays cannot create families without outside help">
  <contributor label="they cannot, without outside help, create a family">
   <part label="they cannot, without outside help, create a family" start="2655" end="2705"/>
  </contributor>
 </scu>
 <scu uid="50" label="Gays want to be married under current definition">
  <contributor label="gays want to be married under current definition">
   <part label="gays want to be married under current definition" start="268" end="316"/>
  </contributor>
 </scu>
 <scu uid="75" label="Marriage outside of the norm is less meaningful than those that fulfill the cultural definition">
  <contributor label="He discusses other marriage outside of the norm-a young woman and older man- as being less meaningful than those that fulfill the cultural definition">
   <part label="He discusses other marriage outside of the norm-a young woman and older man- as being less meaningful than those that fulfill the cultural definition" start="2708" end="2857"/>
  </contributor>
 </scu>
 <scu uid="58" label="No one gets to make that legal definition">
  <contributor label="S2 Asks who gets to make that legal definition">
   <part label="S2 Asks who gets to make that legal definition" start="795" end="841"/>
  </contributor>
 </scu>
 <scu uid="74" label="There is not a set, evolutionary definition of marriage">
  <contributor label="S2 argues against the idea that there is a set, evolutionary definition of marriage">
   <part label="S2 argues against the idea that there is a set, evolutionary definition of marriage" start="4036" end="4119"/>
  </contributor>
 </scu>
</pyramid>
