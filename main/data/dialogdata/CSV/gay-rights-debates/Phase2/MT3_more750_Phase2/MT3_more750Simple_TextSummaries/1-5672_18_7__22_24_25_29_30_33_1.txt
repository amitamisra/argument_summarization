
----------
D0
----------
S1 claims that legalizing same-sex marriage requires redefining family and marriage. 
Legalizing same-sex marriage would affect traditional marriage. 
He references a person named Colson. 
Colson believes that same-sex households are deficient compared to heterosexual households. This is in the same way that a single-parent household is deficient compared to a two-parent household. 
There is nothing inherently wrong with the deficient family systems.
Deficient family systems are not ideal. 
S1 notes that he does sometimes agree with gay rights supporters.
S1 believes that the constitutional amendment banning same-sex marriage is a bad idea. 
Homosexuals do experience violence and harassment.
Homosexuals should not be discriminated against when it comes to adoption, housing, or employment. 

S2 rejects the comparison of same-sex households to single-parent households. 
S2 notes that there is not a constitutional amendment to ban single-parenthood. 
He claims that S1's inconsistent arguments in favor of and against same-sex marriage make it doubtful whether he actually does support gay rights. 
He suggests that it is possible that S1 is being paid to post on the forum.


----------
D1
----------
S1 feels that marriage and family are being redefined.
Including gay marriage into the definition of traditional marriage affects traditional marriage in a negative manner. 
He expresses that he supports some of what Colson says. 
He doesn't support some of the other stuff Colson says. 
He admits to accidentally miss-wording something.
He refutes any "game" being involved. 
He feels that S2 is being hostile.
S2's main reason for being there is to disagree with people and not to debate the topic at hand.
  S2 feels that Colson has done very well for being a felon.
  Colson should stop drawing conclusions about gay parenthood based on observations of single parenthood. 
  He thinks S1 does not actually support gay rights.
  S1 is purposely misrepresenting himself. 
  S2 points to times that S1 said one thing and then voiced opposition to that same thing. 
  He questions wether S1 might be getting paid to be there. 
  He feels this is the least bad reason that S1 could have for the discrepancies in his views.


----------
D2
----------
S1 believes that what is happening is a redefining of the meanings behind family and marriage. 
Specifically, they believe that the definitions for both are being broadened. 
This person argues that to certain people, broadening the meanings of family and marriage is what matters.
This is why people oppose homosexual marriage. 
They point out that those who speak out against it believe that traditional families perform the best in terms of raising children.
Traditional families keep children from becoming criminals. 
He or she argues that these people do not believe that single parent homes are bad, necessarily. 
A home with both a mother and a father are best. 
However, he or she personally agrees with the homosexual movement's opinions on the matter of homosexual marriage.
There should be efforts to stop discrimination and harassment.
S2 believes that people with opinions opposing homosexual marriage are getting their opinions from negative observations of single parenthood. 
They do not believe that S1 has been consistent in their arguments.
S1's opinions lean towards standing against homosexual rights rather than supporting them.


----------
D3
----------
S1 is pointing out that by changing the definition of marriage in a proposed amendment it will affect traditional marriages. 
S1 quotes Colson.
S1 is attempting to make sure that Colson is not misrepresented. 
Colson's belief is that the "traditional" household is the best scenario.
The traditional household does the best job in regards to keeping their children out of prison. 
He asserts that Colson feels the same regarding single parent households as well. 
Single parent households are not "bad". 
Traditional households have the most success. 
S1 agrees with some of Colson's points. 
S1 also agrees with many gay rights issues regarding discrimination, harassment, and adoption. 
S1 also points out that S2 seems to attack posters personally rather than the topics at hand. 
S2 feels that S1 continually draws conclusions about gay marriage from single parent household statistics. 
S2 also feels that S1 may be compensated financially for participation in the forum.
This is due to the discrepancy of S1 opposing the amendment and his stated beliefs. 
S1 constantly brings up arguments which are in opposition to those beliefs.


----------
D4
----------
S1 begins by defending Colson.
Colson does not appear directly in the conversation. 
S1 primarily deals with issues of single parenthood in favor of traditional families.  
This then carries into the debate on same sex marriage arguing the family structure is not as secure.
Even looking at whether or not children of non traditional families end up in prison at higher rates.  
S1's argument gets confused when he later states that he is in favor of sex sex adoption. 
This is quite contrary to the beliefs of Colson whom he was initially defending.  
He also speaks in favor of protects for homosexual relationships and individuals and against the amendment that would prevent gay marriage.
S2 takes issue with what Colson argues pointing out that single parenthood has not been attacked politically through attempts at amendments.   
He mostly questions S1's validity.
S2 accuses him of being paid to post.  
There is not much opinion being given from S2. 
S2 objects to much of what S1 has said both in the current text as well as mentions of previous debates.

