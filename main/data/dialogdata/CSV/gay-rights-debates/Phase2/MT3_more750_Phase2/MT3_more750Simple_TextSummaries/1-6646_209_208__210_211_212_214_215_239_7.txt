
----------
D0
----------
S1 references S2 that marriage is a public institution limited by law. 
This is subject to the will of the people expressed in a vote. 
It's ok to lobby on either side. 
S2 calls for empathy.
People are denying gays what everyone else has. 
S1 claims it is ok for people to believe as they wish on the issue and can lobby on it. 
It is not ok to belittle those who oppose it due to any reason. 
S2 refers back to the empathy question. 
S2 wishes S1 to imagine a reversed world where being heterosexual denied you marriage rights. 
S1 claims it's not injustice because it's not denying something that you're allowed to do. 
S1 refers to society's right to determine its own rules. 
S2 refers to similarity between gay marriage and slavery.
S2 refers to empathy.
S2 claims it is cruel to deny gay marriage. 
S1 denies similarity and degree between gay marriage and slavery.
 S1 again refers to society determining its own rules. 
S2 states they are not comparing the two issues.They are just applying S1's beliefs and standards to slavery.


----------
D1
----------
S1 and S2 are debating gay marriage. 
S1 argues that gay marriage should be decided by a public vote. 
S2 argues that it's cruel to limit someone else from enjoying rights you can currently enjoy. 
Opponents of gay marriage should try to have more empathy for gay people who wish to marry. 
S1 retorts that it is not cruel for society to bar someone from enjoying a particular right. 
Society can and does decide what it deems to be socially acceptable through consensus.  
S2 states that tradition or being mundane does not make something any more wrong or right.  
S2 compares S1's argument to a justification for slavery.  
The majority once held slavery to be correct.
Slavery was morally and socially acceptable according to S1's logic. 
S1 says that he is not arguing for something being correct because it's traditional.
Traditions can be changed by convincing society to change.  
It is up to society to determine what is permissible through a dialog between the proponents of gay marriage and society.


----------
D2
----------
S1 believes that the best way to legalize same sex marriage is through lobbying.  
He thinks that in any discrepancy both sides should lobby. 
The one who has the majority support should win. 
Society is run by the majority opinion and he agrees that this is how it should be.  
He agrees that same sex marriage should be legal.
The only way it can and should happen is when the majority believes it, even if roles were reversed. 
His relationship was not able to be acknowledged as a marriage.  
When S2 compares marriage inequality to slavery he disagree that the comparison is valid. 
S2 continues his argument for lobbying citing Wilberforce in his fight to abolish slavery.
S2 attempts to appeal to S1's emotions by repeatedly asking that he put himself in the shoes of those who are being denied the right to marriage.  
He questions S1's ability to feel empathy. 
He compares the issue of marriage equality to slavery in regards to the majority allowing it to exist.


----------
D3
----------
S1 believes that marriage is a public institution that has its rules and requirements. 
They believe that it is in someone's rights to lobby to change those rules and requirements if they desire. 
However, they also believe that it is equally within someone's rights to lobby to keep the status quo on rules and requirements. 
They do not believe that it is cruel to decide that someone else should not have what another enjoys.
This is based on the idea that it is within one's rights to decide such things. 
This person argues that marriage is seen as a public institution.
Society has the ultimate say in how it is treated. 
S2 believes that it is cruel for the voice of the majority to continually lobby out the minority. 
This person does not believe that they have the right to tell people who they can and cannot marry. 
This person argues that just because something is the law does not make it any less cruel.
This person counters S1's arguments by comparing their views to that of slavery.


----------
D4
----------
S1 maintains that the current definition of marriage, being a public institution has remained unchanged since cave man days.
The current definition of marriage is neither cruel, nor unjust. 
S1 says that if the gay marriage laws of society are not to your liking then you should lobby for change.
Marriage laws should be to a vote and then abide by the decision. 
S1 states that cruelty and injustice occur when you are stopped from doing something that you are not allowed to do.
S1 further denies that the alleged suffering of gays is comparable to that of the slaves. 
    S2 believes that the ban on gay marriage is cruel and unjust.
    He likens the injustice of gay marriage to that of the slave trade. 
    S2 believes that suffering and injustice should not be allowed, even if the institution that allows it has always been the law of the land. 
    S2 states that it is not the suffering of one group to that of the other that was compared.
    S1's beliefs were being applied to the slave trade.
    Slave trade was the law at the time, where today it is illegal.

