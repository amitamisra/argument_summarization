
----------
D0
----------
S1 believes it is important to look at how one treats others.
It is important to not be close minded. 
S1 argues that you can discern right and wrong by learning through how you treat people with kindness and respect. 
S1 does not believe the Bible is trustworthy.
Bible advises against homosexuality.
Bible advocates stoning and other such punishments. 

S2 believes that it is those who don't believe in the Bible that are close minded. 
S2 does not believe it is right to disregard the Bible out of political correctness. 
S2 believes that you can only know right from wrong if you read the Bible and are saved.
S2 believes telling people they are living in sin is done for their best interests in mind. 


----------
D1
----------
S1 attacks previous posts for their bigotry.
S1 argues that the differences between people are not a bad thing. 
S1 suggests that S2 should make their decisions without reference to the Bible. 
S1 claims that the Bible is not an appropriate source for morality.
S1 cites the proscription that misbehaving children should be stoned to death. 
S1 argues that we should instead treat people with kindness and respect. 

S2 claims that their views on homosexuals are defined by the Bible.
S2 claims that he will not listen to those who ignore God's Word. 
S2 argues against sacrificing one's religious beliefs in favor of political correctness.
S2 suggests that homosexuals repent their sins so that they can go to Heaven. 
S2 claims that homosexuality will lead to Hell.


----------
D2
----------
S1 thinks people need to stop being bigots.
S1 thinks people need to open their minds to the fact that not everyone is going to be like they are. 
S1 thinks political correctness is good.
S1 thinks you learn right from wrong on your own by being kind to people and treating them with respect. 
S1 thinks the Bible is a cruel book that shouldn't be used as an example. 
S2 thinks the Bible is right.
S2 thinks the Bible is how people learn right from wrong. 
S2 thinks nothing outside of the Bible is important. 
S2 thinks he is helping gay people by saving them from hell even if it does hurt them while they are here.


----------
D3
----------
S1 feels the posts he has read are bigoted statements.  
S1 also asks people to step out of the Bible to see how these statements are treating people.  
S1 feels the statements are not looking out for the best interests of gay people.
The statements are actually degrading them.
S1 advises people should not trust the Bible as a viable source.
The bible advised to stone children who don't behave.  

S2 along with others is speaking based on their religious beliefs.   
S2 feels the gay community and those who support them should read the Bible and pray. This will save them. They will learn right from wrong.
S2 states religious beliefs should not be given up for political correctness. 


----------
D4
----------
Two people are arguing about religion.  
S2 states that the homosexuals and their supporters are close minded to what the bible teaches about gay sex. 
S2 states that the gay community is saying their sin is ok.
S2 is looking for gays best interests.
S2 asks gays to repent to Jesus Christ for the remission of their sins.  
S1 states that he figured out right from wrong by treating people with kindness and respect.
S1 figured out right from wrong not by a book.  
S1 states that the bible also says to stone children to death.
Bible is not a resource to be trusted.  
S2 states that only believing in Jesus Christ gets you to heaven.
Anything else leads to Hell.