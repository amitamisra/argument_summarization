
----------
D0
----------
S1 is in favor of civil union laws for gay couples.
S1 has a religious based objection to gay marriage.
She deems marriage a sacrament.
She deems homosexuality a sin according to the bible.  
S2 considers the bible to be a complete work of fiction.
S2 makes the argument that a majority group should not deny a minority group a right.
That right does not hurt the majority in any way.  
He likens the situation of gay marriages being denied by a majority to the idea of California being able to vote to ban the citizens of Michigan from driving.  
S1 says S2 should devote more time to perfecting civil union laws.
Gay marriage does hurt the majority population financially.


----------
D1
----------
Two people are discussing civil unions and marriages. 
S1 contends that he favors civil unions for homosexuals but not marriage.
He is religious and views marriage as a sacrament.  
He also states that he can vote using whatever criteria he feels.
The government can't establish religion or give special consideration to certain groups.
As a voter he can give special consideration to certain groups.  
S2 retorts by stating that civil unions are not equivalent to marriages.  
He states that the US government does not consider marriage a sacrament.
He states S1 is mixing religion and government.  
He also states that the majority shouldn't vote on the civil rights of another group that will not affect them.
S1 states that another homosexuals will financially affect other groups.


----------
D2
----------
S1 contends he/she is in support of civil union rights for same sex couples.
S1 is opposed to marriage rights.  
S2 believes a civil union is to be the only right given to same sex couples.
The same right should be true for heterosexuals as well.  
S2 also feels the Bible should have nothing to do with the law.
S2 advises the American government does not recognize marriage as a sacrament due to the separation of church and state.  
S1 agrees church and state are separate entities.
He/she believes voters can cast a ballot in any direction they so choose.
Casting ballots essentially boils down to each voter's religious beliefs. 
S1 contends marriage rights for gays effects the financial welfare of every voter.


----------
D3
----------
S1 reminds S2 that he is in favor of civil unions, not just marriage. 
He also declares that he is a religious person.
The Bible is clear on homosexuality.
 He lets him know he won't vote in favor of civil unions. 
He argues that voters can do as they please. 
As a voter he chooses not to want same sex marriage included under the definition of marriage. 
S1 thinks that he has proven the fact that gay marriage does affect us financially.
S2 argues that the Bible has nothing to do with the law. 
He states that the Bible is nothing but a bunch of fairy tails. 
The government doesn't consider marriage to be a sacrament. 
Marriage licenses are contracts and not a religion.


----------
D4
----------
S1 favors civil unions.
S1 views marriage as a sacrament. 
He feels he is allowed to vote as he wants for whatever reason he wants. 
He believes he can mix religion and government if he chooses.
The government can not mix religion and government. 
He does not agree with S2's suggestion that states be allowed to vote on what other states do. 
S1 thinks granting gays the right to marry has a financial impact on everyone. 
S2 thinks if S1 favors civil unions he should for everyone. 
Heterosexual marriages should be changed to civil unions. 
He thinks the Bible has nothing to do with law. 
He views marriage licenses as a secular documents.
He thinks religion and government should not be mixed.
He thinks rights shouldn't be voted on.

