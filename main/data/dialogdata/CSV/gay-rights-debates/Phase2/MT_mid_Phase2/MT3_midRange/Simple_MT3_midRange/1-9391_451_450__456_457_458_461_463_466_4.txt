
----------
D0
----------
S1 asserts that marriage is a contract between two people and the state.
It is not appropriate for religious institutions to be involved with that decision.
He claims that he is forced to accept heterosexual marriages.
Society should be forced to accept homosexual marriages. 
He accuses Christian organizations for supporting anti-gay legislation.
He claims that denying same-sex marriage is unjust.
Preventing same-sex marriage is a method of promoting heterosexual privilege. 
S2 argues that heterosexual marriage is the status quo.
It rests on proponents of same-sex marriage to demonstrate that this new definition of marriage is superior to the current definition. 
He argues that we do not actually have a basic right to love or to marry whomever we love.


----------
D1
----------
S1 and S2 are discussing gay marriage.  
Both seem to be somewhat in favor of gay marriage.
S2 seems a bit indifferent to the issue.  
S1 feels it is unfair for the gay community to be forced to recognize heterosexual marriages.
Gay citizens are denied the right to wed.  
S2 believes things, including gay marriage, should be voted on.
Right and wrong are personal judgments.  
S1 then wonders if S2 feels all civil rights should be voted on.  
S2 advises that civil rights are actually constitutional rights.
Civil rights cannot and should not be voted on.
S2 does not feel that unmarried people, gay or straight are not denied civil rights.  
Two people with similar opinions but different views.


----------
D2
----------
S1 believes that the entire point of marriage is to create a contract between the state and the two people involved.
Christians believe that the only thing that counts is the religious ceremony of marriage. 
This person does not believe it is anyone's business outside of those involved whether they choose to make that contract or not. 
He or she believes that Christian organizations have been at the forefront of opposing instating homosexual marriage.
S2 believes that official policies are everyone's business since as a society we have to exist together.
What one does legally needs to be approved of by the majority. 
He or she believes that one needs to have a significant justification to change the way things are now.


----------
D3
----------
Two people are discussing gay rights.  
S1 states that marriage is a contract between the state and the two people involved.
He finds irony that the Christians are opposed to gay religious marriage.
The civil part doesn't affect Christians.
Gays can have a religious marriage.
Gays cannot have a contract with the state.  
He states that someone's marriage is their own business.
Just as he is forced to accept heterosexual marriages why shouldn't others accept homosexual marriages.  
S2 contends that since everyone lives in the society, its policy is everyones business.   
He also states that heterosexual marriages is the status quo.
The challenger to that is the one who has to justify the changes.


----------
D4
----------
S1 believes marriage is a contract between the state and two people. 
He compares marriage to forming a corporation. 
He finds it ironic Christians are against the religious aspects of marriage.
They are unable to stop religious marriage they want to stop the contract. 
He believes he is forced to recognize heterosexual marriage.
Others should recognize gay marriages. 
He does not believe rights should be voted on.
He gives an example of slavery. 
S2 does not think anyone is harmed by gay marriage.
He believes official policies are everyone's business. 
Heterosexual marriage is the status quo.
Heterosexual marriage is thus recognized. 
Any changes to the status quo should be justified. 
He doesn't see marriage as a right.
An example being unmarried people.

