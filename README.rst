argument summarization using discourse structure

discourse parser
source gcc_work.txt
 /Users/amita/software/gcc_work.txt 
make clean
make
This installs the parser if working on mac

2to3 all python files from discourse-parser
add .decode to 2 files
http://pawelmazur.net/resources/notes/compilingCharniakJohnson.htm

However, before compiling the parser, we need to clean a bit first, otherwise we are likely to get the following error when trying to run the parser:
./parse.sh: line 6: second-stage/programs/features/best-parses: cannot execute binary file
as second-stage/programs/features/best-parses is not recompiled; so run make clean before executing make.

Then, when trying to run the reranking parser I got two error messages from zcat:
zcat: second-stage/models/ec50spfinal/features.gz.Z: No such file or directory
zcat: second-stage/models/ec50spfinal/cvlm-l1c10P1-weights.gz.Z: No such file or directory
although the file reranking-parser/parse.sh specified features.gz and cvlm-l1c10P1-weights.gz and these files existed in the relevant directory. This did not seem to influence the parsing results, nevertheless renaming the files (adding the .Z extension) and making the corresponding changes to reranking-parser/parse.sh stopped zcat to complain. Alternatively, you can use gzcat instead of zcat in Makefiles.

If you want to run the parser from a directory other than reranking-parser, edit the parse.sh script and add `dirname $0`/ before each call and directory, so that the complete command is:
`dirname $0`/first-stage/PARSE/parseIt -l399 -N50 `dirname $0`/first-stage/DATA/EN/ $* | `dirname $0`/second-stage/programs/features/best-parses -l `dirname $0`/$MODELDIR/features.gz.Z `dirname $0`/$MODELDIR/$ESTIMATORNICKNAME-weights.gz.Z

Last Modified: 24th August 2011

give execute  permissions to
chmod 777 Tools/CharniakParserRerank/parse.sh 
/Discourse_Parser_Dist/Tools/CharniakParserRerank/second-stage/programs/features/best-parses 

chmod 777 Discourse_Parser_Dist/Tools/CharniakParserRerank/first-stage/PARSE/parseIt
Discourse_Parser_Dist/Tools/SPADE_UTILS/bin/edubreak

chmod 777 /Users/amita/git/argument_summarization/main/Discourse_Parser_Dist/Tools/CharniakParserRerank/parse.sh 


 chmod 777 /Users/amita/git/argument_summarization/main/Discourse_Parser_Dist/Tools/CharniakParserRerank/first-stage/PARSE/parseIt

chmod 777 /Users/amita/git/argument_summarization/main/Discourse_Parser_Dist/Tools/CharniakParserRerank/second-stage/programs/features/best-parses 


chmod 777 /Users/amita/git/argument_summarization/main/Discourse_Parser_Dist/Tools/SPADE_UTILS/bin/edubreak 

chmod 777 /Users/amita/git/argument_summarization/main/Discourse_Parser_Dist/Tools/SPADE_UTILS/bin/edubreak.pl 


uncomment train as suggested in documentation
train_model" in do_segment method and run the segmenter. This learns the model and saves it. If it runs once, you don't need to run train_model again. You should comment it to save time


Ins