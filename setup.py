# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='argument_summarization',
    version='0.0.1',
    description='Use discourse structures for summarization',
    long_description=readme,
    author='Amita Misra',
    author_email='amitamisra1@gmail.com',
    url='https://bitbucket.org/amitamisra/argument_summarization',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

